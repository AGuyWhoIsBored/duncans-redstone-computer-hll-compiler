﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace DRCHLLC_Core_Compiler__ARCISS_v1._1_
{
    class syntax
    {
        /*
         * This class will contain all functions and data types about syntax
         * and syntax checking. This will also include the new and redone syntax tree. 
         * 
         * Created 8/30/3016 by AGuyWhoIsBored
         * 
         */

        // the new AST will work pretty much the way the old AST did, although less bulky
        // and confusing, as we won't throw proprietary errors. 
        // Instead, we'll issue an error code, and then pass control to the logger/debugger
        // to handle the error. 
        // the builtin AST will only contain the syntax checking needed for the core language and functions.
        // other modules / extensions can be imported externally

        // **TODO** ADD SUPPORT TO HAVE VARIABLES ASSIGNED TO OUTPUTS OF FUNCTIONS / USE FUNCTION IN PLACE OF VARIABLE

        // kinds of if statements to implement in ARCISS v1.1
        // var -> DATA GROUPING AS WELL!
        // var -> FUNCTION THAT RETURNS A VALUE AS WELL!
        // if (input(X))
        // if (var [cond] var)
        // if ([hcond])
        // if (var [operator] var [operator]... [cond]...) <-- THIS IS NEW PEMDAS MODEL IN v1.1!
        // if (var [cond] var [Loperator] var [cond] var ...) <-- THIS IS ONLY FOR DATA GROUPS OF 2 (var [cond] var)!!
        // if ([hcond] [Loperator] ...) <-- THIS INCLUDES THE ABOVE + HCOND!

        // data groupings in ARCISS v1.1
        // ((function) [operator] [value])... [onion model present in v1.0]
        // (var [operator] var [operator]...) [PEMDAS model new v1.1]
        //

        public static Dictionary<string, string> syntaxFunctionTables = new Dictionary<string, string>();
        /* load syntax rules into this table for all functions, whether it be native language code or extensions
         * syntax for the syntax table is as following:
         *<[globalresourceID],[token1][token2]....>
         *can also induce ....[token1]/[token2][token3].... / = OR (when we want to test for more than one token in that location)
         *error handling is not yet implemented 
         */
        public static string[] tempTokensArray = null;
        public static string[] tempValuesArray = null;
        public static string[] tempLineOfCodeValueArray = null;
        public static int currentTokenIndex = -1;
        public static int errorCount = 0;
        public static bool resourceCheck = false;

        // AST specific for v1.1!
        // this dicates how the language is written

        public static void scanMainHLL()
        {
            logging.logDebug("Starting syntax check ...", logging.loggingSenderID.syntax, true);



            // make temp copy of data - why do we do this again?
            tempTokensArray = compiler.tokenTokensArray;
            tempValuesArray = compiler.tokenValuesArray;
            tempLineOfCodeValueArray = compiler.tokenLineOfCodeValueArray;

            generateCustomFunctionSyntax();

            // dump all syntax tables
            if (syntaxFunctionTables.Count != 0)
            {
                logging.logDebug("The following syntax tables have been loaded for use in program compilation:", logging.loggingSenderID.compiler);
                string[] syntaxFunctionTableKeys = syntaxFunctionTables.Keys.ToArray();
                string[] syntaxFunctionTableValues = syntaxFunctionTables.Values.ToArray();
                for (int i = 0; i < syntaxFunctionTables.Count; i++)
                { logging.logDebug("[" + syntaxFunctionTableKeys[i] + "]: " + syntaxFunctionTableValues[i], logging.loggingSenderID.compiler); }
            }
            // dump syntax binding table
            for (int i = 0; i < tokens.importedSyntaxTokenBindingTable.Length; i++)
            { logging.logDebug("[" + i + "]: " + tokens.importedSyntaxTokenBindingTable[i], logging.loggingSenderID.compiler); }
            // dump all imported token tables too
            if (tokens.importedTokenTable.Length != 0)
            {
                logging.logDebug("The following tokens have been loaded for use in program compilation:", logging.loggingSenderID.compiler);
                for (int i = 0; i < tokens.importedTokenTable.Length; i++)
                { logging.logDebug("[" + i.ToString() + "," + tokens.convertInt(tokens.importedTokenTable[i]) + "]: " + tokens.importedTokenTable[i], logging.loggingSenderID.compiler); }
            }

            // these two numbers should return 0 by the end of syntax check, or else there are unbalanced
            // data groupings in the token arrays (program)
            int curlyBracketCount = 0;
            string currentToken = "";
            string currentTokenValue = "";

            // this is pretty much the syntax tree of every single possible function / command in 
            // the core ARCIS language
            // this is the AST

            TopOfSyntaxTree:

            currentToken = tempTokensArray[currentTokenIndex += 1]; // this statement can be reduced down to remove the currentToken
            logging.logDebug("Hit top of syntax tree", logging.loggingSenderID.syntax);
            logging.logDebug("Token [" + currentTokenIndex.ToString() + "," + tempTokensArray[currentTokenIndex] + "," + tempValuesArray[currentTokenIndex] + "]", logging.loggingSenderID.syntax);

            #region EOF Check
            if (currentToken == tokens.convertInt("identifier_eof").ToString() && curlyBracketCount != 0) logging.logError("1.0", "Current token: [" + currentTokenIndex.ToString() + "," + tempTokensArray[currentTokenIndex] + "," + tempValuesArray[currentTokenIndex] + "]"
                + "\nExpected curly bracket count 0; curly bracket count " + curlyBracketCount.ToString(), logging.loggingSenderID.syntax);
            // syntax check finished!
            else if (currentToken == tokens.convertInt("identifier_eof").ToString()) goto EndOfFunction;
            #endregion

            #region Generic Variable Syntax
            // creating a new variable syntax
            if (currentToken == tokens.convertInt("identifier_new").ToString())
            {
                if (tempTokensArray[currentTokenIndex + 1] == tokens.convertInt("identifier_local").ToString())
                {
                    logging.logDebug("local variable precheck syntax correct", logging.loggingSenderID.syntax);
                    currentTokenIndex++;
                }

                if (tempTokensArray[currentTokenIndex += 1] == tokens.convertInt("variable").ToString())
                {

                TopOfMultiVar:
                    if (tempTokensArray[currentTokenIndex += 1] == tokens.convertInt("operator_equals").ToString())
                    {
                        currentToken = tempTokensArray[currentTokenIndex += 1];
                        if (currentToken == tokens.convertInt("data_int").ToString())
                        {
                            currentToken = tempTokensArray[currentTokenIndex += 1];
                            if (currentToken == tokens.convertInt("end_of_line").ToString())
                            {
                                logging.logDebug("New variable declaration syntax correct", logging.loggingSenderID.syntax);
                                goto TopOfSyntaxTree;
                            }
                            else logging.logError("3.0", "Current token: [" + currentTokenIndex.ToString() + "," + tempTokensArray[currentTokenIndex] + "," + tempValuesArray[currentTokenIndex] + "]", logging.loggingSenderID.syntax);
                        }
                        else if (currentToken == tokens.convertInt("variable").ToString())
                        {
                            currentToken = tempTokensArray[currentTokenIndex += 1];
                            // assigning variable to output of function
                            if (currentToken == tokens.convertInt("grouper_openparentheses").ToString())
                            {
                                // loop for any amount of arguments to pass into function or none at all
                                TopOfArgument:
                                currentToken = tempTokensArray[currentTokenIndex += 1];
                                if (currentToken == tokens.convertInt("variable").ToString()
                                    || currentToken == tokens.convertInt("data_int").ToString()) { goto TopOfArgument; }
                                else if (currentToken == tokens.convertInt("grouper_closeparentheses").ToString())
                                {
                                    currentToken = tempTokensArray[currentTokenIndex += 1];
                                    if (currentToken == tokens.convertInt("end_of_line").ToString())
                                    {
                                        logging.logDebug("Assigning new variable to output of function syntax correct", logging.loggingSenderID.syntax);
                                        goto TopOfSyntaxTree;
                                    }
                                    else logging.logError("ERROR_SEMICOLON_EXPECTED", "Current token: [" + currentTokenIndex.ToString() + "," + tempTokensArray[currentTokenIndex] + "," + tempValuesArray[currentTokenIndex] + "]", logging.loggingSenderID.syntax);
                                }
                                else logging.logError("ERROR_UNEXPECTED_TOKEN_IN_DEFINING_FUNCTION", "Current token: [" + currentTokenIndex.ToString() + "," + tempTokensArray[currentTokenIndex] + "," + tempValuesArray[currentTokenIndex] + "]", logging.loggingSenderID.syntax);
                            }
                            else logging.logError("ERROR_GROUPER_OPENPARENTHESES_EXPECTED", "Current token: [" + currentTokenIndex.ToString() + "," + tempTokensArray[currentTokenIndex] + "," + tempValuesArray[currentTokenIndex] + "]", logging.loggingSenderID.syntax); 
                        }
                        else logging.logError("4.0", "Current token: [" + currentTokenIndex.ToString() + "," + tempTokensArray[currentTokenIndex] + "," + tempValuesArray[currentTokenIndex] + "]", logging.loggingSenderID.syntax);
                    }
                    // for multivar creation
                    else if (currentToken == tokens.convertInt("variable").ToString()) { goto TopOfMultiVar; }
                    else logging.logError("5.0", "Current token: [" + currentTokenIndex.ToString() + "," + tempTokensArray[currentTokenIndex] + "," + tempValuesArray[currentTokenIndex] + "]", logging.loggingSenderID.syntax);
                }
                else logging.logError("6.0", "Current token: [" + currentTokenIndex.ToString() + "," + tempTokensArray[currentTokenIndex] + "," + tempValuesArray[currentTokenIndex] + "]", logging.loggingSenderID.syntax); 
            }
            // changing variables
            if (currentToken == tokens.convertInt("variable").ToString())
            {
                currentToken = tempTokensArray[currentTokenIndex += 1];
                if (currentToken == tokens.convertInt("operator_equals").ToString()
                    || currentToken == tokens.convertInt("operator_plusequals").ToString()
                    || currentToken == tokens.convertInt("operator_minusequals").ToString())
                {
                    // immediate
                    currentToken = tempTokensArray[currentTokenIndex += 1];
                    if (currentToken == tokens.convertInt("data_int").ToString())
                    {
                        currentToken = tempTokensArray[currentTokenIndex += 1];
                        if (currentToken == tokens.convertInt("end_of_line").ToString())
                        {
                            logging.logDebug("Manipulating variable with an immediate value syntax correct", logging.loggingSenderID.syntax);
                            goto TopOfSyntaxTree;
                        }
                        else logging.logError("3.1", "Current token: [" + currentTokenIndex.ToString() + "," + tempTokensArray[currentTokenIndex] + "," + tempValuesArray[currentTokenIndex] + "]", logging.loggingSenderID.syntax);
                    }
                    // copying and inc/dec by variable syntax
                    // && var combos
                    else if (currentToken == tokens.convertInt("variable").ToString())
                    {
                        currentToken = tempTokensArray[currentTokenIndex += 1];
                        if (currentToken == tokens.convertInt("end_of_line").ToString())
                        {
                            logging.logDebug("Manipulating variable with an another variable syntax correct", logging.loggingSenderID.syntax);
                            goto TopOfSyntaxTree;
                        }
                        else if (currentToken == tokens.convertInt("operator_plus").ToString()
                            || currentToken == tokens.convertInt("operator_minus").ToString())
                        {
                            int varCount = 0;
                            int ops = 0;
                        TopOfVarCheck:
                            ops++;
                            if (tempTokensArray[currentTokenIndex += 1] == tokens.convertInt("variable").ToString())
                            {
                                varCount++;
                                if (tempTokensArray[currentTokenIndex += 1] == tokens.convertInt("operator_plus").ToString()
                                    || tempTokensArray[currentTokenIndex] == tokens.convertInt("operator_minus").ToString()) goto TopOfVarCheck;
                                else if (tempTokensArray[currentTokenIndex] == tokens.convertInt("end_of_line").ToString())
                                {
                                    logging.logDebug("Manipulating variable with var combo syntax correct", logging.loggingSenderID.syntax);
                                    goto TopOfSyntaxTree;
                                }
                            }
                        }
                        else logging.logError("3.1", "Current token: [" + currentTokenIndex.ToString() + "," + tempTokensArray[currentTokenIndex] + "," + tempValuesArray[currentTokenIndex] + "]", logging.loggingSenderID.syntax);
                    }
                    // setting var to output of custom function
                    else if (Convert.ToInt16(currentToken) > tokens.totalTokenCount) goto importedFuncSyntax;
                }
                // single-use var change operations
                else if (currentToken == tokens.convertInt("operator_increment").ToString()
                    || currentToken == tokens.convertInt("operator_decrement").ToString()
                    || currentToken == tokens.convertInt("operator_bitwise_not").ToString())
                {
                    currentToken = tempTokensArray[currentTokenIndex += 1];
                    if (currentToken == tokens.convertInt("end_of_line").ToString())
                    {
                        logging.logDebug("single-use variable change operation syntax correct", logging.loggingSenderID.syntax);
                        goto TopOfSyntaxTree;
                    }
                    else logging.logError("3.1", "Current token: [" + currentTokenIndex.ToString() + "," + tempTokensArray[currentTokenIndex] + "," + tempValuesArray[currentTokenIndex] + "]", logging.loggingSenderID.syntax);
                }
                // multi-use var change operations
                else if (currentToken == tokens.convertInt("operator_bitwise_rshift").ToString()
                    || currentToken == tokens.convertInt("operator_bitwise_lshift").ToString())
                {
                    currentToken = tempTokensArray[currentTokenIndex += 1];
                    if (currentToken == tokens.convertInt("end_of_line").ToString())
                    {
                        logging.logDebug("single-use with multi-use variable change operation syntax correct", logging.loggingSenderID.syntax);
                        goto TopOfSyntaxTree;
                    }
                    else if (currentToken == tokens.convertInt("data_int").ToString()
                        || currentToken == tokens.convertInt("variable").ToString())
                    {
                        currentToken = tempTokensArray[currentTokenIndex += 1];
                        if(currentToken == tokens.convertInt("end_of_line").ToString())
                        {
                            logging.logDebug("multi-use with multi-use variable change operation syntax correct", logging.loggingSenderID.syntax);
                            goto TopOfSyntaxTree;
                        }
                        else logging.logError("UPDATE_CODE", "Current token: [" + currentTokenIndex.ToString() + "," + tempTokensArray[currentTokenIndex] + "," + tempValuesArray[currentTokenIndex] + "]", logging.loggingSenderID.syntax);
                    }
                    else logging.logError("3.1", "Current token: [" + currentTokenIndex.ToString() + "," + tempTokensArray[currentTokenIndex] + "," + tempValuesArray[currentTokenIndex] + "]", logging.loggingSenderID.syntax); 
                }
                // executing a custom function
                else if (currentToken == tokens.convertInt("grouper_openparentheses").ToString())
                {
                    // loop for any amount of arguments to pass into function or none at all
                    TopOfArgument:
                    currentToken = tempTokensArray[currentTokenIndex += 1];
                    if (currentToken == tokens.convertInt("variable").ToString()
                        || currentToken == tokens.convertInt("data_int").ToString()) { goto TopOfArgument; }
                    else if (currentToken == tokens.convertInt("grouper_closeparentheses").ToString())
                    {
                        currentToken = tempTokensArray[currentTokenIndex += 1];
                        if (currentToken == tokens.convertInt("end_of_line").ToString())
                        {
                            logging.logDebug("Executing custom function syntax correct", logging.loggingSenderID.syntax);
                            goto TopOfSyntaxTree;
                        }
                        else logging.logError("ERROR_SEMICOLON_EXPECTED", "Current token: [" + currentTokenIndex.ToString() + "," + tempTokensArray[currentTokenIndex] + "," + tempValuesArray[currentTokenIndex] + "]", logging.loggingSenderID.syntax);
                    }
                    else logging.logError("ERROR_UNEXPECTED_TOKEN_IN_DEFINING_FUNCTION", "Current token: [" + currentTokenIndex.ToString() + "," + tempTokensArray[currentTokenIndex] + "," + tempValuesArray[currentTokenIndex] + "]", logging.loggingSenderID.syntax);
                }
                else logging.logError("7.0", "Current token: [" + currentTokenIndex.ToString() + "," + tempTokensArray[currentTokenIndex] + "," + tempValuesArray[currentTokenIndex] + "]", logging.loggingSenderID.syntax);
            }
            #endregion

            #region Core functions syntax
            #region Exit Function
            // exit function
            if (currentToken == tokens.convertInt("func_exit").ToString())
            {
                currentToken = tempTokensArray[currentTokenIndex += 1];
                if (currentToken == tokens.convertInt("grouper_openparentheses").ToString())
                {
                    currentToken = tempTokensArray[currentTokenIndex += 1];
                    if (currentToken == tokens.convertInt("grouper_closeparentheses").ToString())
                    {
                        currentToken = tempTokensArray[currentTokenIndex += 1];
                        if (currentToken == tokens.convertInt("end_of_line").ToString())
                        {
                            logging.logDebug("Exit function syntax correct", logging.loggingSenderID.syntax);
                            goto TopOfSyntaxTree;
                        }
                        else logging.logError("3.2", "Current token: [" + currentTokenIndex.ToString() + "," + tempTokensArray[currentTokenIndex] + "," + tempValuesArray[currentTokenIndex] + "]", logging.loggingSenderID.syntax);
                    }
                    else logging.logError("8.0", "Current token: [" + currentTokenIndex.ToString() + "," + tempTokensArray[currentTokenIndex] + "," + tempValuesArray[currentTokenIndex] + "]", logging.loggingSenderID.syntax);
                }
                else logging.logError("8.0", "Current token: [" + currentTokenIndex.ToString() + "," + tempTokensArray[currentTokenIndex] + "," + tempValuesArray[currentTokenIndex] + "]", logging.loggingSenderID.syntax);
            }
            #endregion
            #region Output Function
            if (currentToken == tokens.convertInt("func_output").ToString())
            {
                currentToken = tempTokensArray[currentTokenIndex += 1];
                if (currentToken == tokens.convertInt("grouper_openparentheses").ToString())
                {
                    currentToken = tempTokensArray[currentTokenIndex += 1];
                    if (currentToken == tokens.convertInt("variable").ToString()
                        || currentToken == tokens.convertInt("data_int").ToString())
                    {
                        // check to make sure there are no registers - cannot combine this with above if statement for some reason
                        if (tempValuesArray[currentTokenIndex] == "%gt_"
                            || tempValuesArray[currentTokenIndex] == "%spd"
                            || tempValuesArray[currentTokenIndex] == "%dp_"
                            || tempValuesArray[currentTokenIndex] == "%ui_"
                            || tempValuesArray[currentTokenIndex] == "%rng"
                            || tempValuesArray[currentTokenIndex] == "%dec"
                            || tempValuesArray[currentTokenIndex] == "%raw"
                            || tempValuesArray[currentTokenIndex] == "%out"
                            || tempValuesArray[currentTokenIndex] == "%stk"
                            || tempValuesArray[currentTokenIndex] == "%gex")
                        {
                            logging.logError("ERROR_NO_REGISTER_ALLOWED", "Current token: [" + currentTokenIndex.ToString() + "," + tempTokensArray[currentTokenIndex] + "," + tempValuesArray[currentTokenIndex] + "]", logging.loggingSenderID.syntax);
                        }
                        currentToken = tempTokensArray[currentTokenIndex += 1];
                        // special case - variables HAVE to be registers
                        if (currentToken == tokens.convertInt("variable").ToString()
                            && (tempValuesArray[currentTokenIndex] == "%dp_"
                            || tempValuesArray[currentTokenIndex] == "%dec"
                            || tempValuesArray[currentTokenIndex] == "%raw"
                            || tempValuesArray[currentTokenIndex] == "%out"
                            || tempValuesArray[currentTokenIndex] == "%gt_"
                            || tempValuesArray[currentTokenIndex] == "%spd"
                            || tempValuesArray[currentTokenIndex] == "%stk"
                            || tempValuesArray[currentTokenIndex] == "%gex"))
                        {
                            currentToken = tempTokensArray[currentTokenIndex += 1];
                            if (currentToken == tokens.convertInt("grouper_closeparentheses").ToString())
                            {
                                currentToken = tempTokensArray[currentTokenIndex += 1];
                                if (currentToken == tokens.convertInt("end_of_line").ToString())
                                {
                                    logging.logDebug("Output function syntax correct", logging.loggingSenderID.syntax);
                                    goto TopOfSyntaxTree;
                                }
                                else logging.logError("ERROR_SEMICOLON_EXPECTED", "Current token: [" + currentTokenIndex.ToString() + "," + tempTokensArray[currentTokenIndex] + "," + tempValuesArray[currentTokenIndex] + "]", logging.loggingSenderID.syntax);
                            }
                            else logging.logError("ERROR_CLOSE_PARENTHESES_EXPECTED", "Current token: [" + currentTokenIndex.ToString() + "," + tempTokensArray[currentTokenIndex] + "," + tempValuesArray[currentTokenIndex] + "]", logging.loggingSenderID.syntax);
                        }
                        else if (currentToken == tokens.convertInt("variable").ToString()) logging.logError("ERROR_REGISTER_EXPECTED", "Current token: [" + currentTokenIndex.ToString() + "," + tempTokensArray[currentTokenIndex] + "," + tempValuesArray[currentTokenIndex] + "]", logging.loggingSenderID.syntax);
                        else logging.logError("ERROR_UNEXPECTED_TOKEN", "Current token: [" + currentTokenIndex.ToString() + "," + tempTokensArray[currentTokenIndex] + "," + tempValuesArray[currentTokenIndex] + "]", logging.loggingSenderID.syntax);
                    }
                    else logging.logError("ERROR_UNEXPECTED_TOKEN", "Current token: [" + currentTokenIndex.ToString() + "," + tempTokensArray[currentTokenIndex] + "," + tempValuesArray[currentTokenIndex] + "]", logging.loggingSenderID.syntax);
                }
                else logging.logError("8.3", "Current token: [" + currentTokenIndex.ToString() + "," + tempTokensArray[currentTokenIndex] + "," + tempValuesArray[currentTokenIndex] + "]", logging.loggingSenderID.syntax);
            }
            #endregion
            #region Input Function
            // if condition syntax will be located in if statement syntax
            if (currentToken == tokens.convertInt("func_input").ToString())
            {
                currentToken = tempTokensArray[currentTokenIndex += 1];
                if (currentToken == tokens.convertInt("grouper_openparentheses").ToString())
                {
                    currentToken = tempTokensArray[currentTokenIndex += 1];
                    // special case - variables HAVE to be registers
                    if (currentToken == tokens.convertInt("variable").ToString()
                        && (tempValuesArray[currentTokenIndex] == "%dp_"
                        || tempValuesArray[currentTokenIndex] == "%ui_"
                        || tempValuesArray[currentTokenIndex] == "%rng"
                        || tempValuesArray[currentTokenIndex] == "%stk"
                        || tempValuesArray[currentTokenIndex] == "%gt_"
                        || tempValuesArray[currentTokenIndex] == "%spd"))
                    {
                        currentToken = tempTokensArray[currentTokenIndex += 1];
                        if (currentToken == tokens.convertInt("variable").ToString())
                        {
                            // check to make sure there are no registers - cannot combine this with above if statement for some reason (probably because I'm lazy to figure out the reason why)
                            if (tempValuesArray[currentTokenIndex] == "%gt_"
                                || tempValuesArray[currentTokenIndex] == "%spd"
                                || tempValuesArray[currentTokenIndex] == "%dp_"
                                || tempValuesArray[currentTokenIndex] == "%ui_"
                                || tempValuesArray[currentTokenIndex] == "%rng"
                                || tempValuesArray[currentTokenIndex] == "%dec"
                                || tempValuesArray[currentTokenIndex] == "%raw"
                                || tempValuesArray[currentTokenIndex] == "%out"
                                || tempValuesArray[currentTokenIndex] == "%stk"
                                || tempValuesArray[currentTokenIndex] == "%gex")
                            {
                                logging.logError("ERROR_NO_REGISTER_ALLOWED", "Current token: [" + currentTokenIndex.ToString() + "," + tempTokensArray[currentTokenIndex] + "," + tempValuesArray[currentTokenIndex] + "]", logging.loggingSenderID.syntax);
                            }
                            currentToken = tempTokensArray[currentTokenIndex += 1];
                            if (currentToken == tokens.convertInt("grouper_closeparentheses").ToString())
                            {
                                currentToken = tempTokensArray[currentTokenIndex += 1];
                                if (currentToken == tokens.convertInt("end_of_line").ToString())
                                {
                                    logging.logDebug("Input function syntax correct", logging.loggingSenderID.syntax);
                                    goto TopOfSyntaxTree;
                                }
                                else logging.logError("ERROR_SEMICOLON_EXPECTED", "Current token: [" + currentTokenIndex.ToString() + "," + tempTokensArray[currentTokenIndex] + "," + tempValuesArray[currentTokenIndex] + "]", logging.loggingSenderID.syntax);
                            }
                            else logging.logError("ERROR_CLOSE_PARENTHESES_EXPECTED", "Current token: [" + currentTokenIndex.ToString() + "," + tempTokensArray[currentTokenIndex] + "," + tempValuesArray[currentTokenIndex] + "]", logging.loggingSenderID.syntax);
                        }
                        else logging.logError("ERROR_UNEXPECTED_TOKEN", "Current token: [" + currentTokenIndex.ToString() + "," + tempTokensArray[currentTokenIndex] + "," + tempValuesArray[currentTokenIndex] + "]", logging.loggingSenderID.syntax);
                    }
                    else if (currentToken == tokens.convertInt("variable").ToString()) logging.logError("ERROR_REGISTER_EXPECTED", "Current token: [" + currentTokenIndex.ToString() + "," + tempTokensArray[currentTokenIndex] + "," + tempValuesArray[currentTokenIndex] + "]", logging.loggingSenderID.syntax);
                    else logging.logError("ERROR_UNEXPECTED_TOKEN", "Current token: [" + currentTokenIndex.ToString() + "," + tempTokensArray[currentTokenIndex] + "," + tempValuesArray[currentTokenIndex] + "]", logging.loggingSenderID.syntax);
                }
                else logging.logError("8.2", "Current token: [" + currentTokenIndex.ToString() + "," + tempTokensArray[currentTokenIndex] + "," + tempValuesArray[currentTokenIndex] + "]", logging.loggingSenderID.syntax);
            }
            #endregion
            #region Jump Function
            if (currentToken == tokens.convertInt("func_jump").ToString())
            {
                currentToken = tempTokensArray[currentTokenIndex += 1];
                if (currentToken == tokens.convertInt("grouper_openparentheses").ToString())
                {
                    currentToken = tempTokensArray[currentTokenIndex += 1];
                    if (currentToken == tokens.convertInt("variable").ToString())
                    {
                        currentToken = tempTokensArray[currentTokenIndex += 1];
                        if (currentToken == tokens.convertInt("grouper_closeparentheses").ToString())
                        {
                            currentToken = tempTokensArray[currentTokenIndex += 1];
                            if (currentToken == tokens.convertInt("end_of_line").ToString())
                            {
                                logging.logDebug("jump function syntax correct", logging.loggingSenderID.syntax);
                                goto TopOfSyntaxTree;
                            }
                            else logging.logError("3.2", "Current token: [" + currentTokenIndex.ToString() + "," + tempTokensArray[currentTokenIndex] + "," + tempValuesArray[currentTokenIndex] + "]", logging.loggingSenderID.syntax);
                        }
                        else logging.logError("8.4", "Current token: [" + currentTokenIndex.ToString() + "," + tempTokensArray[currentTokenIndex] + "," + tempValuesArray[currentTokenIndex] + "]", logging.loggingSenderID.syntax);
                    }
                    else logging.logError("9.7", "Current token: [" + currentTokenIndex.ToString() + "," + tempTokensArray[currentTokenIndex] + "," + tempValuesArray[currentTokenIndex] + "]", logging.loggingSenderID.syntax);
                }
                else logging.logError("8.4", "Current token: [" + currentTokenIndex.ToString() + "," + tempTokensArray[currentTokenIndex] + "," + tempValuesArray[currentTokenIndex] + "]", logging.loggingSenderID.syntax);
            }
            #endregion
            #region Sleep Function
            if (currentToken == tokens.convertInt("func_sleep").ToString())
            {
                currentToken = tempTokensArray[currentTokenIndex += 1];
                if (currentToken == tokens.convertInt("grouper_openparentheses").ToString())
                {
                    currentToken = tempTokensArray[currentTokenIndex += 1];
                    if (currentToken == tokens.convertInt("data_int").ToString())
                    {
                        currentToken = tempTokensArray[currentTokenIndex += 1];
                        if (currentToken == tokens.convertInt("grouper_closeparentheses").ToString())
                        {
                            currentToken = tempTokensArray[currentTokenIndex += 1];
                            if (currentToken == tokens.convertInt("end_of_line").ToString())
                            {
                                logging.logDebug("Sleep function syntax correct", logging.loggingSenderID.syntax);
                                goto TopOfSyntaxTree;
                            }
                            else logging.logError("ERROR_NO_SEMICOLON", "Current token: [" + currentTokenIndex.ToString() + "," + tempTokensArray[currentTokenIndex] + "," + tempValuesArray[currentTokenIndex] + "]", logging.loggingSenderID.syntax);
                        }
                        else logging.logError("ERROR_REQUIRE_CLOSEPARENTHESES", "Current token: [" + currentTokenIndex.ToString() + "," + tempTokensArray[currentTokenIndex] + "," + tempValuesArray[currentTokenIndex] + "]", logging.loggingSenderID.syntax);
                    }
                    else logging.logError("ERROR_REQUIRE_DATA_INT", "Current token: [" + currentTokenIndex.ToString() + "," + tempTokensArray[currentTokenIndex] + "," + tempValuesArray[currentTokenIndex] + "]", logging.loggingSenderID.syntax);
                }
                else logging.logError("ERROR_REQUIRE_OPENPARENTHESES", "Current token: [" + currentTokenIndex.ToString() + "," + tempTokensArray[currentTokenIndex] + "," + tempValuesArray[currentTokenIndex] + "]", logging.loggingSenderID.syntax);
            }
            #endregion


            #endregion

            #region Imported & General functions syntax
            importedFuncSyntax:
            if (Convert.ToInt16(currentToken) > tokens.totalTokenCount)
            {
                // we can probably optimize this entire category!!
                // save location of current token
                int tokenLocation = currentTokenIndex;
                string fName = tokens.convertStr(Convert.ToInt16(currentToken), true);
                string importedSyntax = "";
                string successTableFragment = ""; // just for logging
                logging.logDebug("Begin syntax check on imported function [" + fName + "]", logging.loggingSenderID.syntax);
                logging.logDebug("Acquiring syntax tables", logging.loggingSenderID.syntax);
                string pulledSyntaxTableID = tokens.importedSyntaxTokenBindingTable[Array.IndexOf(tokens.importedTokenTable, fName)];
                logging.logDebug("pulledsyntaxtableID is " + pulledSyntaxTableID, logging.loggingSenderID.syntax);
                int tableCount = pulledSyntaxTableID.Count(x => x == '-');
                if (tableCount == 0)
                {
                    logging.logDebug("[1] syntax table found", logging.loggingSenderID.syntax);
                    importedSyntax = syntaxFunctionTables.First(x => x.Key == tokens.importedSyntaxTokenBindingTable[Array.IndexOf(tokens.importedTokenTable, tokens.convertStr(Convert.ToInt16(currentToken), true))]).Value.ToString();
                    if (!checkImportedSyntax(importedSyntax)) { logging.logError("X1", "Imported function syntax incorrect!", logging.loggingSenderID.syntax); }
                    else logging.logDebug("Syntax of imported function '" + fName + "' correct", logging.loggingSenderID.syntax);
                    // ***FOR NOW*** NO NEED TO INCLUDE EOL TOKEN! CHANGE THIS (below) IF THIS CHANGES!
                    currentToken = tempTokensArray[currentTokenIndex += 1];
                    goto TopOfSyntaxTree;
                }
                else
                {
                    logging.logDebug("[" + tableCount.ToString() + "] syntax tables found", logging.loggingSenderID.syntax);
                    string tableFragment = "";
                    char c;
                    int j = 0;
                    StringReader sr = new StringReader(pulledSyntaxTableID);
                    while (j != pulledSyntaxTableID.Length)
                    {
                        c = (char)sr.Read();
                        if (c == '-')
                        {
                            logging.logDebug("tableFragment is [" + tableFragment + "]", logging.loggingSenderID.syntax);
                            // process 
                            if (checkImportedSyntax(syntaxFunctionTables.First(x => x.Key == tableFragment).Value.ToString())) { successTableFragment = tableFragment; goto syntaxYes; }
                            // reset tokenindex
                            currentTokenIndex = tokenLocation;
                            tableFragment = "";
                        }
                        else { tableFragment += char.ToString(c); j++; }
                    }
                    goto syntaxNo;

                    syntaxYes:
                    logging.logDebug("Syntax of imported function '" + fName + "' correct using syntax [" + successTableFragment + "]", logging.loggingSenderID.syntax);
                    // ***FOR NOW*** NO NEED TO INCLUDE EOL TOKEN! CHANGE THIS (below) IF THIS CHANGES!
                    // DEBUG THIS
                    currentToken = tempTokensArray[currentTokenIndex += 1];
                    goto TopOfSyntaxTree;
                    syntaxNo:
                    logging.logError("X2", "Imported function syntax incorrect!", logging.loggingSenderID.syntax);
                    // ***FOR NOW*** NO NEED TO INCLUDE EOL TOKEN! CHANGE THIS (below) IF THIS CHANGES!
                    // DEBUG THIS
                    currentToken = tempTokensArray[currentTokenIndex += 1];
                    goto TopOfSyntaxTree;
                }
            }
            #endregion

            #region Special Unique Cases Syntax 
            // locations
            if (currentToken == tokens.convertInt("identifier_jumploc").ToString())
            {
                // syntax correct, jump to top of syntax tree
                logging.logDebug("Declaring location syntax correct", logging.loggingSenderID.syntax);
                goto TopOfSyntaxTree;
            }
            // else
            if (currentToken == tokens.convertInt("identifier_else").ToString())
            {
                // syntax correct, jump to top of syntax tree
                logging.logDebug("Else syntax correct", logging.loggingSenderID.syntax);
                goto TopOfSyntaxTree;
            }
            // defining custom function
            if (currentToken == tokens.convertInt("identifier_func").ToString())
            {
                currentToken = tempTokensArray[currentTokenIndex += 1];
                currentTokenValue = tempValuesArray[currentTokenIndex];
                if (tokens.importedTokenTable.Contains(currentTokenValue))
                {
                    currentToken = tempTokensArray[currentTokenIndex += 1];
                    if (currentToken == tokens.convertInt("grouper_openparentheses").ToString())
                    {
                    // loop for any amount of arguments to pass into function or none at all
                    TopOfArgument:
                        currentToken = tempTokensArray[currentTokenIndex += 1];
                        if (currentToken == tokens.convertInt("variable").ToString()) { goto TopOfArgument; }
                        else if (currentToken == tokens.convertInt("grouper_closeparentheses").ToString())
                        {
                            logging.logDebug("Defining function syntax correct", logging.loggingSenderID.syntax);
                            goto TopOfSyntaxTree;
                        }
                        else logging.logError("ERROR_UNEXPECTED_TOKEN_IN_DEFINING_FUNCTION", "Current token: [" + currentTokenIndex.ToString() + "," + tempTokensArray[currentTokenIndex] + "," + tempValuesArray[currentTokenIndex] + "]", logging.loggingSenderID.syntax);
                    }
                    else logging.logError("ERROR_PARENTHESES_REQUIRED", "Current token: [" + currentTokenIndex.ToString() + "," + tempTokensArray[currentTokenIndex] + "," + tempValuesArray[currentTokenIndex] + "]", logging.loggingSenderID.syntax);
                }
                else logging.logError("ERROR_NO_VAR_WHEN_DEFINING_FUNCTION", "Current token: [" + currentTokenIndex.ToString() + "," + tempTokensArray[currentTokenIndex] + "," + tempValuesArray[currentTokenIndex] + "]", logging.loggingSenderID.syntax);
            }
            // return statement
            if (currentToken == tokens.convertInt("func_return").ToString())
            {
                currentToken = tempTokensArray[currentTokenIndex += 1];
                if (currentToken == tokens.convertInt("variable").ToString())
                {
                    currentToken = tempTokensArray[currentTokenIndex += 1];
                    if(currentToken == tokens.convertInt("end_of_line").ToString())
                    {
                        logging.logDebug("Return statement syntax correct", logging.loggingSenderID.syntax);
                        goto TopOfSyntaxTree;
                    }
                    else logging.logError("ERROR_UNEXPECTED_TOKEN", "Current token: [" + currentTokenIndex.ToString() + "," + tempTokensArray[currentTokenIndex] + "," + tempValuesArray[currentTokenIndex] + "]", logging.loggingSenderID.syntax);
                }
                else if (currentToken == tokens.convertInt("end_of_line").ToString())
                {
                    logging.logDebug("Return statement syntax correct", logging.loggingSenderID.syntax);
                    goto TopOfSyntaxTree;
                }
                else logging.logError("ERROR_UNEXPECTED_TOKEN", "Current token: [" + currentTokenIndex.ToString() + "," + tempTokensArray[currentTokenIndex] + "," + tempValuesArray[currentTokenIndex] + "]", logging.loggingSenderID.syntax);
            }
            // curly brackets
            if (currentToken == tokens.convertInt("grouper_openbracket").ToString()) { curlyBracketCount++; goto TopOfSyntaxTree; }
            if (currentToken == tokens.convertInt("grouper_closebracket").ToString()) { curlyBracketCount--; goto TopOfSyntaxTree; }


            #endregion

            #region If Statement Syntax
            if (currentToken == tokens.convertInt("identifier_if").ToString())
            {
                if (tempTokensArray[currentTokenIndex += 1] == tokens.convertInt("grouper_openparentheses").ToString())
                {
                    // specific input(portID) syntax
                    if (tempTokensArray[currentTokenIndex += 1] == tokens.convertInt("func_input").ToString())
                    {
                        if (tempTokensArray[currentTokenIndex += 1] == tokens.convertInt("grouper_openparentheses").ToString())
                        {
                            if (tempTokensArray[currentTokenIndex += 1] == tokens.convertInt("data_int").ToString()
                                && Convert.ToInt16(tempValuesArray[currentTokenIndex]) >= 0 // for 0 <= x <= 3
                                && Convert.ToInt16(tempValuesArray[currentTokenIndex]) <= 3)
                            {
                                if (tempTokensArray[currentTokenIndex += 1] == tokens.convertInt("grouper_closeparentheses").ToString())
                                {
                                    if (tempTokensArray[currentTokenIndex += 1] == tokens.convertInt("grouper_closeparentheses").ToString())
                                    {
                                        logging.logDebug("If statement with portID check syntax correct", logging.loggingSenderID.syntax);
                                        goto TopOfSyntaxTree;
                                    }
                                    else logging.logError("ERROR_PARENTHESES_EXPECTED", "Current token: [" + currentTokenIndex.ToString() + "," + tempTokensArray[currentTokenIndex] + "," + tempValuesArray[currentTokenIndex] + "]", logging.loggingSenderID.syntax);
                                }
                                else logging.logError("ERROR_PARENTHESES_EXPECTED", "Current token: [" + currentTokenIndex.ToString() + "," + tempTokensArray[currentTokenIndex] + "," + tempValuesArray[currentTokenIndex] + "]", logging.loggingSenderID.syntax);
                            }
                            else logging.logError("ERROR_UNEXPECTED TOKEN/VALUE", "Current token: [" + currentTokenIndex.ToString() + "," + tempTokensArray[currentTokenIndex] + "," + tempValuesArray[currentTokenIndex] + "]", logging.loggingSenderID.syntax);
                        }
                        else logging.logError("ERROR_PARENTHESES_EXPECTED", "Current token: [" + currentTokenIndex.ToString() + "," + tempTokensArray[currentTokenIndex] + "," + tempValuesArray[currentTokenIndex] + "]", logging.loggingSenderID.syntax);
                    }
                    // optimize hcond handling!
                    else if (tempTokensArray[currentTokenIndex] == tokens.convertInt("variable").ToString()
                        || tempTokensArray[currentTokenIndex] == tokens.convertInt("data_int").ToString()
                        || tempTokensArray[currentTokenIndex] == tokens.convertInt("hcond_overflow").ToString()
                        || tempTokensArray[currentTokenIndex] == tokens.convertInt("hcond_underflow").ToString()
                        /* hcond imports from internal classes*/
                        || tempTokensArray[currentTokenIndex] == tokens.convertInt("RC40E.RNG_ON").ToString()
                        || tempTokensArray[currentTokenIndex] == tokens.convertInt("RC50E.DATA_RECEIVED").ToString()
                        || tempTokensArray[currentTokenIndex] == tokens.convertInt("RC50E.STACK_OVERFLOW").ToString()
                        || tempTokensArray[currentTokenIndex] == tokens.convertInt("RC50E.DATASTREAM_ACTIVE").ToString())
                    {
                        // this handles:
                        // var [cond] var syntax
                        // var [cond] var [Loperator]... syntax
                        // [hcond] [Loperator]... syntax
                        // var [operator] var [cond] var syntax <-- and any combination of these!

                        // **** ADD ERROR DETECTION ****

                        int varCount = 0;
                        int LOps = 0;
                        int ops = 0;
                        int cond = 0;
                        int hcond = 0;

                        // handle if hcond is first condition
                        if (tempTokensArray[currentTokenIndex] == tokens.convertInt("hcond_overflow").ToString()
                            || tempTokensArray[currentTokenIndex] == tokens.convertInt("hcond_underflow").ToString()
                            || tempTokensArray[currentTokenIndex] == tokens.convertInt("RC40E.RNG_ON").ToString()
                            || tempTokensArray[currentTokenIndex] == tokens.convertInt("RC50E.DATA_RECEIVED").ToString()
                            || tempTokensArray[currentTokenIndex] == tokens.convertInt("RC50E.STACK_OVERFLOW").ToString()
                            || tempTokensArray[currentTokenIndex] == tokens.convertInt("RC50E.DATASTREAM_ACTIVE").ToString())
                        { hcond++; varCount--; }
                    TopOfVarCheck:
                        varCount++;
                        // operators
                        if (tempTokensArray[currentTokenIndex += 1] == tokens.convertInt("operator_plus").ToString()
                            || tempTokensArray[currentTokenIndex] == tokens.convertInt("operator_minus").ToString())
                        {
                            ops++; 
                            if (tempTokensArray[currentTokenIndex += 1] == tokens.convertInt("variable").ToString()
                                || tempTokensArray[currentTokenIndex] == tokens.convertInt("data_int").ToString()) { goto TopOfVarCheck; } // to ensure that we have a var op var 
                        }
                        // conditions
                        else if (tempTokensArray[currentTokenIndex] == tokens.convertInt("operator_cond_greaterthan").ToString()
                            || tempTokensArray[currentTokenIndex] == tokens.convertInt("operator_cond_lessthan").ToString()
                            || tempTokensArray[currentTokenIndex] == tokens.convertInt("operator_cond_equals").ToString()
                            || tempTokensArray[currentTokenIndex] == tokens.convertInt("operator_cond_notequals").ToString()
                            || tempTokensArray[currentTokenIndex] == tokens.convertInt("operator_cond_gequals").ToString()
                            || tempTokensArray[currentTokenIndex] == tokens.convertInt("operator_cond_lequals").ToString()
                            && (varCount % 2 != 0 || ops == varCount - 1))
                        {
                            cond++;
                            if (tempTokensArray[currentTokenIndex += 1] == tokens.convertInt("variable").ToString()
                                || tempTokensArray[currentTokenIndex] == tokens.convertInt("data_int").ToString()) { goto TopOfVarCheck; }
                        }
                        // Loperators
                        else if (tempTokensArray[currentTokenIndex] == tokens.convertInt("operator_cond_and").ToString()
                            || tempTokensArray[currentTokenIndex] == tokens.convertInt("operator_cond_or").ToString())
                        {
                            LOps++;
                            if (tempTokensArray[currentTokenIndex += 1] == tokens.convertInt("variable").ToString()
                                || tempTokensArray[currentTokenIndex] == tokens.convertInt("data_int").ToString()
                                || tempTokensArray[currentTokenIndex] == tokens.convertInt("hcond_overflow").ToString()
                                || tempTokensArray[currentTokenIndex] == tokens.convertInt("hcond_underflow").ToString()
                                || tempTokensArray[currentTokenIndex] == tokens.convertInt("RC40E.RNG_ON").ToString()
                                || tempTokensArray[currentTokenIndex] == tokens.convertInt("RC50E.DATA_RECEIVED").ToString()
                                || tempTokensArray[currentTokenIndex] == tokens.convertInt("RC50E.STACK_OVERFLOW").ToString()
                                || tempTokensArray[currentTokenIndex] == tokens.convertInt("RC50E.DATASTREAM_ACTIVE").ToString()) { goto TopOfVarCheck; }
                        }
                        // hcond conditions
                        else if (tempTokensArray[currentTokenIndex] == tokens.convertInt("hcond_overflow").ToString()
                            || tempTokensArray[currentTokenIndex] == tokens.convertInt("hcond_underflow").ToString()
                            || tempTokensArray[currentTokenIndex] == tokens.convertInt("RC40E.RNG_ON").ToString()
                            || tempTokensArray[currentTokenIndex] == tokens.convertInt("RC50E.DATA_RECEIVED").ToString()
                            || tempTokensArray[currentTokenIndex] == tokens.convertInt("RC50E.STACK_OVERFLOW").ToString()
                            || tempTokensArray[currentTokenIndex] == tokens.convertInt("RC50E.DATASTREAM_ACTIVE").ToString())
                        { hcond++; varCount--; goto TopOfVarCheck; } // varcount-- because variable is NOT next 
                        else if (tempTokensArray[currentTokenIndex] == tokens.convertInt("grouper_closeparentheses").ToString())
                        {
                            logging.logDebug("varcheck syntax correct [var " + varCount.ToString() + ", ops " + ops.ToString() + ", cond " + cond.ToString() + ", LOps " + LOps.ToString() + ", hcond " + hcond.ToString() + "]", logging.loggingSenderID.syntax);
                            goto TopOfSyntaxTree;
                        }
                    }
                    // data groupings
                    else if (tempTokensArray[currentTokenIndex] == tokens.convertInt("grouper_openparentheses").ToString())
                    {
                        logging.logDebug("grouper open hit", logging.loggingSenderID.syntax);
                    }
                    // external functions that return values
                    else if (Convert.ToInt16(tempTokensArray[currentTokenIndex]) > tokens.totalTokenCount)
                    { logging.logDebug("external func hit", logging.loggingSenderID.syntax); }
                }
            }
            #endregion

            // hit here if token is not parsed by ANYTHING above - means that this token is INCORRECT
            logging.logError("ERROR_UNEXPECTED_TOKEN_COMPLETE", "Current token: [" + currentTokenIndex.ToString() + "," + tempTokensArray[currentTokenIndex] + "," + tempValuesArray[currentTokenIndex] + "]", logging.loggingSenderID.syntax);
            goto TopOfSyntaxTree;

            // run a few more general syntax checks here
            EndOfFunction:
            // check to make sure all jump(var) commands have valid defined [var]
            logging.logDebug("Checking for invalid definitions ...", logging.loggingSenderID.syntax);
            for (int i = 0; i < tempTokensArray.Length - 1; i++)
            {
                if (tempTokensArray[i] == tokens.convertInt("func_jump").ToString())
                {
                    i += 2; // skip parentheses
                    logging.logDebug("Checking definition: [" + tempValuesArray[i] + "]", logging.loggingSenderID.syntax);
                    bool chkd = false;
                    for (int j = 0; j < tempTokensArray.Length - 1; j++)
                    {
                        if (tempTokensArray[j] == tokens.convertInt("identifier_jumploc").ToString())
                        { if (tempValuesArray[i] == tempValuesArray[j].Replace("#", "")) chkd = true; }
                    }
                    if (chkd == false)
                    {
                        currentTokenIndex = i;
                        currentToken = tempTokensArray[currentTokenIndex += 1];
                        currentTokenValue = tempValuesArray[currentTokenIndex];
                        logging.logError("20.0", "Definition [" + tempValuesArray[i] + "] not defined in program!", logging.loggingSenderID.syntax);
                    }
                }
            }
            // check to see if variables are used before they are defined
            // deprecated for now since it breaks the linking process
            #region invalid variable access check
            //logging.logDebug("Checking for invalid variable accesses ...", logging.loggingSenderID.syntax);
            //            for (int i = 0; i < tempTokensArray.Length - 1; i++)

            //{
            //                if (tempTokensArray[i] == tokens.convertInt("identifier_new").ToString())
            //                {
            //                    if (tempTokensArray[i + 1] == tokens.convertInt("identifier_local").ToString()) i += 2; else i += 1; // get to variable
            //                    logging.logDebug("Checking variable: [" + tempValuesArray[i] + "]", logging.loggingSenderID.syntax);
            //                    bool chkd = true; // true by default, false if anything goes wrong
            //                    for (int j = 0; j < i; j++) // start from top and work way down to variable declaration 
            //                    {
            //                        if (tempTokensArray[j] == tokens.convertInt("variable").ToString()
            //                            && tempValuesArray[i] == tempValuesArray[j])
            //                        { chkd = false; }
            //                    }
            //                    if (chkd == false)
            //                    {
            //                        currentTokenIndex = i;
            //                        currentToken = tempTokensArray[currentTokenIndex += 1];
            //                        currentTokenValue = tempValuesArray[currentTokenIndex];
            //                        logging.logError("20.0", "Variable [" + tempValuesArray[i] + "] accessed before it was defined!", logging.loggingSenderID.syntax);
            //                    }
            //                }
            //            }
            #endregion
            // check to see if there is an exit point in the program (will only throw warning)
            logging.logDebug("Checking for exit point ...", logging.loggingSenderID.syntax);
            if (!tempTokensArray.Contains(tokens.convertInt("func_exit").ToString()))
            { logging.logWarning("No exit point detected in program! The program is likely to run indefinitely!", logging.loggingSenderID.syntax); }
            // we use logwarning to also push to main output
            if (errorCount == 0) { logging.logDebug("Syntax check completed; no syntax errors found", logging.loggingSenderID.syntax, true); }
            else
            { logging.logWarning("Syntax check completed; " + errorCount.ToString() + " errors found. See the log for more details. Press [enter] to continue ...", logging.loggingSenderID.syntax);
                functionLibrary.writeDebugLogOut(); Console.ReadLine(); Environment.Exit(0);
            }
        }

        static bool checkImportedSyntax(string syntaxTable)
        {
            // make temp copy of data
            string[] tempTokensArray = compiler.tokenTokensArray;
            string[] tempValuesArray = compiler.tokenValuesArray;
            string[] tempLineOfCodeValueArray = compiler.tokenLineOfCodeValueArray;
            string currentToken = "";
            // rewind to ensure accurate parsing
            // fix 1/2 for importedSyntax on first token
            try { currentToken = tempTokensArray[currentTokenIndex -= 1]; }
            catch (Exception) { /* do nothing */ }
            logging.logDebug("bound syntax is [" + syntaxTable + "]", logging.loggingSenderID.syntax);
            // parse importedSyntax
            bool syntaxCorrect = true;
            string syntaxFragment = "";
            char c; int i = 0;
            StringReader sr = new StringReader(syntaxTable);
            while (i != syntaxTable.Length)
            {
                c = (char)sr.Read();
                if (c == '-')
                {
                    logging.logDebug("syntaxFragment is [" + syntaxFragment + "]", logging.loggingSenderID.syntax);
                    if (syntaxFragment.Contains('/'))
                    {
                        // basically repeat what we just did, instead use / instead of - 
                        if (currentTokenIndex != 0) currentToken = tempTokensArray[currentTokenIndex += 1]; // fix 2/2 for importedSyntax on first token
                        string syntaxFragmentMicro = "";
                        char c1;
                        int i1 = 0;
                        bool foundToken = false;
                        StringReader sr1 = new StringReader(syntaxFragment);
                        while (i1 != syntaxFragment.Length)
                        {
                            c1 = (char)sr1.Read();
                            if (c1 == '/')
                            {
                                logging.logDebug("syntaxFragmentMicro is [" + syntaxFragmentMicro + "]", logging.loggingSenderID.syntax);
                                if (currentToken == syntaxFragmentMicro)
                                {
                                    logging.logDebug("currenttoken " + currentToken + " matches syntaxfragmentmicro " + syntaxFragmentMicro, logging.loggingSenderID.syntax);
                                    foundToken = true;
                                    syntaxFragment = "";
                                    break;
                                }
                                syntaxFragmentMicro = "";
                            }
                            else { syntaxFragmentMicro += char.ToString(c1); i1++; }
                        }
                        if (foundToken != true) syntaxCorrect = false;
                    }
                    else
                    {
                        currentToken = tempTokensArray[currentTokenIndex += 1];
                        if (currentToken == syntaxFragment)
                        {
                            logging.logDebug("currenttoken " + currentToken + " matches syntaxFragment " + syntaxFragment, logging.loggingSenderID.syntax);
                        }
                        else { syntaxCorrect = false; }
                    }
                    syntaxFragment = "";
                }
                else { syntaxFragment += char.ToString(c); i++; }
            }
            if (syntaxCorrect != true) { return false; }
            else return true;
        }

        // to determine syntax for custom functions and then implement it
        // massively optimize this function
        // ****MOVE THIS FUNCTION TO LINKER AND IMPORTER****
        // generate custom function tables in TEMP array - THEN reorder based on syntax token order
        static void generateCustomFunctionSyntax()
        {
            string functionToken = "0";
            List<int> functionTokenList = new List<int>();
            List<string> syntaxTableList = new List<string>();
            logging.logDebug("Generating syntax tables for custom functions ...", logging.loggingSenderID.syntax);
            for (int i = 0; i < tempTokensArray.Length; i++)
            {
                if (tempTokensArray[i] == tokens.convertInt("identifier_func").ToString())
                {
                    // check if syntax table already exists
                    if (Program.moreVerboseDebug) logging.logDebug("Function definition token hit at " + i + ", checking syntax table status", logging.loggingSenderID.syntax);
                    bool syntaxFunctionTableFound = false;
                    string[] syntaxFunctionTableArray = syntaxFunctionTables.Values.ToArray();
                    for (int j = 0; j < syntaxFunctionTables.Count; j++)
                    {
                        StringReader sr = new StringReader(syntaxFunctionTableArray[j]);
                        string bufferString = ""; char currentChar = ' ';
                        // grab first component of syntaxTable
                        while (true)
                        {
                            if (currentChar == '-') break;
                            currentChar = (char)sr.Read();
                            bufferString += char.ToString(currentChar);
                        }
                        // remove dash
                        bufferString = bufferString.Replace("-", "");
                        functionToken = tempTokensArray[i + 1];
                        if (bufferString == functionToken)
                        { if (Program.moreVerboseDebug) logging.logDebug("Syntax table for custom token '" + functionToken + "', skipping!", logging.loggingSenderID.syntax); syntaxFunctionTableFound = true; }
                    }
                    if (!syntaxFunctionTableFound)
                    {
                        logging.logDebug("Function definition hit at token " + i + ", generating syntax table ...", logging.loggingSenderID.syntax);
                        string syntaxTable = tempTokensArray[i += 1] + "-";
                        while (true)
                        { syntaxTable += tempTokensArray[i += 1] + "-"; if (tempTokensArray[i] == tokens.convertInt("grouper_closeparentheses").ToString()) break; }
                        logging.logDebug("Generated syntax table is " + syntaxTable, logging.loggingSenderID.syntax);
                        syntaxTableList.Add(syntaxTable);
                    }
                }
            }
            // HAVE to bind the token syntaxes based on the order of the imported syntax table
            // add based on imported tokens and make sure to skip over tokens that we aren't using / have been removed by target resolution!
            for (int i = 0; i < tokens.deletedImportedTokens.Length; i++)
            { syntaxTableList.Add(tokens.deletedImportedTokens[i] + "-NULL"); }
            syntaxTableList.Sort(); // where all the magic happens
            int internalTokenCount = 0; if (importer.ext_RC40E) internalTokenCount += 7; if (importer.ext_RC50E) internalTokenCount += 15;
            for (int j = 0; j < tokens.importedTokenTable.Length - internalTokenCount; j++)
            {
                // grab first token of bound syntax
                string tokenBind = syntaxTableList[j].Remove(syntaxTableList[j].IndexOf('-'));
                if (!tokens.deletedImportedTokens.Contains(tokenBind))
                { logging.logDebug("Binding syntax [" + syntaxTableList[j] + "] to token [" + tokenBind + "]", logging.loggingSenderID.syntax); }
                else { logging.logDebug("Binding NULL syntax to discarded token [" + tokenBind + "]", logging.loggingSenderID.syntax); }
                tokens.bindSyntax("0." + (j + 1).ToString()); syntaxFunctionTables.Add("0." + (j + 1).ToString(), syntaxTableList[j]);
            }
        }
    }
}