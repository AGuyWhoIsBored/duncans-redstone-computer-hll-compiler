﻿using System;
using System.IO;
using System.Linq;

namespace drchllc_uploader_universal
{
    class Program
    {
        /* Duncan's Redstone Computer HLL Compiler [DRCHLL-C] Uploader | drchllc-uploader-universal
         * -------------------------

          Created 6/21/2015.
          Copyright © AGuyWhoIsBored. All rights reserved.

          DRCHLLC Uploader
          Project started on 6/21/15
          Build v1.0a pushed 7/22/16
          Build v1.1a pushed 11/10/18
          Build v1.0 [Official] pushed 9/1/19

        * --------------------------

        // this program will contain the code to upload to certain targets!

        // this program will include things such as:
        // - parsing flags
        // - setting variables
        // - etc...
        // - uploading
        // args :
        // [program] -f "file" -t [targetID] -c [coreID] [-d (toggle debug)] -os [overrideStartCoordinates "x,y,z,yaw,pitch"] -o [optionCollectiveID]

        /* DRCHLLC Uploader changelog / to-do list

        v1.0a: initial release

        v1.1a
        - added arguments for uploader settings to attempt to mitigate some users having issues with the uploader program
        - added datapack uploading method - this is now the recommended method to upload programs

        v1.0 [Official]
        - ported DRCHLLC Uploader to .NET Core 3.0 for multiplatform support
        - DRCHLLC Uploader --> drchllc-uploader-universal
        - removed traditional upload method (uploader now only has datapack uploading capability)
            - removed all deprecated code from codebase regarding this
        - updated code to include things that should have been included but were skipped over
        - added code for multiplatform compatibility
        [X] TEST UPLOADER ON WINDOWS
        [X] TEST UPLOADER ON MACOS
        [X] TEST UPLOADER ON LINUX

        */

        public static string uploaderVersion = "1.0";              // version of uploader program
        public static string compilerLanguageVersion = "1.0, 1.1"; // version(s) of ARCISS that the uploader supports

        // arguments and settings
        public static string compileTarget = "-1";
        public static string overrideStartCoordinates = "";
        public static string compileCoreTarget = "-1";
        public static string sourceFileLocation = "";
        public static string optionCollectiveID = "-1";
        public static bool skipCodeValidation = false;

        public static bool debug = false;
        private static string userCommand = null;

        static void Main(string[] args)
        {
            Console.Title = "DRCHLLC Uploader";
            Console.WriteLine("--------");
            Console.WriteLine("DRCHLL-C Machine Code Uploader v" + uploaderVersion);
            Console.WriteLine("Supports ARCISS spec v" + compilerLanguageVersion);
            Console.WriteLine("Written and developed by AGuyWhoIsBored");
            Console.WriteLine("DRCHLL-C Repository: https://bitbucket.org/AGuyWhoIsBored/duncans-redstone-computer-hll-compiler");
            Console.WriteLine("--------");
            Console.WriteLine("");

        TopOfArgumentAsk:
            try
            {
                // load arguments
                if (args.Length == 0) { Console.WriteLine("Waiting for user input ..."); userCommand = Console.ReadLine(); }
                else
                {
                    Console.WriteLine("Prepassed arguments found!");
                    for (int i = 0; i < args.Length; i++)
                    {
                        // for formatting
                        if (i == 0) { userCommand += args[i]; }
                        else { userCommand += " " + args[i]; }
                    }
                }
                Console.WriteLine("Beginning argument parsing ...");
                #region argument / flag parsing
                int index = 0;
                int flagCollectID = 0;
                int quoteCount = 0;
                char currentChar;
                string bufferString = null;
                userCommand += " ";
                StringReader sr = new StringReader(userCommand);
                while (index != userCommand.Length)
                {
                    currentChar = (char)sr.Read();
                    index++;
                    bufferString += currentChar;
                    switch (currentChar)
                    {
                        case ' ':
                            if (bufferString == "-f ") { flagCollectID = 1; }
                            else if (bufferString == "-t ") { flagCollectID = 2; }
                            else if (bufferString == "-o ") { flagCollectID = 3; }
                            else if (bufferString == "-c ") { flagCollectID = 4; }
                            else if (bufferString == "-d ") { debug = true; Console.WriteLine("debug mode enabled!"); }
                            else if (bufferString == "-os ") { flagCollectID = 5; }
                            else
                            {
                                if (flagCollectID == 2) { compileTarget = bufferString; compileTarget = functionLibrary.RemoveWhitespace(compileTarget); flagCollectID = 0; }
                                else if (flagCollectID == 3) { optionCollectiveID = bufferString; optionCollectiveID = functionLibrary.RemoveWhitespace(optionCollectiveID); flagCollectID = 0; }
                                else if (flagCollectID == 4) { compileCoreTarget = bufferString; compileCoreTarget = functionLibrary.RemoveWhitespace(compileCoreTarget); flagCollectID = 0; }
                            }
                            if (quoteCount == 0) { bufferString = null; }
                            break;
                        case '"':
                            quoteCount++;
                            if (flagCollectID == 1 && quoteCount == 2) { sourceFileLocation = bufferString; sourceFileLocation = sourceFileLocation.Remove(sourceFileLocation.Length - 1); quoteCount = 0; flagCollectID = 0; }
                            if (flagCollectID == 5 && quoteCount == 2) { overrideStartCoordinates = bufferString; overrideStartCoordinates = overrideStartCoordinates.Remove(overrideStartCoordinates.Length - 1); quoteCount = 0; flagCollectID = 0; }
                            bufferString = null;
                            break;
                    }
                }
                sr.Dispose();
                #endregion

                // dumping parsed flag variables
                if (debug)
                {
                    Console.WriteLine("compileTarget: " + compileTarget.ToString());
                    Console.WriteLine("overrideStartCoordinates: " + overrideStartCoordinates);
                    Console.WriteLine("compileCoreTarget: " + compileCoreTarget.ToString());
                    Console.WriteLine("sourceFileLocation: " + sourceFileLocation);
                    Console.WriteLine("optionCollectiveID: " + optionCollectiveID);
                }

                // not going to check advanced argument syntax because we're going to assume that the user knows that they're doing
                // especially if they are using a program like this
                if (compileTarget == "-1"
                    || compileCoreTarget == "-1"
                    || sourceFileLocation == "")
                { Console.WriteLine("Core arguments are incomplete! Please check your arguments and try again!"); goto TopOfArgumentAsk; }

                if (Path.GetExtension(sourceFileLocation).ToLower() != ".pb") { Console.WriteLine("This file isn't a valid program binary file to upload!"); Console.ReadLine(); Environment.Exit(0); }

                #region setting uploader override start coordinates if necessary
                // syntax: -os "x,y,z, yaw, pitch"
                if (overrideStartCoordinates != "")
                {
                    Console.WriteLine("Updating start coordinates to user-defined values!");

                    // break down values
                    string os1 = "", os2 = "", os3 = "", os4 = "", os5 = "";
                    int index1 = 0;
                    int count = 0;
                    char c;
                    string bs2 = null;
                    overrideStartCoordinates += ",";
                    StringReader sr1 = new StringReader(overrideStartCoordinates);
                    while (index1 != overrideStartCoordinates.Length)
                    {
                        c = (char)sr1.Read();
                        index1++;
                        bs2 += c;
                        switch (c)
                        {
                            case ',':
                                count++;
                                switch (count)
                                {
                                    case 1: os1 = bs2; os1 = os1.Remove(os1.Length - 1); break;
                                    case 2: os2 = bs2; os2 = os2.Remove(os2.Length - 1); break;
                                    case 3: os3 = bs2; os3 = os3.Remove(os3.Length - 1); break;
                                    case 4: os4 = bs2; os4 = os4.Remove(os4.Length - 1); break;
                                    case 5: os5 = bs2; os5 = os5.Remove(os5.Length - 1); break;
                                }
                                bs2 = null;
                                break;
                        }
                    }
                    try
                    {
                        if (compileTarget == "1")
                        {
                            switch (compileCoreTarget)
                            {
                                case "1": datapackuploader.RC30C1 = new int[] { Convert.ToInt16(os1), Convert.ToInt16(os2), Convert.ToInt16(os3), Convert.ToInt16(os4), Convert.ToInt16(os5) }; break;
                                case "2": datapackuploader.RC30C2 = new int[] { Convert.ToInt16(os1), Convert.ToInt16(os2), Convert.ToInt16(os3), Convert.ToInt16(os4), Convert.ToInt16(os5) }; break;
                                case "3": datapackuploader.RC30C3 = new int[] { Convert.ToInt16(os1), Convert.ToInt16(os2), Convert.ToInt16(os3), Convert.ToInt16(os4), Convert.ToInt16(os5) }; break;
                                case "4": datapackuploader.RC30C4 = new int[] { Convert.ToInt16(os1), Convert.ToInt16(os2), Convert.ToInt16(os3), Convert.ToInt16(os4), Convert.ToInt16(os5) }; break;
                            }
                        }
                        else if (compileTarget == "2")
                        {
                            switch (compileCoreTarget)
                            {
                                case "1": datapackuploader.RC40C1 = new int[] { Convert.ToInt16(os1), Convert.ToInt16(os2), Convert.ToInt16(os3), Convert.ToInt16(os4), Convert.ToInt16(os5) }; break;
                                case "2": datapackuploader.RC40C2 = new int[] { Convert.ToInt16(os1), Convert.ToInt16(os2), Convert.ToInt16(os3), Convert.ToInt16(os4), Convert.ToInt16(os5) }; break;
                                case "3": datapackuploader.RC40C3 = new int[] { Convert.ToInt16(os1), Convert.ToInt16(os2), Convert.ToInt16(os3), Convert.ToInt16(os4), Convert.ToInt16(os5) }; break;
                                case "4": datapackuploader.RC40C4 = new int[] { Convert.ToInt16(os1), Convert.ToInt16(os2), Convert.ToInt16(os3), Convert.ToInt16(os4), Convert.ToInt16(os5) }; break;
                            }
                        }
                        else if (compileTarget == "3")
                        {
                            switch (compileCoreTarget)
                            {
                                case "1": datapackuploader.RC50C1 = new int[] { Convert.ToInt16(os1), Convert.ToInt16(os2), Convert.ToInt16(os3), Convert.ToInt16(os4), Convert.ToInt16(os5) }; break;
                                case "2": datapackuploader.RC50C2 = new int[] { Convert.ToInt16(os1), Convert.ToInt16(os2), Convert.ToInt16(os3), Convert.ToInt16(os4), Convert.ToInt16(os5) }; break;
                                case "3": datapackuploader.RC50C3 = new int[] { Convert.ToInt16(os1), Convert.ToInt16(os2), Convert.ToInt16(os3), Convert.ToInt16(os4), Convert.ToInt16(os5) }; break;
                                case "4": datapackuploader.RC50C4 = new int[] { Convert.ToInt16(os1), Convert.ToInt16(os2), Convert.ToInt16(os3), Convert.ToInt16(os4), Convert.ToInt16(os5) }; break;
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("An error occured: " + e.Message);
                        Console.ReadLine(); Environment.Exit(0);
                    }
                    sr1.Dispose();
                }
                #endregion

                #region updating machine code array 
                datapackuploader.machineCodeList = File.ReadAllLines(sourceFileLocation).ToList();
                #endregion

                #region applying options 
                // [421] < bits
                // 1: cleanFirstEnabled
                // 2: onlyClean
                // 4: skipCodeValidation
                if (optionCollectiveID != "-1")
                {
                    Console.WriteLine("Updating uploader settings ...");
                    try
                    {
                        optionCollectiveID = functionLibrary.convertNumberToBinary(Convert.ToInt16(optionCollectiveID), true, 3);
                        StringReader sr1 = new StringReader(optionCollectiveID);
                        char currentChar1;
                        int index1 = 0;
                        while (index1 != optionCollectiveID.Length)
                        {
                            currentChar1 = (char)sr1.Read();
                            index1++;
                            if (currentChar1 == '1' && index1 == 1) { datapackuploader.cleanFirstEnabled = true; if (debug) Console.WriteLine("Clean first is enabled!"); }
                            else if (currentChar1 == '1' && index1 == 2) { datapackuploader.onlyClean = true; if (debug) Console.WriteLine("Only clean is enabled!"); }
                            else if (currentChar1 == '1' && index1 == 3) { skipCodeValidation = true; if (debug) Console.WriteLine("Skipping code validation! ***THIS IS NOT RECOMMENDED AND YOU ENABLE THIS OPTION AT YOUR OWN RISK!!***"); }
                        }
                        sr1.Dispose();
                    }
                    catch (Exception e)
                    { Console.WriteLine("An error occured: " + e.Message); }
                }

                #endregion

                Console.WriteLine("Uploader ready to start!");
                if (debug) { Console.WriteLine("Press [enter] to begin upload datapack generation!"); Console.ReadLine(); logging.startStopwatch(); }

                // check integrity of code if not blocked
                #region validating integrity of code

                if (!skipCodeValidation)
                {
                    logging.logDebug("Validating integrity and compatibility of machine code on selected target ...");
                    // validate integrity of machine code provided
                    for (int i = 0; i < datapackuploader.machineCodeList.Count; i++)
                    {
                        if (datapackuploader.machineCodeList[i].Length > 41 && compileTarget == "1")
                        {
                            logging.logDebug("***FATAL ERROR*** Program instruction length(s) exceed capacity of compile target!", true);
                            logging.logDebug("***FATAL ERROR*** This program WILL NOT be able to be uploaded to the compile target!", true);
                            logging.logDebug("***FATAL ERROR*** This was most likely caused by the program being too big!", true);
                            if (debug) Console.ReadLine();
                            Environment.Exit(0);
                        }
                        if (datapackuploader.machineCodeList[i].Length > 84 && compileTarget == "2")
                        {
                            logging.logDebug("***FATAL ERROR*** Program instruction length(s) exceed capacity of compile target!", true);
                            logging.logDebug("***FATAL ERROR*** This program WILL NOT be able to be uploaded to the compile target!", true);
                            logging.logDebug("***FATAL ERROR*** This was most likely caused by the program being too big!", true);
                            if (debug) Console.ReadLine();
                            Environment.Exit(0);
                        }
                        if (datapackuploader.machineCodeList[i].Length > 48 && compileTarget == "3")
                        {
                            logging.logDebug("***FATAL ERROR*** Program instruction length(s) exceed capacity of compile target!", true);
                            logging.logDebug("***FATAL ERROR*** This program WILL NOT be able to be uploaded to the compile target!", true);
                            logging.logDebug("***FATAL ERROR*** This was most likely caused by the program being too big!", true);
                            if (debug) Console.ReadLine();
                            Environment.Exit(0);
                        }
                    }
                    if (datapackuploader.machineCodeList.Count > 20 && datapackuploader.machineCodeList.Count <= 31 && compileTarget == "1")
                    {
                        logging.logDebug("***ERROR*** Program length is higher than default capacity of compile target!", true);
                        logging.logDebug("***ERROR*** Unless you have modified the hardware to allow high capacity, this program WILL NOT be able to be uploaded to the compile target!", true);
                        if (debug) Console.ReadLine();
                        Environment.Exit(0);
                    }
                    if (datapackuploader.machineCodeList.Count > 63 && compileTarget == "2")
                    {
                        logging.logDebug("***FATAL ERROR*** Program instruction length(s) exceed maximum capacity of compile target!", true);
                        logging.logDebug("***FATAL ERROR*** This program WILL NOT be able to be uploaded to the compile target!", true);
                        logging.logDebug("***FATAL ERROR*** This was most likely caused by the program being too big!", true);
                        if (debug) Console.ReadLine();
                        Environment.Exit(0);
                    }
                    if (datapackuploader.machineCodeList.Count > 128 && compileTarget == "3")
                    {
                        logging.logDebug("***FATAL ERROR*** Program instruction length(s) exceed maximum capacity of compile target!", true);
                        logging.logDebug("***FATAL ERROR*** This program WILL NOT be able to be uploaded to the compile target!", true);
                        logging.logDebug("***FATAL ERROR*** This was most likely caused by the program being too big!", true);
                        if (debug) Console.ReadLine();
                        Environment.Exit(0);
                    }
                }
                #endregion

                // begin main upload thread
                datapackuploader.generateDatapack();
                if (debug) { Console.ReadLine(); }
            }
            catch (Exception e)
            { Console.WriteLine("An error occured: " + e.Message); Console.ReadLine(); }
        }
    }
}
