﻿using System;
using System.Collections.Generic;
using System.IO;

namespace drchllc_uploader_universal
{
    class datapackuploader
    {
        /*
         * This class will contain all functions and information needed to be able
         * to generate a datapack that will upload the specified program to the appropriate targets!
         * This will GUARANTEE the integrity of the uploaded program!
         * 
         * Created 10/20/2018 by AGuyWhoIsBored
         * 
         */

        // uploader options
        public static bool cleanFirstEnabled = false;
        public static bool onlyClean = false;
        public static List<string> machineCodeList = new List<string>();


        // variables for uploading
        #region upload vars
        // { x, y, z, yaw, pitch }
        // put defaults in here
        // RC3.0
        public static int[] RC30C1 = { -305, 112, 1656, 0, 0 };
        public static int[] RC30C2 = { -305, 104, 1656, 0, 0 };
        public static int[] RC30C3 = { -183, 112, 1656, 0, 0 };
        public static int[] RC30C4 = { -183, 104, 1656, 0, 0 };

        // RC4.0
        public static int[] RC40C1 = { -072, 117, -383, 180, 0 };
        public static int[] RC40C2 = { -072, 077, -383, 180, 0 };
        public static int[] RC40C3 = { -189, 117, -383, 180, 0 };
        public static int[] RC40C4 = { -189, 077, -383, 180, 0 };

        // RC5.0
        public static int[] RC50C1 = { -304, 165, -16, -90, 0 };
        public static int[] RC50C2 = { -304, 124, -16, -90, 0 };
        public static int[] RC50C3 = { -304, 165, 100, -90, 0 };
        public static int[] RC50C4 = { -304, 124, 100, -90, 0 };
        #endregion

        public static void generateDatapack()
        {
            string datapackNamespace = Path.GetFileNameWithoutExtension(Program.sourceFileLocation) + "[" + Program.compileTarget + "," + Program.compileCoreTarget + "]";
            string datapackRoot = Path.GetDirectoryName(Program.sourceFileLocation);
            string uploadData = "kill @e[type=minecraft:cow]" + Environment.NewLine + "kill @e[type=minecraft:item]" + Environment.NewLine + "say Starting DRCHLLC Uploader for Program [" + datapackNamespace + "]" + Environment.NewLine;
            int x = -1; int y = -1; int z = -1;

            logging.logDebug("Generating uploader commands for datapack...", true);
            // basically run fast clean and upload algorithm here
            if (Program.compileTarget == "1")
            {
                if (cleanFirstEnabled)
                {
                    uploadData += "#Cleaning core" + Environment.NewLine;
                    #region init for clean
                    if (Program.compileCoreTarget == "1") { x = RC30C1[0]; }
                    else if (Program.compileCoreTarget == "2") { x = RC30C2[0]; }
                    else if (Program.compileCoreTarget == "3") { x = RC30C3[0]; }
                    else if (Program.compileCoreTarget == "4") { x = RC30C4[0]; }
                    if (Program.compileCoreTarget == "1") { y = RC30C1[1]; }
                    else if (Program.compileCoreTarget == "2") { y = RC30C2[1]; }
                    else if (Program.compileCoreTarget == "3") { y = RC30C3[1]; }
                    else if (Program.compileCoreTarget == "4") { y = RC30C4[1]; }
                    if (Program.compileCoreTarget == "1") { z = RC30C1[2]; }
                    else if (Program.compileCoreTarget == "2") { z = RC30C2[2]; }
                    else if (Program.compileCoreTarget == "3") { z = RC30C3[2]; }
                    else if (Program.compileCoreTarget == "4") { z = RC30C4[2]; }
                    #endregion

                    // part 1; cleaning goto
                    for (int i = 0; i < 20; i++)
                    {
                        if (i == 0)
                        {
                            if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2") { uploadData += "execute run fill " + x + " " + (y + 1) + " " + z + " " + (x + 28) + " " + (y + 1) + " " + z + " air" + Environment.NewLine; }
                            else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4") { uploadData += "execute run fill " + x + " " + (y + 1) + " " + z + " " + (x - 28) + " " + (y + 1) + " " + z + " air" + Environment.NewLine; }
                        }
                        else
                        {
                            if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2") { uploadData += "execute run fill " + x + " " + (y + 1) + " " + (z += 2) + " " + (x + 28) + " " + (y + 1) + " " + z + " air" + Environment.NewLine; }
                            else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4") { uploadData += "execute run fill " + x + " " + (y + 1) + " " + (z += 2) + " " + (x - 28) + " " + (y + 1) + " " + z + " air" + Environment.NewLine; }
                        }
                    }

                    // part 2; cleaning instruction
                    for (int i = 0; i < 20; i++)
                    {
                        if (i == 0)
                        {
                            if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2") { uploadData += "execute run fill " + x + " " + (y -= 3) + " " + (z -= 38) + " " + (x + 50) + " " + y + " " + z + " air" + Environment.NewLine; }
                            else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4") { uploadData += "execute run fill " + x + " " + (y -= 3) + " " + (z -= 38) + " " + (x - 50) + " " + y + " " + z + " air" + Environment.NewLine; }
                        }
                        else
                        {
                            if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2") { uploadData += "execute run fill " + x + " " + y + " " + (z += 2) + " " + (x + 50) + " " + y + " " + z + " air" + Environment.NewLine; }
                            else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4") { uploadData += "execute run fill " + x + " " + y + " " + (z += 2) + " " + (x - 50) + " " + y + " " + z + " air" + Environment.NewLine; }
                        }
                    }
                }

                #region init for upload
                if (Program.compileCoreTarget == "1") { x = RC30C1[0]; }
                else if (Program.compileCoreTarget == "2") { x = RC30C2[0]; }
                else if (Program.compileCoreTarget == "3") { x = RC30C3[0]; }
                else if (Program.compileCoreTarget == "4") { x = RC30C4[0]; }
                if (Program.compileCoreTarget == "1") { y = RC30C1[1]; }
                else if (Program.compileCoreTarget == "2") { y = RC30C2[1]; }
                else if (Program.compileCoreTarget == "3") { y = RC30C3[1]; }
                else if (Program.compileCoreTarget == "4") { y = RC30C4[1]; }
                if (Program.compileCoreTarget == "1") { z = RC30C1[2]; }
                else if (Program.compileCoreTarget == "2") { z = RC30C2[2]; }
                else if (Program.compileCoreTarget == "3") { z = RC30C3[2]; }
                else if (Program.compileCoreTarget == "4") { z = RC30C4[2]; }
                #endregion

                if (!onlyClean)
                {
                    uploadData += "#Uploading program to core" + Environment.NewLine;
                    for (int i = 0; i < machineCodeList.Count; i++)
                    {
                        StringReader sr = new StringReader(machineCodeList[i]);
                        char currentChar;

                        if (i != 0)
                        {
                            if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                            { x -= 50; y += 4; z += 2; }
                            else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                            { x += 50; y += 4; z += 2; }
                        }
                        for (int j = 0; j < 41 /*machineCodeList[i].Length*/; j++)
                        {
                            currentChar = (char)sr.Read();
                            if (currentChar == '1')
                            { uploadData += "execute run setblock " + x + " " + (y + 1) + " " + z + " redstone_wall_torch[facing=north,lit=false]" + Environment.NewLine; }
                            if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                            {
                                if (j == 14)
                                { x -= 28; y -= 4; }
                                else if (j != 40 /*machineCodeList[i].Length - 1*/)
                                { x += 2; }
                            }
                            else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                            {
                                if (j == 14)
                                { x += 28; y -= 4; }
                                else if (j != 40 /*machineCodeList[i].Length - 1*/)
                                { x -= 2; }
                            }
                        }
                        sr.Dispose();
                    }
                }
            }
            else if (Program.compileTarget == "2")
            {
                if (cleanFirstEnabled)
                {
                    uploadData += "#Cleaning core" + Environment.NewLine;
                    #region init for clean
                    if (Program.compileCoreTarget == "1") { x = RC40C1[0]; }
                    else if (Program.compileCoreTarget == "2") { x = RC40C2[0]; }
                    else if (Program.compileCoreTarget == "3") { x = RC40C3[0]; }
                    else if (Program.compileCoreTarget == "4") { x = RC40C4[0]; }
                    if (Program.compileCoreTarget == "1") { y = RC40C1[1]; }
                    else if (Program.compileCoreTarget == "2") { y = RC40C2[1]; }
                    else if (Program.compileCoreTarget == "3") { y = RC40C3[1]; }
                    else if (Program.compileCoreTarget == "4") { y = RC40C4[1]; }
                    if (Program.compileCoreTarget == "1") { z = RC40C1[2]; }
                    else if (Program.compileCoreTarget == "2") { z = RC40C2[2]; }
                    else if (Program.compileCoreTarget == "3") { z = RC40C3[2]; }
                    else if (Program.compileCoreTarget == "4") { z = RC40C4[2]; }
                    #endregion
                    // part 1; cleaning GPU granular and clock update
                    for (int i = 0; i < 63; i++)
                    {
                        if (i == 0) { uploadData += "execute run fill " + x + " " + (y + 1) + " " + z + " " + (x - 66) + " " + (y + 1) + " " + z + " air" + Environment.NewLine; }
                        else { uploadData += "execute run fill " + x + " " + (y + 1) + " " + (z -= 2) + " " + (x - 66) + " " + (y + 1) + " " + z + " air" + Environment.NewLine; }
                    }
                    // part 2; cleaning instruction
                    for (int i = 0; i < 63; i++)
                    {
                        if (i == 0) { uploadData += "execute run fill " + x + " " + (y -= 7) + " " + (z += 124) + " " + (x - 98) + " " + y + " " + z + " air" + Environment.NewLine; }
                        else { uploadData += "execute run fill " + x + " " + y + " " + (z -= 2) + " " + (x - 98) + " " + y + " " + z + " air" + Environment.NewLine; }
                    }
                }
                #region init for upload
                if (Program.compileCoreTarget == "1") { x = RC40C1[0]; }
                else if (Program.compileCoreTarget == "2") { x = RC40C2[0]; }
                else if (Program.compileCoreTarget == "3") { x = RC40C3[0]; }
                else if (Program.compileCoreTarget == "4") { x = RC40C4[0]; }
                if (Program.compileCoreTarget == "1") { y = RC40C1[1]; }
                else if (Program.compileCoreTarget == "2") { y = RC40C2[1]; }
                else if (Program.compileCoreTarget == "3") { y = RC40C3[1]; }
                else if (Program.compileCoreTarget == "4") { y = RC40C4[1]; }
                if (Program.compileCoreTarget == "1") { z = RC40C1[2]; }
                else if (Program.compileCoreTarget == "2") { z = RC40C2[2]; }
                else if (Program.compileCoreTarget == "3") { z = RC40C3[2]; }
                else if (Program.compileCoreTarget == "4") { z = RC40C4[2]; }
                #endregion
                if (!onlyClean)
                {
                    uploadData += "#Uploading program to core" + Environment.NewLine;
                    for (int i = 0; i < machineCodeList.Count; i++)
                    {
                        StringReader sr = new StringReader(machineCodeList[i]);
                        char currentChar;
                        if (i != 0)
                        { x += 98; y += 8; z -= 2; }
                        for (int j = 0; j < 84 /*machineCodeList[i].Length*/; j++)
                        {
                            currentChar = (char)sr.Read();
                            if (currentChar == '1')
                            { uploadData += "execute run setblock " + x + " " + (y + 1) + " " + z + " redstone_wall_torch[facing=south,lit=false]" + Environment.NewLine; }
                            if (j == 33)
                            { x += 66; y -= 8; }
                            else if (j != 83 /*machineCodeList[i].Length - 1*/)
                            { x -= 2; }
                        }
                        sr.Dispose();
                    }
                }
            }
            else if (Program.compileTarget == "3")
            {
                if (cleanFirstEnabled)
                {
                    uploadData += "#Cleaning core" + Environment.NewLine;
                    #region init for clean
                    if (Program.compileCoreTarget == "1") { x = RC50C1[0]; }
                    else if (Program.compileCoreTarget == "2") { x = RC50C2[0]; }
                    else if (Program.compileCoreTarget == "3") { x = RC50C3[0]; }
                    else if (Program.compileCoreTarget == "4") { x = RC50C4[0]; }
                    if (Program.compileCoreTarget == "1") { y = RC50C1[1]; }
                    else if (Program.compileCoreTarget == "2") { y = RC50C2[1]; }
                    else if (Program.compileCoreTarget == "3") { y = RC50C3[1]; }
                    else if (Program.compileCoreTarget == "4") { y = RC50C4[1]; }
                    if (Program.compileCoreTarget == "1") { z = RC50C1[2]; }
                    else if (Program.compileCoreTarget == "2") { z = RC50C2[2]; }
                    else if (Program.compileCoreTarget == "3") { z = RC50C3[2]; }
                    else if (Program.compileCoreTarget == "4") { z = RC50C4[2]; }
                    #endregion
                    for (int j = 0; j < 8; j++)
                    {
                        for (int i = 0; i < 16; i++)
                        {
                            if (i == 0) { uploadData += "execute run fill " + x + " " + (y + 1) + " " + z + " " + x + " " + (y + 1) + " " + (z + 94) + " air" + Environment.NewLine; }
                            else { uploadData += "execute run fill " + (x += 2) + " " + (y + 1) + " " + z + " " + x + " " + (y + 1) + " " + (z + 94) + " air" + Environment.NewLine; }
                        }
                        y -= 4; x -= 30;
                    }
                }
                #region init for upload
                if (Program.compileCoreTarget == "1") { x = RC50C1[0]; }
                else if (Program.compileCoreTarget == "2") { x = RC50C2[0]; }
                else if (Program.compileCoreTarget == "3") { x = RC50C3[0]; }
                else if (Program.compileCoreTarget == "4") { x = RC50C4[0]; }
                if (Program.compileCoreTarget == "1") { y = RC50C1[1]; }
                else if (Program.compileCoreTarget == "2") { y = RC50C2[1]; }
                else if (Program.compileCoreTarget == "3") { y = RC50C3[1]; }
                else if (Program.compileCoreTarget == "4") { y = RC50C4[1]; }
                if (Program.compileCoreTarget == "1") { z = RC50C1[2]; }
                else if (Program.compileCoreTarget == "2") { z = RC50C2[2]; }
                else if (Program.compileCoreTarget == "3") { z = RC50C3[2]; }
                else if (Program.compileCoreTarget == "4") { z = RC50C4[2]; }
                #endregion
                if (!onlyClean)
                {
                    uploadData += "#Uploading program to core" + Environment.NewLine;
                    // do [list / 16] amount of full row uploads
                    for (int i = 0; i < (machineCodeList.Count / 16); i++)
                    {
                        if (i != 0) { x -= 30; y -= 4; z -= 94; }

                        for (int k = 0; k < 16; k++)
                        {
                            StringReader sr = new StringReader(machineCodeList[16 * i + k]);
                            uploadData += "# [1] Uploading line of code " + (16 * i + k) + Environment.NewLine;
                            char currentChar;
                            for (int j = 0; j < 48 /*machineCodeList[i].Length*/; j++)
                            {
                                currentChar = (char)sr.Read();
                                if (currentChar == '1')
                                { uploadData += "execute run setblock " + x + " " + (y + 1) + " " + z + " redstone_wall_torch[facing=west,lit=false]" + Environment.NewLine; }
                                if (j != 47 /*machineCodeList[i].Length - 1*/)
                                { z += 2; }
                            }
                            if (k != 15) { x += 2; z -= 94; }
                            sr.Dispose();
                        }
                    }
                    // do [list % 16] amount of remainder row upload
                    if ((machineCodeList.Count / 16) != 0) { x -= 30; y -= 4; z -= 94; }
                    int remCount = machineCodeList.Count % 16;
                    for (int k = 0; k < (machineCodeList.Count % 16); k++)
                    {
                        StringReader sr = new StringReader(machineCodeList[machineCodeList.Count - remCount]);
                        uploadData += "# [2] Uploading line of code " + (machineCodeList.Count - remCount) + Environment.NewLine;
                        remCount -= 1;
                        char currentChar;
                        for (int j = 0; j < 48 /*machineCodeList[i].Length*/; j++)
                        {
                            currentChar = (char)sr.Read();
                            if (currentChar == '1')
                            { uploadData += "execute run setblock " + x + " " + (y + 1) + " " + z + " redstone_wall_torch[facing=west,lit=false]" + Environment.NewLine; }
                            if (j != 47 /*machineCodeList[i].Length - 1*/)
                            { z += 2; }
                        }
                        x += 2; z -= 94;
                        sr.Dispose();
                    }
                }
            }
            logging.logDebug("Generating datapack for program [" + datapackNamespace + "] on target [" + Program.compileTarget + "," + Program.compileCoreTarget + "]", true);

            // file manipulations
            // will generate this at the same location as the pb file
            // may have permissions issues
            try
            {
                // make the directories
                Directory.CreateDirectory(datapackRoot + "/drchllc-upload-" + datapackNamespace);
                Directory.CreateDirectory(datapackRoot + "/drchllc-upload-" + datapackNamespace + "/data");
                Directory.CreateDirectory(datapackRoot + "/drchllc-upload-" + datapackNamespace + "/data/drchllc-upload/functions");
                Directory.CreateDirectory(datapackRoot + "/drchllc-upload-" + datapackNamespace + "/data/minecraft/tags/functions");
                // make the files
                File.WriteAllText(datapackRoot + "/drchllc-upload-" + datapackNamespace + "/pack.mcmeta", @"{""pack"": {""pack_format"":1,""description"":""datapack to upload program [" + datapackNamespace + @"]""}}");
                // minecraft hooks
                File.WriteAllText(datapackRoot + "/drchllc-upload-" + datapackNamespace + "/data/minecraft/tags/functions/load.json", @"{""values"":[""drchllc-upload:init""]}");
                File.WriteAllText(datapackRoot + "/drchllc-upload-" + datapackNamespace + "/data/minecraft/tags/functions/tick.json", @"{""values"":[""drchllc-upload:check""]}");
                // uploader specific files
                File.WriteAllText(datapackRoot + "/drchllc-upload-" + datapackNamespace + "/data/drchllc-upload/functions/check.mcfunction", @"execute if entity @e[type=minecraft:cow] run function drchllc-upload:upload");
                File.WriteAllText(datapackRoot + "/drchllc-upload-" + datapackNamespace + "/data/drchllc-upload/functions/init.mcfunction", @"say Loaded DRCHLLC Uploader Datapack for Program '" + datapackNamespace + @"'");
                uploadData += "say Uploading of Program [" + datapackNamespace + "] complete!";
                File.WriteAllText(datapackRoot + "/drchllc-upload-" + datapackNamespace + "/data/drchllc-upload/functions/upload.mcfunction", uploadData);
                logging.logDebug("Databack generation complete! The datapack is located at '" + datapackRoot + "/drchllc-upload-" + datapackNamespace + "'", true);
            }
            catch (UnauthorizedAccessException) { Console.WriteLine("The DRCHLLC Uploader does not have access to write the generated datapack to disk! The uploader will now exit."); Console.ReadLine(); }
            catch (Exception e) { Console.WriteLine("An error occured when writing datapack files to disk: " + e.Message); Console.ReadLine(); }
        }
    }
}
