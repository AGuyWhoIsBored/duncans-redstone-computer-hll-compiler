﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Linq;

namespace DRCHLLC_Core_Compiler
{
    class compiler
    {
        /*
         * This class will contain all functions and information needed to be able to 
         * initiate and complete a compile job from ARCISS source code to ARCISS assembly / ARCISS program binaries.
         * 
         * Created 9/5/2016 by AGuyWhoIsBored
         * 
         */

        public static List<string> sourceCodeBuffer = new List<string>();
        public static string compiledProgramName = "";

        // 
        public static string[] tokenTokensArray = new string[0];
        public static string[] tokenValuesArray = new string[0];
        public static string[] tokenLineOfCodeValueArray = new string[0];

        // optimization flags
        public static bool enableOptimizations = false;
        public static bool enableVariableClockSpeedOptimization = false;
        public static bool enableIndependentConditionalBranchingOptimization = false;
        public static bool enableRedundantCodeRemovalOptimization = false;
        public static bool enableAutomaticFunctionGrouping = false;
        public static bool enableMemoryManagementOptimizations = false;
        public static bool enableInstructionMergingOptimizations = false;

        // memory allocation table arrays
        public static int[] MATAddresses = new int[0]; // address / line of code to goto [in case of jump]
        public static string[] MATAddressPointers = new string[0]; // pointer to address; the variable
        public static int[] MemoryAllocationType = new int[0]; // define here whether it's a variable or a label [in case of jump]

        public static List<string> preParsedAssemblyList = new List<string>();
        public static List<string> parsedAssemblyList = new List<string>();
        public static int tokenValueCount = 0;

        // syntax arrays
        public static string[] syntaxFunctionTableKeys = null;
        public static string[] syntaxFunctionTableValues = null;

        public static void initialize()
        {
            // mostly dumping available compiler tables
            // dump loaded extensions
            logging.logDebug("The following modules and extensions are loaded for use in program compilation:", logging.loggingSenderID.compiler, true);
            // change later
            logging.logDebug("-: ARCISS v1.0", logging.loggingSenderID.compiler, true);
            for (int i = 0; i < importer.resourceIDCounter; i++)
            {
                logging.logDebug(i.ToString() + ": " + importer.loadedResourcesList[i], logging.loggingSenderID.compiler, true);
            }

            // dump loaded & imported tokens
            if(tokens.importedTokenTable.Length != 0)
            {
                logging.logDebug("The following tokens have been loaded for use in program compilation:", logging.loggingSenderID.compiler);
                for (int i = 0; i < tokens.importedTokenTable.Length; i++)
                {
                    logging.logDebug("[" + i.ToString() + "," + tokens.convertInt(tokens.importedTokenTable[i]) + "," + tokens.importedSyntaxTokenBindingTable[i] + "]: " + tokens.importedTokenTable[i], logging.loggingSenderID.compiler);
                }
            }

            // dump loaded syntax tables
            if(syntax.syntaxFunctionTables.Count != 0)
            {
                logging.logDebug("The following syntax tables have been loaded for use in program compilation:", logging.loggingSenderID.compiler);
                syntaxFunctionTableKeys = syntax.syntaxFunctionTables.Keys.ToArray();
                syntaxFunctionTableValues = syntax.syntaxFunctionTables.Values.ToArray();
                for (int i = 0; i < syntax.syntaxFunctionTables.Count; i++)
                {
                    logging.logDebug("[" + syntaxFunctionTableKeys[i] + "]: " + syntaxFunctionTableValues[i], logging.loggingSenderID.compiler);
                }
            }
        }

        public static void compile()
        {
            logging.startStopwatch();
            logging.logDebug("Now is " + DateTime.Now, logging.loggingSenderID.compiler);
            logging.logDebug("Compile job started", logging.loggingSenderID.compiler, true);
            try
            {
                compiledProgramName = Path.GetFileNameWithoutExtension(Program.sourceFileLocation);
                StreamReader sr = new StreamReader(Program.sourceFileLocation);
                logging.logDebug("File reader opened on file " + Program.sourceFileLocation, logging.loggingSenderID.compiler);
                string fileHeader = sr.ReadLine();
                logging.logDebug("Checking if selected file is a redstone computer compiler target file ...", logging.loggingSenderID.compiler);
                if (fileHeader != "#RCCompileTarget")
                {
                    // exit compile method
                    logging.logDebug("File detected as non-redstone computer compiler target file! Did you forget to add #RCCompilerTarget?", logging.loggingSenderID.compiler);
                    logging.logError("0.0", "Compile thread aborted! Reason: file detected as non-redstone computer compiler target file", logging.loggingSenderID.compiler);
                    Console.ReadLine();
                }
                else
                {
                    logging.logDebug("File detected as redstone computer compiler target file", logging.loggingSenderID.compiler);
                    // begin compile
                    logging.logDebug("Beginning compile ...", logging.loggingSenderID.compiler, true);
                    logging.logDebug(File.ReadAllLines(Program.sourceFileLocation).Length.ToString() + " lines of code detected in initial file", logging.loggingSenderID.compiler, true);
                    #region Compile - stage 1 [Moving data into buffers]
                    logging.logDebug("Moving all data from file into buffer ...", logging.loggingSenderID.compiler, true);
                    string lineOfText;
                    int counter = 0;
                    while ((lineOfText = sr.ReadLine()) != null)
                    {
                        // this starts with line #2 of text
                        // add space at the end to make sure EVERYTHING gets lexed and thus tokenized properly!
                        sourceCodeBuffer.Add(lineOfText + " ");
                        logging.logDebug(counter.ToString() + ": [" + lineOfText + "]", logging.loggingSenderID.compiler);
                        counter++;
                    }
                    sr.Close();
                    logging.logDebug("File reader closed and resources released", logging.loggingSenderID.compiler);
                    #endregion
                    #region Compile - stage 2 [Checking for imported extensions and removing all non-code objects]
                    logging.logDebug("Removing all non-code objects ...", logging.loggingSenderID.compiler, true);
                    logging.logDebug("Removing all comments from buffer ... ", logging.loggingSenderID.compiler);
                    // remove all commented lines from all code before syntax checking
                    for (int i = 0; i < sourceCodeBuffer.Count; i++)
                    {
                        if (sourceCodeBuffer[i].Contains("//"))
                        {
                            logging.logDebug("Comments removed from line " + i.ToString(), logging.loggingSenderID.compiler);
                            sourceCodeBuffer[i] = sourceCodeBuffer[i].Remove(sourceCodeBuffer[i].IndexOf("//"), sourceCodeBuffer[i].Length - sourceCodeBuffer[i].IndexOf("//"));
                        }
                    }
                    logging.logDebug("Removing all empty lines of code from buffer ... ", logging.loggingSenderID.compiler);
                    // for consistency
                    for (int i = 0; i < sourceCodeBuffer.Count; i++)
                    {
                        Predicate<string> p = new Predicate<string>(functionLibrary.checkForZero);
                        if (string.IsNullOrWhiteSpace(sourceCodeBuffer[i]))
                        {
                            sourceCodeBuffer.RemoveAll(p);
                        }
                    }
                    logging.logDebug("Buffer now contains " + sourceCodeBuffer.Count.ToString() + " lines of code", logging.loggingSenderID.compiler, true);

                    logging.logDebug("Checking for requested extension imports ...", logging.loggingSenderID.compiler);

                    // change this to read only the first line
                    for (int i = 0; i < sourceCodeBuffer.Count; i++)
                    {
                        // add an extra space after each line for lexing support
                        sourceCodeBuffer[i] = sourceCodeBuffer[i] + " ";

                        if (sourceCodeBuffer[i].Contains("import RC40E"))
                        {
                            if (importer.ext_RC40E)
                            {
                                logging.logDebug("Redstone Computer v4.0 Extension detected and enabled", logging.loggingSenderID.compiler);
                            }
                            else if (importer.ext_RC40E == false) { logging.logDebug("Redstone Computer v4.0 Extension not enabled", logging.loggingSenderID.compiler); }
                            // then remove this line from the buffer as no longer needed
                            sourceCodeBuffer.RemoveAt(i);
                            logging.logDebug("import line removed from buffer", logging.loggingSenderID.compiler);
                            // check to make sure that the 'RCv4.0' compile target is selected; if not, throw a warning that unknown code may be executed when compiled
                            if (Program.compileTarget != "2" && importer.ext_RC40E) { logging.logWarning("Importing code library for the Redstone Computer v4.0 and the selected compile target is Redstone Computer v3.0! Unintended or unknown code may be executed on said compile target, and unknown errors may occur!", logging.loggingSenderID.compiler); }
                        }
                    }

                    logging.logDebug("Dumping program buffer ...", logging.loggingSenderID.compiler);
                    for (int i = 0; i < sourceCodeBuffer.Count; i++) { logging.logDebug(i + ": [" + sourceCodeBuffer[i] + "]", logging.loggingSenderID.compiler); }

                    #endregion
                    #region Compile - stage 3 [Lexing and tokenizing]
                    // this stage will check through the code and give each 'block' of information a value, such as variable, number, etc.
                    logging.logDebug("Lexing data ... ", logging.loggingSenderID.tokenizer, true);
                    logging.logDebug("Lexing data in buffer ... ", logging.loggingSenderID.compiler);
                    for (int i = 0; i < sourceCodeBuffer.Count; i++)
                    {
                        logging.logDebug("Lexing line " + (i + 1).ToString() + " of code from buffer", logging.loggingSenderID.compiler);
                        LexData(sourceCodeBuffer[i], (i + 1).ToString(), 0);
                    }
                    functionLibrary.AddToTokenArrays("21", "$eof$", "-1", 0);
                    logging.logDebug("Lexing and tokenizing complete", logging.loggingSenderID.compiler);
                    logging.logDebug(tokenValueCount.ToString() + " tokens generated from lexer and tokenizer", logging.loggingSenderID.compiler);
                    // then output to debugger token array values before cleanup
                    if(Program.moreVerboseDebug)
                    {
                        logging.logDebug("Dumping token arrays ...", logging.loggingSenderID.compiler);
                        for (int i = 0; i < tokenValueCount; i++)
                        {
                            logging.logDebug("Location " + i.ToString() + " in token array is ["
                                + tokenLineOfCodeValueArray[i] + "," + tokenTokensArray[i] + "," + tokenValuesArray[i] + "]", logging.loggingSenderID.compiler);
                        }
                    }
                    // clean up token arrays
                    logging.logDebug("Detecting invalid tokens ... ", logging.loggingSenderID.compiler);
                    int invalidTokenCount = 0;
                    for (int i = 0; i < tokenValueCount; i++)
                    {
                        if (tokenTokensArray[i] == "0")
                        {
                            invalidTokenCount++;
                            if (Program.moreVerboseDebug) { logging.logDebug("Array location " + i.ToString() + " detected with invalid token", logging.loggingSenderID.compiler); }
                        }
                    }

                    logging.logDebug(invalidTokenCount.ToString() + " invalid tokens detected", logging.loggingSenderID.compiler);

                    // to remove invalid entries from array properly, we'll have to remove ONE AT A TIME!
                    logging.logDebug("Removing invalid tokens ...", logging.loggingSenderID.compiler);
                    int removeOffset = 0;
                    int newTokenValueCount = tokenValueCount - invalidTokenCount;
                    while (invalidTokenCount != 0)
                    {
                        bool enableRemove = true;
                        for (int i = 0; i < tokenValueCount - removeOffset; i++)
                        {
                            if (tokenTokensArray[i] == "0" && enableRemove == true)
                            {
                                // this enableRemove variable exists to be able to remove one at a time
                                enableRemove = false;
                                removeOffset++;
                                tokenTokensArray = functionLibrary.RemoveElementFromArray(tokenTokensArray, i);
                                tokenValuesArray = functionLibrary.RemoveElementFromArray(tokenValuesArray, i);
                                tokenLineOfCodeValueArray = functionLibrary.RemoveElementFromArray(tokenLineOfCodeValueArray, i);
                                invalidTokenCount--;
                            }
                        }
                    }
                    logging.logDebug("Removed all invalid tokens from token arrays", logging.loggingSenderID.compiler);
                    logging.logDebug(newTokenValueCount.ToString() + " tokens in token arrays after removal of invalid tokens", logging.loggingSenderID.compiler);
                    logging.logDebug("Dumping updated token arrays ...", logging.loggingSenderID.compiler);
                    for (int i = 0; i < newTokenValueCount; i++)
                    {
                        logging.logDebug("Location " + i.ToString() + " in token array is ["
                            + tokenLineOfCodeValueArray[i] + "," + tokenTokensArray[i] + "," + tokenValuesArray[i] + "]", logging.loggingSenderID.compiler);
                    }
                    #endregion
                    #region Compile - stage 4 [Syntax check]
                    syntax.scanMainHLL();
                    #endregion
                    #region Compile - stage 5 [HLL to assembly parsing - stage 1]
                    logging.logDebug("Beginning HLL to assembly parsing ...", logging.loggingSenderID.assemblyParser, true);
                    logging.logDebug("Starting HLL to assembly parsing stage 1 ...", logging.loggingSenderID.assemblyParser);
                    assemblyParser.parseStage1();
                    logging.logDebug("Dumping current parsed assembly list ...", logging.loggingSenderID.compiler);
                    for (int i = 0; i < parsedAssemblyList.Count; i++)
                    {
                        logging.logDebug("[" + (i + 1).ToString() + "]: " + parsedAssemblyList[i], logging.loggingSenderID.compiler);
                    }
                    #endregion
                    #region Compile - stage 6 [Optimizations]
                    // we're going to 'perform optimizations' even when optimizations are disabled - i.e. merging test statements
                    optimizer.performOptimizations();
                    #endregion
                    #region Compile - stage 6.5 [memw > ramw / memr > ramr]
                    // convert memw and memr instruction to ramr and ramw instructions IF we're not using cache
                    if (enableMemoryManagementOptimizations == false || enableOptimizations == false || Program.compileTarget == "1")
                    {
                        // insert ramw and ramr statements
                        for (int i = 0; i < parsedAssemblyList.Count; i++)
                        {
                            if (parsedAssemblyList[i].Contains("memw"))
                            {
                                parsedAssemblyList[i] = parsedAssemblyList[i].Replace(parsedAssemblyList[i].Substring(parsedAssemblyList[i].IndexOf("memw"), 4), "ramw");
                            }
                            if (parsedAssemblyList[i].Contains("memr"))
                            {
                                parsedAssemblyList[i] = parsedAssemblyList[i].Replace(parsedAssemblyList[i].Substring(parsedAssemblyList[i].IndexOf("memr"), 4), "ramr");
                            }
                        }
                    }
                    #endregion
                    #region Compile - stage 7 [Generating Memory Allocation Tables pt. 1]
                    // stage 8 - generating memory allocation tables - won't generate jumps!
                    logging.logDebug("Generating memory allocation tables part 1...", logging.loggingSenderID.compiler, true);
                    generateMAT1();
                    // once function finishes, dump MAT tables
                    logging.logDebug("Memory allocation table generation complete", logging.loggingSenderID.compiler);
                    logging.logDebug("Dumping memory allocation table ...", logging.loggingSenderID.compiler);
                    for (int i = 0; i < MATAddresses.Length; i++)
                    {
                        logging.logDebug("[" + i.ToString() + "]: [" + MATAddressPointers[i] + "," + MATAddresses[i].ToString() + "," + MemoryAllocationType[i].ToString() + "]", logging.loggingSenderID.compiler);
                    }

                    #endregion
                    // you will want to modify all parsed code BEFORE this stage, as this and some stages after add branches in and can't easily be changed!
                    #region Compile - stage 8 [assembly parsing - stage 2]
                    logging.logDebug("Beginning assembly parsing stage 2 ...", logging.loggingSenderID.assemblyParser, true);
                    assemblyParser.parseStage2();
                    // once function completes, dump updated / generated assembly
                    logging.logDebug("Stage 2 of assembly parsing complete", logging.loggingSenderID.assemblyParser, true);
                    logging.logDebug("Dumping stage 2 parsed assembly list  ...", logging.loggingSenderID.compiler);
                    for (int i = 0; i < parsedAssemblyList.Count; i++)
                    {
                        logging.logDebug("[" + (i + 1).ToString() + "]: " + parsedAssemblyList[i], logging.loggingSenderID.compiler);
                    }

                    #endregion
                    #region Compile - stage 9 [Updating assembly code with Memory Allocation Table data / generating MAT part 2]
                    // stage 9 - updating assembly code with memory allocation table data - will also generate jumps here
                    logging.logDebug("Generating memory allocation tables part 2 ...", logging.loggingSenderID.compiler, true);
                    generateMAT2();
                    logging.logDebug("Dumping updated memory allocation table ...", logging.loggingSenderID.compiler);
                    for (int i = 0; i < MATAddresses.Length; i++)
                    {
                        logging.logDebug("[" + i.ToString() + "]: [" + MATAddressPointers[i] + "," + MATAddresses[i].ToString() + "," + MemoryAllocationType[i].ToString() + "]", logging.loggingSenderID.compiler);
                    }
                    logging.logDebug("Updating assembly code with memory allocation table data ...", logging.loggingSenderID.compiler, true);
                    updateAssemblyFromMAT();
                    // once function is done, dump updated list
                    logging.logDebug("Updated assembly code with MAT data", logging.loggingSenderID.compiler);
                    logging.logDebug("Dumping updated parsed assembly list ...", logging.loggingSenderID.compiler);
                    for (int i = 0; i < parsedAssemblyList.Count; i++)
                    {
                        logging.logDebug("[" + (i + 1).ToString() + "]: " + parsedAssemblyList[i], logging.loggingSenderID.compiler);
                    }
                    #endregion
                    #region Compile - stage 10 [assembly to machine code translation]
                    logging.logDebug("Beginning machine code generation ...", logging.loggingSenderID.compiler, true);
                    machineCodeParser.generateMachineCode();
                    #endregion
                    // WILL NEED TO VERIFY INTEGRITY OF MACHINE CODE AFTER IF UPLOADING! (i.e. if length is longer than expected, or if lines of code exceed available program mem space!)
                    #region Compile - stage 11 [Compile complete! Post operations]
                    logging.logDebug("Compilation complete! This program took " + logging.getStopwatchTimeInMS() / 1000.00 + " seconds to compile!", logging.loggingSenderID.compiler, true);
                    logging.logDebug(machineCodeParser.machineCodeList.Count.ToString() + " lines of machine code generated for compiler target!", logging.loggingSenderID.compiler, true);
                    // grab size of generated machine code
                    int generatedCodeSize = 0;
                    for (int i = 0; i < machineCodeParser.machineCodeList.Count; i++)
                    {
                        generatedCodeSize += machineCodeParser.machineCodeList[i].Length;
                    }
                    generatedCodeSize = generatedCodeSize / 8;
                    logging.logDebug("Size of compiled program is " + generatedCodeSize.ToString() + " bytes", logging.loggingSenderID.compiler);
                    // calculating MD5 of program
                    StringBuilder sb = new StringBuilder();
                    foreach (string value in machineCodeParser.machineCodeList) { sb.Append(value); }
                    logging.logDebug("MD5 hash of compiled program is " + functionLibrary.calculateMD5Hash(sb.ToString()), logging.loggingSenderID.compiler);
                    // write compiled machine code to file for further manipulation
                    // .pb extension - program binary
                    logging.logDebug("Exported compiled program to '" + Path.GetDirectoryName(Program.sourceFileLocation) + "\\" + compiledProgramName + ".pb'", logging.loggingSenderID.compiler, true);
                    File.WriteAllLines(Path.GetDirectoryName(Program.sourceFileLocation) + "\\" + compiledProgramName + ".pb", machineCodeParser.machineCodeList);
                    // if debug mode enabled, write debug log to text file
                    functionLibrary.writeDebugLogOut();
                    if (Program.debug) { logging.logDebug("Press [enter] to continue ...", logging.loggingSenderID.compiler, true); Console.ReadLine(); Environment.Exit(0); }
                    #endregion
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("[*FATAL ERROR*]: " + e.ToString());
                Console.WriteLine("[*FATAL ERROR*]: If you see this please file a bug report!");
                Console.WriteLine("[*FATAL ERROR*]: The program has crashed. Press [enter] to continue...");
                logging.logDebug("[*FATAL ERROR OCCURED*]: " + e.ToString(), logging.loggingSenderID.compiler);
                functionLibrary.writeDebugLogOut();
                Console.ReadLine();
                Environment.Exit(1);
            }
        }

        public static void LexData(string lineOfCode, string lineOfCodeValue, int sender)
        {
            // sender 0 = compiler, sender 1 = importer
            // this function will take each line of our compile list and read one character at a time and write it to a buffer. When it sees 
            // special characters, it will stop and determine what the data is that it just read, and it will give it a token value for our compiler
            // to understand later.
            logging.logDebug("Starting lexdata function", logging.loggingSenderID.tokenizer);
            StringReader sr = new StringReader(lineOfCode);
            int indexLocation = 0;
            int hashKeyCount = 0;
            char currentChar;
            string bufferString = "";
            string bufferString2 = "";
            string tokenedValue = "";
            while (indexLocation != lineOfCode.Length)
            {
                currentChar = (char)sr.Read();

                bufferString += char.ToString(currentChar);
                indexLocation++;

                // for debug
                if (Program.moreVerboseDebug) { logging.logDebug("bufferstring: " + bufferString, logging.loggingSenderID.tokenizer); }
                switch (currentChar)
                {
                    // detecting whitespace
                    case ' ':
                        // remove whitespace
                        bufferString = functionLibrary.RemoveWhitespace(bufferString);
                        logging.logDebug("reached special character [whitespace] " + bufferString + " " + bufferString.Length.ToString(), logging.loggingSenderID.tokenizer);
                        tokenedValue = tokens.convertStr(tokens.Tokenize(bufferString, sender));
                        // add to token array [token, value]
                        if(tokenedValue != "0")
                        {
                            logging.logDebug("Buffer tokened as " + tokenedValue, logging.loggingSenderID.tokenizer);
                            functionLibrary.AddToTokenArrays(tokenedValue, bufferString, lineOfCodeValue, sender);
                            if (Program.moreVerboseDebug) { logging.logDebug("Token and buffer added to token arrays", logging.loggingSenderID.tokenizer); }
                        }
                        else { if (Program.moreVerboseDebug) logging.logDebug("Invalid token detected - will omit from token buffers", logging.loggingSenderID.tokenizer); }
                        bufferString = null; // reset buffer to go again to tokenize
                        break;
                    // detecting jump regions
                    case '#':
                        hashKeyCount++;
                        if (hashKeyCount == 2)
                        {
                            // remove whitespace
                            bufferString = functionLibrary.RemoveWhitespace(bufferString);
                            logging.logDebug("reached special case [##] " + bufferString + " " + bufferString.Length.ToString(), logging.loggingSenderID.tokenizer);
                            tokenedValue = "19"; // special token case
                            functionLibrary.AddToTokenArrays(tokenedValue, bufferString, lineOfCodeValue, sender);
                            if (Program.moreVerboseDebug) { logging.logDebug("Token and buffer added to token arrays", logging.loggingSenderID.tokenizer); }
                            bufferString = null;
                        }
                        break;
                    // detecting data groupings 
                    case '(':
                        // remove whitespace
                        // this is a unique case
                        // if bufferstring is more than one character, use special double token case
                        // if bufferstring is one character, send a regular token
                        // bufferstring - value before parentheses if longer than one character
                        // bufferstring2 - parentheses
                        logging.logDebug("reached special charcater [(] " + bufferString + " " + bufferString.Length.ToString(), logging.loggingSenderID.tokenizer);
                        logging.logDebug("reached unique conditional double/single token case", logging.loggingSenderID.tokenizer);
                        bufferString = functionLibrary.RemoveWhitespace(bufferString);
                        if (bufferString.Length == 1)
                        {
                            // tokenize regularly
                            tokenedValue = tokens.convertStr(tokens.Tokenize(bufferString, sender));
                            logging.logDebug("Buffer tokened as " + tokenedValue, logging.loggingSenderID.tokenizer);
                            functionLibrary.AddToTokenArrays(tokenedValue, bufferString, lineOfCodeValue, sender);
                            if (Program.moreVerboseDebug) { logging.logDebug("Token and buffer added to token arrays", logging.loggingSenderID.tokenizer); }
                            bufferString = null;
                            break;
                        }
                        else
                        {
                            // tokenize both the parentheses and the object before it
                            bufferString2 = bufferString;
                            bufferString = bufferString.Replace('(', ' ');
                            bufferString = functionLibrary.RemoveWhitespace(bufferString);
                            bufferString2 = bufferString2.Remove(0, bufferString2.Length - 1);
                            tokenedValue = tokens.convertStr(tokens.Tokenize(bufferString, sender));
                            logging.logDebug("Buffer1 tokened as " + tokenedValue, logging.loggingSenderID.tokenizer);
                            functionLibrary.AddToTokenArrays(tokenedValue, bufferString, lineOfCodeValue, sender);
                            if (Program.moreVerboseDebug) { logging.logDebug("Token and buffer added to token arrays", logging.loggingSenderID.tokenizer); }
                            tokenedValue = tokens.convertStr(tokens.Tokenize(bufferString2, sender));
                            logging.logDebug("Buffer2 tokened as " + tokenedValue, logging.loggingSenderID.tokenizer);
                            functionLibrary.AddToTokenArrays(tokenedValue, bufferString2, lineOfCodeValue, sender);
                            if (Program.moreVerboseDebug) { logging.logDebug("Token and buffer added to token arrays", logging.loggingSenderID.tokenizer); }
                            bufferString = null;
                            break;
                        }
                    case ')':
                        // remove whitespace
                        // tokenize BOTH the value BEFORE the parentheses AND the parentheses!
                        // bufferString - value before parentheses
                        // bufferString2 - parentheses
                        bufferString2 = bufferString;
                        bufferString = bufferString.Replace(')', ' ');
                        bufferString = functionLibrary.RemoveWhitespace(bufferString);
                        bufferString2 = bufferString2.Remove(0, bufferString2.Length - 1);
                        logging.logDebug("reached special character [)] " + bufferString + " " + bufferString.Length.ToString(), logging.loggingSenderID.tokenizer);
                        logging.logDebug("reached special double token case", logging.loggingSenderID.tokenizer);
                        tokenedValue = tokens.convertStr(tokens.Tokenize(bufferString, sender));
                        logging.logDebug("Buffer1 tokened as " + tokenedValue, logging.loggingSenderID.tokenizer);
                        functionLibrary.AddToTokenArrays(tokenedValue, bufferString, lineOfCodeValue, sender);
                        if (Program.moreVerboseDebug) { logging.logDebug("Token and buffer added to token arrays", logging.loggingSenderID.tokenizer); }
                        tokenedValue = tokens.convertStr(tokens.Tokenize(bufferString2, sender));
                        logging.logDebug("Buffer2 tokened as " + tokenedValue, logging.loggingSenderID.tokenizer);
                        functionLibrary.AddToTokenArrays(tokenedValue, bufferString2, lineOfCodeValue, sender);
                        if (Program.moreVerboseDebug) { logging.logDebug("Token and buffer added to token arrays", logging.loggingSenderID.tokenizer); }
                        bufferString = null;
                        break;
                    case '{':
                        // remove whitespace
                        bufferString = functionLibrary.RemoveWhitespace(bufferString);
                        logging.logDebug("reached special character [{] " + bufferString + " " + bufferString.Length.ToString(), logging.loggingSenderID.tokenizer);
                        tokenedValue = tokens.convertStr(tokens.Tokenize(bufferString, sender));
                        logging.logDebug("Buffer tokened as " + tokenedValue, logging.loggingSenderID.tokenizer);
                        functionLibrary.AddToTokenArrays(tokenedValue, bufferString, lineOfCodeValue, sender);
                        if (Program.moreVerboseDebug) { logging.logDebug("Token and buffer added to token arrays", logging.loggingSenderID.tokenizer); }
                        bufferString = null;
                        break;
                    case '}':
                        // remove whitespace
                        bufferString = functionLibrary.RemoveWhitespace(bufferString);
                        logging.logDebug("reached special character [}] " + bufferString + " " + bufferString.Length.ToString(), logging.loggingSenderID.tokenizer);
                        tokenedValue = tokens.convertStr(tokens.Tokenize(bufferString, sender));
                        logging.logDebug("Buffer tokened as " + tokenedValue, logging.loggingSenderID.tokenizer);
                        functionLibrary.AddToTokenArrays(tokenedValue, bufferString, lineOfCodeValue, sender);
                        if (Program.moreVerboseDebug) { logging.logDebug("Token and buffer added to token arrays", logging.loggingSenderID.tokenizer); }
                        bufferString = null;
                        break;
                    // detecting semicolon
                    case ';':
                        // remove whitespace and semicolon from bufferstring
                        bufferString2 = bufferString;
                        bufferString = bufferString.Replace(';', ' ');
                        bufferString = functionLibrary.RemoveWhitespace(bufferString);
                        bufferString2 = bufferString2.Remove(0, bufferString2.Length - 1);
                        logging.logDebug("reached special character [semicolon/EOL] " + bufferString + " " + bufferString.Length.ToString(), logging.loggingSenderID.tokenizer);
                        logging.logDebug("reached special double token case", logging.loggingSenderID.tokenizer);
                        tokenedValue = tokens.convertStr(tokens.Tokenize(bufferString, sender));
                        logging.logDebug("Buffer tokened as " + tokenedValue, logging.loggingSenderID.tokenizer);
                        functionLibrary.AddToTokenArrays(tokenedValue, bufferString, lineOfCodeValue, sender);
                        tokenedValue = tokens.convertStr(tokens.Tokenize(bufferString2, sender));
                        logging.logDebug("Buffer2 tokened as " + tokenedValue, logging.loggingSenderID.tokenizer);
                        functionLibrary.AddToTokenArrays(tokenedValue, bufferString2, lineOfCodeValue, sender);
                        if (Program.moreVerboseDebug) { logging.logDebug("Token and buffer added to token arrays", logging.loggingSenderID.tokenizer); }
                        bufferString = null; // reset buffer string to go again to tokenize
                        break;
                    // put all special conditions here
                    default:
                        if (bufferString != null && bufferString.Contains(","))
                        {
                            // in this case, we will just token the value w/o the comma, and continue lexing
                            logging.logDebug("reached special case [,] " + bufferString + " " + bufferString.Length.ToString(), logging.loggingSenderID.tokenizer);
                            logging.logDebug("comma will be ignored in tokenization", logging.loggingSenderID.tokenizer);
                            bufferString = bufferString.Replace(',', ' ');
                            bufferString = functionLibrary.RemoveWhitespace(bufferString);
                            tokenedValue = tokens.convertStr(tokens.Tokenize(bufferString, sender));
                            logging.logDebug("Buffer tokened as " + tokenedValue, logging.loggingSenderID.tokenizer);
                            functionLibrary.AddToTokenArrays(tokenedValue, bufferString, lineOfCodeValue, sender);
                            if (Program.moreVerboseDebug) { logging.logDebug("Token and buffer added to token arrays", logging.loggingSenderID.tokenizer); }
                            bufferString = null;
                        }
                        if (bufferString != null && bufferString.Contains("else"))
                        {
                            logging.logDebug("reached special case [else] " + bufferString + " " + bufferString.Length.ToString(), logging.loggingSenderID.tokenizer);
                            bufferString = functionLibrary.RemoveWhitespace(bufferString);
                            tokenedValue = tokens.convertStr(tokens.Tokenize(bufferString, sender));
                            logging.logDebug("Buffer tokened as " + tokenedValue, logging.loggingSenderID.tokenizer);
                            functionLibrary.AddToTokenArrays(tokenedValue, bufferString, lineOfCodeValue, sender);
                            if (Program.moreVerboseDebug) { logging.logDebug("Token and buffer added to token arrays", logging.loggingSenderID.tokenizer); }
                            bufferString = null;
                        }
                        if (bufferString != null && bufferString.Contains("++"))
                        {
                            // bufferstring - variable
                            // bufferstring2 - ++ operator
                            bufferString2 = bufferString;
                            bufferString = bufferString.Replace('+', ' ');
                            bufferString = functionLibrary.RemoveWhitespace(bufferString);
                            bufferString2 = bufferString2.Remove(0, bufferString2.Length - 2);
                            logging.logDebug("reached special case [++] " + bufferString + " " + bufferString.Length.ToString(), logging.loggingSenderID.tokenizer);
                            logging.logDebug("reached special double token case", logging.loggingSenderID.tokenizer);
                            tokenedValue = tokens.convertStr(tokens.Tokenize(bufferString, sender));
                            logging.logDebug("Buffer tokened as " + tokenedValue, logging.loggingSenderID.tokenizer);
                            functionLibrary.AddToTokenArrays(tokenedValue, bufferString, lineOfCodeValue, sender);
                            tokenedValue = tokens.convertStr(tokens.Tokenize(bufferString2, sender));
                            logging.logDebug("Buffer2 tokened as " + tokenedValue, logging.loggingSenderID.tokenizer);
                            functionLibrary.AddToTokenArrays(tokenedValue, bufferString2, lineOfCodeValue, sender);
                            if (Program.moreVerboseDebug) { logging.logDebug("Token and buffer added to token arrays", logging.loggingSenderID.tokenizer); }
                            bufferString = null;
                        }
                        if (bufferString != null && bufferString.Contains("--"))
                        {
                            // bufferstring - variable
                            // bufferstring2 - -- operator
                            bufferString2 = bufferString;
                            bufferString = bufferString.Replace('-', ' ');
                            bufferString = functionLibrary.RemoveWhitespace(bufferString);
                            bufferString2 = bufferString2.Remove(0, bufferString2.Length - 2);
                            logging.logDebug("reached special case [--] " + bufferString + " " + bufferString.Length.ToString(), logging.loggingSenderID.tokenizer);
                            logging.logDebug("reached special double token case", logging.loggingSenderID.tokenizer);
                            tokenedValue = tokens.convertStr(tokens.Tokenize(bufferString, sender));
                            logging.logDebug("Buffer tokened as " + tokenedValue, logging.loggingSenderID.tokenizer);
                            functionLibrary.AddToTokenArrays(tokenedValue, bufferString, lineOfCodeValue, sender);
                            tokenedValue = tokens.convertStr(tokens.Tokenize(bufferString2, sender));
                            logging.logDebug("Buffer2 tokened as " + tokenedValue, logging.loggingSenderID.tokenizer);
                            functionLibrary.AddToTokenArrays(tokenedValue, bufferString2, lineOfCodeValue, sender);
                            if (Program.moreVerboseDebug) { logging.logDebug("Token and buffer added to token arrays", logging.loggingSenderID.tokenizer); }
                            bufferString = null;
                        }
                        break;
                }
            }
        }

        private static void generateMAT1()
        {
            // will be ID's as assemblyParser

            // we will go through tokens until we find a variable token, then add MAT entry
            int MATIndex = -1;
            int AddressCounter = 0; // so that we have most efficient use of address space as possible
            int currentTokenIndex = -1;
            string currentTokenID = null;
            string currentTokenValue = null;
            bool mallocCalled = false;
            bool temp1Allocated = false;
            bool temp2Allocated = false;

            // for cache
            string[] tokenFirstCache = new string[1];   // var1
            string[] tokenPartnerCache = new string[1]; // var2
            int cacheArrayIndex = 0;
            bool skipCacheAlgorithm = false;

            TopOfMATGenerator:
            currentTokenIndex++;
            currentTokenID = tokenTokensArray[currentTokenIndex];
            currentTokenValue = tokenValuesArray[currentTokenIndex];
            // eof check
            if (currentTokenID == (tokens.convertInt("identifier_eof")).ToString())
            {
                goto EndOfMATGeneration;
            }

            // before any of this, run through and see if we have temp1 or temp2 mem locations existing! If there are, add a 
            // value in the dictionary for em
            for (int i = 0; i < parsedAssemblyList.Count; i++)
            {
                if (parsedAssemblyList[i].Contains("~temp1~") && temp1Allocated == false)
                {
                    temp1Allocated = true;
                    MATIndex++;
                    AddressCounter++;
                    Array.Resize(ref MATAddresses, MATAddresses.Length + 1);
                    Array.Resize(ref MATAddressPointers, MATAddressPointers.Length + 1);
                    Array.Resize(ref MemoryAllocationType, MemoryAllocationType.Length + 1);
                    MATAddresses[MATIndex] = AddressCounter;
                    MATAddressPointers[MATIndex] = "~temp1~";
                    MemoryAllocationType[MATIndex] = 1; // 1 is var, 2 is jump, 3 is cached var
                }
                else if (parsedAssemblyList[i].Contains("~temp2~") && temp2Allocated == true)
                {
                    temp2Allocated = true;
                    MATIndex++;
                    AddressCounter++;
                    Array.Resize(ref MATAddresses, MATAddresses.Length + 1);
                    Array.Resize(ref MATAddressPointers, MATAddressPointers.Length + 1);
                    Array.Resize(ref MemoryAllocationType, MemoryAllocationType.Length + 1);
                    MATAddresses[MATIndex] = AddressCounter;
                    MATAddressPointers[MATIndex] = "~temp2~";
                    MemoryAllocationType[MATIndex] = 1; // 1 is var, 2 is jump, 3 is cached var
                }
            }

            if (currentTokenID == (tokens.convertInt("variable")).ToString())
            {
                // check if this variable already exists in MAT
                for (int i = 0; i < MATAddressPointers.Length; i++)
                {
                    if (MATAddressPointers[i] == (currentTokenValue)) { goto TopOfMATGenerator; }
                }
                // check if we've ran a malloc command for our found variable
                for (int i = 0; i < parsedAssemblyList.Count; i++)
                {
                    if (parsedAssemblyList[i].Contains("%compiler%: malloc") && parsedAssemblyList[i].Contains(currentTokenValue)) { mallocCalled = true; }
                }
                if (mallocCalled == true) { mallocCalled = false; }
                else
                {
                    logging.logDebug("Found new variable entry for MAT, but it was not initialized beforehand! Ignoring ...", logging.loggingSenderID.assemblyParser);
                    goto TopOfMATGenerator;
                }

                logging.logDebug("Found new variable entry for MAT", logging.loggingSenderID.assemblyParser);

                MATIndex++;
                AddressCounter++;
                Array.Resize(ref MATAddresses, MATAddresses.Length + 1);
                Array.Resize(ref MATAddressPointers, MATAddressPointers.Length + 1);
                Array.Resize(ref MemoryAllocationType, MemoryAllocationType.Length + 1);
                MATAddresses[MATIndex] = AddressCounter;
                MATAddressPointers[MATIndex] = currentTokenValue;
                MemoryAllocationType[MATIndex] = 1; // 1 is var, 2 is jump

                goto TopOfMATGenerator;
            }
            else
            {
                goto TopOfMATGenerator;
            }
        EndOfMATGeneration:

            // run RC40E cache algorithm
            if (rc40e.enableCache)
            {
                logging.logDebug("Executing MAT caching algorithm...", logging.loggingSenderID.assemblyParser);
                /* now we determine whether the variable entry we detected will be a dualread variable, or a singleread variable,
                 * based off of the use case of the variable in the code
                 * for now, this algorithm will set the dualread flag for the specific variable if it's used in dual-read scenarios more than 75% of the time
                 * the default flag will be set to singleread
                 * criteria for dualread flag:
                 * add[var, var1], sub[var, var1], cmp[var, var1], gpu_encode[var, var1],
                 * if the following cases arise, then we will fallback to using RAM for memory management (for that particular variable)
                 * -if the dualread var is manipulated with more than one other variable
                 */

                for (int i = 0; i < MATAddressPointers.Length; i++)
                {
                    logging.logDebug("Determining cache status of variable [" + MATAddressPointers[i] + "]", logging.loggingSenderID.assemblyParser);
                    logging.logDebug("Acquiring all variable partners...", logging.loggingSenderID.assemblyParser);
                    List<string> varPartners = new List<string>();
                    for (int j = 0; j < tokenTokensArray.Length; j++)
                    {
                        if(tokenValuesArray[j] == MATAddressPointers[i] && 
                            tokenTokensArray[j + 2] == tokens.convertInt("variable").ToString() && 
                            (tokenTokensArray[j + 1] == tokens.convertInt("operator_plus").ToString()
                            || tokenTokensArray[j + 1] == tokens.convertInt("operator_minus").ToString()
                            || tokenTokensArray[j + 1] == tokens.convertInt("operator_greaterthan").ToString()
                            || tokenTokensArray[j + 1] == tokens.convertInt("operator_ifequals").ToString() // check this
                            || tokenTokensArray[j + 1] == tokens.convertInt("operator_lessthan").ToString()))
                        {
                            logging.logDebug("Partner found: [" + MATAddressPointers[i] + "," + tokenValuesArray[j + 2] + "] at location " + j.ToString(), logging.loggingSenderID.assemblyParser);
                            // add to array
                            varPartners.Add(tokenValuesArray[j + 2]);
                        }
                    }
                    // check if all partners the same
                    string partner = null;
                    try
                    {
                        partner = varPartners[varPartners.Count - (varPartners.Count - 1)];
                    }
                    catch (Exception)
                    {
                        logging.logDebug("No partners found!", logging.loggingSenderID.assemblyParser);
                    }
                    bool partnerMatch = true;
                    foreach (string p in varPartners)
                    {
                        if(p != partner)
                        {
                            partnerMatch = false;
                            logging.logDebug("partners DO NOT match! Setting this variable to RAM", logging.loggingSenderID.assemblyParser);
                        }
                    }
                    if(partnerMatch)
                    {
                        logging.logDebug("partners match!", logging.loggingSenderID.assemblyParser);
                    }
                }

                   



            }

            logging.logDebug("MAT Generator function complete", logging.loggingSenderID.assemblyParser);
            // end of function

            // perform var = 0 malloc optimization if enabled
            if (enableRedundantCodeRemovalOptimization == true && enableOptimizations == true)
            {
                logging.logDebug("Performing redundant code removal optimizations ...", logging.loggingSenderID.assemblyParser);
            TopOfV0MO:
                for (int i = 0; i < parsedAssemblyList.Count; i++)
                {
                    if (parsedAssemblyList[i].Contains("%compiler%: malloc"))
                    {
                        // we'll have to grab blocks
                        string temp = parsedAssemblyList[i].Remove(parsedAssemblyList[i].IndexOf("%compiler%:"), 19);
                        if (temp.Contains("fjump")) { temp = temp.Remove(temp.IndexOf("fjump")); }
                        if (temp.Contains("tjump")) { temp = temp.Remove(temp.IndexOf("tjump")); }
                        string block1 = null;
                        string block2 = null;
                        int readLoc = 0;
                        StringReader sr = new StringReader(temp);
                        char currentChar;

                        // determine syntax
                        currentChar = (char)sr.Read();
                        block1 += currentChar;
                        readLoc++;
                        while (currentChar != ' ')
                        {
                            readLoc++;
                            currentChar = (char)sr.Read();
                            block1 += currentChar;
                        }
                        currentChar = (char)sr.Read();
                        block2 += currentChar;
                        readLoc++;
                        while (/*currentChar != ' '*/ readLoc != temp.Length)
                        {
                            readLoc++;
                            currentChar = (char)sr.Read();
                            block2 += currentChar;
                        }
                        // only concerned w/ block2
                        block2 = functionLibrary.RemoveWhitespace(block2);
                        if (Convert.ToInt16(block2) == 0)
                        {
                            parsedAssemblyList.RemoveAt(i);
                            goto TopOfV0MO;
                        }
                    }
                }
            }
        }

        private static void generateMAT2()
        {
            // will be ID'd as assemblyParser

            // here we will generate the jump locations in MAT
            // will do this the same way we do LexData
            int MATIndex = MATAddressPointers.Length - 1;
            bool foundJalloc = false;

            for (int i = 0; i < parsedAssemblyList.Count; i++)
            {
                StringReader sr = new StringReader(parsedAssemblyList[i]);
                char currentChar;
                int readLoc = 0;
                string bufferString = null;

                while (readLoc != parsedAssemblyList[i].Length)
                {
                    currentChar = (char)sr.Read();
                    bufferString += char.ToString(currentChar);
                    readLoc++;
                    switch (currentChar)
                    {
                        case ' ':
                            bufferString = functionLibrary.RemoveWhitespace(bufferString);
                            if (bufferString == "jalloc" && foundJalloc == false)
                            {
                                foundJalloc = true;
                            }
                            else if (foundJalloc == true)
                            {
                                foundJalloc = false;
                                // check and add
                                // check if this jump entry already exists in MAT; if it does, abort!
                                for (int j = 0; j < MATAddressPointers.Length; j++)
                                {
                                    if (MATAddressPointers[j].Contains(bufferString) && MATAddressPointers[j] != null)
                                    {
                                        logging.logDebug("ERROR: MAT Generator found more than one of the same jump locations!", logging.loggingSenderID.assemblyParser, true);
                                        if (Program.debug) logging.logDebug("INTERNAL: FIX HANDLING OF THIS ERROR!", logging.loggingSenderID.assemblyParser);
                                        if (Program.debug) Console.ReadLine();
                                    }
                                }
                                logging.logDebug("Found new jump entry for MAT", logging.loggingSenderID.assemblyParser);
                                MATIndex++;
                                Array.Resize(ref MATAddresses, MATAddresses.Length + 1);
                                Array.Resize(ref MATAddressPointers, MATAddressPointers.Length + 1);
                                Array.Resize(ref MemoryAllocationType, MemoryAllocationType.Length + 1);
                                MATAddresses[MATIndex] = i + 1; // to be the next line of code
                                MATAddressPointers[MATIndex] = bufferString;
                                MemoryAllocationType[MATIndex] = 2; // 1 is var, 2 is jump
                            }
                            bufferString = null;
                            break;
                    }
                }
            }
            logging.logDebug("MAT generation function finished", logging.loggingSenderID.assemblyParser);
        }

        private static void updateAssemblyFromMAT()
        {
        // will be ID'd as assemblyParser
        // check each line of code and see if we have a match from our MAT Address Pointers
        // will write the function similarly to the lexdata function
        TopOfUpdate:
            for (int i = 0; i < parsedAssemblyList.Count; i++)
            {
                parsedAssemblyList[i] += " ";
                StringReader sr = new StringReader(parsedAssemblyList[i]);
                int indexLocation = 0;
                char currentChar;
                string bufferString = null;
                while (indexLocation != parsedAssemblyList[i].Length)
                {
                    currentChar = (char)sr.Read();
                    bufferString += char.ToString(currentChar);
                    indexLocation++;
                    // need to also grab last block; this code isn't doing that and thus is causing memory leaks
                    // to do this, simply add space at end of each line of code
                    if (currentChar == ' ')
                    {
                        bufferString = functionLibrary.RemoveWhitespace(bufferString);
                        for (int j = 0; j < MATAddressPointers.Length; j++)
                        {
                            if (bufferString == MATAddressPointers[j])
                            {
                                if(Program.moreVerboseDebug)
                                {
                                    logging.logDebug("matap: " + MATAddressPointers[j], logging.loggingSenderID.assemblyParser);
                                    logging.logDebug("mata: " + MATAddresses[j].ToString(), logging.loggingSenderID.assemblyParser);
                                }
                                // for easy readability
                                if (MemoryAllocationType[j] == 2)
                                {
                                    parsedAssemblyList[i] = functionLibrary.ReplaceFirst(parsedAssemblyList[i], bufferString, MATAddresses[j].ToString());
                                }
                                else
                                {
                                    //logging.logDebug("bufferstring replace: " + bufferString);
                                    //parsedAssemblyList[i] = parsedAssemblyList[i].Replace(bufferString, "$" + MATAddresses[j].ToString() + "$");
                                    parsedAssemblyList[i] = functionLibrary.ReplaceFirst(parsedAssemblyList[i], bufferString, "$" + MATAddresses[j].ToString() + "$");
                                }
                                if (Program.moreVerboseDebug)
                                {
                                    logging.logDebug("Updated line " + i.ToString() + " of code with MAT data", logging.loggingSenderID.assemblyParser);
                                    logging.logDebug("new line of code: " + parsedAssemblyList[i], logging.loggingSenderID.assemblyParser); }
                                goto TopOfUpdate; // this exists b/c when update is made, it shortens the length(s) of the lines of code, thus
                                                  // we need to restart the function and reload those length values for accurate updating
                            }
                        }
                        bufferString = null;
                    }
                }
            }

            // removing all merged jalloc statements
            for (int i = 0; i < parsedAssemblyList.Count; i++)
            {
                if (parsedAssemblyList[i].Contains("jalloc"))
                {
                    int IDIndexCounter = 1;
                // uncomment this if we need to debug internally!
                //logging.logDebug("acquireblockID stringbefore: " + codeToParse.Substring(0, lengthOfStringBefore));
                TopOfFunction:
                    try
                    {
                        Convert.ToInt16(parsedAssemblyList[i].Substring(7, IDIndexCounter));
                        IDIndexCounter++;
                        goto TopOfFunction;
                    }
                    catch (FormatException) { parsedAssemblyList[i] = parsedAssemblyList[i].Remove((parsedAssemblyList[i].IndexOf("jalloc")), IDIndexCounter + 6); }
                }
            }
        }

    }
}
