﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace DRCHLLC_Core_Compiler
{
    class syntax
    {
        /*
         * This class will contain all functions and data types about syntax
         * and syntax checking. This will also include the new and redone syntax tree. 
         * 
         * Created 8/30/3016 by AGuyWhoIsBored
         * 
         */

        // the new AST will work pretty much the way the old AST did, although less bulky
        // and confusing, as we won't throw proprietary errors. 
        // Instead, we'll issue an error code, and then pass control to the logger/debugger
        // to handle the error. 
        // the builtin AST will only contain the syntax checking needed for the core language and functions.
        // other modules / extensions can be imported externally

        /*Syntax checker info
        When syntax checker throws error, either give line # or area of code that's incorrect [gives line #, give everything else] [should be done]
            Make it so that syntax checker also dumps part of code it's throwing error for (maybe a buffer for tokens it's checking through?) [this ability is passed to logging class]
        Check to see if variables are used before they are declared
        Check to see if entire line of code / thing being syntax checked is on one line
        Check to see if numbers <= 256 (at least for currently available compile targets, as these computers are 8bit)
        Add syntax check for << and >> functions(or change way those functions syntax to fit into the syntax checker) [done]
        Add syntax check in if statements so that we use ==, NOT = in if statements! [done]
        When checking syntax, add another array for each line of code to see what type of syntax it had(if it has multiple types), so that the parser
            will be able to parse better[not really needed]
        Have syntax for most variableoperator functions be very flexible
        Add syntax check for if(((functions)++) ...) // if(((functions)) ...) // if(((functions)>>) ...) // if(((functions)<<) ...) // if(((functions) !!) ...) [done]
            Currently throws 'Unrecognized check value in if statement!'
            Currently throws 'Unbalanced parentheses in data grouped function!'
        Make syntax checker more flexible when using datagrouped values to change variables
            value1 = (value1 + value3) works
            (value1 + value3) = value1 doesn't work
        Make syntax checker invalidate if statement grouping syntax if there is one operator and two parentheses groups[done]
        */

        public static Dictionary<string, string> syntaxFunctionTables = new Dictionary<string, string>();
        /* load syntax rules into this table for all functions, whether it be native language code or extensions
         * syntax for the syntax table is as following:
         *<[globalresourceID],[token1][token2]....>
         *can also induce ....[token1]/[token2][token3].... / = OR (when we want to test for more than one token in that location)
         *error handling is not yet implemented 
         */
        public static string[] tempTokensArray = null;
        public static string[] tempValuesArray = null;
        public static string[] tempLineOfCodeValueArray = null;
        public static int currentTokenIndex = -1;
        public static int errorCount = 0;

        // ONLY for ARCISS v1.0!
        public static void scanMainHLL()
        {
            logging.logDebug("Starting syntax check ...", logging.loggingSenderID.syntax, true);

            // make temp copy of data
            tempTokensArray = compiler.tokenTokensArray;
            tempValuesArray = compiler.tokenValuesArray;
            tempLineOfCodeValueArray = compiler.tokenLineOfCodeValueArray;

            // these two numbers should return 0 by the end of syntax check, or else there are unbalanced
            // data groupings in the token arrays (program)
            int parenthesesCount = 0;
            int curlyBracketCount = 0;
            int dataGroupingReturnLocation = 1;
            // location 1: if statement syntax
            // location 3: data grouping for variable setting syntax
            string currentToken = "";
            string currentTokenValue = "";
            bool ifOperatorsHitYet = false; // this variable will detect whether we've hit '==', '<', or '>' operators yet
            int dataGroupingCount = 0; // this is to detect if there are more than 3 data groupings in if statement!
                                       // e.g. if((value1 + value3) == (value3 + value4) (value21 + value6))  this should be incorrect syntax!

        // this is pretty much the syntax tree of every single possible function / command in 
        // the core ARCIS language
        // this is the AST

        TopOfSyntaxTree:

            currentToken = tempTokensArray[currentTokenIndex += 1];
            logging.logDebug("Hit top of syntax tree", logging.loggingSenderID.syntax);
            logging.logDebug("Token [" + currentTokenIndex.ToString() + "," + tempTokensArray[currentTokenIndex] + "," + tempValuesArray[currentTokenIndex] + "]", logging.loggingSenderID.syntax);

            #region EOF Check
            if (currentToken == (tokens.convertInt("identifier_eof")).ToString())
            {
                if (curlyBracketCount != 0)
                {
                    logging.logError("1.0", "Current token: [" + currentTokenIndex.ToString() + "," + tempTokensArray[currentTokenIndex] + "," + tempValuesArray[currentTokenIndex] + "]" 
                        + "\nExpected curly bracket count 0; curly bracket count " + curlyBracketCount.ToString(), logging.loggingSenderID.syntax);
                }
                else
                {
                    // syntax check finished!
                    goto EndOfFunction;
                }
            }
            #endregion

            #region Generic Variable Syntax
            // creating a new variable syntax
            if (currentToken == (tokens.convertInt("identifier_new")).ToString())
            {
                currentToken = tempTokensArray[currentTokenIndex += 1];
                if (currentToken == (tokens.convertInt("variable")).ToString())
                {
                    currentToken = tempTokensArray[currentTokenIndex += 1];
                    if (currentToken == (tokens.convertInt("operator_equals")).ToString())
                    {
                        currentToken = tempTokensArray[currentTokenIndex += 1];
                        if (currentToken == (tokens.convertInt("data_int")).ToString())
                        {
                            currentToken = tempTokensArray[currentTokenIndex += 1];
                            if (currentToken == (tokens.convertInt("end_of_line")).ToString())
                            {
                                logging.logDebug("New variable declaration syntax correct", logging.loggingSenderID.syntax);
                                goto TopOfSyntaxTree;
                            }
                            else
                            {
                                logging.logError("3.0", "Current token: " + currentTokenIndex.ToString() + "," + tempTokensArray[currentTokenIndex] + "," + tempValuesArray[currentTokenIndex] + "]", logging.loggingSenderID.syntax);
                            }
                        }
                        else
                        {
                            logging.logError("4.0", "Current token: " + currentTokenIndex.ToString() + "," + tempTokensArray[currentTokenIndex] + "," + tempValuesArray[currentTokenIndex] + "]", logging.loggingSenderID.syntax);
                        }
                    }
                    else
                    {
                        logging.logError("5.0", "Current token: " + currentTokenIndex.ToString() + "," + tempTokensArray[currentTokenIndex] + "," + tempValuesArray[currentTokenIndex] + "]", logging.loggingSenderID.syntax);
                    }
                }
                else
                {
                    logging.logError("6.0", "Current token: " + currentTokenIndex.ToString() + "," + tempTokensArray[currentTokenIndex] + "," + tempValuesArray[currentTokenIndex] + "]", logging.loggingSenderID.syntax);
                }
            }
            // changing variables
            if (currentToken == (tokens.convertInt("variable")).ToString())
            {
                currentToken = tempTokensArray[currentTokenIndex += 1];
                if (currentToken == (tokens.convertInt("operator_equals")).ToString())
                {
                    currentToken = tempTokensArray[currentTokenIndex += 1];
                    if (currentToken == (tokens.convertInt("data_int")).ToString())
                    {
                        currentToken = tempTokensArray[currentTokenIndex += 1];
                        if (currentToken == (tokens.convertInt("end_of_line")).ToString())
                        {
                            logging.logDebug("Manipulating variable with an immediate value syntax correct", logging.loggingSenderID.syntax);
                            goto TopOfSyntaxTree;
                        }
                        else
                        {
                            logging.logError("3.1", "Current token: " + currentTokenIndex.ToString() + "," + tempTokensArray[currentTokenIndex] + "," + tempValuesArray[currentTokenIndex] + "]", logging.loggingSenderID.syntax);
                        }
                    }
                    // copying variables syntax
                    else if (currentToken == (tokens.convertInt("variable")).ToString())
                    {
                        currentToken = tempTokensArray[currentTokenIndex += 1];
                        if (currentToken == (tokens.convertInt("end_of_line")).ToString())
                        {
                            // syntax is correct; jump to top of tree
                            logging.logDebug("Manipulating variable with an another variable syntax correct", logging.loggingSenderID.syntax);
                            goto TopOfSyntaxTree;
                        }
                        else
                        {
                            logging.logError("3.1", "Current token: " + currentTokenIndex.ToString() + "," + tempTokensArray[currentTokenIndex] + "," + tempValuesArray[currentTokenIndex] + "]", logging.loggingSenderID.syntax);
                        }
                    }
                    else if (currentToken == (tokens.convertInt("grouper_openparentheses")).ToString())
                    {
                        dataGroupingReturnLocation = 3;
                        goto DataGroupingSyntax;
                    }
                }
                // bitwise operations
                else if (currentToken == (tokens.convertInt("operator_rshift")).ToString()
                    || currentToken == (tokens.convertInt("operator_lshift")).ToString()
                    || currentToken == (tokens.convertInt("operator_increment")).ToString()
                    || currentToken == (tokens.convertInt("operator_decrement")).ToString()
                    || currentToken == (tokens.convertInt("operator_not")).ToString())
                {
                    currentToken = tempTokensArray[currentTokenIndex += 1];
                    if (currentToken == (tokens.convertInt("end_of_line")).ToString())
                    {
                        logging.logDebug("++ /  / !! / << / >> syntax correct", logging.loggingSenderID.syntax);
                        goto TopOfSyntaxTree;
                    }
                    else
                    {
                        logging.logError("3.1", "Current token: " + currentTokenIndex.ToString() + "," + tempTokensArray[currentTokenIndex] + "," + tempValuesArray[currentTokenIndex] + "]", logging.loggingSenderID.syntax);
                    }
                }
                else
                {
                    logging.logError("7.0", "Current token: " + currentTokenIndex.ToString() + "," + tempTokensArray[currentTokenIndex] + "," + tempValuesArray[currentTokenIndex] + "]", logging.loggingSenderID.syntax);
                }
            }
            #endregion

            #region Core functions syntax
            #region Exit Function
            // exit function
            if (currentToken == (tokens.convertInt("func_exit")).ToString())
            {
                currentToken = tempTokensArray[currentTokenIndex += 1];
                if (currentToken == (tokens.convertInt("grouper_openparentheses")).ToString())
                {
                    currentToken = tempTokensArray[currentTokenIndex += 1];
                    if (currentToken == (tokens.convertInt("grouper_closeparentheses")).ToString())
                    {
                        currentToken = tempTokensArray[currentTokenIndex += 1];
                        if (currentToken == (tokens.convertInt("end_of_line")).ToString())
                        {
                            logging.logDebug("Exit function syntax correct", logging.loggingSenderID.syntax);
                            goto TopOfSyntaxTree;
                        }
                        else
                        {
                            logging.logError("3.2", "Current token: " + currentTokenIndex.ToString() + "," + tempTokensArray[currentTokenIndex] + "," + tempValuesArray[currentTokenIndex] + "]", logging.loggingSenderID.syntax);
                        }
                    }
                    else
                    {
                        logging.logError("8.0", "Current token: " +currentTokenIndex.ToString() + "," + tempTokensArray[currentTokenIndex] + "," + tempValuesArray[currentTokenIndex] + "]", logging.loggingSenderID.syntax);
                    }
                }
                else
                {
                    logging.logError("8.0", "Current token: " + currentTokenIndex.ToString() + "," + tempTokensArray[currentTokenIndex] + "," + tempValuesArray[currentTokenIndex] + "]", logging.loggingSenderID.syntax);
                }
            }
            #endregion
            #region Flush Function
            if (currentToken == (tokens.convertInt("func_flush")).ToString())
            {
                currentToken = tempTokensArray[currentTokenIndex += 1];
                if (currentToken == (tokens.convertInt("grouper_openparentheses")).ToString())
                {
                    currentToken = tempTokensArray[currentTokenIndex += 1];
                    if (currentToken == (tokens.convertInt("data_int")).ToString())
                    {
                        // only accept 0, 1, and 2
                        currentTokenValue = tempValuesArray[currentTokenIndex];
                        if (Convert.ToInt32(currentTokenValue) >= 0 && Convert.ToInt32(currentTokenValue) <= 2)
                        {
                            currentToken = tempTokensArray[currentTokenIndex += 1];
                            if (currentToken == (tokens.convertInt("grouper_closeparentheses")).ToString())
                            {
                                currentToken = tempTokensArray[currentTokenIndex += 1];
                                if (currentToken == (tokens.convertInt("end_of_line")).ToString())
                                {
                                    logging.logDebug("Flush function syntax correct", logging.loggingSenderID.syntax);
                                    goto TopOfSyntaxTree;
                                }
                                else
                                {
                                    logging.logError("3.2", "Current token: " + currentTokenIndex.ToString() + "," + tempTokensArray[currentTokenIndex] + "," + tempValuesArray[currentTokenIndex] + "]", logging.loggingSenderID.syntax);
                                }
                            }
                            else
                            {
                                logging.logError("8.1", "Current token: " + currentTokenIndex.ToString() + "," + tempTokensArray[currentTokenIndex] + "," + tempValuesArray[currentTokenIndex] + "]", logging.loggingSenderID.syntax);
                            }
                        }
                        else
                        {
                            logging.logError("9.0", "Current token: " + currentTokenIndex.ToString() + "," + tempTokensArray[currentTokenIndex] + "," + tempValuesArray[currentTokenIndex] + "]", logging.loggingSenderID.syntax);
                        }
                    }
                    else
                    {
                        logging.logError("10.0", "Current token: " + currentTokenIndex.ToString() + "," + tempTokensArray[currentTokenIndex] + "," + tempValuesArray[currentTokenIndex] + "]", logging.loggingSenderID.syntax);
                    }
                }
                else
                {
                    logging.logError("8.1", "Current token: " + currentTokenIndex.ToString() + "," + tempTokensArray[currentTokenIndex] + "," + tempValuesArray[currentTokenIndex] + "]", logging.loggingSenderID.syntax);
                }

            }
            #endregion
            #region Output Function
            if (currentToken == (tokens.convertInt("func_output")).ToString())
            {
                currentToken = tempTokensArray[currentTokenIndex += 1];
                if (currentToken == (tokens.convertInt("grouper_openparentheses")).ToString())
                {
                    currentToken = tempTokensArray[currentTokenIndex += 1];
                    if (currentToken == (tokens.convertInt("data_int")).ToString())
                    {
                        // this argument has special requirements, so check for these
                        currentTokenValue = tempValuesArray[currentTokenIndex];
                        if (Convert.ToInt32(currentTokenValue) >= 0 && Convert.ToInt32(currentTokenValue) <= 6)
                        {
                            currentToken = tempTokensArray[currentTokenIndex += 1];
                            if (currentToken == (tokens.convertInt("variable")).ToString()
                                || currentToken == (tokens.convertInt("data_int")).ToString()) // checking for both variables AND numbers!
                            {
                                currentToken = tempTokensArray[currentTokenIndex += 1];
                                if (currentToken == (tokens.convertInt("grouper_closeparentheses")).ToString())
                                { 
                                    currentToken = tempTokensArray[currentTokenIndex += 1];
                                    if (currentToken == (tokens.convertInt("end_of_line")).ToString())
                                    {
                                        logging.logDebug("Output function syntax is correct", logging.loggingSenderID.syntax);
                                        goto TopOfSyntaxTree;
                                    }
                                    else
                                    {
                                        logging.logError("3.2", "Current token: " + currentTokenIndex.ToString() + "," + tempTokensArray[currentTokenIndex] + "," + tempValuesArray[currentTokenIndex] + "]", logging.loggingSenderID.syntax);
                                    }
                                }
                                else
                                {
                                    logging.logError("8.3", "Current token: " + currentTokenIndex.ToString() + "," + tempTokensArray[currentTokenIndex] + "," + tempValuesArray[currentTokenIndex] + "]", logging.loggingSenderID.syntax);
                                }
                            }
                            else if (currentToken == (tokens.convertInt("grouper_closeparentheses")).ToString())
                            {
                                currentToken = tempTokensArray[currentTokenIndex += 1];
                                if (currentToken == (tokens.convertInt("end_of_line")).ToString())
                                {
                                    logging.logDebug("Output function syntax is correct", logging.loggingSenderID.syntax);
                                    goto TopOfSyntaxTree;
                                }
                                else
                                {
                                    logging.logError("3.2", "Current token: " + currentTokenIndex.ToString() + "," + tempTokensArray[currentTokenIndex] + "," + tempValuesArray[currentTokenIndex] + "]", logging.loggingSenderID.syntax);
                                }
                            }
                            else
                            {
                                logging.logError("8.3", "Current token: " + currentTokenIndex.ToString() + "," + tempTokensArray[currentTokenIndex] + "," + tempValuesArray[currentTokenIndex] + "]", logging.loggingSenderID.syntax);
                            }
                        }
                        else
                        {
                            logging.logError("9.1", "Current token: " + currentTokenIndex.ToString() + "," + tempTokensArray[currentTokenIndex] + "," + tempValuesArray[currentTokenIndex] + "]", logging.loggingSenderID.syntax);
                        }
                    }
                    else
                    {
                        logging.logError("10.0", "Current token: " + currentTokenIndex.ToString() + "," + tempTokensArray[currentTokenIndex] + "," + tempValuesArray[currentTokenIndex] + "]", logging.loggingSenderID.syntax);
                    }
                }
                else
                {
                    logging.logError("8.3", "Current token: " + currentTokenIndex.ToString() + "," + tempTokensArray[currentTokenIndex] + "," + tempValuesArray[currentTokenIndex] + "]", logging.loggingSenderID.syntax);
                }
            }
            #endregion
            #region Input Function
            // only syntax of input(portID, var1) stored here. The other syntax will be
            // store in the if statements syntax, as that will be used to 
            // only check, as that syntax is a conditional check, not a function
            if (currentToken == (tokens.convertInt("func_input")).ToString())
            {
                currentToken = tempTokensArray[currentTokenIndex += 1];
                if (currentToken == (tokens.convertInt("grouper_openparentheses")).ToString())
                {
                    currentToken = tempTokensArray[currentTokenIndex += 1];
                    if (currentToken == (tokens.convertInt("data_int")).ToString())
                    {
                        // has special requirements; check those
                        currentTokenValue = tempValuesArray[currentTokenIndex];
                        if (Convert.ToInt32(currentTokenValue) >= 0 && Convert.ToInt32(currentTokenValue) <= 6)
                        {
                            currentToken = tempTokensArray[currentTokenIndex += 1];
                            if (currentToken == (tokens.convertInt("variable")).ToString())
                            {
                                currentToken = tempTokensArray[currentTokenIndex += 1];
                                if (currentToken == (tokens.convertInt("grouper_closeparentheses")).ToString())
                                {
                                    currentToken = tempTokensArray[currentTokenIndex += 1];
                                    if (currentToken == (tokens.convertInt("end_of_line")).ToString())
                                    {
                                        logging.logDebug("Input function syntax correct", logging.loggingSenderID.syntax);
                                        goto TopOfSyntaxTree;
                                    }
                                    else
                                    {
                                        logging.logError("3.2", "Current token: " + currentTokenIndex.ToString() + "," + tempTokensArray[currentTokenIndex] + "," + tempValuesArray[currentTokenIndex] + "]", logging.loggingSenderID.syntax);
                                    }
                                }
                                else
                                {
                                    logging.logError("8.2", "Current token: " + currentTokenIndex.ToString() + "," + tempTokensArray[currentTokenIndex] + "," + tempValuesArray[currentTokenIndex] + "]", logging.loggingSenderID.syntax);
                                }
                            }
                            else if (currentToken == "23")
                            {
                                logging.logError("11.0", "Current token: " + currentTokenIndex.ToString() + "," + tempTokensArray[currentTokenIndex] + "," + tempValuesArray[currentTokenIndex] + "]", logging.loggingSenderID.syntax);
                            }
                            else
                            {
                                logging.logError("9.4", "Current token: " + currentTokenIndex.ToString() + "," + tempTokensArray[currentTokenIndex] + "," + tempValuesArray[currentTokenIndex] + "]", logging.loggingSenderID.syntax);
                            }
                        }
                        else
                        {
                            logging.logError("9.6", "Current token: " + currentTokenIndex.ToString() + "," + tempTokensArray[currentTokenIndex] + "," + tempValuesArray[currentTokenIndex] + "]", logging.loggingSenderID.syntax);
                        }
                    }
                    else
                    {
                        logging.logError("9.6", "Current token: " + currentTokenIndex.ToString() + "," + tempTokensArray[currentTokenIndex] + "," + tempValuesArray[currentTokenIndex] + "]", logging.loggingSenderID.syntax);
                    }
                }
                else
                {
                    logging.logError("8.2", "Current token: " + currentTokenIndex.ToString() + "," + tempTokensArray[currentTokenIndex] + "," + tempValuesArray[currentTokenIndex] + "]", logging.loggingSenderID.syntax);
                }
            }
            #endregion
            #region Jump Function
            if (currentToken == (tokens.convertInt("func_jump")).ToString())
            {
                currentToken = tempTokensArray[currentTokenIndex += 1];
                if (currentToken == (tokens.convertInt("grouper_openparentheses")).ToString())
                {
                    currentToken = tempTokensArray[currentTokenIndex += 1];
                    if (currentToken == (tokens.convertInt("variable")).ToString())
                    {
                        currentToken = tempTokensArray[currentTokenIndex += 1];
                        if (currentToken == (tokens.convertInt("grouper_closeparentheses")).ToString())
                        {
                            currentToken = tempTokensArray[currentTokenIndex += 1];
                            if (currentToken == (tokens.convertInt("end_of_line")).ToString())
                            {
                                logging.logDebug("jump function syntax correct", logging.loggingSenderID.syntax);
                                goto TopOfSyntaxTree;
                            }
                            else
                            {
                                logging.logError("3.2", "Current token: " + currentTokenIndex.ToString() + "," + tempTokensArray[currentTokenIndex] + "," + tempValuesArray[currentTokenIndex] + "]", logging.loggingSenderID.syntax);
                            }
                        }
                        else
                        {
                            logging.logError("8.4", "Current token: " + currentTokenIndex.ToString() + "," + tempTokensArray[currentTokenIndex] + "," + tempValuesArray[currentTokenIndex] + "]", logging.loggingSenderID.syntax);
                        }
                    }
                    else
                    {
                        logging.logError("9.7", "Current token: " + currentTokenIndex.ToString() + "," + tempTokensArray[currentTokenIndex] + "," + tempValuesArray[currentTokenIndex] + "]", logging.loggingSenderID.syntax);
                    }
                }
                else
                {
                    logging.logError("8.4", "Current token: " + currentTokenIndex.ToString() + "," + tempTokensArray[currentTokenIndex] + "," + tempValuesArray[currentTokenIndex] + "]", logging.loggingSenderID.syntax);
                }
            }
            #endregion


            #endregion

            #region Imported functions syntax
            if(Convert.ToInt16(currentToken) > 41)
            {
                // we can probably optimize this entire category!!
                // save location of current token
                int tokenLocation = currentTokenIndex; 
                string fName = tokens.convertStr(Convert.ToInt16(currentToken), true);
                string importedSyntax = "";
                string successTableFragment = ""; // just for logging
                logging.logDebug("Begin syntax check on imported function", logging.loggingSenderID.syntax);
                logging.logDebug("Acquiring syntax tables", logging.loggingSenderID.syntax);
                string pulledSyntaxTableID = tokens.importedSyntaxTokenBindingTable[Array.IndexOf(tokens.importedTokenTable, tokens.convertStr(Convert.ToInt16(currentToken), true))];
                int tableCount = pulledSyntaxTableID.Count(x => x == '-');
                if(tableCount == 0)
                {
                    logging.logDebug("[1] syntax table found", logging.loggingSenderID.syntax);
                    importedSyntax = syntaxFunctionTables.First(x => x.Key == (tokens.importedSyntaxTokenBindingTable[Array.IndexOf(tokens.importedTokenTable, tokens.convertStr(Convert.ToInt16(currentToken), true))])).Value.ToString();
                    if (!checkImportedSyntax(importedSyntax)) { logging.logError("X", "Imported function syntax incorrect!", logging.loggingSenderID.syntax); }
                    else logging.logDebug("Syntax of imported function '" + fName + "' correct", logging.loggingSenderID.syntax);
                    // ***FOR NOW*** NO NEED TO INCLUDE EOL TOKEN! CHANGE THIS (below) IF THIS CHANGES!
                    currentToken = tempTokensArray[currentTokenIndex += 1];
                    goto TopOfSyntaxTree;
                }
                else
                {
                    logging.logDebug("[" + tableCount.ToString() + "] syntax tables found", logging.loggingSenderID.syntax);
                    string tableFragment = "";
                    char c;
                    int j = 0;
                    StringReader sr = new StringReader(pulledSyntaxTableID);
                    while (j != pulledSyntaxTableID.Length)
                    {
                        c = (char)sr.Read();
                        if (c == '-')
                        {
                            logging.logDebug("tableFragment is [" + tableFragment + "]", logging.loggingSenderID.syntax);
                            // process 
                            if(checkImportedSyntax(syntaxFunctionTables.First(x => x.Key == tableFragment).Value.ToString())) { successTableFragment = tableFragment; goto syntaxYes; }
                            // reset tokenindex
                            currentTokenIndex = tokenLocation;
                            tableFragment = "";
                        }
                        else { tableFragment += char.ToString(c); j++; }
                    }
                    goto syntaxNo;

                    syntaxYes:
                    logging.logDebug("Syntax of imported function '" + fName + "' correct using syntax [" + successTableFragment + "]", logging.loggingSenderID.syntax);
                    // ***FOR NOW*** NO NEED TO INCLUDE EOL TOKEN! CHANGE THIS (below) IF THIS CHANGES!
                    // DEBUG THIS
                    currentToken = tempTokensArray[currentTokenIndex += 1];
                    goto TopOfSyntaxTree;
                syntaxNo:
                    logging.logError("X", "Imported function syntax incorrect!", logging.loggingSenderID.syntax);
                    // ***FOR NOW*** NO NEED TO INCLUDE EOL TOKEN! CHANGE THIS (below) IF THIS CHANGES!
                    // DEBUG THIS
                    currentToken = tempTokensArray[currentTokenIndex += 1];
                    goto TopOfSyntaxTree;
                }
            }
            #endregion

            #region Special Unique Cases Syntax 
            // locations
            if (currentToken == (tokens.convertInt("identifier_jumploc")).ToString())
            {
                // syntax correct, jump to top of syntax tree
                logging.logDebug("Declaring location syntax correct", logging.loggingSenderID.syntax);
                goto TopOfSyntaxTree;
            }
            // else
            if (currentToken == (tokens.convertInt("identifier_else")).ToString())
            {
                // syntax correct, jump to top of syntax tree
                logging.logDebug("Else syntax correct", logging.loggingSenderID.syntax);
                goto TopOfSyntaxTree;
            }

            #endregion

            #region If Statement Syntax
            if (currentToken == (tokens.convertInt("identifier_if")).ToString())
            { 
                currentToken = tempTokensArray[currentTokenIndex += 1];
                if (currentToken == (tokens.convertInt("grouper_openparentheses")).ToString())
                {
                    // specific syntax for 'input(portID_userInput)'
                    currentToken = tempTokensArray[currentTokenIndex += 1];
                    if (currentToken == (tokens.convertInt("func_input")).ToString())
                    {
                        currentToken = tempTokensArray[currentTokenIndex += 1];
                        if (currentToken == (tokens.convertInt("grouper_openparentheses")).ToString())
                        {
                            currentToken = tempTokensArray[currentTokenIndex += 1];
                            if (currentToken == (tokens.convertInt("data_int")).ToString())
                            {
                                currentTokenValue = tempValuesArray[currentTokenIndex];
                                if (Convert.ToInt32(currentTokenValue) >= 0 && Convert.ToInt32(currentTokenValue) <= 3)
                                {
                                    currentToken = tempTokensArray[currentTokenIndex += 1];
                                    if (currentToken == (tokens.convertInt("grouper_closeparentheses")).ToString())
                                    {
                                        currentToken = tempTokensArray[currentTokenIndex += 1];
                                        if (currentToken == (tokens.convertInt("grouper_closeparentheses")).ToString())
                                        {
                                            logging.logDebug("if input syntax correct", logging.loggingSenderID.syntax);
                                            goto TopOfSyntaxTree;
                                        }
                                        else
                                        {
                                            logging.logError("12.0", "Current token: " + currentTokenIndex.ToString() + "," + tempTokensArray[currentTokenIndex] + "," + tempValuesArray[currentTokenIndex] + "]", logging.loggingSenderID.syntax);
                                        }
                                    }
                                    else
                                    {
                                        logging.logError("8.2", "Current token: " + currentTokenIndex.ToString() + "," + tempTokensArray[currentTokenIndex] + "," + tempValuesArray[currentTokenIndex] + "]", logging.loggingSenderID.syntax);
                                    }
                                }
                                else
                                {
                                    logging.logError("9.6", "Current token: " + currentTokenIndex.ToString() + "," + tempTokensArray[currentTokenIndex] + "," + tempValuesArray[currentTokenIndex] + "]", logging.loggingSenderID.syntax);
                                }
                            }
                            else
                            {
                                logging.logError("10.0", "Current token: " + currentTokenIndex.ToString() + "," + tempTokensArray[currentTokenIndex] + "," + tempValuesArray[currentTokenIndex] + "]", logging.loggingSenderID.syntax);
                            }
                        }
                        else
                        {
                            logging.logError("8.2", "Current token: " + currentTokenIndex.ToString() + "," + tempTokensArray[currentTokenIndex] + "," + tempValuesArray[currentTokenIndex] + "]", logging.loggingSenderID.syntax);
                        }
                    }
                    else
                    {
                        currentToken = tempTokensArray[currentTokenIndex -= 1];
                        dataGroupingReturnLocation = 1;
                        goto DataGroupingSyntax;
                    }
                }
                else
                {
                    logging.logError("12.0", "Current token: " + currentTokenIndex.ToString() + "," + tempTokensArray[currentTokenIndex] + "," + tempValuesArray[currentTokenIndex] + "]", logging.loggingSenderID.syntax);
                }
            }
        #endregion

            #region Data Grouping Syntax
            DataGroupingSyntax:
            // parentheses
            if (currentToken == (tokens.convertInt("grouper_openparentheses")).ToString())
            {
                parenthesesCount++;
                currentToken = tempTokensArray[currentTokenIndex += 1];
                logging.logDebug("parentheses count: " + parenthesesCount.ToString(), logging.loggingSenderID.syntax);
                // we will determine how many tokens of type 22 there are
                goto DataGroupingSyntax;
            }
            if (currentToken == (tokens.convertInt("grouper_closeparentheses")).ToString())
            {
                parenthesesCount--;
                currentToken = tempTokensArray[currentTokenIndex += 1];
                logging.logDebug("parentheses count: " + parenthesesCount.ToString(), logging.loggingSenderID.syntax);
                // counting data groupings
                if (parenthesesCount == 1) // denoting that it's the last parentheses in data group
                {
                    dataGroupingCount++;
                    logging.logDebug("data grouping count now " + dataGroupingCount.ToString(), logging.loggingSenderID.syntax);
                    // check if we have more than 3 data groupings
                    if (dataGroupingCount > 3) // subtract the last required parentheses out
                    {
                        currentTokenValue = tempValuesArray[currentTokenIndex];
                        logging.logError("12.1", "Current token: " + currentTokenValue + " assigned type " + currentToken, logging.loggingSenderID.syntax);
                    }
                }

                goto DataGroupingSyntax;
            }
            // curly brackets
            logging.logDebug("datagroupingreturnlocation: " + dataGroupingReturnLocation.ToString(), logging.loggingSenderID.syntax);
            if (currentToken == (tokens.convertInt("grouper_openbracket")).ToString() && dataGroupingReturnLocation <= 0) // need <= b/c something we can have a datagroupingreturnlocation of 1
            {
                curlyBracketCount++;
                logging.logDebug("curly bracket count: " + curlyBracketCount.ToString(), logging.loggingSenderID.syntax);
                goto TopOfSyntaxTree;
            }
            if (currentToken == (tokens.convertInt("grouper_closebracket")).ToString() && dataGroupingReturnLocation <= 0) // need <= b/c something we can have a datagroupingreturnlocation of 1
            {
                curlyBracketCount--;
                logging.logDebug("curly bracket count: " + curlyBracketCount.ToString(), logging.loggingSenderID.syntax);
                goto TopOfSyntaxTree;
            }
            // check for other operators
            else
            {
                // check for ==, <, and > operators and have it so that we can only have one of these (unless or or and statements introduced, in ARCIS Spec v1.1)
                if (currentToken == (tokens.convertInt("operator_ifequals")).ToString()
                    || currentToken == (tokens.convertInt("operator_greaterthan")).ToString()
                    || currentToken == (tokens.convertInt("operator_lessthan")).ToString())
                {
                    logging.logDebug("Hit == / < / > operators", logging.loggingSenderID.syntax);
                    if (ifOperatorsHitYet == false)
                    {
                        ifOperatorsHitYet = true;
                        currentToken = tempTokensArray[currentTokenIndex += 1];
                        if (currentToken == (tokens.convertInt("grouper_openparentheses")).ToString())
                        {
                            goto DataGroupingSyntax;
                        }
                        else if (currentToken != (tokens.convertInt("data_int")).ToString()
                            && currentToken != (tokens.convertInt("variable")).ToString())
                        {
                            currentTokenValue = tempValuesArray[currentTokenIndex];
                            logging.logError("12.2", "Current token: " + currentTokenValue + " assigned type " + currentToken, logging.loggingSenderID.syntax);
                        }
                    }
                    else if (ifOperatorsHitYet == true)
                    {
                        currentTokenValue = tempValuesArray[currentTokenIndex];
                        logging.logError("12.3", "Current token: " + currentTokenValue + " assigned type " + currentToken, logging.loggingSenderID.syntax);
                    }
                }
                if (currentToken == (tokens.convertInt("identifier_causes")).ToString() && ifOperatorsHitYet == false)
                {
                    currentToken = tempTokensArray[currentTokenIndex += 1];
                    if (currentToken == (tokens.convertInt("sc_overflow")).ToString()
                        || currentToken == (tokens.convertInt("sc_underflow")).ToString())
                    {
                        currentToken = tempTokensArray[currentTokenIndex += 1];
                        goto DataGroupingSyntax;
                    }
                    else
                    {
                        currentTokenValue = tempValuesArray[currentTokenIndex];
                        logging.logError("13.0", "Current token: " + currentTokenValue + " assigned type " + currentToken, logging.loggingSenderID.syntax);
                    }
                }
                else if (currentToken == (tokens.convertInt("identifier_causes")).ToString() && ifOperatorsHitYet == true)
                {
                    currentTokenValue = tempValuesArray[currentTokenIndex];
                    logging.logError("14.0", "Current token: " + currentTokenValue + " assigned type " + currentToken, logging.loggingSenderID.syntax);
                }
                if (parenthesesCount == 0 && dataGroupingReturnLocation != 0)
                {
                    // all data grouping syntax correct; jump to top of syntax tree
                    logging.logDebug("Data grouped parentheses syntax correct", logging.loggingSenderID.syntax);
                    // reset ifOperatorsHitYet and data groupings
                    ifOperatorsHitYet = false;
                    dataGroupingCount = 0;
                    if (dataGroupingReturnLocation == 1)
                    {
                        // subtract one to compensate for errors made [just based on the way the program works]
                        currentToken = tempTokensArray[currentTokenIndex -= 1];
                        logging.logDebug("If statement syntax correct", logging.loggingSenderID.syntax);
                        dataGroupingReturnLocation = 0;
                        goto TopOfSyntaxTree;
                    }
                    else if (dataGroupingReturnLocation == 3)
                    {
                        if (currentToken == (tokens.convertInt("end_of_line")).ToString())
                        {
                            logging.logDebug("Changing variable data using data groupings syntax correct", logging.loggingSenderID.syntax);
                            dataGroupingReturnLocation = 0;
                            goto TopOfSyntaxTree;
                        }
                        else
                        {
                            currentTokenValue = tempValuesArray[currentTokenIndex];
                            logging.logError("3.3", "Current token: " + currentTokenValue + " assigned type " + currentToken, logging.loggingSenderID.syntax);
                        }
                    }
                }
                // conditions
                else if (currentToken == (tokens.convertInt("variable")).ToString())
                {
                    goto IfConditionsSyntax;
                }
                else if (Convert.ToInt32(currentToken) >= (tokens.convertInt("operator_plus"))
                    && (Convert.ToInt32(currentToken) <= (tokens.convertInt("operator_not"))
                    && (Convert.ToInt32(currentToken) != (tokens.convertInt("operator_ifequals")))))
                {
                    goto IfConditionsOperator;
                }
                else
                {
                    currentTokenValue = tempValuesArray[currentTokenIndex];
                    logging.logError("15.0", "Current token: " + currentTokenValue + " assigned type " + currentToken, logging.loggingSenderID.syntax);
                }
            }


        #endregion

            #region If Conditions Syntax
            IfConditionsSyntax:
            // variable 1 first
            // need to add syntax for: <<, >>, ++, 
            if (currentToken == (tokens.convertInt("variable")).ToString())
            {
                currentToken = tempTokensArray[currentTokenIndex += 1];
                goto IfConditionsOperator;
            }
        // any operator EXCEPT the = sign! < implement this! [implemented]
        IfConditionsOperator:
            if (Convert.ToInt32(currentToken) >= (tokens.convertInt("operator_plus"))
                && (Convert.ToInt32(currentToken) <= (tokens.convertInt("operator_not")))
                && currentToken != ((tokens.convertInt("operator_equals")).ToString()))
            {
                // for !!, ++, , <<, >>
                if (currentToken == (tokens.convertInt("operator_rshift")).ToString()
                    || currentToken == (tokens.convertInt("operator_lshift")).ToString()
                    || currentToken == (tokens.convertInt("operator_increment")).ToString()
                    || currentToken == (tokens.convertInt("operator_decrement")).ToString()
                    || currentToken == (tokens.convertInt("operator_not")).ToString())
                {
                    currentToken = tempTokensArray[currentTokenIndex += 1];
                    goto DataGroupingSyntax;
                }
                currentToken = tempTokensArray[currentTokenIndex += 1];
                if (currentToken == (tokens.convertInt("variable")).ToString()
                    || currentToken == (tokens.convertInt("data_int")).ToString())
                {
                    currentToken = tempTokensArray[currentTokenIndex += 1];
                    goto IfConditionsOperator;
                }
                else if (currentToken == (tokens.convertInt("grouper_openparentheses")).ToString())
                {
                    goto DataGroupingSyntax;
                }
                else
                {
                    currentTokenValue = tempValuesArray[currentTokenIndex];
                    logging.logError("16.0", "Current token: " + currentTokenValue + " assigned type " + currentToken, logging.loggingSenderID.syntax);

                }
            }
            else if (currentToken == (tokens.convertInt("grouper_closeparentheses").ToString()))
            {
                goto DataGroupingSyntax;
            }
            else
            {
                currentTokenValue = tempValuesArray[currentTokenIndex];
                logging.logError("17.0", "Current token: " + currentTokenValue + " assigned type " + currentToken, logging.loggingSenderID.syntax);
                // internal note  add functionality for '18.0' error code as well
            }
            #endregion

            EndOfFunction:
            // check to make sure all jump(var) commands have valid defined [var]
            // highly unoptimized; however, can fix in the future
            logging.logDebug("Checking for invalid definitions ...", logging.loggingSenderID.syntax);
            for (int i = 0; i < tempTokensArray.Length - 1; i++)
            {
                string varName = "";
                if (tempTokensArray[i] == tokens.convertInt("func_jump").ToString())
                {
                    i += 2; // skip parentheses
                    varName = tempValuesArray[i];
                    logging.logDebug("Checking definition: [" + varName + "]", logging.loggingSenderID.syntax);
                    bool chkd = false;
                    for (int j = 0; j < tempTokensArray.Length - 1; j++)
                    {
                        string definedVarName = "";
                        if (tempTokensArray[j] == tokens.convertInt("identifier_jumploc").ToString())
                        {
                            definedVarName = tempValuesArray[j].Replace("#", "");
                            if (varName == definedVarName) chkd = true;
                            }
                        }
                    if (chkd == false)
                    {
                        currentTokenIndex = i;
                        currentToken = tempTokensArray[currentTokenIndex += 1];
                        currentTokenValue = tempValuesArray[currentTokenIndex];
                        logging.logError("20.0", "Definition [" + varName + "] not defined in program!", logging.loggingSenderID.syntax);
                        break;
                    }
                }
            }
            // we use logwarning to also push to main output
            if (errorCount == 0) { logging.logDebug("Syntax check completed; no syntax errors found", logging.loggingSenderID.syntax, true); }
            else 
            {
                logging.logWarning("Syntax check completed; " + errorCount.ToString() + " errors found. See the log for more details.", logging.loggingSenderID.syntax);
                functionLibrary.writeDebugLogOut();
                Console.ReadLine();
                Environment.Exit(0);
            }
        }

        static bool checkImportedSyntax(string syntaxTable)
        {
            // make temp copy of data
            string[] tempTokensArray = compiler.tokenTokensArray;
            string[] tempValuesArray = compiler.tokenValuesArray;
            string[] tempLineOfCodeValueArray = compiler.tokenLineOfCodeValueArray;
            string currentToken = "";
            // rewind to ensure accurate parsing
            // fix 1/2 for importedSyntax on first token
            try { currentToken = tempTokensArray[currentTokenIndex -= 1]; }
            catch (Exception) { }
            logging.logDebug("bound syntax is [" + syntaxTable + "]", logging.loggingSenderID.syntax);
            // parse importedSyntax
            bool syntaxCorrect = true;
            string syntaxFragment = "";
            char c;
            int i = 0;
            StringReader sr = new StringReader(syntaxTable);
            while (i != syntaxTable.Length)
            {
                c = (char)sr.Read();
                if (c == '-')
                {
                    logging.logDebug("syntaxFragment is [" + syntaxFragment + "]", logging.loggingSenderID.syntax);
                    if (syntaxFragment.Contains('/'))
                    {
                        // basically repeat what we just did, instead use / instead of - 
                        if(currentTokenIndex != 0) currentToken = tempTokensArray[currentTokenIndex += 1]; // fix 2/2 for importedSyntax on first token
                        string syntaxFragmentMicro = "";
                        char c1;
                        int i1 = 0;
                        bool foundToken = false;
                        StringReader sr1 = new StringReader(syntaxFragment);
                        while (i1 != syntaxFragment.Length)
                        {
                            c1 = (char)sr1.Read();
                            if (c1 == '/')
                            {
                                logging.logDebug("syntaxFragmentMicro is [" + syntaxFragmentMicro + "]", logging.loggingSenderID.syntax);
                                if (currentToken == syntaxFragmentMicro)
                                {
                                    logging.logDebug("currenttoken " + currentToken + " matches syntaxfragmentmicro " + syntaxFragmentMicro, logging.loggingSenderID.syntax);
                                    foundToken = true;
                                    syntaxFragment = "";
                                    break;
                                }
                                syntaxFragmentMicro = "";
                            }
                            else { syntaxFragmentMicro += char.ToString(c1); i1++; }
                        }
                        if (foundToken != true) syntaxCorrect = false;
                    }
                    else
                    {
                        currentToken = tempTokensArray[currentTokenIndex += 1];
                        if (currentToken == syntaxFragment)
                        {
                            logging.logDebug("currenttoken " + currentToken + " matches syntaxFragment " + syntaxFragment, logging.loggingSenderID.syntax);
                        }
                        else { syntaxCorrect = false; }
                    }
                    syntaxFragment = "";
                }
                else { syntaxFragment += char.ToString(c); i++; }
            }
            if (syntaxCorrect != true) { return false; }
            else return true;
        }

        // will also put resource syntax checker here
        public static void resourceSyntaxCheck()
        {
            // work on having multiple functions go through syntax check

            //logging.logDebug("Starting syntax check on resouce " + importer.resourceIDDict[importer.resourceIDCounter], logging.loggingSenderID.syntax);
            // finding how many functions there are
            //int functionCount = 0;


            //TopOfResuorceSyntaxCheck:

            int currentTokenIndex = 1;
            string currentTokenValue = "";
            string currentToken = "";
            string functionName = "";

            currentTokenIndex++;
            currentToken = importer.resourceTTokens[currentTokenIndex];
            currentTokenValue = importer.resourceTValues[currentTokenIndex];
            logging.logDebug("Hit top of resource syntax tree", logging.loggingSenderID.syntax);
            //logging.logDebug("CTARI value: " + currentTokenIndex.ToString(), logging.loggingSenderID.syntax);
            //logging.logDebug("Token data: " + currentTokenValue + " assigned type " + currentToken, logging.loggingSenderID.syntax);

            // will check to make sure all of the required tokens are present

            if (currentToken == tokens.types[34])
            {
                currentTokenIndex++; currentToken = importer.resourceTTokens[currentTokenIndex];
                if (currentToken == tokens.types[4])
                {
                    currentTokenIndex++;
                    currentToken = importer.resourceTTokens[currentTokenIndex];
                    currentTokenValue = importer.resourceTValues[currentTokenIndex - 1];
                    functionName = currentTokenValue;
                    if (currentToken == tokens.types[23])
                    {
                        currentTokenIndex++; currentToken = importer.resourceTTokens[currentTokenIndex];
                    TopOfArgumentSyntaxCheck:
                        // will loop to check for arguments
                        if (currentToken == tokens.types[2] || currentToken == tokens.types[3])
                        {
                            currentTokenIndex++; currentToken = importer.resourceTTokens[currentTokenIndex];
                            if (currentToken == tokens.types[4])
                            {
                                currentTokenIndex++; currentToken = importer.resourceTTokens[currentTokenIndex];
                                goto TopOfArgumentSyntaxCheck;
                            }
                            else { /* throw error*/ }
                        }
                        else if (currentToken == tokens.types[24])
                        {
                            // start of line 2
                            currentTokenIndex++; currentToken = importer.resourceTTokens[currentTokenIndex];
                            if (currentToken == tokens.types[36]
                                || currentToken == tokens.types[37]
                                || currentToken == tokens.types[38])
                            {
                                // create new mainUI tokenarrays with values from code in function
                                logging.logDebug("Initial resource syntax check for function " + functionName + " passed...", logging.loggingSenderID.syntax);
                                logging.logDebug("Beginning syntax check on function code ...", logging.loggingSenderID.syntax);
                                for (int i = currentTokenIndex + 1; i < importer.resourceTTokens.Length; i++) // i = CTARI + 1 circumvents a bug that would skip adding some of the required tokens for the regular syntax checker
                                {
                                    currentToken = importer.resourceTTokens[i];
                                    if (currentToken != tokens.types[39])
                                    {
                                        functionLibrary.AddToTokenArrays(importer.resourceTTokens[i], importer.resourceTValues[i], importer.resourceTLineOfCodeValue[i], 2);
                                    }
                                    else if (currentToken == tokens.types[39]) break; // this makes it so that only one function is syntax checked at a time
                                }

                                functionLibrary.AddToTokenArrays("20", "$eof$", "1", 2);
                                //checkSyntaxOfResouce = true;
                                //check(1); // if successful, will continue
                                //checkSyntaxOfResouce = false;



                            }
                        }
                        else { /*throw error*/ }
                    }
                    else { /*throw error*/ }
                }
                else { /*throw error*/ }
            }
            else { /*throw error*/ }

        }
    }
}
