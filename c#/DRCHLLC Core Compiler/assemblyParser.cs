﻿using System;
using System.Collections.Generic;
using System.IO;

namespace DRCHLLC_Core_Compiler
{
    class assemblyParser
    {
        /*
         * This class will contain all functions and information needed for parsing code from the
         * ARCIS Spec to ARCISS assembly. 
         * 
         * Created 9/5/2016 by AGuyWhoIsBored
         * 
         */

        public static void parseStage1()
        {
            // stage 1 will
            //      -convert most of the functions and tokens into lines of assembly code
            //      -generate writeback statements
            //          writeback statements will be generated for:
            //                -
            // stage 1 will also add compiler notes into assembly output for
            //      -memory allocation requests
            //      -all branches, conditional or nonconditional
            //      -all code blocks - i.e ( ) / { }
            // these will be taken care of in later stages

            // instructions that need writeback instructions
            // add (if funcCount != 1; if it does, check for where the function group came from (if / var set) and set mem write to that
            // sub (if funcCount != 1; if it does, check for where the function group came from (if / var set) and set mem write to that
            // bsr, bsl, ad1, sb1, 

            // in the near future, move compiler requests / information to separate buffer to prevent confusing with user vars

            // rewriting
            string parsedAssemblyOutput = null;
            string currentTokenID = "";
            string currentTokenValue = "";
            int currentTokenIndex = -1; // 0-based index
            int lineOfCodeGeneratedAssembly = 0;
            int jumpLocationAfterFunctionGenerate = 0;
            //  index of jump locations:
            //      1 - if statement
            //      2 - if statement pt. 2 (for ==, <, >)
            int functionCount = 0;
            int curlyBracketCount = 0;
            int tempMemLocCount = 0; // mainly used for if statements, when we have something like if((value1 + value2) < (value3 + value4))
                                     // this value shouldn't exceed 2
            int ifOperatorType = 0; // consider removing
            // index of if operator types:
            //      1 - <
            //      2 - >
            //      3 - ==
            string modifyVarValue = null; // varname that was being modified in function generator

        // MEMW SYNTAX: memw [location] [value] 

        // will use parse tree, which is very similar to syntax tree
        TopOfParseTree:

            currentTokenIndex++;

            #region fix
            //fix this bug
            //EOF check 2 (different than other one); this is here so that we don't go outside array bounds!
            //for some reason, when compiling using RC3.0, we go outside of array bounds! This is why this is here.
            try
            {
                currentTokenID = compiler.tokenTokensArray[currentTokenIndex];
                currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
            }
            catch
            {
                goto EndOfParsingStage1;
            }
            #endregion

            lineOfCodeGeneratedAssembly++;
            ifOperatorType = 0;
            modifyVarValue = null;
            tempMemLocCount = 0;
            jumpLocationAfterFunctionGenerate = 0;
            logging.logDebug("Hit top of parse tree: ", logging.loggingSenderID.assemblyParser);
            logging.logDebug("Parser info: " + "[ln:" + lineOfCodeGeneratedAssembly.ToString() + ",cti:" + currentTokenIndex.ToString() + ",ctid:" + currentTokenID + ",ctv:" + currentTokenValue + "]", logging.loggingSenderID.assemblyParser);

            // EOF check
            #region End of File Check
            if (currentTokenID == "21")
            {
                goto EndOfParsingStage1;
            }
            #endregion

            #region fix
            //this is needed b / c in some functions, when we hit the top of the parse tree we get a "(" or ")"; so, we back it up one token and check again
            #region Parentheses Check
            if (currentTokenID == "23" || currentTokenID == "24")
            {
                currentTokenIndex -= 2;
                lineOfCodeGeneratedAssembly -= 2;
                goto TopOfParseTree;
            }
            #endregion
            #endregion

            #region Compiler Requests & Info For Compiler
            if (currentTokenID == "20")
            {
                // gather data from additional tokens and add a compiler memory allocation request
                parsedAssemblyOutput += "%compiler%: malloc";
                currentTokenIndex++;
                currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                parsedAssemblyOutput += " " + currentTokenValue;
                currentTokenIndex += 2; // skip the operator (=) to get straight to the value of the variable
                currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                parsedAssemblyOutput += " " + currentTokenValue;
                currentTokenIndex++; // to get to the semicolon; when we hit top of parse tree will read next token
                logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: [compiler request]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                parsedAssemblyOutput = null;
                goto TopOfParseTree;
            }
            if (currentTokenID == "19")
            {
                // add a compiler code jump location request
                parsedAssemblyOutput += "%compiler%: jalloc";
                // get variable from location
                currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                currentTokenValue = currentTokenValue.Replace('#', ' ');
                currentTokenValue = functionLibrary.RemoveWhitespace(currentTokenValue);
                parsedAssemblyOutput += " " + currentTokenValue;
                logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: [compiler request]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                parsedAssemblyOutput = null;
                goto TopOfParseTree;
            }
            if (currentTokenID == "25")
            {
                // add a compiler code 'block' marker (start block)
                curlyBracketCount++;
                parsedAssemblyOutput += "%compiler%: sblock" + curlyBracketCount.ToString();
                logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: [compiler note]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                parsedAssemblyOutput = null;
                goto TopOfParseTree;
            }
            if (currentTokenID == "26")
            {
                // add a compiler code 'block' marker (terminate block)
                parsedAssemblyOutput += "%compiler%: tblock" + curlyBracketCount.ToString();
                curlyBracketCount--;
                logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: [compiler note]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                parsedAssemblyOutput = null;
                goto TopOfParseTree;
            }
            #endregion

            #region If Statement Parsing
            if (currentTokenID == "16")
            {
                // recursively loop and generate line of code for each function inside if statement if there are any
                // skip first required parentheses
                currentTokenIndex += 2;
                currentTokenID = compiler.tokenTokensArray[currentTokenIndex];
                currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                // make sure we actually need the function generator before calling it
                if (currentTokenID == "23")
                {
                    // start function generator
                    // generate function BEFORE if/test code
                    logging.logDebug("Parser: function generator started", logging.loggingSenderID.assemblyParser);
                    jumpLocationAfterFunctionGenerate = 1;
                    goto FunctionGenerator;
                }
                // if we don't need it, check for all testable if conditions that don't need a causes statement (input(portID), <, >, ==)
                else
                {
                    if (currentTokenID == "30")
                    {
                        // gather additional data and build assembly
                        parsedAssemblyOutput += "test";
                        // skip required open parentheses
                        currentTokenIndex += 2;
                        currentTokenID = compiler.tokenTokensArray[currentTokenIndex];
                        currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                        parsedAssemblyOutput += " ui" + currentTokenValue;
                        // tjump and fjump parts will be added in stage 2
                        logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                        compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                        parsedAssemblyOutput = null;
                        // skip to closed parentheses token in token list so that next line will be parsed
                        currentTokenIndex += 2;
                        goto TopOfParseTree;
                    }
                    if (currentTokenID == "4")
                    {
                        // we will include <, >, and == checks here
                        currentTokenIndex++;
                        currentTokenID = compiler.tokenTokensArray[currentTokenIndex];
                        if (currentTokenID == "12")
                        {
                            // gather additional data and build assembly
                            parsedAssemblyOutput += "test ==";
                            currentTokenIndex--;
                            currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                            parsedAssemblyOutput += " " + currentTokenValue;
                            currentTokenIndex += 2;
                            currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                            parsedAssemblyOutput += " " + currentTokenValue;
                            // tjump and fjump parts will be added in stage 2
                            logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                            compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                            parsedAssemblyOutput = null;
                            // skip to end parentheses token so that next line will be parsed
                            currentTokenIndex++;
                            goto TopOfParseTree;
                        }
                        else if (currentTokenID == "9")
                        {
                            // gather additional data and build assembly
                            parsedAssemblyOutput += "test <";
                            currentTokenIndex--;
                            currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                            parsedAssemblyOutput += " " + currentTokenValue;
                            currentTokenIndex += 2;
                            currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                            parsedAssemblyOutput += " " + currentTokenValue;
                            // tjump and fjump parts will be added in stage 2
                            logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                            compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                            parsedAssemblyOutput = null;
                            // skip to end parentheses token so that next line will be parsed
                            currentTokenIndex++;
                            goto TopOfParseTree;
                        }
                        else if (currentTokenID == "8")
                        {
                            // gather additional data and build assembly
                            parsedAssemblyOutput += "test >";
                            currentTokenIndex--;
                            currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                            parsedAssemblyOutput += " " + currentTokenValue;
                            currentTokenIndex += 2;
                            currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                            parsedAssemblyOutput += " " + currentTokenValue;
                            // tjump and fjump parts will be added in stage 2
                            logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                            compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                            parsedAssemblyOutput = null;
                            // skip to end parentheses token so that next line will be parsed
                            currentTokenIndex++;
                            goto TopOfParseTree;
                        }
                    }
                }
            }
            #endregion

            #region Functions
            #region Exit Function
            if (currentTokenID == "27")
            {
                // build function
                parsedAssemblyOutput += "exit";
                logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                parsedAssemblyOutput = null;
                // need to advance three tokens forward
                currentTokenIndex += 3;
                goto TopOfParseTree;
            }
            #endregion
            #region Else 'Function'
            if (currentTokenID == "17")
            {
                parsedAssemblyOutput += "%compiler%: else";
                logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                parsedAssemblyOutput = null;
                goto TopOfParseTree;
            }
            #endregion
            #region Flush Function
            if (currentTokenID == "28")
            {
                // build assembly
                parsedAssemblyOutput += "flush";
                currentTokenIndex += 2;
                currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                parsedAssemblyOutput += " " + currentTokenValue;
                currentTokenIndex += 2;
                logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                parsedAssemblyOutput = null;
                goto TopOfParseTree;
            }
            #endregion
            #region Output Function
            if (currentTokenID == "29")
            {
                // build function
                parsedAssemblyOutput += "output";
                currentTokenIndex += 2; // skip required parentheses
                currentTokenID = compiler.tokenTokensArray[currentTokenIndex];
                currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                // syntax one
                if (currentTokenID == "2")
                {
                    // denoting syntax

                    // we will check for which kind of syntax we're dealing with here; makes it easier for machine code generator
                    currentTokenIndex++;
                    currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                    if (currentTokenValue != ")") { parsedAssemblyOutput += "1"; }
                    else { parsedAssemblyOutput += "2"; }
                    currentTokenIndex--;
                    currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];

                    parsedAssemblyOutput += " " + currentTokenValue;
                    currentTokenIndex++;
                    currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                    if (currentTokenValue != ")") { parsedAssemblyOutput += " " + currentTokenValue; }
                    // ^^ this will make it so that when we're outputting a computer indicator, the parsed line won't have a ')'
                    logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                    compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                    parsedAssemblyOutput = null;
                    // advance two tokens
                    currentTokenIndex += 2;
                    goto TopOfParseTree;
                }
            }
            #endregion
            #region Input Function
            if (currentTokenID == "30")
            {
                parsedAssemblyOutput += "input";
                currentTokenIndex += 2;
                currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                parsedAssemblyOutput += " " + currentTokenValue;
                logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);

                currentTokenIndex++;
                currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                parsedAssemblyOutput += " " + currentTokenValue;
                logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                parsedAssemblyOutput = null;

                currentTokenIndex += 2;
                goto TopOfParseTree;
            }
            #endregion
            #region Jump Function
            if (currentTokenID == "31")
            {
                parsedAssemblyOutput += "jump";
                currentTokenIndex += 2;
                currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                parsedAssemblyOutput += " " + currentTokenValue;
                logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                parsedAssemblyOutput = null;
                currentTokenIndex += 2;
                goto TopOfParseTree;
            }
            #endregion
            #endregion

            // RC40E extension functions
            #region RC40E Extension Functions
            #region GPU Encode Function
            if (currentTokenID == tokens.convertInt("GPU_encode").ToString() && importer.ext_RC40E)
            {
                parsedAssemblyOutput += "gpuencode";
                currentTokenIndex += 2;
                currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                parsedAssemblyOutput += " " + currentTokenValue;
                currentTokenIndex++;
                currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                parsedAssemblyOutput += " " + currentTokenValue;
                logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                parsedAssemblyOutput = null;
                lineOfCodeGeneratedAssembly++;

                parsedAssemblyOutput += "memw";
                currentTokenIndex++;
                currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                parsedAssemblyOutput += " " + currentTokenValue;
                parsedAssemblyOutput += " ~temp~";
                logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                parsedAssemblyOutput = null;

                currentTokenIndex += 2;
                goto TopOfParseTree;
            }
            #endregion
            #region GPU Clear Function
            if (currentTokenID == tokens.convertInt("GPU_clear").ToString() && importer.ext_RC40E)
            {
                parsedAssemblyOutput += "gpuclr";
                currentTokenIndex += 3;
                logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                parsedAssemblyOutput = null;
                goto TopOfParseTree;
            }
            #endregion
            #region GPU Draw Point Function
            if (currentTokenID == tokens.convertInt("GPU_drawPoint").ToString() && importer.ext_RC40E)
            {
                parsedAssemblyOutput += "gpudraw";
                currentTokenIndex += 2;
                currentTokenID = compiler.tokenTokensArray[currentTokenIndex];
                currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                if (currentTokenID == "4")
                {
                    parsedAssemblyOutput += " " + currentTokenValue;
                }
                if (currentTokenID == "2")
                {
                    parsedAssemblyOutput += " " + currentTokenValue;
                    currentTokenIndex++;
                    currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                    parsedAssemblyOutput += " " + currentTokenValue;
                }
                currentTokenIndex += 2;
                logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                parsedAssemblyOutput = null;
                goto TopOfParseTree;
            }
            #endregion          
            #region GPU Erase Point Function
            if (currentTokenID == tokens.convertInt("GPU_erasePoint").ToString() && importer.ext_RC40E)
            {
                parsedAssemblyOutput += "gpuerase";
                currentTokenIndex += 2;
                currentTokenID = compiler.tokenTokensArray[currentTokenIndex];
                currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                if (currentTokenID == "4")
                {
                    parsedAssemblyOutput += " " + currentTokenValue;
                }
                if (currentTokenID == "2")
                {
                    parsedAssemblyOutput += " " + currentTokenValue;
                    currentTokenIndex++;
                    currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                    parsedAssemblyOutput += " " + currentTokenValue;
                }
                currentTokenIndex += 2;
                logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                parsedAssemblyOutput = null;
                goto TopOfParseTree;
            }
            #endregion
            #region GPU Granular Update Function
            if (currentTokenID == tokens.convertInt("GPU_granularUpdate").ToString() && importer.ext_RC40E)
            {
                parsedAssemblyOutput += "gpugu";
                currentTokenIndex += 2;
                currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                parsedAssemblyOutput += " " + currentTokenValue;
                currentTokenIndex++;
                currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                parsedAssemblyOutput += " " + currentTokenValue;
                currentTokenIndex++;
                currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                parsedAssemblyOutput += " " + currentTokenValue;
                currentTokenIndex++;
                currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                parsedAssemblyOutput += " " + currentTokenValue;
                currentTokenIndex += 2;
                logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                parsedAssemblyOutput = null;
                goto TopOfParseTree;
            }
            #endregion
            #region Update Speed Function
            if (currentTokenID == tokens.convertInt("RC40E_updateSpeed").ToString() & importer.ext_RC40E)
            {
                parsedAssemblyOutput += "updspd";
                currentTokenIndex += 2;
                currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                parsedAssemblyOutput += " " + currentTokenValue;
                logging.logDebug("[" + lineOfCodeGeneratedAssembly + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                parsedAssemblyOutput = null;
                currentTokenIndex += 2;
                goto TopOfParseTree;
            }
            #endregion
            #endregion

            #region Variable Parsing
            // this section will contain the following parse checks:
            //      changing variables using all operators (=, >>, <<, ++, --, !!, var1 = var2)
            if (currentTokenID == "4")
            {
                currentTokenIndex++;
                currentTokenID = compiler.tokenTokensArray[currentTokenIndex];
                currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                if (currentTokenID == "7")
                {
                    // MEMW SYNTAX: memw [location] [value]
                    // set operator to memw [mem write] and gather data
                    parsedAssemblyOutput += "memw";
                    currentTokenIndex--;
                    currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                    parsedAssemblyOutput += " " + currentTokenValue;
                    currentTokenIndex += 2;
                    currentTokenID = compiler.tokenTokensArray[currentTokenIndex];
                    // check for # and variable
                    if (currentTokenID == "2")
                    {
                        currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                        parsedAssemblyOutput += " " + currentTokenValue;
                        logging.logDebug("Parser: [" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                        compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                        parsedAssemblyOutput = null;
                        currentTokenIndex++;
                        goto TopOfParseTree;
                    }
                    else if (currentTokenID == "23")
                    {
                        currentTokenIndex -= 2;
                        modifyVarValue = compiler.tokenValuesArray[currentTokenIndex];
                        currentTokenIndex += 2;
                        parsedAssemblyOutput = null;
                        goto FunctionGenerator;
                    }
                    // for copying values
                    else if (currentTokenID == "4")
                    {
                        // reset parsedassemblyoutput
                        parsedAssemblyOutput = null;
                        // memr, then memw
                        parsedAssemblyOutput += "memw";
                        currentTokenIndex -= 2;
                        currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                        parsedAssemblyOutput += " " + currentTokenValue;

                        parsedAssemblyOutput += " memr";
                        currentTokenIndex += 2;
                        currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                        parsedAssemblyOutput += " " + currentTokenValue;

                        logging.logDebug("Parser: [" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                        compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                        parsedAssemblyOutput = null;
                        currentTokenIndex++;
                        goto TopOfParseTree;
                    }
                }
                else if (currentTokenID == "10")
                {
                    parsedAssemblyOutput += "bsr";
                    currentTokenIndex--;
                    currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                    parsedAssemblyOutput += " " + currentTokenValue;
                    logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                    compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                    parsedAssemblyOutput = null;
                    lineOfCodeGeneratedAssembly++;

                    parsedAssemblyOutput += "memw " + currentTokenValue;
                    parsedAssemblyOutput += " ~temp~";
                    logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                    compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                    parsedAssemblyOutput = null;
                    currentTokenIndex += 2;
                    goto TopOfParseTree;
                }
                else if (currentTokenID == "11")
                {
                    parsedAssemblyOutput += "bsl";
                    currentTokenIndex--;
                    currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                    parsedAssemblyOutput += " " + currentTokenValue;
                    logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                    compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                    parsedAssemblyOutput = null;
                    lineOfCodeGeneratedAssembly++;

                    parsedAssemblyOutput += "memw " + currentTokenValue;
                    parsedAssemblyOutput += " ~temp~";
                    logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                    compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                    parsedAssemblyOutput = null;
                    currentTokenIndex += 2;
                    goto TopOfParseTree;
                }
                else if (currentTokenID == "13")
                {

                    parsedAssemblyOutput += "ad1";
                    currentTokenIndex--;
                    currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                    parsedAssemblyOutput += " " + currentTokenValue;
                    logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                    compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                    parsedAssemblyOutput = null;
                    lineOfCodeGeneratedAssembly++;

                    parsedAssemblyOutput += "memw " + currentTokenValue;
                    parsedAssemblyOutput += " ~temp~";
                    logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                    compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                    parsedAssemblyOutput = null;
                    currentTokenIndex += 2;
                    goto TopOfParseTree;
                }
                else if (currentTokenID == "14")
                {

                    parsedAssemblyOutput += "sb1";
                    currentTokenIndex--;
                    currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                    parsedAssemblyOutput += " " + currentTokenValue;
                    logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                    compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                    parsedAssemblyOutput = null;
                    lineOfCodeGeneratedAssembly++;

                    parsedAssemblyOutput += "memw " + currentTokenValue;
                    parsedAssemblyOutput += " ~temp~";
                    logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                    compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                    parsedAssemblyOutput = null;
                    currentTokenIndex += 2;
                    goto TopOfParseTree;
                }
                else if (currentTokenID == "15")
                {
                    parsedAssemblyOutput += "not";
                    currentTokenIndex--;
                    currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                    parsedAssemblyOutput += " " + currentTokenValue;
                    logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                    compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                    parsedAssemblyOutput = null;
                    lineOfCodeGeneratedAssembly++;

                    parsedAssemblyOutput += "memw " + currentTokenValue;
                    parsedAssemblyOutput += " ~temp~";
                    logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                    compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                    parsedAssemblyOutput = null;
                    currentTokenIndex += 2;
                    goto TopOfParseTree;
                }
            }
        #endregion


        // this needs to be at very end of tree b/c there are no checks to stop the generator from being enabled
        #region Function Generator and Parser

        FunctionGenerator:
            if (currentTokenID == "23")
            {
                functionCount++;
                currentTokenIndex++;
                currentTokenID = compiler.tokenTokensArray[currentTokenIndex];
                currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                // repeat recursively until not found anymore
                goto FunctionGenerator;
            }
            else
            {
                // actual function generator here
                /*
                    This block of code works, but is very confusing and possibly cluttered. Try to optimize / make easier to understand
                    in the future?
                */

                logging.logDebug("Parser: found " + functionCount.ToString() + " function(s) to generate and parse", logging.loggingSenderID.assemblyParser);
                // loop from innermost parentheses / function and work our way out
                tempMemLocCount++; // want to increment this for all groups of functions - see comment on this var
                string[] buffertokenTokensArray = compiler.tokenTokensArray;
                string[] buffertokenValuesArray = compiler.tokenValuesArray;
                string tokensInFunctionCurrentTokenID = currentTokenID;
                string tokensInFunctionCurrentTokenValue = currentTokenValue;
                int functionCountBuffer = functionCount; // this is to check for the last function and if not in an if statement, set the var value
                for (int i = 0; i < functionCount; i++)
                {
                    // loop through until first close parentheses found
                    // each token scanned before will be put into a list and removed from a buffered value of the token arrays
                    // this list will then be parsed as one line of code
                    // set starting token reading location to one before the one we started with and repeat until there are no more functions
                    List<string> tokensInFunction = new List<string>();
                    List<string> tokenValuesInFunction = new List<string>();
                    int tokensInFunctionCurrentTokenIndex = -1; // basically equals zero
                    tokensInFunction.Add(tokensInFunctionCurrentTokenID);
                    tokenValuesInFunction.Add(tokensInFunctionCurrentTokenValue);
                    logging.logDebug("read(pre): " + currentTokenIndex.ToString() + "," + tokensInFunctionCurrentTokenID + "," + tokensInFunctionCurrentTokenValue, logging.loggingSenderID.assemblyParser);
                    functionLibrary.RemoveElementFromArray(buffertokenTokensArray, currentTokenIndex);
                    functionLibrary.RemoveElementFromArray(buffertokenValuesArray, currentTokenIndex);
                    while (tokensInFunctionCurrentTokenID != "24")
                    {
                        currentTokenIndex++;
                        tokensInFunctionCurrentTokenID = buffertokenTokensArray[currentTokenIndex];
                        tokensInFunctionCurrentTokenValue = buffertokenValuesArray[currentTokenIndex];
                        tokensInFunction.Add(tokensInFunctionCurrentTokenID);
                        tokenValuesInFunction.Add(tokensInFunctionCurrentTokenValue);
                        logging.logDebug("read: " + currentTokenIndex.ToString() + "," + tokensInFunctionCurrentTokenID + "," + tokensInFunctionCurrentTokenValue, logging.loggingSenderID.assemblyParser);
                    }
                    // this part of code will be reached when we've hit a close parentheses
                    // remove 6_2 / 24 token from list
                    tokensInFunction.Remove("24");
                    logging.logDebug("Parser: " + tokensInFunction.Count.ToString() + " tokens have been detected in function:", logging.loggingSenderID.assemblyParser);
                    for (int j = 0; j < tokensInFunction.Count; j++)
                    {
                        logging.logDebug("[" + j.ToString() + "]: " + tokensInFunction[j] + "," + tokenValuesInFunction[j], logging.loggingSenderID.assemblyParser);
                    }
                    logging.logDebug("Parser: Beginning function parse ...", logging.loggingSenderID.assemblyParser);

                    // now parse that function
                    // allowed operators in if functions: +, -, ++, --, <<, >>, !!
                    // operators such as ==, <, and > will be added in if / test call when function generation is complete
                    // **NOTE** This function will generate temp mem alloc and write calls!

                    // load operator first
                    if (tokensInFunction.Count >= 3 || tokensInFunction.Count == 2) { tokensInFunctionCurrentTokenIndex += 2; }
                    else { tokensInFunctionCurrentTokenIndex++; }
                    tokensInFunctionCurrentTokenID = tokensInFunction[tokensInFunctionCurrentTokenIndex];
                    logging.logDebug("current token: " + tokensInFunctionCurrentTokenID, logging.loggingSenderID.assemblyParser);

                    if (tokensInFunctionCurrentTokenID == "4")
                    {
                        // subtract one token and see if our operator is there
                        tokensInFunctionCurrentTokenIndex--;
                        tokensInFunctionCurrentTokenID = tokensInFunction[tokensInFunctionCurrentTokenIndex];
                    }

                    if (tokensInFunctionCurrentTokenID == "5")
                    {
                        // set operand as add and gather data
                        parsedAssemblyOutput += "add";
                        // gather var A if it exists; if not, insert temp mem location there
                        try
                        {
                            tokensInFunctionCurrentTokenIndex--;
                            tokensInFunctionCurrentTokenID = tokensInFunction[tokensInFunctionCurrentTokenIndex];
                            tokensInFunctionCurrentTokenValue = tokenValuesInFunction[tokensInFunctionCurrentTokenIndex];
                            parsedAssemblyOutput += " " + tokensInFunctionCurrentTokenValue;
                        }
                        catch (ArgumentOutOfRangeException)
                        {
                            logging.logDebug("Parser: add-on function detected", logging.loggingSenderID.assemblyParser);
                            // insert temp mem location as var A
                            parsedAssemblyOutput += " ~temp" + tempMemLocCount.ToString() + "~";
                        }
                        // gather var B
                        tokensInFunctionCurrentTokenIndex += 2;
                        tokensInFunctionCurrentTokenID = tokensInFunction[tokensInFunctionCurrentTokenIndex];
                        tokensInFunctionCurrentTokenValue = tokenValuesInFunction[tokensInFunctionCurrentTokenIndex];
                        parsedAssemblyOutput += " " + tokensInFunctionCurrentTokenValue;
                        // insert var C (mem write location)
                        // check if functionCountBuffer equals 1 (last function)
                        if (functionCountBuffer == 1)
                        {
                            // now check if we've been sent from if statement or a variable
                            if (modifyVarValue != null)
                            {
                                parsedAssemblyOutput += " " + modifyVarValue;
                                logging.logDebug("Parser: [" + lineOfCodeGeneratedAssembly.ToString() + "][f" + (i + 1).ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                                compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                                parsedAssemblyOutput = null;
                                // REQUIRED PIECE OF CODE! OR ELSE EVERYTHING WILL GET FKED UP!
                                lineOfCodeGeneratedAssembly++;
                                currentTokenIndex++;
                                functionCountBuffer--;
                                tokensInFunctionCurrentTokenID = buffertokenTokensArray[currentTokenIndex];
                                tokensInFunctionCurrentTokenValue = buffertokenValuesArray[currentTokenIndex];
                            }
                            else
                            {
                                parsedAssemblyOutput += " ~temp" + tempMemLocCount.ToString() + "~";
                                logging.logDebug("Parser: [" + lineOfCodeGeneratedAssembly.ToString() + "][f" + (i + 1).ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                                compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                                parsedAssemblyOutput = null;
                                // REQUIRED PIECE OF CODE! OR ELSE EVERYTHING WILL GET FKED UP!
                                lineOfCodeGeneratedAssembly++;
                                currentTokenIndex++;
                                functionCountBuffer--;
                                tokensInFunctionCurrentTokenID = buffertokenTokensArray[currentTokenIndex];
                                tokensInFunctionCurrentTokenValue = buffertokenValuesArray[currentTokenIndex];
                            }
                        }
                        else if (functionCountBuffer != 1)
                        {
                            parsedAssemblyOutput += " ~temp" + tempMemLocCount.ToString() + "~";
                            logging.logDebug("Parser: [" + lineOfCodeGeneratedAssembly.ToString() + "][f" + (i + 1).ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                            compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                            parsedAssemblyOutput = null;
                            lineOfCodeGeneratedAssembly++;

                            // will also need to generate writeback instruction!
                            parsedAssemblyOutput += "memw ~temp" + tempMemLocCount.ToString() + "~";
                            logging.logDebug("Parser: [" + lineOfCodeGeneratedAssembly.ToString() + "][f" + (i + 1).ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                            compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                            parsedAssemblyOutput = null;

                            // REQUIRED PIECE OF CODE! OR ELSE EVERYTHING WILL GET FKED UP!
                            lineOfCodeGeneratedAssembly++;
                            currentTokenIndex++;
                            functionCountBuffer--;
                            tokensInFunctionCurrentTokenID = buffertokenTokensArray[currentTokenIndex];
                            tokensInFunctionCurrentTokenValue = buffertokenValuesArray[currentTokenIndex];
                        }

                    }
                    else if (tokensInFunctionCurrentTokenID == "6")
                    {
                        // set operand as sub and gather data
                        parsedAssemblyOutput += "sub";
                        // gather var A if it exists; if not, insert temp mem location there
                        try
                        {
                            tokensInFunctionCurrentTokenIndex--;
                            tokensInFunctionCurrentTokenID = tokensInFunction[tokensInFunctionCurrentTokenIndex];
                            tokensInFunctionCurrentTokenValue = tokenValuesInFunction[tokensInFunctionCurrentTokenIndex];
                            parsedAssemblyOutput += " " + tokensInFunctionCurrentTokenValue;
                        }
                        catch (ArgumentOutOfRangeException)
                        {
                            logging.logDebug("Parser: add-on function detected", logging.loggingSenderID.assemblyParser);
                            // insert temp mem location as var A
                            parsedAssemblyOutput += " ~temp" + tempMemLocCount.ToString() + "~";
                        }
                        // gather var B
                        tokensInFunctionCurrentTokenIndex += 2;
                        tokensInFunctionCurrentTokenID = tokensInFunction[tokensInFunctionCurrentTokenIndex];
                        tokensInFunctionCurrentTokenValue = tokenValuesInFunction[tokensInFunctionCurrentTokenIndex];
                        parsedAssemblyOutput += " " + tokensInFunctionCurrentTokenValue;
                        // insert var C (mem write location)
                        if (functionCountBuffer == 1)
                        {
                            if (modifyVarValue != null)
                            {
                                parsedAssemblyOutput += " " + modifyVarValue;
                                logging.logDebug("Parser: [" + lineOfCodeGeneratedAssembly.ToString() + "][f" + (i + 1).ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                                compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                                parsedAssemblyOutput = null;
                                // REQUIRED PIECE OF CODE! OR ELSE EVERYTHING WILL GET FKED UP!
                                lineOfCodeGeneratedAssembly++;
                                currentTokenIndex++;
                                functionCountBuffer--;
                                tokensInFunctionCurrentTokenID = buffertokenTokensArray[currentTokenIndex];
                                tokensInFunctionCurrentTokenValue = buffertokenValuesArray[currentTokenIndex];
                            }
                            else
                            {
                                parsedAssemblyOutput += " ~temp" + tempMemLocCount.ToString() + "~";
                                logging.logDebug("Parser: [" + lineOfCodeGeneratedAssembly.ToString() + "][f" + (i + 1).ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                                compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                                parsedAssemblyOutput = null;
                                // REQUIRED PIECE OF CODE! OR ELSE EVERYTHING WILL GET FKED UP!
                                lineOfCodeGeneratedAssembly++;
                                currentTokenIndex++;
                                functionCountBuffer--;
                                tokensInFunctionCurrentTokenID = buffertokenTokensArray[currentTokenIndex];
                                tokensInFunctionCurrentTokenValue = buffertokenValuesArray[currentTokenIndex];
                            }
                        }
                        else if (functionCountBuffer != 1)
                        {
                            parsedAssemblyOutput += " ~temp" + tempMemLocCount.ToString() + "~";
                            logging.logDebug("Parser: [" + lineOfCodeGeneratedAssembly.ToString() + "][f" + (i + 1).ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                            compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                            parsedAssemblyOutput = null;
                            lineOfCodeGeneratedAssembly++;

                            // will also need to generate writeback instruction!
                            parsedAssemblyOutput += "memw ~temp" + tempMemLocCount.ToString() + "~";
                            logging.logDebug("Parser: [" + lineOfCodeGeneratedAssembly.ToString() + "][f" + (i + 1).ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                            compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                            parsedAssemblyOutput = null;

                            // REQUIRED PIECE OF CODE! OR ELSE EVERYTHING WILL GET FKED UP!
                            lineOfCodeGeneratedAssembly++;
                            currentTokenIndex++;
                            functionCountBuffer--;
                            tokensInFunctionCurrentTokenID = buffertokenTokensArray[currentTokenIndex];
                            tokensInFunctionCurrentTokenValue = buffertokenValuesArray[currentTokenIndex];
                        }
                    }
                    // redo these functions
                    else if (tokensInFunctionCurrentTokenID == "10")
                    {
                        // set operand as bsr and gather data
                        parsedAssemblyOutput += "bsr";
                        // gather var A if it exists; if not, insert temp mem location there
                        try
                        {
                            tokensInFunctionCurrentTokenIndex--;
                            tokensInFunctionCurrentTokenID = tokensInFunction[tokensInFunctionCurrentTokenIndex];
                            tokensInFunctionCurrentTokenValue = tokenValuesInFunction[tokensInFunctionCurrentTokenIndex];
                            parsedAssemblyOutput += " " + tokensInFunctionCurrentTokenValue;
                        }
                        catch (ArgumentOutOfRangeException)
                        {
                            logging.logDebug("Parser: add-on function detected", logging.loggingSenderID.assemblyParser);
                            // insert temp mem location as var A
                            parsedAssemblyOutput += " ~temp" + tempMemLocCount.ToString() + "~";
                        }
                        // insert var B - writeback location
                        if (functionCountBuffer == 1)
                        {
                            if (modifyVarValue != null)
                            {
                                parsedAssemblyOutput += " " + modifyVarValue;
                                logging.logDebug("Parser: [" + lineOfCodeGeneratedAssembly.ToString() + "][f" + (i + 1).ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                                compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                                parsedAssemblyOutput = null;
                                // REQUIRED PIECE OF CODE! OR ELSE EVERYTHING WILL GET FKED UP!
                                lineOfCodeGeneratedAssembly++;
                                currentTokenIndex++;
                                functionCountBuffer--;
                                tokensInFunctionCurrentTokenID = buffertokenTokensArray[currentTokenIndex];
                                tokensInFunctionCurrentTokenValue = buffertokenValuesArray[currentTokenIndex];
                            }
                            else
                            {
                                parsedAssemblyOutput += " ~temp" + tempMemLocCount.ToString() + "~";
                                logging.logDebug("Parser: [" + lineOfCodeGeneratedAssembly.ToString() + "][f" + (i + 1).ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                                compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                                parsedAssemblyOutput = null;
                                // REQUIRED PIECE OF CODE! OR ELSE EVERYTHING WILL GET FKED UP!
                                lineOfCodeGeneratedAssembly++;
                                currentTokenIndex++;
                                functionCountBuffer--;
                                tokensInFunctionCurrentTokenID = buffertokenTokensArray[currentTokenIndex];
                                tokensInFunctionCurrentTokenValue = buffertokenValuesArray[currentTokenIndex];
                            }

                        }
                        else if (functionCountBuffer != 1)
                        {
                            parsedAssemblyOutput += " ~temp" + tempMemLocCount.ToString() + "~";
                            logging.logDebug("Parser: [" + lineOfCodeGeneratedAssembly.ToString() + "][f" + (i + 1).ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                            compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                            parsedAssemblyOutput = null;
                            lineOfCodeGeneratedAssembly++;

                            // will also need to generate writeback instruction!
                            parsedAssemblyOutput += "memw ~temp" + tempMemLocCount.ToString() + "~";
                            logging.logDebug("Parser: [" + lineOfCodeGeneratedAssembly.ToString() + "][f" + (i + 1).ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                            compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                            parsedAssemblyOutput = null;

                            // REQUIRED PIECE OF CODE! OR ELSE EVERYTHING WILL GET FKED UP!
                            lineOfCodeGeneratedAssembly++;
                            currentTokenIndex++;
                            functionCountBuffer--;
                            tokensInFunctionCurrentTokenID = buffertokenTokensArray[currentTokenIndex];
                            tokensInFunctionCurrentTokenValue = buffertokenValuesArray[currentTokenIndex];
                        }
                    }
                    else if (tokensInFunctionCurrentTokenID == "11")
                    {
                        // set operand as bsl and gather data
                        parsedAssemblyOutput += "bsl";
                        // gather var A if it exists; if not, insert temp mem location there
                        try
                        {
                            tokensInFunctionCurrentTokenIndex--;
                            tokensInFunctionCurrentTokenID = tokensInFunction[tokensInFunctionCurrentTokenIndex];
                            tokensInFunctionCurrentTokenValue = tokenValuesInFunction[tokensInFunctionCurrentTokenIndex];
                            parsedAssemblyOutput += " " + tokensInFunctionCurrentTokenValue;
                        }
                        catch (ArgumentOutOfRangeException)
                        {
                            logging.logDebug("Parser: add-on function detected", logging.loggingSenderID.assemblyParser);
                            // insert temp mem location as var A
                            parsedAssemblyOutput += " ~temp" + tempMemLocCount.ToString() + "~";
                        }
                        // insert var B - writeback location
                        if (functionCountBuffer == 1)
                        {
                            if (modifyVarValue != null)
                            {
                                parsedAssemblyOutput += " " + modifyVarValue;
                                logging.logDebug("Parser: [" + lineOfCodeGeneratedAssembly.ToString() + "][f" + (i + 1).ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                                compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                                parsedAssemblyOutput = null;
                                // REQUIRED PIECE OF CODE! OR ELSE EVERYTHING WILL GET FKED UP!
                                lineOfCodeGeneratedAssembly++;
                                currentTokenIndex++;
                                functionCountBuffer--;
                                tokensInFunctionCurrentTokenID = buffertokenTokensArray[currentTokenIndex];
                                tokensInFunctionCurrentTokenValue = buffertokenValuesArray[currentTokenIndex];
                            }
                            else
                            {
                                parsedAssemblyOutput += " ~temp" + tempMemLocCount.ToString() + "~";
                                logging.logDebug("Parser: [" + lineOfCodeGeneratedAssembly.ToString() + "][f" + (i + 1).ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                                compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                                parsedAssemblyOutput = null;
                                // REQUIRED PIECE OF CODE! OR ELSE EVERYTHING WILL GET FKED UP!
                                lineOfCodeGeneratedAssembly++;
                                currentTokenIndex++;
                                functionCountBuffer--;
                                tokensInFunctionCurrentTokenID = buffertokenTokensArray[currentTokenIndex];
                                tokensInFunctionCurrentTokenValue = buffertokenValuesArray[currentTokenIndex];
                            }
                        }
                        else if (functionCountBuffer != 1)
                        {
                            parsedAssemblyOutput += " ~temp" + tempMemLocCount.ToString() + "~";
                            logging.logDebug("Parser: [" + lineOfCodeGeneratedAssembly.ToString() + "][f" + (i + 1).ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                            compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                            parsedAssemblyOutput = null;
                            lineOfCodeGeneratedAssembly++;

                            // will also need to generate writeback instruction!
                            parsedAssemblyOutput += "memw ~temp" + tempMemLocCount.ToString() + "~";
                            logging.logDebug("Parser: [" + lineOfCodeGeneratedAssembly.ToString() + "][f" + (i + 1).ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                            compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                            parsedAssemblyOutput = null;

                            // REQUIRED PIECE OF CODE! OR ELSE EVERYTHING WILL GET FKED UP!
                            lineOfCodeGeneratedAssembly++;
                            currentTokenIndex++;
                            functionCountBuffer--;
                            tokensInFunctionCurrentTokenID = buffertokenTokensArray[currentTokenIndex];
                            tokensInFunctionCurrentTokenValue = buffertokenValuesArray[currentTokenIndex];
                        }
                    }
                    else if (tokensInFunctionCurrentTokenID == "13")
                    {
                        // set operand as ad1 and gather data
                        parsedAssemblyOutput += "ad1";
                        // gather var A if it exists; if not, insert temp mem location there
                        try
                        {
                            tokensInFunctionCurrentTokenIndex--;
                            tokensInFunctionCurrentTokenID = tokensInFunction[tokensInFunctionCurrentTokenIndex];
                            tokensInFunctionCurrentTokenValue = tokenValuesInFunction[tokensInFunctionCurrentTokenIndex];
                            parsedAssemblyOutput += " " + tokensInFunctionCurrentTokenValue;
                        }
                        catch (ArgumentOutOfRangeException)
                        {
                            logging.logDebug("Parser: add-on function detected", logging.loggingSenderID.assemblyParser);
                            // insert temp mem location as var A
                            parsedAssemblyOutput += " ~temp" + tempMemLocCount.ToString() + "~";
                        }
                        // insert var B - writeback location
                        if (functionCountBuffer == 1)
                        {
                            if (modifyVarValue != null)
                            {
                                parsedAssemblyOutput += " " + modifyVarValue;
                                logging.logDebug("Parser: [" + lineOfCodeGeneratedAssembly.ToString() + "][f" + (i + 1).ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                                compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                                parsedAssemblyOutput = null;
                                // REQUIRED PIECE OF CODE! OR ELSE EVERYTHING WILL GET FKED UP!
                                lineOfCodeGeneratedAssembly++;
                                currentTokenIndex++;
                                functionCountBuffer--;
                                tokensInFunctionCurrentTokenID = buffertokenTokensArray[currentTokenIndex];
                                tokensInFunctionCurrentTokenValue = buffertokenValuesArray[currentTokenIndex];
                            }
                            else
                            {
                                parsedAssemblyOutput += " ~temp" + tempMemLocCount.ToString() + "~";
                                logging.logDebug("Parser: [" + lineOfCodeGeneratedAssembly.ToString() + "][f" + (i + 1).ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                                compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                                parsedAssemblyOutput = null;
                                // REQUIRED PIECE OF CODE! OR ELSE EVERYTHING WILL GET FKED UP!
                                lineOfCodeGeneratedAssembly++;
                                currentTokenIndex++;
                                functionCountBuffer--;
                                tokensInFunctionCurrentTokenID = buffertokenTokensArray[currentTokenIndex];
                                tokensInFunctionCurrentTokenValue = buffertokenValuesArray[currentTokenIndex];
                            }
                        }
                        else if (functionCountBuffer != 1)
                        {
                            parsedAssemblyOutput += " ~temp" + tempMemLocCount.ToString() + "~";
                            logging.logDebug("Parser: [" + lineOfCodeGeneratedAssembly.ToString() + "][f" + (i + 1).ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                            compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                            parsedAssemblyOutput = null;
                            lineOfCodeGeneratedAssembly++;

                            // will also need to generate writeback instruction!
                            parsedAssemblyOutput += "memw ~temp" + tempMemLocCount.ToString() + "~";
                            logging.logDebug("Parser: [" + lineOfCodeGeneratedAssembly.ToString() + "][f" + (i + 1).ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                            compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                            parsedAssemblyOutput = null;

                            // REQUIRED PIECE OF CODE! OR ELSE EVERYTHING WILL GET FKED UP!
                            lineOfCodeGeneratedAssembly++;
                            currentTokenIndex++;
                            functionCountBuffer--;
                            tokensInFunctionCurrentTokenID = buffertokenTokensArray[currentTokenIndex];
                            tokensInFunctionCurrentTokenValue = buffertokenValuesArray[currentTokenIndex];
                        }
                    }
                    else if (tokensInFunctionCurrentTokenID == "14")
                    {
                        // set operand as sb1 and gather data
                        parsedAssemblyOutput += "sb1";
                        // gather var A if it exists; if not, insert temp mem location there
                        try
                        {
                            tokensInFunctionCurrentTokenIndex--;
                            tokensInFunctionCurrentTokenID = tokensInFunction[tokensInFunctionCurrentTokenIndex];
                            tokensInFunctionCurrentTokenValue = tokenValuesInFunction[tokensInFunctionCurrentTokenIndex];
                            parsedAssemblyOutput += " " + tokensInFunctionCurrentTokenValue;
                        }
                        catch (ArgumentOutOfRangeException)
                        {
                            logging.logDebug("Parser: add-on function detected", logging.loggingSenderID.assemblyParser);
                            // insert temp mem location as var A
                            parsedAssemblyOutput += " ~temp" + tempMemLocCount.ToString() + "~";
                        }
                        // insert var B - writeback location
                        if (functionCountBuffer == 1)
                        {
                            if (modifyVarValue != null)
                            {
                                parsedAssemblyOutput += " " + modifyVarValue;
                                logging.logDebug("Parser: [" + lineOfCodeGeneratedAssembly.ToString() + "][f" + (i + 1).ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                                compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                                parsedAssemblyOutput = null;
                                // REQUIRED PIECE OF CODE! OR ELSE EVERYTHING WILL GET FKED UP!
                                lineOfCodeGeneratedAssembly++;
                                currentTokenIndex++;
                                functionCountBuffer--;
                                tokensInFunctionCurrentTokenID = buffertokenTokensArray[currentTokenIndex];
                                tokensInFunctionCurrentTokenValue = buffertokenValuesArray[currentTokenIndex];
                            }
                            else
                            {
                                parsedAssemblyOutput += " ~temp" + tempMemLocCount.ToString() + "~";
                                logging.logDebug("Parser: [" + lineOfCodeGeneratedAssembly.ToString() + "][f" + (i + 1).ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                                compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                                parsedAssemblyOutput = null;
                                // REQUIRED PIECE OF CODE! OR ELSE EVERYTHING WILL GET FKED UP!
                                lineOfCodeGeneratedAssembly++;
                                currentTokenIndex++;
                                functionCountBuffer--;
                                tokensInFunctionCurrentTokenID = buffertokenTokensArray[currentTokenIndex];
                                tokensInFunctionCurrentTokenValue = buffertokenValuesArray[currentTokenIndex];
                            }
                        }
                        else if (functionCountBuffer != 1)
                        {
                            parsedAssemblyOutput += " ~temp" + tempMemLocCount.ToString() + "~";
                            logging.logDebug("Parser: [" + lineOfCodeGeneratedAssembly.ToString() + "][f" + (i + 1).ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                            compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                            parsedAssemblyOutput = null;
                            lineOfCodeGeneratedAssembly++;

                            // will also need to generate writeback instruction!
                            parsedAssemblyOutput += "memw ~temp" + tempMemLocCount.ToString() + "~";
                            logging.logDebug("Parser: [" + lineOfCodeGeneratedAssembly.ToString() + "][f" + (i + 1).ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                            compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                            parsedAssemblyOutput = null;

                            // REQUIRED PIECE OF CODE! OR ELSE EVERYTHING WILL GET FKED UP!
                            lineOfCodeGeneratedAssembly++;
                            currentTokenIndex++;
                            functionCountBuffer--;
                            tokensInFunctionCurrentTokenID = buffertokenTokensArray[currentTokenIndex];
                            tokensInFunctionCurrentTokenValue = buffertokenValuesArray[currentTokenIndex];
                        }
                    }
                    else if (tokensInFunctionCurrentTokenID == "15")
                    {
                        // set operand as not and gather data
                        parsedAssemblyOutput += "not";
                        // gather var A if it exists; if not, insert temp mem location there
                        try
                        {
                            tokensInFunctionCurrentTokenIndex--;
                            tokensInFunctionCurrentTokenID = tokensInFunction[tokensInFunctionCurrentTokenIndex];
                            tokensInFunctionCurrentTokenValue = tokenValuesInFunction[tokensInFunctionCurrentTokenIndex];
                            parsedAssemblyOutput += " " + tokensInFunctionCurrentTokenValue;
                        }
                        catch (ArgumentOutOfRangeException)
                        {
                            logging.logDebug("Parser: add-on function detected", logging.loggingSenderID.assemblyParser);
                            // insert temp mem location as var A
                            parsedAssemblyOutput += " ~temp" + tempMemLocCount.ToString() + "~";
                        }
                        // insert var B - writeback location
                        if (functionCountBuffer == 1)
                        {
                            if (modifyVarValue != null)
                            {
                                parsedAssemblyOutput += " " + modifyVarValue;
                                logging.logDebug("Parser: [" + lineOfCodeGeneratedAssembly.ToString() + "][f" + (i + 1).ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                                compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                                parsedAssemblyOutput = null;
                                // REQUIRED PIECE OF CODE! OR ELSE EVERYTHING WILL GET FKED UP!
                                lineOfCodeGeneratedAssembly++;
                                currentTokenIndex++;
                                functionCountBuffer--;
                                tokensInFunctionCurrentTokenID = buffertokenTokensArray[currentTokenIndex];
                                tokensInFunctionCurrentTokenValue = buffertokenValuesArray[currentTokenIndex];
                            }
                            else
                            {
                                parsedAssemblyOutput += " ~temp" + tempMemLocCount.ToString() + "~";
                                logging.logDebug("Parser: [" + lineOfCodeGeneratedAssembly.ToString() + "][f" + (i + 1).ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                                compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                                parsedAssemblyOutput = null;
                                // REQUIRED PIECE OF CODE! OR ELSE EVERYTHING WILL GET FKED UP!
                                lineOfCodeGeneratedAssembly++;
                                currentTokenIndex++;
                                functionCountBuffer--;
                                tokensInFunctionCurrentTokenID = buffertokenTokensArray[currentTokenIndex];
                                tokensInFunctionCurrentTokenValue = buffertokenValuesArray[currentTokenIndex];
                            }
                        }
                        else if (functionCountBuffer != 1)
                        {
                            parsedAssemblyOutput += " ~temp" + tempMemLocCount.ToString() + "~";
                            logging.logDebug("Parser: [" + lineOfCodeGeneratedAssembly.ToString() + "][f" + (i + 1).ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                            compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                            parsedAssemblyOutput = null;
                            lineOfCodeGeneratedAssembly++;

                            // will also need to generate writeback instruction!
                            parsedAssemblyOutput += "memw ~temp" + tempMemLocCount.ToString() + "~";
                            logging.logDebug("Parser: [" + lineOfCodeGeneratedAssembly.ToString() + "][f" + (i + 1).ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                            compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                            parsedAssemblyOutput = null;

                            // REQUIRED PIECE OF CODE! OR ELSE EVERYTHING WILL GET FKED UP!
                            lineOfCodeGeneratedAssembly++;
                            currentTokenIndex++;
                            functionCountBuffer--;
                            tokensInFunctionCurrentTokenID = buffertokenTokensArray[currentTokenIndex];
                            tokensInFunctionCurrentTokenValue = buffertokenValuesArray[currentTokenIndex];
                        }
                    }

                }
                // return to location we jumped from
                logging.logDebug("Function generation complete", logging.loggingSenderID.assemblyParser);
                functionCount = 0;
                if (jumpLocationAfterFunctionGenerate == 1) { goto CompletingIfStatementParsing; }
                if (jumpLocationAfterFunctionGenerate == 2) { goto CompletingIfStatementParsingPt2; }
            }
        #endregion
        #region Completing If Statement Parsing
        CompletingIfStatementParsing:
            currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
            currentTokenID = compiler.tokenTokensArray[currentTokenIndex];
            // causes
            if (currentTokenID == "18")
            {
                // now check for shift overflow/underflow
                currentTokenIndex++;
                currentTokenID = compiler.tokenTokensArray[currentTokenIndex];
                if (currentTokenID == "32")
                {
                    // build assembly command
                    // easy!
                    parsedAssemblyOutput += "test SO ~otemp~";
                    logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                    compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                    parsedAssemblyOutput = null;
                    // goto parentheses token so next token will be loaded
                    currentTokenIndex++;
                    tempMemLocCount = 0;
                    goto TopOfParseTree;
                }
                else if (currentTokenID == "33")
                {
                    // build assembly command
                    // easy!
                    parsedAssemblyOutput += "test SU ~otemp~";
                    logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                    compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                    parsedAssemblyOutput = null;
                    // goto parentheses token so next token will be loaded
                    currentTokenIndex++;
                    tempMemLocCount = 0;
                    goto TopOfParseTree;
                }
            }
            // <, >, and == operators
            else if (currentTokenID == "8" || currentTokenID == "9" || currentTokenID == "12")
            {
                string tokenIDtoValue = "";
                if (currentTokenID == "8") { tokenIDtoValue = "<"; ifOperatorType = 1; }
                if (currentTokenID == "9") { tokenIDtoValue = ">"; ifOperatorType = 2; }
                if (currentTokenID == "12") { tokenIDtoValue = "=="; ifOperatorType = 3; }
                // check to see what the next token is
                currentTokenIndex++;
                currentTokenID = compiler.tokenTokensArray[currentTokenIndex];
                currentTokenValue = compiler.tokenValuesArray[currentTokenIndex];
                if (currentTokenID == "2")
                {
                    // build assembly command
                    parsedAssemblyOutput += "test " + tokenIDtoValue + " ~temp~ " + currentTokenValue;
                    logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                    compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                    parsedAssemblyOutput = null;
                    // goto parentheses token so next token will be loaded
                    currentTokenIndex++;
                    tempMemLocCount = 0;
                    goto TopOfParseTree;
                }
                else if (currentTokenID == "3")
                {
                    // build assembly command
                    parsedAssemblyOutput += "test " + tokenIDtoValue + " ~temp~ " + currentTokenValue;
                    logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                    compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                    parsedAssemblyOutput = null;
                    // goto parentheses token so next token will be loaded
                    currentTokenIndex++;
                    tempMemLocCount = 0;
                    goto TopOfParseTree;
                }
                else if (currentTokenID == "23")
                {
                    // goto function generator
                    logging.logDebug("Function generator started", logging.loggingSenderID.assemblyParser);
                    jumpLocationAfterFunctionGenerate = 2;
                    goto FunctionGenerator;
                }
            }
        #endregion
        #region Completing If Statement Parsing - If Operators
        CompletingIfStatementParsingPt2:
            // in this stage, we will add the command for testing using if operators
            if (ifOperatorType != 0)
            {
                // add command
                if (ifOperatorType == 1)
                {
                    // build assembly command
                    parsedAssemblyOutput += "test < ~temp1~ ~temp2~;";
                    logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                    compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                    parsedAssemblyOutput = null;
                    tempMemLocCount = 0;
                    ifOperatorType = 0;
                    goto TopOfParseTree;
                }
                else if (ifOperatorType == 2)
                {
                    // build assembly command
                    parsedAssemblyOutput += "test > ~temp1~ ~temp2~";
                    logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                    compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                    parsedAssemblyOutput = null;
                    tempMemLocCount = 0;
                    ifOperatorType = 0;
                    goto TopOfParseTree;
                }
                else if (ifOperatorType == 3)
                {
                    // build assembly command
                    parsedAssemblyOutput += "test == ~temp1~ ~temp2~";
                    logging.logDebug("[" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
                    compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
                    parsedAssemblyOutput = null;
                    tempMemLocCount = 0;
                    ifOperatorType = 0;
                    goto TopOfParseTree;
                }
            }
            else
            {
                // goto parentheses token so next token will be loaded
                //currentTokenIndex++; - DEBUG IF THIS SHOULD STILL BE HERE!
                logging.logDebug("2nd stage if statement completion hit, yet no operator type detected", logging.loggingSenderID.assemblyParser);
                goto TopOfParseTree;
            }
        #endregion

        EndOfParsingStage1:
            // end of parsing stage 1!

            // before end of parsing stage 1, generate writebacks!
            #region generating writeback statements for ad1, sb1, add, and sub
            // before all of this, add in the writeback statements from add/sub/ad1/sb1
            for (int i = 0; i < compiler.parsedAssemblyList.Count; i++)
            {
                if (compiler.parsedAssemblyList[i].Contains("add") || compiler.parsedAssemblyList[i].Contains("sub") || compiler.parsedAssemblyList[i].Contains("ad1") || compiler.parsedAssemblyList[i].Contains("sb1"))
                {
                    // pretty inefficient code here
                    if (i + 1 != compiler.parsedAssemblyList.Count)
                    {
                        if (!compiler.parsedAssemblyList[i + 1].Contains("test"))
                        {
                            string ramWriteAddress = "";
                            // generate writeback
                            // grab last address from each of the commands
                            if (compiler.parsedAssemblyList[i].Contains("add") || compiler.parsedAssemblyList[i].Contains("sub"))
                            {
                                string temp = "";
                                if (compiler.parsedAssemblyList[i].Contains("add"))
                                {
                                    temp = compiler.parsedAssemblyList[i].Remove(compiler.parsedAssemblyList[i].IndexOf("add"), 4);
                                }
                                else if (compiler.parsedAssemblyList[i].Contains("sub"))
                                {
                                    temp = compiler.parsedAssemblyList[i].Remove(compiler.parsedAssemblyList[i].IndexOf("sub"), 4);
                                }
                                string block1 = null;
                                string block2 = null;
                                string block3 = null;
                                int readLoc = 0;
                                StringReader sr1 = new StringReader(temp);
                                char currentChar;

                                // determine syntax
                                currentChar = (char)sr1.Read();
                                block1 += currentChar;
                                readLoc++;
                                while (currentChar != ' ')
                                {
                                    readLoc++;
                                    currentChar = (char)sr1.Read();
                                    block1 += currentChar;
                                }
                                currentChar = (char)sr1.Read();
                                block2 += currentChar;
                                readLoc++;
                                while (currentChar != ' ')
                                {
                                    readLoc++;
                                    currentChar = (char)sr1.Read();
                                    block2 += currentChar;
                                }
                                readLoc++;
                                currentChar = (char)sr1.Read();
                                block3 += currentChar;
                                while (readLoc != temp.Length)
                                {
                                    readLoc++;
                                    currentChar = (char)sr1.Read();
                                    block3 += currentChar;
                                }
                                ramWriteAddress = block3;
                            }
                            else if (compiler.parsedAssemblyList[i].Contains("ad1") || compiler.parsedAssemblyList[i].Contains("sb1"))
                            {
                                if (!compiler.parsedAssemblyList[i + 1].Contains("memw"))
                                {
                                    string temp = "";
                                    if (compiler.parsedAssemblyList[i].Contains("ad1"))
                                    {
                                        temp = compiler.parsedAssemblyList[i].Remove(compiler.parsedAssemblyList[i].IndexOf("ad1"), 4);
                                    }
                                    else if (compiler.parsedAssemblyList[i].Contains("sb1"))
                                    {
                                        temp = compiler.parsedAssemblyList[i].Remove(compiler.parsedAssemblyList[i].IndexOf("sb1"), 4);
                                    }
                                    string block2 = null;
                                    string block3 = null;
                                    int readLoc = 0;
                                    StringReader sr1 = new StringReader(temp);
                                    char currentChar;
                                    currentChar = (char)sr1.Read();
                                    block2 += currentChar;
                                    readLoc++;
                                    while (currentChar != ' ')
                                    {
                                        readLoc++;
                                        currentChar = (char)sr1.Read();
                                        block2 += currentChar;
                                    }
                                    readLoc++;
                                    currentChar = (char)sr1.Read();
                                    block3 += currentChar;
                                    while (readLoc != temp.Length)
                                    {
                                        readLoc++;
                                        currentChar = (char)sr1.Read();
                                        block3 += currentChar;
                                    }
                                    ramWriteAddress = block3;
                                }
                            }
                            // generate writeback assembly
                            if (ramWriteAddress != "")
                            {
                                parsedAssemblyOutput = null;
                                parsedAssemblyOutput += "memw " + ramWriteAddress;
                                compiler.parsedAssemblyList.Insert(i + 1, parsedAssemblyOutput);
                                lineOfCodeGeneratedAssembly++;
                            }
                        }
                    }
                    else
                    {
                        string ramWriteAddress = "";
                        // generate writeback
                        // grab last address from each of the commands
                        if (compiler.parsedAssemblyList[i].Contains("add") || compiler.parsedAssemblyList[i].Contains("sub"))
                        {
                            string temp = "";
                            if (compiler.parsedAssemblyList[i].Contains("add"))
                            {
                                temp = compiler.parsedAssemblyList[i].Remove(compiler.parsedAssemblyList[i].IndexOf("add"), 4);
                            }
                            else if (compiler.parsedAssemblyList[i].Contains("sub"))
                            {
                                temp = compiler.parsedAssemblyList[i].Remove(compiler.parsedAssemblyList[i].IndexOf("sub"), 4);
                            }
                            string block1 = null;
                            string block2 = null;
                            string block3 = null;
                            int readLoc = 0;
                            StringReader sr1 = new StringReader(temp);
                            char currentChar;

                            // determine syntax
                            currentChar = (char)sr1.Read();
                            block1 += currentChar;
                            readLoc++;
                            while (currentChar != ' ')
                            {
                                readLoc++;
                                currentChar = (char)sr1.Read();
                                block1 += currentChar;
                            }
                            currentChar = (char)sr1.Read();
                            block2 += currentChar;
                            readLoc++;
                            while (currentChar != ' ')
                            {
                                readLoc++;
                                currentChar = (char)sr1.Read();
                                block2 += currentChar;
                            }
                            readLoc++;
                            currentChar = (char)sr1.Read();
                            block3 += currentChar;
                            while (readLoc != temp.Length)
                            {
                                readLoc++;
                                currentChar = (char)sr1.Read();
                                block3 += currentChar;
                            }
                            ramWriteAddress = block3;
                        }
                        else if (compiler.parsedAssemblyList[i].Contains("ad1") || compiler.parsedAssemblyList[i].Contains("sb1"))
                        {
                            if (!compiler.parsedAssemblyList[i + 1].Contains("memw"))
                            {
                                string temp = "";
                                if (compiler.parsedAssemblyList[i].Contains("ad1"))
                                {
                                    temp = compiler.parsedAssemblyList[i].Remove(compiler.parsedAssemblyList[i].IndexOf("ad1"), 4);
                                }
                                else if (compiler.parsedAssemblyList[i].Contains("sb1"))
                                {
                                    temp = compiler.parsedAssemblyList[i].Remove(compiler.parsedAssemblyList[i].IndexOf("sb1"), 4);
                                }
                                string block2 = null;
                                string block3 = null;
                                int readLoc = 0;
                                StringReader sr1 = new StringReader(temp);
                                char currentChar;
                                currentChar = (char)sr1.Read();
                                block2 += currentChar;
                                readLoc++;
                                while (currentChar != ' ')
                                {
                                    readLoc++;
                                    currentChar = (char)sr1.Read();
                                    block2 += currentChar;
                                }
                                readLoc++;
                                currentChar = (char)sr1.Read();
                                block3 += currentChar;
                                while (readLoc != temp.Length)
                                {
                                    readLoc++;
                                    currentChar = (char)sr1.Read();
                                    block3 += currentChar;
                                }
                                ramWriteAddress = block3;
                            }
                        }
                        // generate writeback assembly
                        if (ramWriteAddress != "")
                        {
                            parsedAssemblyOutput = null;
                            parsedAssemblyOutput += "memw " + ramWriteAddress;
                            compiler.parsedAssemblyList.Insert(i + 1, parsedAssemblyOutput);
                            lineOfCodeGeneratedAssembly++;
                        }
                    }
                }
            }
            #endregion

            // add 'EOF' to assembly
            parsedAssemblyOutput = null;
            parsedAssemblyOutput += "EOF";
            logging.logDebug("Parser: [" + lineOfCodeGeneratedAssembly.ToString() + "]: " + parsedAssemblyOutput, logging.loggingSenderID.assemblyParser);
            compiler.parsedAssemblyList.Add(parsedAssemblyOutput);
            parsedAssemblyOutput = null;
            logging.logDebug("Stage 1 parsing complete", logging.loggingSenderID.assemblyParser, true);
        }

        public static void parseStage2()
        {
            // objectives to complete in stage 2 of parsing:
            // add tjump and fjump statements to if statements
            // add jumps for else statements
            // add jump statements from last line of code inside block to outside block

            // deprecated - rewriting!
            #region deprecated
            int blockID = 0;
            int eBlockID = 0;

            // fix bug where it crashes when we have two or more sblock / tblock / else / jalloc / jump statements
            // next to each other
            // to circumvent bug, can simply add any line of code inbetween
            // maybe mark it as syntax error? 

            #region Merging sblock, tblock, else, jalloc, and jump statements / instructions  / removing whitespace from parsed assembly list
            // merge sblock, tblock, and else statements with lines of code
            //TopOfMerge:
            for (int i = 0; i < compiler.parsedAssemblyList.Count; i++)
            {
                if (compiler.parsedAssemblyList[i].Contains("%compiler%: sblock"))
                {
                    // move this to the next instruction
                    compiler.parsedAssemblyList[i + 1] = compiler.parsedAssemblyList[i].Substring(12) + " " + compiler.parsedAssemblyList[i + 1];
                    compiler.parsedAssemblyList[i] = compiler.parsedAssemblyList[i].Remove(compiler.parsedAssemblyList[i].IndexOf('%'));
                }
                else if (compiler.parsedAssemblyList[i].Contains("%compiler%: tblock"))
                {
                    compiler.parsedAssemblyList[i - 1] = compiler.parsedAssemblyList[i].Substring(12) + " " + compiler.parsedAssemblyList[i - 1];
                    compiler.parsedAssemblyList[i] = compiler.parsedAssemblyList[i].Remove(compiler.parsedAssemblyList[i].IndexOf('%'));
                }
                else if (compiler.parsedAssemblyList[i].Contains("%compiler%: else") && !compiler.parsedAssemblyList[i + 1].Contains("%compiler%: sblock"))
                {
                    // move this to next instruction
                    compiler.parsedAssemblyList[i + 1] = compiler.parsedAssemblyList[i].Substring(12) + " " + compiler.parsedAssemblyList[i + 1];
                    compiler.parsedAssemblyList[i] = compiler.parsedAssemblyList[i].Remove(compiler.parsedAssemblyList[i].IndexOf('%'));
                }
                else if (compiler.parsedAssemblyList[i].Contains("%compiler%: else") && compiler.parsedAssemblyList[i + 1].Contains("%compiler%: sblock"))
                {
                    // move sblock AND else to instruction two lines down
                    // else
                    compiler.parsedAssemblyList[i + 2] = compiler.parsedAssemblyList[i].Substring(12) + " " + compiler.parsedAssemblyList[i + 2];
                    // sblock
                    compiler.parsedAssemblyList[i + 2] = compiler.parsedAssemblyList[i + 1].Substring(12) + " " + compiler.parsedAssemblyList[i + 2];
                    // remove both
                    compiler.parsedAssemblyList[i] = compiler.parsedAssemblyList[i].Remove(compiler.parsedAssemblyList[i].IndexOf('%'));
                    compiler.parsedAssemblyList[i + 1] = compiler.parsedAssemblyList[i + 1].Remove(compiler.parsedAssemblyList[i + 1].IndexOf('%'));
                }
                else if (compiler.parsedAssemblyList[i].Contains("%compiler%: jalloc"))
                {
                    // move this to the next instruction
                    compiler.parsedAssemblyList[i + 1] = compiler.parsedAssemblyList[i].Substring(compiler.parsedAssemblyList[i].IndexOf("jalloc")) + " " + compiler.parsedAssemblyList[i + 1];
                    compiler.parsedAssemblyList[i] = compiler.parsedAssemblyList[i].Remove(0);
                }
                else if (compiler.parsedAssemblyList[i].Contains("jump"))
                {
                    // replace jump w/ fjump; to make it easier
                    // move this to previous instruction
                    compiler.parsedAssemblyList[i - 1] = compiler.parsedAssemblyList[i - 1] + " " + compiler.parsedAssemblyList[i];
                    compiler.parsedAssemblyList[i - 1] = compiler.parsedAssemblyList[i - 1].Replace("jump", "fjump");
                    compiler.parsedAssemblyList[i] = compiler.parsedAssemblyList[i].Remove(0);
                }

            }

        TopOfWhiteSpaceRemove1:
            for (int i = 0; i < compiler.parsedAssemblyList.Count; i++)
            {
                if (string.IsNullOrWhiteSpace(compiler.parsedAssemblyList[i]))
                {
                    compiler.parsedAssemblyList.RemoveAt(i);
                    goto TopOfWhiteSpaceRemove1;
                }
            }

            // add else block ID
            for (int i = 0; i < compiler.parsedAssemblyList.Count; i++)
            {
                if (compiler.parsedAssemblyList[i].Contains("tblock"))
                {
                    //check if there is an else statement after this; if there is, also move this to next instruction!

                    if (compiler.parsedAssemblyList[i + 1].Contains("else"))
                    {
                        logging.logDebug("acquireblockid: " + compiler.parsedAssemblyList[i].Substring(6), logging.loggingSenderID.assemblyParser);
                        int IDIndexCounter = 1;
                        blockID = -1;
                        logging.logDebug(compiler.parsedAssemblyList[i].Substring(0, 6), logging.loggingSenderID.assemblyParser);
                    TopOfFunction:
                        try
                        {
                            Convert.ToInt16(compiler.parsedAssemblyList[i].Substring(6, IDIndexCounter));
                            IDIndexCounter++;
                            goto TopOfFunction;
                        }
                        catch (FormatException)
                        {
                            blockID = Convert.ToInt16(compiler.parsedAssemblyList[i].Substring(6, IDIndexCounter - 1));
                        }
                        catch (ArgumentOutOfRangeException)
                        {
                            blockID = Convert.ToInt16(compiler.parsedAssemblyList[i].Substring(6, IDIndexCounter - 1));
                        }
                        logging.logDebug("blockid " + blockID.ToString(), logging.loggingSenderID.assemblyParser);
                        compiler.parsedAssemblyList[i + 1] = "eblock" + blockID.ToString() + " " + compiler.parsedAssemblyList[i + 1];
                    }
                }
            }

        // now check for cases of lone tblock statements; this will happen in the following scenarios:
        /*
            if(x)
            {
                if(y)
                {
                    something here
                }
            }
        */
        // this function seems to be acting a bit weird, watch out!
        TopOfTBlockCheck:
            for (int i = 0; i < compiler.parsedAssemblyList.Count; i++)
            {
                if (compiler.parsedAssemblyList[i].Contains("tblock"))
                {
                    try
                    {
                        logging.logDebug(("tblock check: " + Convert.ToInt16(compiler.parsedAssemblyList[i].Substring(6))).ToString(), logging.loggingSenderID.assemblyParser);
                        // if we can do the above following, then we only have a tblock[x]
                        // merge these with the line of code above it
                        compiler.parsedAssemblyList[i - 1] = compiler.parsedAssemblyList[i] + "" + compiler.parsedAssemblyList[i - 1];
                        compiler.parsedAssemblyList[i] = "";
                        goto TopOfTBlockCheck;
                    }
                    catch (FormatException)
                    {
                        // do nothing
                    }
                }
            }
        // removing whitespace
        TopOfWhiteSpaceRemove2:
            for (int i = 0; i < compiler.parsedAssemblyList.Count; i++)
            {
                if (string.IsNullOrWhiteSpace(compiler.parsedAssemblyList[i]))
                {
                    compiler.parsedAssemblyList.RemoveAt(i);
                    goto TopOfWhiteSpaceRemove2;
                }
            }

            #endregion

            logging.logDebug("dump after merge", logging.loggingSenderID.assemblyParser);
            for (int i = 0; i < compiler.parsedAssemblyList.Count; i++)
            {
                logging.logDebug("[" + (i + 1).ToString() + "]: " + compiler.parsedAssemblyList[i], logging.loggingSenderID.assemblyParser);
            }

            #region generating tjump and fjump statements from if / else instructions
            // adding tjump and fjump statements to test instructions
            logging.logDebug("Adding branch code part 1 ...", logging.loggingSenderID.assemblyParser);
            for (int i = 0; i < compiler.parsedAssemblyList.Count; i++)
            {
                if (compiler.parsedAssemblyList[i].Contains("test"))
                {
                    logging.logDebug("hit test instruction!: [ln" + (i + 1).ToString() + ", " + compiler.parsedAssemblyList[i] + "]", logging.loggingSenderID.assemblyParser);
                    // now we will gather tjump location and fjump location
                    if (compiler.parsedAssemblyList[i + 1].Contains("sblock"))
                    {
                        blockID = functionLibrary.AcquireBlockID(compiler.parsedAssemblyList[i + 1], (compiler.parsedAssemblyList[i + 1].IndexOf("sblock")) + 6);
                        logging.logDebug("blockID: " + blockID.ToString(), logging.loggingSenderID.assemblyParser);
                        // tjump
                        compiler.parsedAssemblyList[i] += " tjump " + (i + 2).ToString();
                        // fjump
                        // set it to i because we only want it reading from the current location down, not from the beginning of the list
                        int fjumpIndex = (i + 1); // VERY IMPORTANT!
                        logging.logDebug("fji before fjump find: " + fjumpIndex.ToString(), logging.loggingSenderID.assemblyParser);
                        while (!compiler.parsedAssemblyList[fjumpIndex - 1].Contains("tblock" + blockID.ToString())) // we do -1 b/c zero-based index
                        {
                            fjumpIndex++;
                            logging.logDebug("fji now " + fjumpIndex.ToString() + ", points to line of code " + fjumpIndex.ToString(), logging.loggingSenderID.assemblyParser);
                        }
                        logging.logDebug("line of code found after fjump find: [" + fjumpIndex.ToString() + ", " + compiler.parsedAssemblyList[fjumpIndex - 1] + "]", logging.loggingSenderID.assemblyParser); // we do -1 on the array index b/c zero-based index

                        #region else statement fjump branch map info
                        /* else statement fjump branch map
                        This will be a basic map of the different types of fjump branches and stuff that this compiler will use and do in order to generate correct
                        branches. This is to help me visualize everything and not get ultra confused

                        *refer to physical copy*

                        */
                        #endregion

                        // this will be based on the else statement fjump branch map!

                        // check if we need to do else branching (if else exists)
                        // check for 'else' 1 line past the one fjump gen found
                        if (compiler.parsedAssemblyList[fjumpIndex].Contains("else"))
                        {
                            logging.logDebug("else statement detected as line of code to be set!: [" + (fjumpIndex + 1).ToString() + ", " + compiler.parsedAssemblyList[fjumpIndex] + "]", logging.loggingSenderID.assemblyParser);
                            // now check if eblock tied to else statement found matches blockID
                            eBlockID = functionLibrary.AcquireBlockID(compiler.parsedAssemblyList[fjumpIndex], (compiler.parsedAssemblyList[fjumpIndex].IndexOf("eblock")) + 6);
                            if (eBlockID == blockID)
                            {
                                // eID = bID case
                                compiler.parsedAssemblyList[i] += " fjump " + (fjumpIndex + 1).ToString();
                                logging.logDebug("fji determined from function is [" + (fjumpIndex + 1).ToString() + "] - took eID = bID case", logging.loggingSenderID.assemblyParser);
                            }
                            // this case means that we have to jump outside of the nested else's in if statement!
                            else
                            {
                                logging.logDebug("eID != bID! Generating jump for outside of if statement!", logging.loggingSenderID.assemblyParser);
                                int blockIDTemp = blockID;
                                int lowestBlockID = blockIDTemp;
                                for (int j = fjumpIndex; j < compiler.parsedAssemblyList.Count; j++)
                                {
                                    if (compiler.parsedAssemblyList[j - 1].Contains("tblock"))
                                    {
                                        blockIDTemp = functionLibrary.AcquireBlockID(compiler.parsedAssemblyList[j - 1], (compiler.parsedAssemblyList[j - 1].IndexOf("tblock")) + 6);
                                        if (blockIDTemp < lowestBlockID)
                                        {
                                            lowestBlockID = blockIDTemp;
                                            logging.logDebug("lowestblockid has been updated to " + lowestBlockID.ToString(), logging.loggingSenderID.assemblyParser);
                                        }
                                    }
                                }
                                logging.logDebug("lowestblockid generated is " + lowestBlockID.ToString(), logging.loggingSenderID.assemblyParser);
                                logging.logDebug("finding end of if block ...", logging.loggingSenderID.assemblyParser);
                            TopOfEndIfBlockFind:
                                while (!compiler.parsedAssemblyList[fjumpIndex - 1].Contains("tblock" + lowestBlockID.ToString()))
                                {
                                    fjumpIndex++;
                                    logging.logDebug("fji now " + fjumpIndex.ToString() + ", points to line of code " + fjumpIndex.ToString(), logging.loggingSenderID.assemblyParser);
                                }
                                if (compiler.parsedAssemblyList[fjumpIndex].Contains("else"))
                                {
                                    fjumpIndex++;
                                    logging.logDebug("else found; jumping to line of code " + fjumpIndex.ToString(), logging.loggingSenderID.assemblyParser);
                                    goto TopOfEndIfBlockFind;
                                }
                                else if (!compiler.parsedAssemblyList[fjumpIndex].Contains("else"))
                                {
                                    compiler.parsedAssemblyList[i] += " fjump " + (fjumpIndex + 1).ToString();
                                    logging.logDebug("fji determined from function is [" + (fjumpIndex + 1).ToString() + "] - took eID != bID case", logging.loggingSenderID.assemblyParser);
                                }

                            }
                        }
                        else
                        {
                            // default case; jumping outside of if statement block (no else statement)
                            compiler.parsedAssemblyList[i] += " fjump " + (fjumpIndex + 1).ToString();
                            logging.logDebug("fji determined from function is [" + (fjumpIndex + 1).ToString() + "] - took default case", logging.loggingSenderID.assemblyParser);
                        }
                    }
                }
            }
            #endregion

            logging.logDebug("dump after adding branch code part 1", logging.loggingSenderID.assemblyParser);
            for (int i = 0; i < compiler.parsedAssemblyList.Count; i++)
            {
                logging.logDebug("[" + (i + 1).ToString() + "]: " + compiler.parsedAssemblyList[i], logging.loggingSenderID.assemblyParser);
            }

            #region generating fjump statements for end of blocks
            logging.logDebug("Adding branch code part 2 ...", logging.loggingSenderID.assemblyParser);
            // adding fjumps to end of blocks
            for (int i = 0; i < compiler.parsedAssemblyList.Count; i++)
            {
                int fjumpIndex = i;
                if (compiler.parsedAssemblyList[i].Contains("tblock") && !compiler.parsedAssemblyList[i].Contains("jump") && !compiler.parsedAssemblyList[i].Contains("exit")) // add the jump & exit check as well b/c they don't need fjump statement
                {
                    logging.logDebug("Line of code at end of block found!: [" + (i + 1).ToString() + ", " + compiler.parsedAssemblyList[i] + "]", logging.loggingSenderID.assemblyParser);
                    // check if next line of code contains else; if it does, jump past it until we don't hit else
                    if (compiler.parsedAssemblyList[i + 1].Contains("else"))
                    {
                        logging.logDebug("line of code to load has else in it! jumping out ...", logging.loggingSenderID.assemblyParser);
                        fjumpIndex++;
                    // find place where there is no else statement
                    TopOfEndBlockFind:
                        while (!compiler.parsedAssemblyList[fjumpIndex - 1].Contains("tblock"))
                        {
                            fjumpIndex++;
                            logging.logDebug("fji is now " + fjumpIndex, logging.loggingSenderID.assemblyParser);
                        }
                        if (compiler.parsedAssemblyList[fjumpIndex].Contains("else"))
                        {
                            fjumpIndex++;
                            goto TopOfEndBlockFind;
                        }
                        else
                        {
                            logging.logDebug("place with no else statment found! found else before", logging.loggingSenderID.assemblyParser);
                            logging.logDebug("[" + (fjumpIndex + 1).ToString() + ", " + compiler.parsedAssemblyList[fjumpIndex] + "]", logging.loggingSenderID.assemblyParser);
                            compiler.parsedAssemblyList[i] += " fjump " + (fjumpIndex + 1).ToString();
                        }
                    }
                    else
                    {
                        logging.logDebug("place with no else statment found! didnt find else before", logging.loggingSenderID.assemblyParser);
                        logging.logDebug(compiler.parsedAssemblyList[fjumpIndex + 1], logging.loggingSenderID.assemblyParser);
                        compiler.parsedAssemblyList[i] += " fjump " + (fjumpIndex + 2).ToString();
                    }
                }
            }
            #endregion

            logging.logDebug("dump after adding branch code part 2", logging.loggingSenderID.assemblyParser);
            for (int i = 0; i < compiler.parsedAssemblyList.Count; i++)
            {
                logging.logDebug("[" + (i + 1).ToString() + "]: " + compiler.parsedAssemblyList[i], logging.loggingSenderID.assemblyParser);
            }

            #region removing sblock, tblock, eblock, and else compiler notes
            logging.logDebug("Updating and normalizing parsed assembly code ...", logging.loggingSenderID.assemblyParser);
        TopOfRemoveBlockCompilerNotes:
            for (int i = 0; i < compiler.parsedAssemblyList.Count; i++)
            {
                if (compiler.parsedAssemblyList[i].Contains("tblock"))
                {
                    // find length of tblock string WITH ID
                    int tempBlockID = functionLibrary.AcquireBlockID(compiler.parsedAssemblyList[i], (compiler.parsedAssemblyList[i].IndexOf("tblock")) + 6);
                    compiler.parsedAssemblyList[i] = compiler.parsedAssemblyList[i].Remove((compiler.parsedAssemblyList[i].IndexOf("tblock")), (7 + tempBlockID.ToString().Length)); // 7 for tblock + whitespace
                    logging.logDebug("line of code " + (i + 1).ToString() + " updated!: " + compiler.parsedAssemblyList[i], logging.loggingSenderID.assemblyParser);
                    goto TopOfRemoveBlockCompilerNotes;
                }
                if (compiler.parsedAssemblyList[i].Contains("sblock"))
                {
                    // find length of tblock string WITH ID
                    int tempBlockID = functionLibrary.AcquireBlockID(compiler.parsedAssemblyList[i], (compiler.parsedAssemblyList[i].IndexOf("sblock")) + 6);
                    compiler.parsedAssemblyList[i] = compiler.parsedAssemblyList[i].Remove((compiler.parsedAssemblyList[i].IndexOf("sblock")), (7 + tempBlockID.ToString().Length)); // 7 for tblock + whitespace
                    logging.logDebug("line of code " + (i + 1).ToString() + " updated!: " + compiler.parsedAssemblyList[i], logging.loggingSenderID.assemblyParser);
                    goto TopOfRemoveBlockCompilerNotes;
                }
                if (compiler.parsedAssemblyList[i].Contains("eblock"))
                {
                    // find length of tblock string WITH ID
                    int tempBlockID = functionLibrary.AcquireBlockID(compiler.parsedAssemblyList[i], (compiler.parsedAssemblyList[i].IndexOf("eblock")) + 6);
                    compiler.parsedAssemblyList[i] = compiler.parsedAssemblyList[i].Remove((compiler.parsedAssemblyList[i].IndexOf("eblock")), (7 + tempBlockID.ToString().Length)); // 7 for tblock + whitespace
                    logging.logDebug("line of code " + (i + 1).ToString() + " updated!: " + compiler.parsedAssemblyList[i], logging.loggingSenderID.assemblyParser);
                    goto TopOfRemoveBlockCompilerNotes;
                }
                if (compiler.parsedAssemblyList[i].Contains("else"))
                {
                    compiler.parsedAssemblyList[i] = compiler.parsedAssemblyList[i].Remove((compiler.parsedAssemblyList[i].IndexOf("else")), 5); // 5 for else length + 1 (whitespace)
                    logging.logDebug("line of code " + (i + 1).ToString() + " updated!: " + compiler.parsedAssemblyList[i], logging.loggingSenderID.assemblyParser);

                    goto TopOfRemoveBlockCompilerNotes;
                }
            }

            #endregion
            #endregion
        }
    }
}
