﻿using System;
using System.Collections.Generic;
using System.IO;

namespace DRCHLLC_Core_Compiler
{
    class importer
    {
        /*
         * This class will contain all functions and information needed importing additional
         * information, resources, and extensions into the compiler and into the environment
         * 
         * Created 9/14/2016 by AGuyWhoIsBored
         * 
         */

        // resources will be loaded on program launch
        // resources (for now) will be located at program directory + /resources
        // in the ~\resources folder, there will be a ref.drchllc file that will contain the references 
        //      to loading all of the extensions
        // for the reference file, you only need the name of the file. Resources MUST be in the same folder as the
        // reference file!

        // resources will be loaded one file at a time
        // resource loading and order / queue management will be handled by other methods
        // how resources will be added and merged into compiler environment (step by step)
        // copy file to internal buffers > assign global runtime resource ID & ext. ID
        // > syntax check all function data / code > (if successful) > load in and merge function(s) syntax tables
        // > load in and merge functon token(s) >load in and merge function code > complete

        // INTERNAL: REDO THIS CLASS ALSO! 

        public static bool ext_RC40E = false;

        public static Dictionary<int, string> loadedResourcesList = new Dictionary<int, string>(); // <globalResourceID, resourceName>
        public static int resourceIDCounter = 0; // 0-based index

        public static string[] resourceTTokens = new string[0];
        public static string[] resourceTValues = new string[0];
        public static string[] resourceTLineOfCodeValue = new string[0];
        public static int resourceTCount = 0;

        // main entry point for importing and loading all extensions
        public static void initialize()
        {
            // ******** NEED TO ADD FUNCTION SUPPORT INTO LANGUAGE FIRST, IMPORTING WILL BE PUT ON HOLD ********
            // exit out in MainUI.cs

            // MAKE SURE EXTENSIONS ARE NOT LOADED WITH THE SAME NAME!!

            logging.logDebug("Begin extension loading ...", logging.loggingSenderID.importer, true);
            logging.logDebug("Starting internal resource importing ...", logging.loggingSenderID.importer, true);
            /* RC40E */
            rc40e.checkExtensionStatus();


            // main importer code
            #region importing code
            logging.logDebug("Starting external resource importing ...", logging.loggingSenderID.importer, true);
            logging.logDebug("Loading resource reference file ...", logging.loggingSenderID.importer);
            if (!File.Exists(Program.resourceFolderLocation + "\\refs.drchllc"))
            { logging.logDebug("No resource reference file found! Skipping external importing ...", logging.loggingSenderID.importer, true); }
            else if (new FileInfo(Program.resourceFolderLocation + "\\refs.drchllc").Length == 0) { logging.logDebug("Resource reference file is empty! Skipping importing ...", logging.loggingSenderID.importer); }
            else
            {
                logging.logDebug("Resource reference file found! Contains " + File.ReadAllLines(Program.resourceFolderLocation + "\\refs.drchllc").Length.ToString() + " lines / imports", logging.loggingSenderID.importer);
                StreamReader sr = new StreamReader(Program.resourceFolderLocation + "\\refs.drchllc");
                for (int a = 0; a < File.ReadAllLines(Program.resourceFolderLocation + "\\refs.drchllc").Length; a++)
                {
                    string extFileName = sr.ReadLine();
                    logging.logDebug("Loading resource '" + extFileName + "' ...", logging.loggingSenderID.importer, true);
                    if (!File.Exists(Program.resourceFolderLocation + "\\" + extFileName + ".drchllc")) { logging.logDebug("Resource '" + extFileName + ".drchllc' not found! Skipping import ...", logging.loggingSenderID.importer); }
                    else if (new FileInfo(Program.resourceFolderLocation + "\\" + extFileName + ".drchllc").Length == 0) { logging.logDebug("Resource '" + extFileName + ".drchllc' is empty! Skipping import ...", logging.loggingSenderID.importer); }
                    else
                    {
                        // actually begin importing process here
                        // move all contents of file to buffer
                        #region moving contents of file to buffer
                        List<string> resourceFileList = new List<string>();
                        StreamReader sr1 = new StreamReader(Program.resourceFolderLocation + "\\" + extFileName + ".drchllc");
                        for (int b = 0; b < File.ReadAllLines(Program.resourceFolderLocation + "\\" + extFileName + ".drchllc").Length; b++)
                        {
                            resourceFileList.Add(sr1.ReadLine());
                        }
                        sr1.Close();
                        logging.logDebug("Moved resource file to buffer", logging.loggingSenderID.importer);
                        logging.logDebug("Removing non-code objects from buffer ...", logging.loggingSenderID.importer);

                        for (int b = 0; b < resourceFileList.Count; b++)
                        {
                            if (resourceFileList[b].Contains("//")) { resourceFileList[b] = resourceFileList[b].Remove(resourceFileList[b].IndexOf("//"), resourceFileList[b].Length - resourceFileList[b].IndexOf("//")); }
                        }

                        for (int b = 0; b < resourceFileList.Count; b++)
                        {
                            Predicate<string> p = new Predicate<string>(functionLibrary.checkForZero);
                            if (resourceFileList[b] == string.Empty) { resourceFileList.RemoveAll(p); }
                        }

                        // add a ' ' at the end of each line for proper lexing
                        for (int b = 0; b < resourceFileList.Count; b++)
                        {
                            resourceFileList[b] = resourceFileList[b] + " ";
                        }

                        logging.logDebug("Dumping buffer ...", logging.loggingSenderID.importer);

                        for (int b = 0; b < resourceFileList.Count; b++)
                        {
                            logging.logDebug(b.ToString() + ": [" + resourceFileList[b] + "]", logging.loggingSenderID.importer);
                        }
                        #endregion

                        // assigning global runtime resource ID and extension ID to resource
                        #region assigning ID's
                        loadedResourcesList.Add(resourceIDCounter, resourceFileList[0]);
                        resourceIDCounter++;
                        logging.logDebug("Resource named '" + resourceFileList[0] + "' assigned global resource identifier " + (resourceIDCounter - 1), logging.loggingSenderID.importer);
                        resourceFileList.RemoveAt(0);
                        #endregion

                        // lexing and tokenizing extension file
                        #region tokenizing
                        logging.logDebug("Tokenizing ...", logging.loggingSenderID.importer);
                        for (int b = 0; b < resourceFileList.Count; b++)
                        {
                            logging.logDebug("Starting LexData on line " + b.ToString(), logging.loggingSenderID.importer);
                            compiler.LexData(resourceFileList[b], (b + 1).ToString(), 1);
                        }

                        // remove invalids and dump arrays
                        logging.logDebug("Removing invalid tokens ...", logging.loggingSenderID.importer);

                        int invalidTokenCount = 0;
                        for (int b = 0; b < resourceTCount; b++)
                        {
                            if (resourceTTokens[b].Contains("-1"))
                            {
                                invalidTokenCount++;
                                //logging.logDebug("Array location " + i.ToString() + " detected with invalid token", logging.loggingSenderID.compiler);
                            }
                        }
                        int removeOffset = 0;
                        int newTokenValueCount = resourceTCount - invalidTokenCount;
                        while (invalidTokenCount != 0)
                        {
                            bool enableRemove = true;
                            for (int b = 0; b < resourceTCount - removeOffset; b++)
                            {
                                if (resourceTTokens[b].Contains("-1") && enableRemove == true)
                                {
                                    // this enableRemove variable exists to be able to remove one at a time
                                    enableRemove = false;
                                    removeOffset++;
                                    resourceTTokens = functionLibrary.RemoveElementFromArray(resourceTTokens, b);
                                    resourceTValues = functionLibrary.RemoveElementFromArray(resourceTValues, b);
                                    resourceTLineOfCodeValue = functionLibrary.RemoveElementFromArray(resourceTLineOfCodeValue, b);
                                    invalidTokenCount--;
                                }
                            }
                        }

                        logging.logDebug("Tokenizing complete - dumping arrays", logging.loggingSenderID.importer);
                        for (int b = 0; b < newTokenValueCount; b++)
                        {
                            logging.logDebug(b.ToString() + ": [" + resourceTLineOfCodeValue[b] + "," + resourceTTokens[b] + "," + resourceTValues[b] + "]", logging.loggingSenderID.importer);
                        }
                        #endregion

                        // check syntax of all function definitions and data in file
                        #region syntax check
                        syntax.resourceSyntaxCheck();
                        #endregion

                        // we will land here if syntax doesn't catch anything
                        #region merging function tokens

                        #endregion

                    }
                }

                sr.Close();
                logging.logDebug("Importing complete!", logging.loggingSenderID.importer, true);
            }
            #endregion
        }
    }
}
