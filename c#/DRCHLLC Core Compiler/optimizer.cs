﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DRCHLLC_Core_Compiler
{
    class optimizer
    {
        /*
         * This class will contain all functions and information required to 
         * be able to perform code optimizations on the input code.
         * 
         * Created 2/14/2017 by AGuyWhoIsBored
         * 
         */


        // MAKE SURE WE THOROUGHLY DEBUG OPTIMIZATIONS!!!
        public static void performOptimizations()
        {
            // here is where we'll perform all of the optimizations the user has requested, and some that the user hasn't requested :p
            if (compiler.enableOptimizations == true)
            {
                logging.logDebug("Optimizing program ...", logging.loggingSenderID.optimizer, true);
            }

            #region independent conditional branching optimization
            if (compiler.enableIndependentConditionalBranchingOptimization == false) // we have false as default generated code is already optimized for independent conditional branching!
            {
                // make sure this will work 100% of the time! do extensive debugging!
                // works for now; DEBUG EXTENSIVELY!!
                logging.logDebug("Performing conditional branching optimizations for program space ...", logging.loggingSenderID.optimizer);
                TopOfICBO:
                for (int i = 0; i < compiler.parsedAssemblyList.Count; i++)
                {
                    if (compiler.parsedAssemblyList[i].Contains("test")
                        && !compiler.parsedAssemblyList[i].Contains("NOMERGE"))
                    {
                        // check for all cases where we cannot apply this optimization
                        //logging.logDebug("[" + (i + 1).ToString() + "]: found test for cond. branch. optmztn: i-1 is: " + compiler.parsedAssemblyList[i - 1]);
                        if (!compiler.parsedAssemblyList[i - 1].Contains("%compiler%")
                            && !compiler.parsedAssemblyList[i - 1].Contains("exit")
                            && !compiler.parsedAssemblyList[i - 1].Contains("fjump")
                            && !compiler.parsedAssemblyList[i - 1].Contains("tjump"))
                        {
                            // merge test instruction with instruction before it
                            compiler.parsedAssemblyList[i - 1] = compiler.parsedAssemblyList[i - 1] + " " + compiler.parsedAssemblyList[i];
                            compiler.parsedAssemblyList[i - 1] = compiler.parsedAssemblyList[i - 1] + " NOMERGE"; // tag to make sure that we don't repeat merge
                            compiler.parsedAssemblyList.RemoveAt(i);
                            goto TopOfICBO;
                        }
                    }
                }
                // at the end, remove all 'NOMERGE' statements
                for (int i = 0; i < compiler.parsedAssemblyList.Count; i++)
                {
                    if (compiler.parsedAssemblyList[i].Contains("NOMERGE"))
                    {
                        compiler.parsedAssemblyList[i] = compiler.parsedAssemblyList[i].Replace(" NOMERGE", "");
                    }
                }
            }
            // dump after optimization

            else if (compiler.enableIndependentConditionalBranchingOptimization == true) { logging.logDebug("Performing conditional branching optimizations for speed ...", logging.loggingSenderID.optimizer); }
            #endregion

            #region instruction merging optimization
            if (compiler.enableInstructionMergingOptimizations == true && compiler.enableOptimizations == true)
            {
                // list of instructions that can be combined
                // bsr inst. + memw inst.
                // bsl inst. + memw inst.
                // ad1 inst. + memw inst.
                // sb1 inst. + memw inst.
                // not inst. + memw inst.
                logging.logDebug("Performing instruction merging optimizations ...", logging.loggingSenderID.optimizer);
                for (int i = 0; i < compiler.parsedAssemblyList.Count; i++)
                {
                    if (compiler.parsedAssemblyList[i].Contains("bsr") && compiler.parsedAssemblyList[i + 1].Contains("memw"))
                    {
                        compiler.parsedAssemblyList[i + 1] = compiler.parsedAssemblyList[i] + " " + compiler.parsedAssemblyList[i + 1];
                        compiler.parsedAssemblyList.RemoveAt(i);
                    }
                    else if (compiler.parsedAssemblyList[i].Contains("bsl") && compiler.parsedAssemblyList[i + 1].Contains("memw"))
                    {
                        compiler.parsedAssemblyList[i + 1] = compiler.parsedAssemblyList[i] + " " + compiler.parsedAssemblyList[i + 1];
                        compiler.parsedAssemblyList.RemoveAt(i);
                    }
                    else if (compiler.parsedAssemblyList[i].Contains("ad1") && compiler.parsedAssemblyList[i + 1].Contains("memw"))
                    {
                        compiler.parsedAssemblyList[i + 1] = compiler.parsedAssemblyList[i] + " " + compiler.parsedAssemblyList[i + 1];
                        compiler.parsedAssemblyList.RemoveAt(i);
                    }
                    else if (compiler.parsedAssemblyList[i].Contains("sb1") && compiler.parsedAssemblyList[i + 1].Contains("memw"))
                    {
                        compiler.parsedAssemblyList[i + 1] = compiler.parsedAssemblyList[i] + " " + compiler.parsedAssemblyList[i + 1];
                        compiler.parsedAssemblyList.RemoveAt(i);
                    }
                    else if (compiler.parsedAssemblyList[i].Contains("not") && compiler.parsedAssemblyList[i + 1].Contains("memw"))
                    {
                        compiler.parsedAssemblyList[i + 1] = compiler.parsedAssemblyList[i] + " " + compiler.parsedAssemblyList[i + 1];
                        compiler.parsedAssemblyList.RemoveAt(i);
                    }

                }
            }
            #endregion

            #region redundant code removal optimization
            if (compiler.enableRedundantCodeRemovalOptimization == true && compiler.enableOptimizations == true)
            {
                logging.logDebug("Performing redundant code removal optimizations ...", logging.loggingSenderID.optimizer);
                // optimizations to perform:
                // duplicate instruction removal optimization
                // removal of unreachable code
                // removal of var = 0 malloc instructions - this optimization will be done AFTER MAT generation, but before MAT jump generation!
                // jump consolidation optimization

                #region duplicate instruction removal optimization
                // duplicate instruction removal optimization
                TopOfDIRO:
                for (int i = 0; i < compiler.parsedAssemblyList.Count; i++)
                {
                    //logging.logDebug(i.ToString());
                    if (i != 0)
                    {
                        if (functionLibrary.RemoveWhitespace(compiler.parsedAssemblyList[i]) == functionLibrary.RemoveWhitespace(compiler.parsedAssemblyList[i - 1]))
                        {
                            //logging.logDebug("hit remove");
                            compiler.parsedAssemblyList.RemoveAt(i);
                            goto TopOfDIRO;
                        }
                    }
                }
                #endregion

                #region removal of unreachable code optimization
                // removal of unreachable code optimization
                // only case of this so far is code after exit statements (but before end of blocks)
                // other case is code after jump statements
                TopOfUCO:
                for (int i = 0; i < compiler.parsedAssemblyList.Count; i++)
                {
                    if (compiler.parsedAssemblyList[i].Contains("exit"))
                    {
                        // check if the instruction after is a tblock; if it is, ignore
                        // if not, remove the next line of code
                        try
                        {
                            if (!compiler.parsedAssemblyList[i + 1].Contains("tblock")
                                && !compiler.parsedAssemblyList[i + 1].Contains("EOF")
                                && !compiler.parsedAssemblyList[i + 1].Contains("jalloc"))
                            {
                                compiler.parsedAssemblyList.RemoveAt(i + 1);
                                goto TopOfUCO;
                            }
                        }
                        catch (Exception) { /*do nothing*/ }
                    }
                }
            }
            #endregion

            #endregion

            #region automatic function grouping
            if (compiler.enableAutomaticFunctionGrouping == true && compiler.enableOptimizations == true)
            {
                logging.logDebug("Performing automatic function grouping optimizations ...", logging.loggingSenderID.optimizer);
                // optimizations to perform:
                // automatic function grouping (ambigious I know)
                // conditional determination-result optimization (?)
                // --when if statements are executed, code in if statement executed, then disregarded
                //      if you want to run that same code with the same result, answer is disregarded, thus increasing program runtime


                // how this will work:
                // check each line of code and see if it's identical to any other line of code
                // gather this info, and if there are multiple identical lines of code next to each other
                // group those into one, remove all of the copies except one
                // and initialize a pointer to the other one (fjump?)
                #region automatic function grouping
                // initialize
                List<string> matchArray = new List<string>();
                List<string> functionGroupArrayData = new List<string>();
                List<int> arrayMatched = new List<int>(); // unnecessary, but im lazy
                int funcGroupCount = -1;
                bool searchAgain = false;

                logging.logDebug("Parsed list before:", logging.loggingSenderID.optimizer);
                for (int i = 0; i < compiler.parsedAssemblyList.Count; i++)
                {
                    logging.logDebug("[" + i.ToString() + "]: " + compiler.parsedAssemblyList[i], logging.loggingSenderID.optimizer);
                }

                logging.logDebug("Searching for matches ...", logging.loggingSenderID.optimizer);
                for (int i = 0; i < compiler.parsedAssemblyList.Count; i++)
                {
                    bool addBlocker = false;
                    // search for matches - ignore ones that are itself
                    // also only record matches once
                    for (int j = 0; j < compiler.parsedAssemblyList.Count; j++)
                    {
                        if(compiler.parsedAssemblyList[i] == compiler.parsedAssemblyList[j] && i != j)
                        {
                            // add other case b/c we don't want things like "1,3" and then "3,1" - only need one entry
                            if (!matchArray.Contains(i.ToString() + "," + j.ToString()) 
                                && !matchArray.Contains(j.ToString() + "," + i.ToString()))
                            {
                                //logging.logDebug("Found new match: [" + i.ToString() + "," + j.ToString()+ "]", logging.loggingSenderID.optimizer);
                                matchArray.Add(i.ToString() + "," + j.ToString());
                                addBlocker = true;
                            }
                        }
                        else if(addBlocker)
                        {
                            //logging.logDebug("Added blocker at: [" + i.ToString() + "," + j.ToString() + "]", logging.loggingSenderID.optimizer);
                            addBlocker = false;
                        }
                    }
                }
                logging.logDebug("Dumping match table:", logging.loggingSenderID.optimizer);
                for (int i = 0; i < matchArray.Count; i++) { logging.logDebug("[" + i.ToString() + "]: " + matchArray[i], logging.loggingSenderID.optimizer); }

                // now group matches 
                // want to check i+1, j+1 to determine function matches
                TopOfFuncGroupFind:
                funcGroupCount++;
                functionGroupArrayData.Add("");
                searchAgain = false;
                string funcData = null;
                for (int i = 0; i < matchArray.Count; i++)
                {
                    #region deprecated
                    //                if (matchArray.Contains((Convert.ToInt32(matchArray[i].Remove(matchArray[i].IndexOf(","))) + 1).ToString() + "," + ((Convert.ToInt32(matchArray[i].Remove(0, matchArray[i].IndexOf(",") + 1)) + 1).ToString()))
                    //&& !arrayMatched.Contains(i) && funcData == null)
                    //                {
                    //                    searchAgain = true;
                    //                    logging.logDebug("Locked onto head of function group: [" + i.ToString() + "," + matchArray.IndexOf((Convert.ToInt32(matchArray[i].Remove(matchArray[i].IndexOf(","))) + 1).ToString() + "," + ((Convert.ToInt32(matchArray[i].Remove(0, matchArray[i].IndexOf(",") + 1)) + 1).ToString())) + "]", logging.loggingSenderID.optimizer);
                    //                    functionGroupArrayData[funcGroupCount] = functionGroupArrayData[funcGroupCount] + " " + i.ToString() + "," + matchArray.IndexOf((Convert.ToInt32(matchArray[i].Remove(matchArray[i].IndexOf(","))) + 1).ToString() + "," + ((Convert.ToInt32(matchArray[i].Remove(0, matchArray[i].IndexOf(",") + 1)) + 1).ToString()));
                    //                    funcData = functionLibrary.RemoveWhitespace(functionGroupArrayData[funcGroupCount]);
                    //                    logging.logDebug("funcdata now " + funcData, logging.loggingSenderID.optimizer);
                    //                    arrayMatched.Add(i);
                    //                }
                    //                else if (funcData != null && matchArray.Contains((Convert.ToInt32(funcData.Remove(funcData.IndexOf(","))) + 1).ToString() + "," + ((Convert.ToInt32(funcData.Remove(0, funcData.IndexOf(",") + 1)) + 1).ToString())))
                    //                {
                    //                    logging.logDebug("Found child of function group: [" + i.ToString() + "," + matchArray.IndexOf((Convert.ToInt32(funcData.Remove(funcData.IndexOf(","))) + 1).ToString() + "," + ((Convert.ToInt32(funcData.Remove(0, funcData.IndexOf(",") + 1)) + 1).ToString())) + "]", logging.loggingSenderID.optimizer);
                    //                    functionGroupArrayData[funcGroupCount] = functionGroupArrayData[funcGroupCount] + " " + i.ToString() + "," + matchArray.IndexOf((Convert.ToInt32(funcData.Remove(funcData.IndexOf(","))) + 1).ToString() + "," + ((Convert.ToInt32(funcData.Remove(0, funcData.IndexOf(",") + 1)) + 1).ToString()));
                    //                    funcData = i.ToString() + "," + matchArray.IndexOf((Convert.ToInt32(funcData.Remove(funcData.IndexOf(","))) + 1).ToString() + "," + ((Convert.ToInt32(funcData.Remove(0, funcData.IndexOf(",") + 1)) + 1).ToString()));
                    //                    logging.logDebug("funcdata now " + funcData, logging.loggingSenderID.optimizer);
                    //                    arrayMatched.Add(i);
                    //                }
                    #endregion
                    // slightly tricky
                    // **NOTE TO SELF** need to add check to stop adding onto fgad table when matches aren't found anymore

                    // initial finding
                    // optimize this later
                    if (matchArray.Contains((Convert.ToInt32(matchArray[i].Remove(matchArray[i].IndexOf(","))) + 1).ToString() + "," + ((Convert.ToInt32(matchArray[i].Remove(0, matchArray[i].IndexOf(",") + 1)) + 1).ToString()))
                        && !arrayMatched.Contains(i) && funcData == null)
                    {
                        logging.logDebug("Locked onto head of function group: [" + i.ToString() + "," + matchArray.IndexOf((Convert.ToInt32(matchArray[i].Remove(matchArray[i].IndexOf(","))) + 1).ToString() + "," + ((Convert.ToInt32(matchArray[i].Remove(0, matchArray[i].IndexOf(",") + 1)) + 1).ToString())) + "]", logging.loggingSenderID.optimizer);
                        functionGroupArrayData[funcGroupCount] = functionGroupArrayData[funcGroupCount] + i.ToString() + "," + matchArray.IndexOf((Convert.ToInt32(matchArray[i].Remove(matchArray[i].IndexOf(","))) + 1).ToString() + "," + ((Convert.ToInt32(matchArray[i].Remove(0, matchArray[i].IndexOf(",") + 1)) + 1).ToString()));
                        funcData = matchArray[matchArray.IndexOf((Convert.ToInt32(matchArray[i].Remove(matchArray[i].IndexOf(","))) + 1).ToString() + "," + ((Convert.ToInt32(matchArray[i].Remove(0, matchArray[i].IndexOf(",") + 1)) + 1).ToString()))];
                        logging.logDebug("funcdata now " + funcData, logging.loggingSenderID.optimizer);
                        arrayMatched.Add(i);
                    }
                    // adding on
                    else if (funcData != null
                        && matchArray.Contains((Convert.ToInt32(funcData.Remove(funcData.IndexOf(","))) + 1).ToString() + "," + ((Convert.ToInt32(funcData.Remove(0, funcData.IndexOf(",") + 1)) + 1).ToString())))
                    {
                        searchAgain = true;
                        logging.logDebug("Found child of function group: [" + matchArray.IndexOf((Convert.ToInt32(funcData.Remove(funcData.IndexOf(","))) + 1).ToString() + "," + ((Convert.ToInt32(funcData.Remove(0, funcData.IndexOf(",") + 1)) + 1).ToString())) + "]", logging.loggingSenderID.optimizer);
                        functionGroupArrayData[funcGroupCount] = functionGroupArrayData[funcGroupCount] + "," + matchArray.IndexOf((Convert.ToInt32(funcData.Remove(funcData.IndexOf(","))) + 1).ToString() + "," + ((Convert.ToInt32(funcData.Remove(0, funcData.IndexOf(",") + 1)) + 1).ToString()));
                        funcData = (Convert.ToInt32(funcData.Remove(funcData.IndexOf(","))) + 1).ToString() + "," + ((Convert.ToInt32(funcData.Remove(0, funcData.IndexOf(",") + 1)) + 1).ToString());
                    }

                }
                if (searchAgain) goto TopOfFuncGroupFind;
                // clean function group array data table
                for (int i = 0; i < functionGroupArrayData.Count; i++)
                {
                    if (functionGroupArrayData[i] == "") { functionGroupArrayData.RemoveAt(i); }

                }
                logging.logDebug("Dumping first pass of function group array data table ...", logging.loggingSenderID.optimizer);
                for (int i = 0; i < functionGroupArrayData.Count; i++) { logging.logDebug("[" + i.ToString() + "]: " + functionGroupArrayData[i], logging.loggingSenderID.optimizer); }
                // now need to comb through FGAD table to see if any duplicates of function found and update accordingly
                // (which will happen if more than 2 instances of same function exist in program)

                for (int i = 0; i < functionGroupArrayData.Count; i++)
                {
                    string functionHandler = functionGroupArrayData[i];


                }

                #endregion

                #region conditional determination-result optimization
                // not sure if we're going to implement this one
                #endregion
            }
            #endregion

            #region memory management optimizations
            if (compiler.enableMemoryManagementOptimizations == true && compiler.enableOptimizations == true)
            {
                // optimizations to perform:
                // memory location optimization (?) - done already for now
                // enabling of memory cache
            }
            #endregion

            #region variable clock speed optimization
            if (compiler.enableVariableClockSpeedOptimization == true && Program.compileTarget == "2" && compiler.enableOptimizations == true) // as this optimization is supported on the RC4.0 only
            {
                logging.logDebug("Performing variable clock speed optimizations ...", logging.loggingSenderID.optimizer);
                // will work on this later
            }
            #endregion

            logging.logDebug("Dumping current parsed assembly list after optimizations ...", logging.loggingSenderID.optimizer);
            for (int i = 0; i < compiler.parsedAssemblyList.Count; i++)
            {
                logging.logDebug("[" + (i + 1).ToString() + "]: " + compiler.parsedAssemblyList[i], logging.loggingSenderID.optimizer);
            }
        }
    }
}
