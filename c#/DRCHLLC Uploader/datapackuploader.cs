﻿using System.IO;
using System;

namespace DRCHLLC_Uploader
{
    class datapackuploader
    {
        /*
         * This class will contain all functions and information needed to be able
         * to generate a datapack that will upload the specified program to the appropriate targets!
         * This will GUARANTEE the integrity of the uploaded program!
         * 
         * Created 10/20/2018 by AGuyWhoIsBored
         * 
         */

        public static void generateDatapack()
        {
            string datapackNamespace = Path.GetFileNameWithoutExtension(Program.sourceFileLocation) + "[" + Program.compileTarget + "," + Program.compileCoreTarget + "]";
            string datapackRoot = Path.GetDirectoryName(Program.sourceFileLocation);
            string uploadData = "kill @e[type=minecraft:cow]" + Environment.NewLine + "kill @e[type=minecraft:item]" + Environment.NewLine + "say Starting DRCHLLC Uploader for Program [" + datapackNamespace + "]" + Environment.NewLine;
            int x = -1; int y = -1; int z = -1;

            logging.logDebug("Generating uploader commands for datapack...", true);
            // basically run fast clean and upload algorithm here
            if (Program.compileTarget == "1")
            {
                if (uploader.cleanFirstEnabled)
                {
                    uploadData += "#Cleaning core" + Environment.NewLine;
                    #region init for clean
                    if (Program.compileCoreTarget == "1") { x = uploader.RC30C1[0]; }
                    else if (Program.compileCoreTarget == "2") { x = uploader.RC30C2[0]; }
                    else if (Program.compileCoreTarget == "3") { x = uploader.RC30C3[0]; }
                    else if (Program.compileCoreTarget == "4") { x = uploader.RC30C4[0]; }
                    if (Program.compileCoreTarget == "1") { y = uploader.RC30C1[1]; }
                    else if (Program.compileCoreTarget == "2") { y = uploader.RC30C2[1]; }
                    else if (Program.compileCoreTarget == "3") { y = uploader.RC30C3[1]; }
                    else if (Program.compileCoreTarget == "4") { y = uploader.RC30C4[1]; }
                    if (Program.compileCoreTarget == "1") { z = uploader.RC30C1[2]; }
                    else if (Program.compileCoreTarget == "2") { z = uploader.RC30C2[2]; }
                    else if (Program.compileCoreTarget == "3") { z = uploader.RC30C3[2]; }
                    else if (Program.compileCoreTarget == "4") { z = uploader.RC30C4[2]; }
                    #endregion
                    // part 1; cleaning goto
                    for (int i = 0; i < 20; i++)
                    {
                        if (i == 0)
                        {
                            if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2") { uploadData += "execute run fill " + x + " " + (y + 1) + " " + z + " " + (x + 28) + " " + (y + 1) + " " + z + " air" + Environment.NewLine; }
                            else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4") { uploadData += "execute run fill " + x + " " + (y + 1) + " " + z + " " + (x - 28) + " " + (y + 1) + " " + z + " air" + Environment.NewLine; }
                        }
                        else
                        {
                            if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2") { uploadData += "execute run fill " + x + " " + (y + 1) + " " + (z += 2) + " " + (x + 28) + " " + (y + 1) + " " + z + " air" + Environment.NewLine; }
                            else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4") { uploadData += "execute run fill " + x + " " + (y + 1) + " " + (z += 2) + " " + (x - 28) + " " + (y + 1) + " " + z + " air" + Environment.NewLine; }
                        }
                    }
                        // part 2; cleaning instruction
                        for (int i = 0; i < 20; i++)
                        {
                            if (i == 0)
                            {
                                if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2") { uploadData += "execute run fill " + x + " " + (y -= 3) + " " + (z -= 38) + " " + (x + 50) + " " + y + " " + z + " air" + Environment.NewLine; }
                                else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4") { uploadData += "execute run fill " + x + " " + (y -= 3) + " " + (z -= 38) + " " + (x - 50) + " " + y + " " + z + " air" + Environment.NewLine; }
                            }
                            else
                            {
                                if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2") { uploadData += "execute run fill " + x + " " + y + " " + (z += 2) + " " + (x + 50) + " " + y + " " + z + " air" + Environment.NewLine; }
                                else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4") { uploadData += "execute run fill " + x + " " + y + " " + (z += 2) + " " + (x - 50) + " " + y + " " + z + " air" + Environment.NewLine; }
                            }
                        }
                }
                #region init for upload

                if (Program.compileCoreTarget == "1") { x = uploader.RC30C1[0]; }
                else if (Program.compileCoreTarget == "2") { x = uploader.RC30C2[0]; }
                else if (Program.compileCoreTarget == "3") { x = uploader.RC30C3[0]; }
                else if (Program.compileCoreTarget == "4") { x = uploader.RC30C4[0]; }
                if (Program.compileCoreTarget == "1") { y = uploader.RC30C1[1]; }
                else if (Program.compileCoreTarget == "2") { y = uploader.RC30C2[1]; }
                else if (Program.compileCoreTarget == "3") { y = uploader.RC30C3[1]; }
                else if (Program.compileCoreTarget == "4") { y = uploader.RC30C4[1]; }
                if (Program.compileCoreTarget == "1") { z = uploader.RC30C1[2]; }
                else if (Program.compileCoreTarget == "2") { z = uploader.RC30C2[2]; }
                else if (Program.compileCoreTarget == "3") { z = uploader.RC30C3[2]; }
                else if (Program.compileCoreTarget == "4") { z = uploader.RC30C4[2]; }
                #endregion
                if (!uploader.onlyClean)
                {
                    uploadData += "#Uploading program to core" + Environment.NewLine;
                    for (int i = 0; i < uploader.machineCodeList.Count; i++)
                    {
                        StringReader sr = new StringReader(uploader.machineCodeList[i]);
                        char currentChar;

                        if (i != 0)
                        {
                            if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                            { x -= 50; y += 4; z += 2; }
                            else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                            { x += 50; y += 4; z += 2; }
                        }
                        for (int j = 0; j < 41 /*machineCodeList[i].Length*/; j++)
                        {
                            currentChar = (char)sr.Read();
                            if (currentChar == '1')
                            { uploadData += "execute run setblock " + x + " " + (y + 1) + " " + z + " redstone_wall_torch[facing=north,lit=false]" + Environment.NewLine; }
                            if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                            {
                                if (j == 14)
                                { x -= 28; y -= 4; }
                                else if (j != 40 /*machineCodeList[i].Length - 1*/)
                                { x += 2; }
                            }
                            else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                            {
                                if (j == 14)
                                { x += 28; y -= 4; }
                                else if (j != 40 /*machineCodeList[i].Length - 1*/)
                                { x -= 2; }
                            }
                        }
                    }
                }
            }
            else if (Program.compileTarget == "2")
            {
                if (uploader.cleanFirstEnabled)
                {
                    uploadData += "#Cleaning core" + Environment.NewLine;
                    #region init for clean
                    if (Program.compileCoreTarget == "1") { x = uploader.RC40C1[0]; }
                    else if (Program.compileCoreTarget == "2") { x = uploader.RC40C2[0]; }
                    else if (Program.compileCoreTarget == "3") { x = uploader.RC40C3[0]; }
                    else if (Program.compileCoreTarget == "4") { x = uploader.RC40C4[0]; }
                    if (Program.compileCoreTarget == "1") { y = uploader.RC40C1[1]; }
                    else if (Program.compileCoreTarget == "2") { y = uploader.RC40C2[1]; }
                    else if (Program.compileCoreTarget == "3") { y = uploader.RC40C3[1]; }
                    else if (Program.compileCoreTarget == "4") { y = uploader.RC40C4[1]; }
                    if (Program.compileCoreTarget == "1") { z = uploader.RC40C1[2]; }
                    else if (Program.compileCoreTarget == "2") { z = uploader.RC40C2[2]; }
                    else if (Program.compileCoreTarget == "3") { z = uploader.RC40C3[2]; }
                    else if (Program.compileCoreTarget == "4") { z = uploader.RC40C4[2]; }
                    #endregion
                    // part 1; cleaning GPU granular and clock update
                    for (int i = 0; i < 63; i++)
                    {
                        if (i == 0) { uploadData += "execute run fill " + x + " " + (y + 1) + " " + z + " " + (x - 66) + " " + (y + 1) + " " + z + " air" + Environment.NewLine; }
                        else { uploadData += "execute run fill " + x + " " + (y + 1) + " " + (z -= 2) + " " + (x - 66) + " " + (y + 1) + " " + z + " air" + Environment.NewLine; }
                    }
                    // part 2; cleaning instruction
                    for (int i = 0; i < 63; i++)
                    {
                        if (i == 0) { uploadData += "execute run fill " + x + " " + (y -= 7) + " " + (z += 124) + " " + (x - 98) + " " + y + " " + z + " air" + Environment.NewLine; }
                        else { uploadData += "execute run fill " + x + " " + y + " " + (z -= 2) + " " + (x - 98) + " " + y + " " + z + " air" + Environment.NewLine; }
                    }
                }
                #region init for upload
                if (Program.compileCoreTarget == "1") { x = uploader.RC40C1[0]; }
                else if (Program.compileCoreTarget == "2") { x = uploader.RC40C2[0]; }
                else if (Program.compileCoreTarget == "3") { x = uploader.RC40C3[0]; }
                else if (Program.compileCoreTarget == "4") { x = uploader.RC40C4[0]; }
                if (Program.compileCoreTarget == "1") { y = uploader.RC40C1[1]; }
                else if (Program.compileCoreTarget == "2") { y = uploader.RC40C2[1]; }
                else if (Program.compileCoreTarget == "3") { y = uploader.RC40C3[1]; }
                else if (Program.compileCoreTarget == "4") { y = uploader.RC40C4[1]; }
                if (Program.compileCoreTarget == "1") { z = uploader.RC40C1[2]; }
                else if (Program.compileCoreTarget == "2") { z = uploader.RC40C2[2]; }
                else if (Program.compileCoreTarget == "3") { z = uploader.RC40C3[2]; }
                else if (Program.compileCoreTarget == "4") { z = uploader.RC40C4[2]; }
                #endregion
                if (!uploader.onlyClean)
                {
                    uploadData += "#Uploading program to core" + Environment.NewLine;
                    for (int i = 0; i < uploader.machineCodeList.Count; i++)
                    {
                        StringReader sr = new StringReader(uploader.machineCodeList[i]);
                        char currentChar;
                        if (i != 0)
                        { x += 98; y += 8; z -= 2; }
                        for (int j = 0; j < 84 /*machineCodeList[i].Length*/; j++)
                        {
                            currentChar = (char)sr.Read();
                            if (currentChar == '1')
                            { uploadData += "execute run setblock " + x + " " + (y + 1) + " " + z + " redstone_wall_torch[facing=south,lit=false]" + Environment.NewLine; }
                            if (j == 33)
                            { x += 66; y -= 8; }
                            else if (j != 83 /*machineCodeList[i].Length - 1*/)
                            { x -= 2; }
                        }
                    }
                }
            }
            else if (Program.compileTarget == "3")
            {
                if (uploader.cleanFirstEnabled)
                {
                    uploadData += "#Cleaning core" + Environment.NewLine;
                    #region init for clean
                    if (Program.compileCoreTarget == "1") { x = uploader.RC50C1[0]; }
                    else if (Program.compileCoreTarget == "2") { x = uploader.RC50C2[0]; }
                    else if (Program.compileCoreTarget == "3") { x = uploader.RC50C3[0]; }
                    else if (Program.compileCoreTarget == "4") { x = uploader.RC50C4[0]; }
                    if (Program.compileCoreTarget == "1") { y = uploader.RC50C1[1]; }
                    else if (Program.compileCoreTarget == "2") { y = uploader.RC50C2[1]; }
                    else if (Program.compileCoreTarget == "3") { y = uploader.RC50C3[1]; }
                    else if (Program.compileCoreTarget == "4") { y = uploader.RC50C4[1]; }
                    if (Program.compileCoreTarget == "1") { z = uploader.RC50C1[2]; }
                    else if (Program.compileCoreTarget == "2") { z = uploader.RC50C2[2]; }
                    else if (Program.compileCoreTarget == "3") { z = uploader.RC50C3[2]; }
                    else if (Program.compileCoreTarget == "4") { z = uploader.RC50C4[2]; }
                    #endregion
                    for (int j = 0; j < 8; j++)
                    {
                        for (int i = 0; i < 16; i++)
                        {
                            if (i == 0) { uploadData += "execute run fill " + x + " " + (y + 1) + " " + z + " " + x + " " + (y + 1) + " " + (z + 94) + " air" + Environment.NewLine; }
                            else { uploadData += "execute run fill " + (x += 2) + " " + (y + 1) + " " + z + " " + x + " " + (y + 1) + " " + (z + 94) + " air" + Environment.NewLine; }
                        }
                        y -= 4; x -= 30;
                    }
                }
                #region init for upload
                if (Program.compileCoreTarget == "1") { x = uploader.RC50C1[0]; }
                else if (Program.compileCoreTarget == "2") { x = uploader.RC50C2[0]; }
                else if (Program.compileCoreTarget == "3") { x = uploader.RC50C3[0]; }
                else if (Program.compileCoreTarget == "4") { x = uploader.RC50C4[0]; }
                if (Program.compileCoreTarget == "1") { y = uploader.RC50C1[1]; }
                else if (Program.compileCoreTarget == "2") { y = uploader.RC50C2[1]; }
                else if (Program.compileCoreTarget == "3") { y = uploader.RC50C3[1]; }
                else if (Program.compileCoreTarget == "4") { y = uploader.RC50C4[1]; }
                if (Program.compileCoreTarget == "1") { z = uploader.RC50C1[2]; }
                else if (Program.compileCoreTarget == "2") { z = uploader.RC50C2[2]; }
                else if (Program.compileCoreTarget == "3") { z = uploader.RC50C3[2]; }
                else if (Program.compileCoreTarget == "4") { z = uploader.RC50C4[2]; }
                #endregion
                if (!uploader.onlyClean)
                {
                    uploadData += "#Uploading program to core" + Environment.NewLine;
                    // do [list / 16] amount of full row uploads
                    for (int i = 0; i < (uploader.machineCodeList.Count / 16); i++)
                    {
                        if (i != 0) { x -= 30; y -= 4; z -= 94; }

                        for (int k = 0; k < 16; k++)
                        {
                            StringReader sr = new StringReader(uploader.machineCodeList[16 * i + k]);
                            uploadData += "# [1] Uploading line of code " + (16 * i + k) + Environment.NewLine;
                            char currentChar;
                            for (int j = 0; j < 48 /*machineCodeList[i].Length*/; j++)
                            {
                                currentChar = (char)sr.Read();
                                if (currentChar == '1')
                                { uploadData += "execute run setblock " + x + " " + (y + 1) + " " + z + " redstone_wall_torch[facing=west,lit=false]" + Environment.NewLine; }
                                if (j != 47 /*machineCodeList[i].Length - 1*/)
                                { z += 2; }
                            }
                            if (k != 15) { x += 2; z -= 94; }
                        }
                    }
                    // do [list % 16] amount of remainder row upload
                    if ((uploader.machineCodeList.Count / 16) != 0) { x -= 30; y -= 4; z -= 94; }
                    int remCount = uploader.machineCodeList.Count % 16;
                    for (int k = 0; k < (uploader.machineCodeList.Count % 16); k++)
                    {
                        StringReader sr = new StringReader(uploader.machineCodeList[uploader.machineCodeList.Count - remCount]);
                        uploadData += "# [2] Uploading line of code " + (uploader.machineCodeList.Count - remCount) + Environment.NewLine;
                        remCount -= 1;
                        char currentChar;
                        for (int j = 0; j < 48 /*machineCodeList[i].Length*/; j++)
                        {
                            currentChar = (char)sr.Read();
                            if (currentChar == '1')
                            { uploadData += "execute run setblock " + x + " " + (y + 1) + " " + z + " redstone_wall_torch[facing=west,lit=false]" + Environment.NewLine; }
                            if (j != 47 /*machineCodeList[i].Length - 1*/)
                            { z += 2; }
                        }
                        x += 2; z -= 94;
                    }
                }
            }
            logging.logDebug("Generating datapack for program [" + datapackNamespace + "] on target [" + Program.compileTarget + "," + Program.compileCoreTarget + "]", true);

            // file manipulations
            // will generate this at the same location as the pb file
            // make the directories
            Directory.CreateDirectory(datapackRoot + "/drchllc-upload-" + datapackNamespace);
            Directory.CreateDirectory(datapackRoot + "/drchllc-upload-" + datapackNamespace + "/data");
            Directory.CreateDirectory(datapackRoot + "/drchllc-upload-" + datapackNamespace + "/data/drchllc-upload/functions");
            Directory.CreateDirectory(datapackRoot + "/drchllc-upload-" + datapackNamespace + "/data/minecraft/tags/functions");
            // make the files
            File.WriteAllText(datapackRoot + "/drchllc-upload-" + datapackNamespace + "/pack.mcmeta", @"{""pack"": {""pack_format"":1,""description"":""datapack to upload program [" + datapackNamespace + @"]""}}");
            // minecraft hooks
            File.WriteAllText(datapackRoot + "/drchllc-upload-" + datapackNamespace + "/data/minecraft/tags/functions/load.json", @"{""values"":[""drchllc-upload:init""]}");
            File.WriteAllText(datapackRoot + "/drchllc-upload-" + datapackNamespace + "/data/minecraft/tags/functions/tick.json", @"{""values"":[""drchllc-upload:check""]}");
            // uploader specific files
            File.WriteAllText(datapackRoot + "/drchllc-upload-" + datapackNamespace + "/data/drchllc-upload/functions/check.mcfunction", @"execute if entity @e[type=minecraft:cow] run function drchllc-upload:upload");
            File.WriteAllText(datapackRoot + "/drchllc-upload-" + datapackNamespace + "/data/drchllc-upload/functions/init.mcfunction", @"say Loaded DRCHLLC Uploader Datapack for Program '" + datapackNamespace + @"'");
            uploadData += "say Uploading of Program [" + datapackNamespace + "] complete!";
            File.WriteAllText(datapackRoot + "/drchllc-upload-" + datapackNamespace + "/data/drchllc-upload/functions/upload.mcfunction", uploadData);
            logging.logDebug("Databack generation complete! The datapack is located at '" + datapackRoot + "/drchllc-upload-" + datapackNamespace + "'", true);
        }
    }
}
