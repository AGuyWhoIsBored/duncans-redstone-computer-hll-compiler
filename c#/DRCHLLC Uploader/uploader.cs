﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace DRCHLLC_Uploader
{
    class uploader
    {
        // check integrity of code
        // check compatibility of code
        // upload code to compiler targets

        /*
         * This class will contain all functions and information needed to be able
         * to upload compiled programs to specific targets.
         * 
         * Created 1/1/2017 by AGuyWhoIsBored
         * 
         */

        // ****** THIS ENTIRE CLASS MAY BE DEPRECATED IN THE FUTURE DUE TO MUCH SUPERIOR DATAPACK IMPLEMENTATION! ******

        //going to try the thing where uploading will stop / pause when we lose focus of minecraft window
        [DllImport("user32.dll")]
        static extern IntPtr GetForegroundWindow();
        [DllImport("user32.dll")]
        static extern int GetWindowText(IntPtr hWnd, StringBuilder text, int count);

        // for uploading
        public static int countdownInt = 10;
        public static bool cleanFirstEnabled = false;
        public static bool onlyClean = false;
        public static bool fastUpload = false;
        public static bool alternateUploadMethod = false;
        private static double progressBarValue = 0;
        private static double cleanProgBarValue = 0;
        private static double progressionValue = 0;
        public static bool requestUploadTerminate = false;
        public static List<string> machineCodeList = new List<string>();

        static ProgressBar progress;
        static Stopwatch uploadDuration;

        static string GetActiveWindowTitle()
        {
            const int nChars = 256;
            StringBuilder Buff = new StringBuilder(nChars);
            IntPtr handle = GetForegroundWindow();
            if (GetWindowText(handle, Buff, nChars) > 0) { return Buff.ToString(); }
            return null;
        }

        public static string uploadWindowTitle = "";

        // variables for uploading
        #region upload vars
        // { x, y, z, yaw, pitch }
        // put defaults in here
        // RC3.0
        public static int[] RC30C1 = { -305, 112, 1656, 0, 0 };
        public static int[] RC30C2 = { -305, 104, 1656, 0, 0 };
        public static int[] RC30C3 = { -183, 112, 1656, 0, 0 };
        public static int[] RC30C4 = { -183, 104, 1656, 0, 0 };

        // RC4.0
        public static int[] RC40C1 = { -072, 117, -383, 180, 0 };
        public static int[] RC40C2 = { -072, 077, -383, 180, 0 };
        public static int[] RC40C3 = { -189, 117, -383, 180, 0 };
        public static int[] RC40C4 = { -189, 077, -383, 180, 0 };

        // RC5.0
        public static int[] RC50C1 = { -304, 165, -16, -90, 0 };
        public static int[] RC50C2 = { -304, 124, -16, -90, 0 };
        public static int[] RC50C3 = { -304, 165, 100, -90, 0 };
        public static int[] RC50C4 = { -304, 124, 100, -90, 0 };

        #endregion
        private static double uploadDurationTime = 0;

        public static void uploadMachineCodeToCompileTarget()
        {
            uploadDuration = new Stopwatch();
            uploadDuration.Start();
            // initialize variables
            string RC30C1s = RC30C1[0] + " " + RC30C1[1] + " " + RC30C1[2] + " " + RC30C1[3] + " " + RC30C1[4];
            string RC30C2s = RC30C2[0] + " " + RC30C2[1] + " " + RC30C2[2] + " " + RC30C2[3] + " " + RC30C2[4];
            string RC30C3s = RC30C3[0] + " " + RC30C3[1] + " " + RC30C3[2] + " " + RC30C3[3] + " " + RC30C3[4];
            string RC30C4s = RC30C4[0] + " " + RC30C4[1] + " " + RC30C4[2] + " " + RC30C4[3] + " " + RC30C4[4];
            string RC40C1s = RC40C1[0] + " " + RC40C1[1] + " " + RC40C1[2] + " " + RC40C1[3] + " " + RC40C1[4];
            string RC40C2s = RC40C2[0] + " " + RC40C2[1] + " " + RC40C2[2] + " " + RC40C2[3] + " " + RC40C2[4];
            string RC40C3s = RC40C3[0] + " " + RC40C3[1] + " " + RC40C3[2] + " " + RC40C3[3] + " " + RC40C3[4];
            string RC40C4s = RC40C4[0] + " " + RC40C4[1] + " " + RC40C4[2] + " " + RC40C4[3] + " " + RC40C4[4];
            string RC50C1s = RC50C1[0] + " " + RC50C1[1] + " " + RC50C1[2] + " " + RC50C1[3] + " " + RC50C1[4];
            string RC50C2s = RC50C2[0] + " " + RC50C2[1] + " " + RC50C2[2] + " " + RC50C2[3] + " " + RC50C2[4];
            string RC50C3s = RC50C3[0] + " " + RC50C3[1] + " " + RC50C3[2] + " " + RC50C3[3] + " " + RC50C3[4];
            string RC50C4s = RC50C4[0] + " " + RC50C4[1] + " " + RC50C4[2] + " " + RC50C4[3] + " " + RC50C4[4];


            logging.logDebug("Starting upload thread ...", true);

            #region generating operation progressbar values for upload
            // generating progress bar values for upload based on program to upload
            if (fastUpload == true && onlyClean == false)
            {
                // count the 1's in our program and add that to value
                for (int i = 0; i < machineCodeList.Count; i++)
                {
                    StringReader sr0 = new StringReader(machineCodeList[i]);
                    char currentChar;

                    for (int j = 0; j < machineCodeList[i].Length; j++)
                    {
                        currentChar = (char)sr0.Read();
                        if (currentChar == '1') { progressBarValue += 1; }
                    }
                }
            }
            if (fastUpload == false && onlyClean == false)
            {
                // multiply amount of program lines by how many bits are in each line - they should be the same
                progressBarValue += machineCodeList.Count * machineCodeList[0].Length;
            }
            if (cleanFirstEnabled == true && fastUpload == true)
            {
                if (Program.compileTarget == "1") { cleanProgBarValue += 40; }
                else if (Program.compileTarget == "2") { cleanProgBarValue += 126; }
                else if (Program.compileTarget == "3") { cleanProgBarValue += 128; }
            }
            if (cleanFirstEnabled == true && fastUpload == false)
            {
                if (Program.compileTarget == "1") { cleanProgBarValue += 820; }
                else if (Program.compileTarget == "2") { cleanProgBarValue += 5292; }
                else if (Program.compileTarget == "3") { cleanProgBarValue += 6144; }
            }
            #endregion

            try
            {
                #region MC commands and settings
                // RC3.0 upload settings
                // defaults
                // programming orientation: south
                // core 1 program memory start location: /tp AGuyWhoIsBored -305 112 1656 0 0 [we have 0 0 to have char face straight south] [change first 0 for orientation]
                // core 2 program memory start location: /tp AGuyWhoIsBored -305 104 1656 0 0
                // core 3 program memory start location: /tp AGuyWhoIsBored -183 112 1656 0 0
                // core 4 program memory start location: /tp AGuyWhoIsBored -183 104 1656 0 0

                // RC4.0 upload settings
                // defaults
                // programming orientation: north
                // core 1 program memory start location: /tp AGuyWhoIsBored -72 117 -383 180 0
                // core 2 program memory start location: /tp AGuyWhoIsBored -72 77 -383 180 0
                // core 3 program memory start location: /tp AGuyWhoIsBored -189 117 -383 180 0
                // core 4 program memory start location: /tp AGuyWhoIsBored -189 77 -383 180 0

                // placing redstone torches:
                // placing for south: /setblock ~ ~1 ~ redstone_torch 4
                // placing for north: /setblock ~ ~1 ~ redstone_torch 3
                // placing for east: /setblock ~ ~1 ~ redstone_torch 2
                // placing for west: /setblock ~ ~1 ~ redstone_torch 1
                #endregion

                #region init
                // initialization
                if (Program.compileTarget == "1")
                {
                    if (Program.compileCoreTarget == "1") sendCommandToMC("/tp @p " + RC30C1s); 
                    else if (Program.compileCoreTarget == "2") sendCommandToMC("/tp @p " + RC30C2s);
                    else if (Program.compileCoreTarget == "3") sendCommandToMC("/tp @p " + RC30C3s);
                    else if (Program.compileCoreTarget == "4") sendCommandToMC("/tp @p " + RC30C4s);
                }
                if (Program.compileTarget == "2")
                {
                    if (Program.compileCoreTarget == "1") sendCommandToMC("/tp @p " + RC40C1s);
                    else if (Program.compileCoreTarget == "2") sendCommandToMC("/tp @p " + RC40C2s);
                    else if (Program.compileCoreTarget == "3") sendCommandToMC("/tp @p " + RC40C3s);
                    else if (Program.compileCoreTarget == "4") sendCommandToMC("/tp @p " + RC40C4s);
                }
                if (Program.compileTarget == "3")
                {
                    if (Program.compileCoreTarget == "1") sendCommandToMC("/tp @p " + RC50C1s);
                    else if (Program.compileCoreTarget == "2") sendCommandToMC("/tp @p " + RC50C2s);
                    else if (Program.compileCoreTarget == "3") sendCommandToMC("/tp @p " + RC50C3s);
                    else if (Program.compileCoreTarget == "4") sendCommandToMC("/tp @p " + RC50C4s);
                }
                #endregion

                #region rc3.0 uploading
                if (Program.compileTarget == "1" && requestUploadTerminate == false)
                {
                    #region init temp vars
                    // temp vars
                    int x = -1;
                    int y = -1;
                    int z = -1;
                    int yaw = -1;
                    int pitch = -1;
                    // x
                    if (Program.compileCoreTarget == "1") { x = RC30C1[0]; }
                    else if (Program.compileCoreTarget == "2") { x = RC30C2[0]; }
                    else if (Program.compileCoreTarget == "3") { x = RC30C3[0]; }
                    else if (Program.compileCoreTarget == "4") { x = RC30C4[0]; }
                    // y
                    if (Program.compileCoreTarget == "1") { y = RC30C1[1]; }
                    else if (Program.compileCoreTarget == "2") { y = RC30C2[1]; }
                    else if (Program.compileCoreTarget == "3") { y = RC30C3[1]; }
                    else if (Program.compileCoreTarget == "4") { y = RC30C4[1]; }
                    //z
                    if (Program.compileCoreTarget == "1") { z = RC30C1[2]; }
                    else if (Program.compileCoreTarget == "2") { z = RC30C2[2]; }
                    else if (Program.compileCoreTarget == "3") { z = RC30C3[2]; }
                    else if (Program.compileCoreTarget == "4") { z = RC30C4[2]; }
                    // yaw
                    if (Program.compileCoreTarget == "1") { yaw = RC30C1[3]; }
                    else if (Program.compileCoreTarget == "2") { yaw = RC30C2[3]; }
                    else if (Program.compileCoreTarget == "3") { yaw = RC30C3[3]; }
                    else if (Program.compileCoreTarget == "4") { yaw = RC30C4[3]; }
                    // pitch
                    if (Program.compileCoreTarget == "1") { pitch = RC30C1[4]; }
                    else if (Program.compileCoreTarget == "2") { pitch = RC30C2[4]; }
                    else if (Program.compileCoreTarget == "3") { pitch = RC30C3[4]; }
                    else if (Program.compileCoreTarget == "4") { pitch = RC30C4[4]; }
                    #endregion

                    #region clearing selected core program memory
                    if (cleanFirstEnabled == true)
                    {
                        logging.logDebug("Clearing selected core program memory ...", true);
                        progress = new ProgressBar();
                        Thread.Sleep(200);
                        sendCommandToMC("[compiler]: clearing selected core program memory ...");
                        Thread.Sleep(200);

                        #region slow clean
                        if (fastUpload == false)
                        {
                            for (int i = 0; i < 20; i++)
                            {
                                if (requestUploadTerminate == false)
                                {
                                    if (i != 0)
                                    {
                                        if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                                        { sendCommandToMC("/tp @p " + (x -= 50).ToString() + " " + (y += 4).ToString() + " " + (z += 2).ToString() + " " + yaw.ToString() + " " + pitch.ToString()); }
                                        else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                                        { sendCommandToMC("/tp @p " + (x += 50).ToString() + " " + (y += 4).ToString() + " " + (z += 2).ToString() + " " + yaw.ToString() + " " + pitch.ToString()); }
                                        progress.Report(Convert.ToDouble(progressionValue += 1 / cleanProgBarValue));
                                    }
                                    for (int j = 0; j < 41 /*machineCodeList[i].Length*/; j++)
                                    {
                                        if (requestUploadTerminate == false)
                                        {
                                            Thread.Sleep(Program.commandWaitInterval); // this seems to fix integrity bug
                                            sendCommandToMC("/setblock {~} {~}1 {~} air");
                                            Thread.Sleep(Program.commandWaitInterval);
                                            if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                                            {
                                                if (j == 14)
                                                {
                                                    Thread.Sleep(Program.commandWaitInterval);
                                                    sendCommandToMC("/tp @p " + (x -= 28).ToString() + " " + (y -= 4).ToString() + " {~}" + " " + yaw.ToString() + " " + pitch.ToString());
                                                    Thread.Sleep(Program.commandWaitInterval);
                                                }
                                                else if (j != 40 /*machineCodeList[i].Length - 1*/)
                                                {
                                                    Thread.Sleep(Program.commandWaitInterval);
                                                    sendCommandToMC("/tp @p " + (x += 2).ToString() + " " + y.ToString() + " " + z.ToString() + " " + yaw.ToString() + " " + pitch.ToString());
                                                    Thread.Sleep(Program.commandWaitInterval);
                                                }
                                            }
                                            else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                                            {
                                                if (j == 14)
                                                {
                                                    Thread.Sleep(Program.commandWaitInterval);
                                                    sendCommandToMC("/tp @p " + (x += 28).ToString() + " " + (y -= 4).ToString() + " {~}" + " " + yaw.ToString() + " " + pitch.ToString());
                                                    Thread.Sleep(Program.commandWaitInterval);
                                                }
                                                else if (j != 40 /*machineCodeList[i].Length - 1*/)
                                                {
                                                    Thread.Sleep(Program.commandWaitInterval);
                                                    sendCommandToMC("/tp @p " + (x -= 2).ToString() + " " + y.ToString() + " " + z.ToString() + " " + yaw.ToString() + " " + pitch.ToString());
                                                    Thread.Sleep(Program.commandWaitInterval);
                                                }
                                            }
                                            progress.Report(Convert.ToDouble(progressionValue += 1 / cleanProgBarValue));
                                        }
                                        else { goto ExitUploadThread; } 
                                    }
                                }
                                else { goto ExitUploadThread; } 
                            }
                        }
                        #endregion

                        #region fast clean
                        if (fastUpload == true)
                        {
                            // part 1; cleaning goto
                            for (int i = 0; i < 20; i++)
                            {
                                if (requestUploadTerminate == false)
                                {
                                    if (i == 0)
                                    {
                                        if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                                        {
                                            Thread.Sleep(Program.commandWaitInterval);
                                            sendCommandToMC("/fill " + x + " " + (y + 1) + " " + z + " " + (x + 28) + " " + (y + 1) + " " + z + " air");
                                            Thread.Sleep(Program.commandWaitInterval);
                                        }
                                        else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                                        {
                                            Thread.Sleep(Program.commandWaitInterval);
                                            sendCommandToMC("/fill " + x + " " + (y + 1) + " " + z + " " + (x - 28) + " " + (y + 1) + " " + z + " air");
                                            Thread.Sleep(Program.commandWaitInterval);
                                        }
                                    }
                                    else
                                    {
                                        if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                                        {
                                            Thread.Sleep(Program.commandWaitInterval);
                                            sendCommandToMC("/fill " + x + " " + (y + 1) + " " + (z += 2) + " " + (x + 28) + " " + (y + 1) + " " + z + " air");
                                            Thread.Sleep(Program.commandWaitInterval);
                                        }
                                        else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                                        {
                                            Thread.Sleep(Program.commandWaitInterval);
                                            sendCommandToMC("/fill " + x + " " + (y + 1) + " " + (z += 2) + " " + (x - 28) + " " + (y + 1) + " " + z + " air");
                                            Thread.Sleep(Program.commandWaitInterval);
                                        }
                                    }
                                    progress.Report(Convert.ToDouble(progressionValue += 1 / cleanProgBarValue));
                                }
                                else { goto ExitUploadThread; }
                            }
                            // part 2; cleaning instruction
                            for (int i = 0; i < 20; i++)
                            {
                                if (requestUploadTerminate == false)
                                {
                                    if (i == 0)
                                    {
                                        if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                                        {
                                            Thread.Sleep(Program.commandWaitInterval);
                                            sendCommandToMC("/fill " + x + " " + (y -= 3) + " " + (z -= 38) + " " + (x + 50) + " " + (y) + " " + z + " air");
                                            Thread.Sleep(Program.commandWaitInterval);
                                        }
                                        else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                                        {
                                            Thread.Sleep(Program.commandWaitInterval);
                                            sendCommandToMC("/fill " + x + " " + (y -= 3) + " " + (z -= 38) + " " + (x - 50) + " " + (y) + " " + z + " air");
                                            Thread.Sleep(Program.commandWaitInterval);
                                        }
                                    }
                                    else
                                    {
                                        if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                                        {
                                            Thread.Sleep(Program.commandWaitInterval);
                                            sendCommandToMC("/fill " + x + " " + (y) + " " + (z += 2) + " " + (x + 50) + " " + (y) + " " + z + " air");
                                            Thread.Sleep(Program.commandWaitInterval);
                                        }
                                        else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                                        {
                                            Thread.Sleep(Program.commandWaitInterval);
                                            sendCommandToMC("/fill " + x + " " + (y) + " " + (z += 2) + " " + (x - 50) + " " + (y) + " " + z + " air");
                                            Thread.Sleep(Program.commandWaitInterval);
                                        }
                                    }
                                    progress.Report(Convert.ToDouble(progressionValue += 1 / cleanProgBarValue));
                                }
                                else { goto ExitUploadThread; }
                            }
                        }
                        #endregion
                    }
                    #endregion

                    #region init temp vars (again)
                    // temp vars
                    x = -1;
                    y = -1;
                    z = -1;
                    yaw = -1;
                    pitch = -1;
                    // x
                    if (Program.compileCoreTarget == "1") { x = RC30C1[0]; }
                    else if (Program.compileCoreTarget == "2") { x = RC30C2[0]; }
                    else if (Program.compileCoreTarget == "3") { x = RC30C3[0]; }
                    else if (Program.compileCoreTarget == "4") { x = RC30C4[0]; }
                    // y
                    if (Program.compileCoreTarget == "1") { y = RC30C1[1]; }
                    else if (Program.compileCoreTarget == "2") { y = RC30C2[1]; }
                    else if (Program.compileCoreTarget == "3") { y = RC30C3[1]; }
                    else if (Program.compileCoreTarget == "4") { y = RC30C4[1]; }
                    //z
                    if (Program.compileCoreTarget == "1") { z = RC30C1[2]; }
                    else if (Program.compileCoreTarget == "2") { z = RC30C2[2]; }
                    else if (Program.compileCoreTarget == "3") { z = RC30C3[2]; }
                    else if (Program.compileCoreTarget == "4") { z = RC30C4[2]; }
                    // yaw
                    if (Program.compileCoreTarget == "1") { yaw = RC30C1[3]; }
                    else if (Program.compileCoreTarget == "2") { yaw = RC30C2[3]; }
                    else if (Program.compileCoreTarget == "3") { yaw = RC30C3[3]; }
                    else if (Program.compileCoreTarget == "4") { yaw = RC30C4[3]; }
                    // pitch
                    if (Program.compileCoreTarget == "1") { pitch = RC30C1[4]; }
                    else if (Program.compileCoreTarget == "2") { pitch = RC30C2[4]; }
                    else if (Program.compileCoreTarget == "3") { pitch = RC30C3[4]; }
                    else if (Program.compileCoreTarget == "4") { pitch = RC30C4[4]; }
                    #endregion

                    #region uploading
                    if (onlyClean == false)
                    {
                        if (progress != null) { progress.Dispose(); }
                        progressionValue = 0;
                        logging.logDebug("Uploading program to selected core ...", true);
                        progress = new ProgressBar();
                        Thread.Sleep(100);
                        sendCommandToMC("[compiler]: Uploading program to core ...");
                        Thread.Sleep(100);

                        #region slow upload
                        if (fastUpload == false)
                        {
                            for (int i = 0; i < machineCodeList.Count; i++)
                            {
                                if (requestUploadTerminate == false)
                                {
                                    StringReader sr = new StringReader(machineCodeList[i]);
                                    char currentChar;

                                    if (i != 0)
                                    {
                                        if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                                        {
                                            sendCommandToMC("/tp @p " + (x -= 50).ToString() + " " + (y += 4).ToString() + " " + (z += 2).ToString() + " " + yaw.ToString() + " " + pitch.ToString());

                                        }
                                        else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                                        {
                                            sendCommandToMC("/tp @p " + (x += 50).ToString() + " " + (y += 4).ToString() + " " + (z += 2).ToString() + " " + yaw.ToString() + " " + pitch.ToString());
                                        }
                                    }
                                    for (int j = 0; j < 41 /*machineCodeList[i].Length*/; j++)
                                    {
                                        if (requestUploadTerminate == false)
                                        {
                                            currentChar = (char)sr.Read();
                                            Thread.Sleep(Program.commandWaitInterval);
                                            if (currentChar == '1')
                                            {
                                                sendCommandToMC("/setblock {~} {~}1 {~} redstone_torch 4");
                                            }
                                            else if (currentChar == '0')
                                            {
                                                sendCommandToMC("/setblock {~} {~}1 {~} air");
                                            }
                                            Thread.Sleep(Program.commandWaitInterval);
                                            if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                                            {
                                                // based on defaults
                                                // add 2 to x
                                                if (j == 14)
                                                {
                                                    Thread.Sleep(Program.commandWaitInterval);
                                                    sendCommandToMC("/tp @p " + (x -= 28).ToString() + " " + (y -= 4).ToString() + " {~}" + " " + yaw.ToString() + " " + pitch.ToString());
                                                    Thread.Sleep(Program.commandWaitInterval);
                                                }
                                                else if (j != 40 /*machineCodeList[i].Length - 1*/)
                                                {
                                                    Thread.Sleep(Program.commandWaitInterval);
                                                    sendCommandToMC("/tp @p " + (x += 2).ToString() + " " + y.ToString() + " " + z.ToString() + " " + yaw.ToString() + " " + pitch.ToString());
                                                    Thread.Sleep(Program.commandWaitInterval);
                                                }
                                            }
                                            else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                                            {
                                                // based on defaults
                                                // subtract 2 to x
                                                if (j == 14)
                                                {
                                                    Thread.Sleep(Program.commandWaitInterval);
                                                    sendCommandToMC("/tp @p " + (x += 28).ToString() + " " + (y -= 4).ToString() + " {~}" + " " + yaw.ToString() + " " + pitch.ToString());
                                                    Thread.Sleep(Program.commandWaitInterval);
                                                }
                                                else if (j != 40 /*machineCodeList[i].Length - 1*/)
                                                {
                                                    Thread.Sleep(Program.commandWaitInterval);
                                                    sendCommandToMC("/tp @p " + (x -= 2).ToString() + " " + y.ToString() + " " + z.ToString() + " " + yaw.ToString() + " " + pitch.ToString());
                                                    Thread.Sleep(Program.commandWaitInterval);
                                                }
                                            }
                                            //mainForm.updateProgressBarValue(mainForm.operationProgressBar, progressBarValue -= 1);
                                            progress.Report( Convert.ToDouble(progressionValue += 1 / progressBarValue));
                                        }
                                        else { goto ExitUploadThread; } // break out of for loop
                                    }
                                }
                                else { goto ExitUploadThread; } // break out of for loop
                            }
                        }
                        #endregion

                        // we just run through our machine code and if it's a 1 then we jump to that location in MC program memory and set, instead
                        // of TPing to every single location and checking one at a time
                        #region fast upload
                        if (fastUpload == true)
                        {
                            for (int i = 0; i < machineCodeList.Count; i++)
                            {
                                if (requestUploadTerminate == false)
                                {
                                    StringReader sr = new StringReader(machineCodeList[i]);
                                    char currentChar;

                                    if (i != 0)
                                    {
                                        if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                                        {
                                            x -= 50;
                                            y += 4;
                                            z += 2;

                                        }
                                        else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                                        {
                                            x += 50;
                                            y += 4;
                                            z += 2;
                                        }
                                    }
                                    for (int j = 0; j < 41 /*machineCodeList[i].Length*/; j++)
                                    {
                                        if (requestUploadTerminate == false)
                                        {
                                            currentChar = (char)sr.Read();
                                            if (currentChar == '1')
                                            {
                                                Thread.Sleep(Program.commandWaitInterval);
                                                sendCommandToMC("/tp @p " + x + " " + y + " " + z + " " + yaw + " " + pitch);
                                                Thread.Sleep(Program.commandWaitInterval);
                                                sendCommandToMC("/setblock {~} {~}1 {~} redstone_torch 4");
                                                Thread.Sleep(Program.commandWaitInterval);
                                                //mainForm.updateProgressBarValue(mainForm.operationProgressBar, progressBarValue -= 1);
                                                progress.Report(Convert.ToDouble(progressionValue += 1 / progressBarValue));
                                            }
                                            if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                                            {
                                                // based on defaults
                                                // add 2 to x
                                                if (j == 14)
                                                {
                                                    x -= 28;
                                                    y -= 4;
                                                }
                                                else if (j != 40 /*machineCodeList[i].Length - 1*/)
                                                {
                                                    x += 2;
                                                }
                                            }
                                            else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                                            {
                                                // based on defaults
                                                // subtract 2 to x
                                                if (j == 14)
                                                {
                                                    x += 28;
                                                    y -= 4;
                                                }
                                                else if (j != 40 /*machineCodeList[i].Length - 1*/)
                                                {
                                                    x -= 2;
                                                }
                                            }
                                        }
                                        else { goto ExitUploadThread; } // break out of for loop
                                    }
                                }
                                else { goto ExitUploadThread; } // break out of for loop
                            }
                        }
                        #endregion
                        goto NormalExitUploadThread;
                    }
                    #endregion
                }
                #endregion

                #region rc4.0 uploading
                if (Program.compileTarget == "2" && requestUploadTerminate == false)
                {
                    #region init temp vars
                    // temp vars
                    int x = -1;
                    int y = -1;
                    int z = -1;
                    int yaw = -1;
                    int pitch = -1;
                    // x
                    if (Program.compileCoreTarget == "1") { x = RC40C1[0]; }
                    else if (Program.compileCoreTarget == "2") { x = RC40C2[0]; }
                    else if (Program.compileCoreTarget == "3") { x = RC40C3[0]; }
                    else if (Program.compileCoreTarget == "4") { x = RC40C4[0]; }
                    // y
                    if (Program.compileCoreTarget == "1") { y = RC40C1[1]; }
                    else if (Program.compileCoreTarget == "2") { y = RC40C2[1]; }
                    else if (Program.compileCoreTarget == "3") { y = RC40C3[1]; }
                    else if (Program.compileCoreTarget == "4") { y = RC40C4[1]; }
                    //z
                    if (Program.compileCoreTarget == "1") { z = RC40C1[2]; }
                    else if (Program.compileCoreTarget == "2") { z = RC40C2[2]; }
                    else if (Program.compileCoreTarget == "3") { z = RC40C3[2]; }
                    else if (Program.compileCoreTarget == "4") { z = RC40C4[2]; }
                    // yaw
                    if (Program.compileCoreTarget == "1") { yaw = RC40C1[3]; }
                    else if (Program.compileCoreTarget == "2") { yaw = RC40C2[3]; }
                    else if (Program.compileCoreTarget == "3") { yaw = RC40C3[3]; }
                    else if (Program.compileCoreTarget == "4") { yaw = RC40C4[3]; }
                    // pitch
                    if (Program.compileCoreTarget == "1") { pitch = RC40C1[4]; }
                    else if (Program.compileCoreTarget == "2") { pitch = RC40C2[4]; }
                    else if (Program.compileCoreTarget == "3") { pitch = RC40C3[4]; }
                    else if (Program.compileCoreTarget == "4") { pitch = RC40C4[4]; }
                    #endregion

                    #region clearing selected core program memory
                    if (cleanFirstEnabled == true)
                    {
                        logging.logDebug("Clearing selected core program memory ...", true);
                        progress = new ProgressBar();
                        Thread.Sleep(200);
                        sendCommandToMC("[uploader]: clearing selected core program memory ...");
                        Thread.Sleep(200);

                        #region slow clean
                        if (fastUpload == false)
                        {
                            for (int i = 0; i < 63; i++)
                            {
                                if (requestUploadTerminate == false)
                                {
                                    if (i != 0)
                                    { sendCommandToMC("/tp @p " + (x += 98).ToString() + " " + (y += 8).ToString() + " " + (z -= 2).ToString() + " " + yaw.ToString() + " " + pitch.ToString()); }
                                    for (int j = 0; j < 84 /*machineCodeList[i].Length*/; j++)
                                    {
                                        if (requestUploadTerminate == false)
                                        {
                                            Thread.Sleep(Program.commandWaitInterval); // this seems to fix integrity bug
                                            sendCommandToMC("/setblock {~} {~}1 {~} air");
                                            Thread.Sleep(Program.commandWaitInterval);
                                            if (j == 33)
                                            {
                                                Thread.Sleep(Program.commandWaitInterval);
                                                sendCommandToMC("/tp @p " + (x += 66).ToString() + " " + (y -= 8).ToString() + " {~}" + " " + yaw.ToString() + " " + pitch.ToString());
                                                Thread.Sleep(Program.commandWaitInterval);
                                            }
                                            else if (j != 83 /*machineCodeList[i].Length - 1*/)
                                            {
                                                Thread.Sleep(Program.commandWaitInterval);
                                                sendCommandToMC("/tp @p " + (x -= 2).ToString() + " " + y.ToString() + " " + z.ToString() + " " + yaw.ToString() + " " + pitch.ToString());
                                                Thread.Sleep(Program.commandWaitInterval);
                                            }
                                            progress.Report(Convert.ToDouble(progressionValue += 1 / cleanProgBarValue));
                                        }
                                        else { goto ExitUploadThread; }
                                    }
                                }
                                else { goto ExitUploadThread; } 
                            }
                        }
                        #endregion

                        #region fast clean
                        if (fastUpload == true)
                        {
                            // part 1; cleaning GPU granular and clock update
                            for (int i = 0; i < 63; i++)
                            {
                                if (requestUploadTerminate == false)
                                {
                                    if (i == 0)
                                    {
                                        Thread.Sleep(Program.commandWaitInterval);
                                        sendCommandToMC("/fill " + x + " " + (y + 1) + " " + z + " " + (x - 66) + " " + (y + 1) + " " + z + " air");
                                        Thread.Sleep(Program.commandWaitInterval);
                                    }
                                    else
                                    {
                                        Thread.Sleep(Program.commandWaitInterval);
                                        sendCommandToMC("/fill " + x + " " + (y + 1) + " " + (z -= 2) + " " + (x - 66) + " " + (y + 1) + " " + z + " air");
                                        Thread.Sleep(Program.commandWaitInterval);
                                    }
                                    progress.Report(Convert.ToDouble(progressionValue += 1 / cleanProgBarValue));
                                }
                                else { goto ExitUploadThread; }

                            }
                            // part 2; cleaning instruction
                            for (int i = 0; i < 63; i++)
                            {
                                if (requestUploadTerminate == false)
                                {
                                    if (i == 0)
                                    {
                                        Thread.Sleep(Program.commandWaitInterval);
                                        sendCommandToMC("/fill " + x + " " + (y -= 7) + " " + (z += 124) + " " + (x - 98) + " " + (y) + " " + z + " air");
                                        Thread.Sleep(Program.commandWaitInterval);
                                    }
                                    else
                                    {
                                        Thread.Sleep(Program.commandWaitInterval);
                                        sendCommandToMC("/fill " + x + " " + (y) + " " + (z -= 2) + " " + (x - 98) + " " + (y) + " " + z + " air");
                                        Thread.Sleep(Program.commandWaitInterval);
                                    }
                                    progress.Report(Convert.ToDouble(progressionValue += 1 / cleanProgBarValue));
                                }
                                else { goto ExitUploadThread; }
                            }
                        }
                        #endregion
                    }
                    #endregion

                    #region init temp vars (again)
                    // temp vars
                    x = -1;
                    y = -1;
                    z = -1;
                    yaw = -1;
                    pitch = -1;
                    // x
                    if (Program.compileCoreTarget == "1") { x = RC40C1[0]; }
                    else if (Program.compileCoreTarget == "2") { x = RC40C2[0]; }
                    else if (Program.compileCoreTarget == "3") { x = RC40C3[0]; }
                    else if (Program.compileCoreTarget == "4") { x = RC40C4[0]; }
                    // y
                    if (Program.compileCoreTarget == "1") { y = RC40C1[1]; }
                    else if (Program.compileCoreTarget == "2") { y = RC40C2[1]; }
                    else if (Program.compileCoreTarget == "3") { y = RC40C3[1]; }
                    else if (Program.compileCoreTarget == "4") { y = RC40C4[1]; }
                    //z
                    if (Program.compileCoreTarget == "1") { z = RC40C1[2]; }
                    else if (Program.compileCoreTarget == "2") { z = RC40C2[2]; }
                    else if (Program.compileCoreTarget == "3") { z = RC40C3[2]; }
                    else if (Program.compileCoreTarget == "4") { z = RC40C4[2]; }
                    // yaw
                    if (Program.compileCoreTarget == "1") { yaw = RC40C1[3]; }
                    else if (Program.compileCoreTarget == "2") { yaw = RC40C2[3]; }
                    else if (Program.compileCoreTarget == "3") { yaw = RC40C3[3]; }
                    else if (Program.compileCoreTarget == "4") { yaw = RC40C4[3]; }
                    // pitch
                    if (Program.compileCoreTarget == "1") { pitch = RC40C1[4]; }
                    else if (Program.compileCoreTarget == "2") { pitch = RC40C2[4]; }
                    else if (Program.compileCoreTarget == "3") { pitch = RC40C3[4]; }
                    else if (Program.compileCoreTarget == "4") { pitch = RC40C4[4]; }
                    #endregion

                    #region uploading

                    #region slow upload
                    if (fastUpload == false && onlyClean == false)
                    {
                        if (progress != null) { progress.Dispose(); }
                        progressionValue = 0;
                        logging.logDebug("Uploading program to selected core ...", true);
                        progress = new ProgressBar();
                        sendCommandToMC("[uploader]: Uploading program to core ...");
                        for (int i = 0; i < machineCodeList.Count; i++)
                        {
                            if (requestUploadTerminate == false)
                            {
                                StringReader sr = new StringReader(machineCodeList[i]);
                                char currentChar;

                                if (i != 0)
                                { sendCommandToMC("/tp @p " + (x += 98).ToString() + " " + (y += 8).ToString() + " " + (z -= 2).ToString() + " " + yaw.ToString() + " " + pitch.ToString()); }
                                for (int j = 0; j < 84 /*machineCodeList[i].Length*/; j++)
                                {
                                    if (requestUploadTerminate == false)
                                    {
                                        currentChar = (char)sr.Read();
                                        Thread.Sleep(Program.commandWaitInterval);
                                        if (currentChar == '1')
                                        { sendCommandToMC("/setblock {~} {~}1 {~} redstone_torch 3"); }
                                        else if (currentChar == '0')
                                        { sendCommandToMC("/setblock {~} {~}1 {~} air"); }
                                        Thread.Sleep(Program.commandWaitInterval);
                                        if (j == 33)
                                        {
                                            Thread.Sleep(Program.commandWaitInterval);
                                            sendCommandToMC("/tp @p " + (x += 66).ToString() + " " + (y -= 8).ToString() + " {~}" + " " + yaw.ToString() + " " + pitch.ToString());
                                            Thread.Sleep(Program.commandWaitInterval);
                                        }
                                        else if (j != 83 /*machineCodeList[i].Length - 1*/)
                                        {
                                            Thread.Sleep(Program.commandWaitInterval);
                                            sendCommandToMC("/tp @p " + (x -= 2).ToString() + " " + y.ToString() + " " + z.ToString() + " " + yaw.ToString() + " " + pitch.ToString());
                                            Thread.Sleep(Program.commandWaitInterval);
                                        }
                                        progress.Report(Convert.ToDouble(progressionValue += 1 / progressBarValue));
                                    }
                                    else { goto ExitUploadThread; }
                                }
                            }
                            else { goto ExitUploadThread; }
                        }
                    }
                    #endregion

                    #region fast upload
                    if (fastUpload == true && onlyClean == false)
                    {
                        if (progress != null) { progress.Dispose(); }
                        progressionValue = 0;
                        logging.logDebug("Uploading program to selected core ...", true);
                        progress = new ProgressBar();
                        sendCommandToMC("[uploader]: Uploading program to core ...");
                        for (int i = 0; i < machineCodeList.Count; i++)
                        {
                            if (requestUploadTerminate == false)
                            {
                                StringReader sr = new StringReader(machineCodeList[i]);
                                char currentChar;

                                if (i != 0)
                                { x += 98; y += 8; z -= 2; }
                                for (int j = 0; j < 84 /*machineCodeList[i].Length*/; j++)
                                {
                                    if (requestUploadTerminate == false)
                                    {
                                        currentChar = (char)sr.Read();
                                        if (currentChar == '1')
                                        {
                                            Thread.Sleep(Program.commandWaitInterval);
                                            sendCommandToMC("/tp @p " + x + " " + y + " " + z + " " + yaw + " " + pitch);
                                            Thread.Sleep(Program.commandWaitInterval);
                                            sendCommandToMC("/setblock {~} {~}1 {~} redstone_torch 3");
                                            Thread.Sleep(Program.commandWaitInterval);
                                            progress.Report(Convert.ToDouble(progressionValue += 1 / progressBarValue));
                                        }
                                        if (j == 33)
                                        { x += 66; y -= 8; }
                                        else if (j != 83 /*machineCodeList[i].Length - 1*/)
                                        { x -= 2; }
                                    }
                                    else { goto ExitUploadThread; } // break out of for loop
                                }
                            }
                            else { goto ExitUploadThread; } // break out of for loop
                        }
                    }
                    #endregion
                    goto NormalExitUploadThread;

                    #endregion
                }
                #endregion

                #region rc5.0 uploading
                if (Program.compileTarget == "3" && requestUploadTerminate == false)
                {
                    #region init temp vars
                    // temp vars
                    int x = -1;
                    int y = -1;
                    int z = -1;
                    int yaw = -1;
                    int pitch = -1;
                    // x
                    if (Program.compileCoreTarget == "1") { x = RC50C1[0]; }
                    else if (Program.compileCoreTarget == "2") { x = RC50C2[0]; }
                    else if (Program.compileCoreTarget == "3") { x = RC50C3[0]; }
                    else if (Program.compileCoreTarget == "4") { x = RC50C4[0]; }
                    // y
                    if (Program.compileCoreTarget == "1") { y = RC50C1[1]; }
                    else if (Program.compileCoreTarget == "2") { y = RC50C2[1]; }
                    else if (Program.compileCoreTarget == "3") { y = RC50C3[1]; }
                    else if (Program.compileCoreTarget == "4") { y = RC50C4[1]; }
                    //z
                    if (Program.compileCoreTarget == "1") { z = RC50C1[2]; }
                    else if (Program.compileCoreTarget == "2") { z = RC50C2[2]; }
                    else if (Program.compileCoreTarget == "3") { z = RC50C3[2]; }
                    else if (Program.compileCoreTarget == "4") { z = RC50C4[2]; }
                    // yaw
                    if (Program.compileCoreTarget == "1") { yaw = RC50C1[3]; }
                    else if (Program.compileCoreTarget == "2") { yaw = RC50C2[3]; }
                    else if (Program.compileCoreTarget == "3") { yaw = RC50C3[3]; }
                    else if (Program.compileCoreTarget == "4") { yaw = RC50C4[3]; }
                    // pitch
                    if (Program.compileCoreTarget == "1") { pitch = RC50C1[4]; }
                    else if (Program.compileCoreTarget == "2") { pitch = RC50C2[4]; }
                    else if (Program.compileCoreTarget == "3") { pitch = RC50C3[4]; }
                    else if (Program.compileCoreTarget == "4") { pitch = RC50C4[4]; }
                    #endregion

                    #region clearing selected core program memory
                    if (cleanFirstEnabled == true)
                    {
                        logging.logDebug("Clearing selected core program memory ...", true);
                        progress = new ProgressBar();
                        Thread.Sleep(200);
                        sendCommandToMC("[uploader]: clearing selected core program memory ...");
                        Thread.Sleep(200);

                        #region slow clean
                        if (fastUpload == false)
                        {
                            for (int i = 0; i < 8; i++)
                            {
                                if (requestUploadTerminate == false)
                                {
                                    if (i != 0)
                                    { sendCommandToMC("/tp @p " + (x -= 30).ToString() + " " + (y -= 4).ToString() + " " + (z -= 94).ToString() + " " + yaw.ToString() + " " + pitch.ToString()); }

                                    for (int k = 0; k < 16; k++)
                                    {
                                        for (int j = 0; j < 48 /*machineCodeList[i].Length*/; j++)
                                        {
                                            if (requestUploadTerminate == false)
                                            {
                                                Thread.Sleep(Program.commandWaitInterval); // this seems to fix integrity bug
                                                sendCommandToMC("/setblock {~} {~}1 {~} air");
                                                Thread.Sleep(Program.commandWaitInterval);
                                                if (j != 47 /*machineCodeList[i].Length - 1*/)
                                                {
                                                    Thread.Sleep(Program.commandWaitInterval);
                                                    sendCommandToMC("/tp @p " + x.ToString() + " " + y.ToString() + " " + (z += 2).ToString() + " " + yaw.ToString() + " " + pitch.ToString());
                                                    Thread.Sleep(Program.commandWaitInterval);
                                                }
                                                progress.Report(Convert.ToDouble(progressionValue += 1 / cleanProgBarValue));
                                            }
                                            else { goto ExitUploadThread; } 
                                        }
                                        if (k != 15) sendCommandToMC("/tp @p " + (x += 2).ToString() + " " + y.ToString() + " " + (z -= 94).ToString() + " " + yaw.ToString() + " " + pitch.ToString());
                                    }
                                }
                                else { goto ExitUploadThread; }
                            }
                        }
                        #endregion
                        
                        #region fast clean
                        if (fastUpload == true)
                        {
                            for (int j = 0; j < 8; j++)
                            {
                                for (int i = 0; i < 16; i++)
                                {
                                    if (requestUploadTerminate == false)
                                    {
                                        if (i == 0)
                                        {
                                            Thread.Sleep(Program.commandWaitInterval);
                                            sendCommandToMC("/fill " + x + " " + (y + 1) + " " + z + " " + x + " " + (y + 1) + " " + (z + 94) + " air");
                                            Thread.Sleep(Program.commandWaitInterval);
                                        }
                                        else
                                        {
                                            Thread.Sleep(Program.commandWaitInterval);
                                            sendCommandToMC("/fill " + (x += 2) + " " + (y + 1) + " " + z + " " + x + " " + (y + 1) + " " + (z + 94) + " air");
                                            Thread.Sleep(Program.commandWaitInterval);
                                        }
                                        progress.Report(Convert.ToDouble(progressionValue += 1 / cleanProgBarValue));
                                    }
                                    else { goto ExitUploadThread; }
                                }
                                y -= 4; x -= 30;
                            }
                        }
                        #endregion
                    }
                    #endregion

                    #region init temp vars (again)
                    // temp vars
                    x = -1;
                    y = -1;
                    z = -1;
                    yaw = -1;
                    pitch = -1;
                    // x
                    if (Program.compileCoreTarget == "1") { x = RC50C1[0]; }
                    else if (Program.compileCoreTarget == "2") { x = RC50C2[0]; }
                    else if (Program.compileCoreTarget == "3") { x = RC50C3[0]; }
                    else if (Program.compileCoreTarget == "4") { x = RC50C4[0]; }
                    // y
                    if (Program.compileCoreTarget == "1") { y = RC50C1[1]; }
                    else if (Program.compileCoreTarget == "2") { y = RC50C2[1]; }
                    else if (Program.compileCoreTarget == "3") { y = RC50C3[1]; }
                    else if (Program.compileCoreTarget == "4") { y = RC50C4[1]; }
                    //z
                    if (Program.compileCoreTarget == "1") { z = RC50C1[2]; }
                    else if (Program.compileCoreTarget == "2") { z = RC50C2[2]; }
                    else if (Program.compileCoreTarget == "3") { z = RC50C3[2]; }
                    else if (Program.compileCoreTarget == "4") { z = RC50C4[2]; }
                    // yaw
                    if (Program.compileCoreTarget == "1") { yaw = RC50C1[3]; }
                    else if (Program.compileCoreTarget == "2") { yaw = RC50C2[3]; }
                    else if (Program.compileCoreTarget == "3") { yaw = RC50C3[3]; }
                    else if (Program.compileCoreTarget == "4") { yaw = RC50C4[3]; }
                    // pitch
                    if (Program.compileCoreTarget == "1") { pitch = RC50C1[4]; }
                    else if (Program.compileCoreTarget == "2") { pitch = RC50C2[4]; }
                    else if (Program.compileCoreTarget == "3") { pitch = RC50C3[4]; }
                    else if (Program.compileCoreTarget == "4") { pitch = RC50C4[4]; }
                    #endregion

                    #region uploading

                    #region slow upload
                    if (fastUpload == false && onlyClean == false)
                    {
                        if (progress != null) { progress.Dispose(); }
                        progressionValue = 0;
                        logging.logDebug("Uploading program to selected core ...", true);
                        progress = new ProgressBar();
                        sendCommandToMC("[uploader]: Uploading program to core ...");

                        // do [list / 16] amount of full row uploads
                        for (int i = 0; i < (machineCodeList.Count / 16); i++)
                        {
                            if (requestUploadTerminate == false)
                            {
                                if (i != 0)
                                { sendCommandToMC("/tp @p " + (x -= 30).ToString() + " " + (y -= 4).ToString() + " " + (z -= 94).ToString() + " " + yaw.ToString() + " " + pitch.ToString()); }

                                for (int k = 0; k < 16; k++)
                                {
                                    StringReader sr = new StringReader(machineCodeList[i]);
                                    char currentChar;
                                    for (int j = 0; j < 48 /*machineCodeList[i].Length*/; j++)
                                    {
                                        if (requestUploadTerminate == false)
                                        {
                                            currentChar = (char)sr.Read();
                                            Thread.Sleep(Program.commandWaitInterval); // this seems to fix integrity bug
                                            if (currentChar == '1') sendCommandToMC("/setblock {~} {~}1 {~} redstone_wall_torch[facing=west]"); // 1.13 command syntax, means that people using the uploader will have to run v1.13+ of MC
                                            else if (currentChar == '0') sendCommandToMC("/setblock {~} {~}1 {~} air");
                                            Thread.Sleep(Program.commandWaitInterval);
                                            if (j != 47 /*machineCodeList[i].Length - 1*/)
                                            {
                                                Thread.Sleep(Program.commandWaitInterval);
                                                sendCommandToMC("/tp @p " + x.ToString() + " " + y.ToString() + " " + (z += 2).ToString() + " " + yaw.ToString() + " " + pitch.ToString());
                                                Thread.Sleep(Program.commandWaitInterval);
                                            }
                                            progress.Report(Convert.ToDouble(progressionValue += 1 / progressBarValue));
                                        }
                                        else { goto ExitUploadThread; }
                                    }
                                    if (k != 15) sendCommandToMC("/tp @p " + (x += 2).ToString() + " " + y.ToString() + " " + (z -= 94).ToString() + " " + yaw.ToString() + " " + pitch.ToString());
                                }
                            }
                            else { goto ExitUploadThread; } 
                        }

                        // do [list % 16] amount of remainder row upload
                        sendCommandToMC("hit modulus component of upload");
                        sendCommandToMC("/tp @p " + (x -= 30).ToString() + " " + (y -= 4).ToString() + " " + (z -= 94).ToString() + " " + yaw.ToString() + " " + pitch.ToString());
                        int remCount = machineCodeList.Count % 16;
                        for (int k = 0; k < (machineCodeList.Count % 16); k++)
                        {
                            StringReader sr = new StringReader(machineCodeList[machineCodeList.Count - remCount]);
                            remCount -= 1;
                            char currentChar;
                            for (int j = 0; j < 48 /*machineCodeList[i].Length*/; j++)
                            {
                                if (requestUploadTerminate == false)
                                {
                                    currentChar = (char)sr.Read();
                                    Thread.Sleep(Program.commandWaitInterval); // this seems to fix integrity bug
                                    if (currentChar == '1') sendCommandToMC("/setblock {~} {~}1 {~} redstone_wall_torch[facing=west]"); // 1.13 command syntax, means that people using the uploader will have to run v1.13+ of MC
                                    else if (currentChar == '0') sendCommandToMC("/setblock {~} {~}1 {~} air");
                                    Thread.Sleep(Program.commandWaitInterval);
                                    if (j != 47 /*machineCodeList[i].Length - 1*/)
                                    {
                                        Thread.Sleep(Program.commandWaitInterval);
                                        sendCommandToMC("/tp @p " + x.ToString() + " " + y.ToString() + " " + (z += 2).ToString() + " " + yaw.ToString() + " " + pitch.ToString());
                                        Thread.Sleep(Program.commandWaitInterval);
                                    }
                                    progress.Report(Convert.ToDouble(progressionValue += 1 / progressBarValue));
                                }
                                else { goto ExitUploadThread; }
                            }
                            sendCommandToMC("/tp @p " + (x += 2).ToString() + " " + y.ToString() + " " + (z -= 94).ToString() + " " + yaw.ToString() + " " + pitch.ToString());
                        }
                    }
                    #endregion

                    #region fast upload
                    if (fastUpload == true && onlyClean == false)
                    {
                        if (progress != null) { progress.Dispose(); }
                        progressionValue = 0;
                        logging.logDebug("Uploading program to selected core ...", true);
                        progress = new ProgressBar();
                        sendCommandToMC("[uploader]: Uploading program to core ...");

                        // do [list / 16] amount of full row uploads
                        for (int i = 0; i < (machineCodeList.Count / 16); i++)
                        {
                            if (requestUploadTerminate == false)
                            {
                                if (i != 0) { x -= 30;  y -= 4;  z -= 94; }

                                for (int k = 0; k < 16; k++)
                                {
                                    StringReader sr = new StringReader(machineCodeList[i]);
                                    char currentChar;
                                    for (int j = 0; j < 48 /*machineCodeList[i].Length*/; j++)
                                    {
                                        if (requestUploadTerminate == false)
                                        {
                                            currentChar = (char)sr.Read();
                                            Thread.Sleep(Program.commandWaitInterval); // this seems to fix integrity bug
                                            if (currentChar == '1')
                                            {
                                                sendCommandToMC("/tp @p " + x + " " + y + " " + z + " " + yaw + " " + pitch);
                                                sendCommandToMC("/setblock {~} {~}1 {~} redstone_wall_torch[facing=west]");
                                            } // 1.13 command syntax, means that people using the uploader will have to run v1.13+ of MC
                                            Thread.Sleep(Program.commandWaitInterval);
                                            if (j != 47 /*machineCodeList[i].Length - 1*/)
                                            { z += 2; }
                                            progress.Report(Convert.ToDouble(progressionValue += 1 / progressBarValue));
                                        }
                                        else { goto ExitUploadThread; } 
                                    }
                                    if (k != 15) { x += 2; z -= 94; }
                                }
                            }
                            else { goto ExitUploadThread; } 
                        }

                        // do [list % 16] amount of remainder row upload
                        sendCommandToMC("hit modulus component of upload");
                        if (machineCodeList.Count / 16 != 0) { x -= 30; y -= 4; z -= 94; }
                        int remCount = machineCodeList.Count % 16;
                        for (int k = 0; k < (machineCodeList.Count % 16); k++)
                        {
                            StringReader sr = new StringReader(machineCodeList[machineCodeList.Count - remCount]);
                            remCount -= 1;
                            char currentChar;
                            for (int j = 0; j < 48 /*machineCodeList[i].Length*/; j++)
                            {
                                if (requestUploadTerminate == false)
                                {
                                    currentChar = (char)sr.Read();
                                    Thread.Sleep(Program.commandWaitInterval); // this seems to fix integrity bug
                                    if (currentChar == '1')
                                    {
                                        sendCommandToMC("/tp @p " + x + " " + y + " " + z + " " + yaw + " " + pitch);
                                        sendCommandToMC("/setblock {~} {~}1 {~} redstone_wall_torch[facing=west]");
                                    } // 1.13 command syntax, means that people using the uploader will have to run v1.13+ of MC
                                    Thread.Sleep(Program.commandWaitInterval);
                                    if (j != 47 /*machineCodeList[i].Length - 1*/)
                                    { z += 2; }
                                    progress.Report(Convert.ToDouble(progressionValue += 1 / progressBarValue));
                                }
                                else { goto ExitUploadThread; }
                            }
                            x += 2; z -= 94;
                        }
                    }
                    #endregion
                    goto NormalExitUploadThread;

                    #endregion
                }
            #endregion              

            NormalExitUploadThread:
                uploadDuration.Stop();
                uploadDurationTime = uploadDuration.ElapsedMilliseconds / 1000.0;
                uploadDurationTime = Math.Round(uploadDurationTime, 2);
                progress.Dispose();
                logging.logDebug("Upload complete! Took " + uploadDurationTime.ToString() + " seconds!", true);
                Thread.Sleep(100);
                sendCommandToMC("[uploader]: Upload successfully completed! Took " + uploadDurationTime.ToString() + " seconds!");
                uploadDurationTime = 0;
                requestUploadTerminate = false;
                countdownInt = 10;

            ExitUploadThread:
                logging.logDebug("Upload thread called exit. Exiting gracefully ...");
                Thread.Sleep(100);
            }
            catch (Exception e)
            { Console.WriteLine("An error occured while uploading: " + e.ToString()); }

        }

        private static void sendCommandToMC(string command)
        {
            SendKeys.SendWait("t");
            Thread.Sleep(Program.commandSendInterval);
            SendKeys.SendWait(command);
            Thread.Sleep(Program.commandSendInterval);
            SendKeys.SendWait("{ENTER}");
        }
    }
}
