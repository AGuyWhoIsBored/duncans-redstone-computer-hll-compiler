﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace drchllc_core_universal
{
    class compiler
    {
        /*
         * This class will contain all functions and information needed to be able to 
         * initiate and complete a compile job from ARCISS source code to ARCISS assembly / ARCISS program binaries.
         * 
         * Created 9/5/2016 by AGuyWhoIsBored
         * ARCISS v1.1 compiler development started 4/21/2018
         * 
         */

        public static List<string> sourceCodeBuffer = new List<string>();
        public static string compiledProgramName = "";

        // primary token list
        public static string[] tokenTokensArray = new string[0];
        public static string[] tokenValuesArray = new string[0];
        public static string[] tokenLineOfCodeValueArray = new string[0];
        public static int tokenValueCount = 0;

        // secondary token list
        public static string[] linkTokenTokensArray = new string[0];
        public static string[] linkTokenValuesArray = new string[0];
        public static string[] linkTokenLOCodeArray = new string[0];
        public static int linkTokenValueCount = 0;

        // ALL function variables (program-specific functions & imported functions will go here)
        // follow importer style

        // optimization flags
        public static bool enableOptimizations = false;
        public static bool enableVariableClockSpeedOptimization = false;
        public static bool enableIndependentConditionalBranchingOptimization = false;
        public static bool enableRedundantCodeRemovalOptimization = false;
        public static bool enableAutomaticFunctionGrouping = false;
        public static bool enableMemoryManagementOptimizations = false;
        public static bool enableInstructionMergingOptimizations = false;

        // memory allocation table arrays
        public static int[] MATAddresses = new int[0];              // address / line of code to goto [in case of jump]
        public static string[] MATAddressPointers = new string[0];  // pointer to address; the variable
        public static int[] MemoryAllocationType = new int[0];      // define here whether it's a variable (and its type) or a jump label (and its type) 
        // MemoryAllocationTypes:
        //      1 - global variable
        //      2 - global jump location
        //      3 - local variable
        //      4 - function jump (for function calls)

        public static List<string> parsedAssemblyList = new List<string>();

        // syntax arrays
        public static string[] syntaxFunctionTableKeys = null;
        public static string[] syntaxFunctionTableValues = null;

        public static void dumpInitializedData()
        {
            // mostly dumping available compiler tables
            // dump loaded extensions
            logging.logDebug("The following modules and extensions are loaded for use in program compilation:", logging.loggingSenderID.compiler, true);
            for (int i = 0; i < importer.resourceIDCounter; i++)
            { logging.logDebug(i + ": " + importer.loadedResourcesList[i], logging.loggingSenderID.compiler, true); }

            // dump loaded & imported tokens
            if (tokens.importedTokenTable.Length != 0)
            {
                logging.logDebug("The following tokens have been loaded for use in program compilation:", logging.loggingSenderID.compiler);
                for (int i = 0; i < tokens.importedTokenTable.Length; i++)
                { logging.logDebug("[" + i + "," + tokens.convertInt(tokens.importedTokenTable[i]) + "]: " + tokens.importedTokenTable[i], logging.loggingSenderID.compiler); }
            }

            // dump loaded syntax tables
            if (syntax.syntaxFunctionTables.Count != 0)
            {
                logging.logDebug("The following syntax tables have been loaded for use in program compilation:", logging.loggingSenderID.compiler);
                syntaxFunctionTableKeys = syntax.syntaxFunctionTables.Keys.ToArray(); syntaxFunctionTableValues = syntax.syntaxFunctionTables.Values.ToArray();
                for (int i = 0; i < syntax.syntaxFunctionTables.Count; i++)
                { logging.logDebug("[" + syntaxFunctionTableKeys[i] + "]: " + syntaxFunctionTableValues[i], logging.loggingSenderID.compiler); }
            }
        }

        public static void compileFromSource()
        {
            logging.startStopwatch();
            logging.logDebug("Now is " + DateTime.Now, logging.loggingSenderID.compiler);
            logging.logDebug("Compiling with DRCHLLC v" + Program.compilerVersion, logging.loggingSenderID.compiler);
            logging.logDebug("Compiler supports ARCISS v" + Program.compilerLanguageVersion, logging.loggingSenderID.compiler);
            logging.logDebug("Compiler arguments: [" + Program.userCommand + "]", logging.loggingSenderID.compiler);
            logging.logDebug("Compile job from source started", logging.loggingSenderID.compiler, true);
            try
            {

                compiledProgramName = Path.GetFileNameWithoutExtension(Program.sourceFileLocation);
                StreamReader sr = new StreamReader(Program.sourceFileLocation);
                logging.logDebug("File reader opened on file " + Program.sourceFileLocation, logging.loggingSenderID.compiler);
                string fileHeader = sr.ReadLine();
                logging.logDebug("Checking if selected file is a redstone computer compiler target file ...", logging.loggingSenderID.compiler);
                // this code seems a little bit everywhere, see if we can compact / make more succinct
                if (fileHeader != "#RCCompileTarget")
                {
                    // exit compile method
                    logging.logDebug("File detected as non-redstone computer compiler target file! Did you forget to add #RCCompilerTarget?", logging.loggingSenderID.compiler);
                    logging.logError("0.0", "Compile thread aborted! Reason: file detected as non-redstone computer compiler target file", logging.loggingSenderID.compiler);
                    Console.ReadLine();
                }
                else
                {
                    logging.logDebug("File detected as redstone computer compiler target file", logging.loggingSenderID.compiler);
                    // ADD BACK #RCCompileTarget header FOR NOW
                    sourceCodeBuffer.Add("#RCCompileTarget");
                    // "import" v1.1 core - this is so that we have a resourceID for custom functions in the program
                    importer.loadedResourcesList.Add(importer.resourceIDCounter, "ARCISS_v1.1"); importer.resourceIDCounter++;

                    // begin compile
                    logging.logDebug("Beginning compile ...", logging.loggingSenderID.compiler, true);
                    logging.logDebug(File.ReadAllLines(Program.sourceFileLocation).Length.ToString() + " lines of code detected in initial file", logging.loggingSenderID.compiler, true);

                    #region Compile - stage 01 [Moving data into buffers]
                    logging.logDebug("Moving all data from file into buffer ...", logging.loggingSenderID.compiler, true);
                    string lineOfText;
                    int counter = 1; // b/c of added #RCCompileTarget header
                    logging.logDebug("0: [#RCCompileTarget]", logging.loggingSenderID.compiler);
                    while ((lineOfText = sr.ReadLine()) != null)
                    {
                        // this starts with line #2 of text
                        sourceCodeBuffer.Add(lineOfText);
                        logging.logDebug(counter.ToString() + ": [" + lineOfText + "]", logging.loggingSenderID.compiler);
                        counter++;
                    }
                    sr.Close();
                    logging.logDebug("File reader closed and resources released", logging.loggingSenderID.compiler);
                    #endregion

                    #region Compile - stage 02 [removing all non-code objects]
                    logging.logDebug("Removing all non-code objects ...", logging.loggingSenderID.compiler, true);
                    logging.logDebug("Removing all comments from buffer ... ", logging.loggingSenderID.compiler);
                    // remove all commented lines from all code before syntax checking
                    for (int i = 0; i < sourceCodeBuffer.Count; i++)
                    {
                        if (sourceCodeBuffer[i].Contains("//"))
                        {
                            sourceCodeBuffer[i] = sourceCodeBuffer[i].Remove(sourceCodeBuffer[i].IndexOf("//"));
                            if (Program.moreVerboseDebug) logging.logDebug("Comments removed from line " + i, logging.loggingSenderID.compiler);
                        }
                    }
                    logging.logDebug("Removing all empty lines of code from buffer ... ", logging.loggingSenderID.compiler);
                    sourceCodeBuffer.RemoveAll(new Predicate<string>(functionLibrary.checkForZero));
                    // remove trailing whitespaces
                    for (int i = 0; i < sourceCodeBuffer.Count; i++) { sourceCodeBuffer[i] = sourceCodeBuffer[i].Trim(); }
                    logging.logDebug("Sanitized buffer now contains " + sourceCodeBuffer.Count.ToString() + " lines of code:", logging.loggingSenderID.compiler);
                    for (int i = 0; i < sourceCodeBuffer.Count; i++) { logging.logDebug(i + ": [" + sourceCodeBuffer[i] + "]", logging.loggingSenderID.compiler); } // make sure our lines of code match up with any error LOCs that we provide
                    #endregion

                    #region Compile - stage 03 [build available resources list and link requested extensions]
                    logging.logDebug("Checking for requested extension imports on master code base ...", logging.loggingSenderID.compiler, true);
                    while (sourceCodeBuffer[1].Contains("import"))
                    {
                        string extRequest = sourceCodeBuffer[1].Remove(sourceCodeBuffer[1].IndexOf("import"), 7).Trim();
                        importer.checkExtensionStatus(extRequest); sourceCodeBuffer.RemoveAt(1);
                    }
                    logging.logDebug("All extension imports resolved successfully", logging.loggingSenderID.compiler);
                    sourceCodeBuffer.RemoveAt(0); // remove #RCCompileTarget - do this so that this will work with extensions also
                    dumpInitializedData();
                    #endregion

                    #region Compile - stage 04 [Lexing and tokenizing]
                    // this stage will check through the code and give each 'block' of information a value, such as variable, number, etc.
                    logging.logDebug("Lexing master code base... ", logging.loggingSenderID.tokenizer, true);
                    logging.logDebug("Lexing data in master code base buffer ... ", logging.loggingSenderID.compiler);
                    for (int i = 0; i < sourceCodeBuffer.Count; i++)
                    {
                        logging.logDebug("Lexing line " + (i + 1).ToString() + " of code from buffer", logging.loggingSenderID.compiler);
                        LexData(sourceCodeBuffer[i], (i + 1).ToString(), 0);
                    }
                    // add in tokens from secondary code base
                    logging.logDebug("Merging secondary code base into primary code base ...", logging.loggingSenderID.linker);
                    int lastLOC = Convert.ToInt32(tokenLineOfCodeValueArray[tokenLineOfCodeValueArray.Length - 1]);
                    for (int i = 0; i < linkTokenTokensArray.Length; i++)
                    { functionLibrary.addToTokenArrays(linkTokenTokensArray[i], linkTokenValuesArray[i], (Convert.ToInt32(linkTokenLOCodeArray[i]) + lastLOC).ToString(), 0); }

                    functionLibrary.addToTokenArrays(tokens.convertInt("identifier_eof").ToString(), "$eof$", "-1", 0);
                    logging.logDebug("Lexing and tokenizing complete", logging.loggingSenderID.compiler);
                    logging.logDebug(tokenValueCount.ToString() + " tokens generated from lexer and tokenizer", logging.loggingSenderID.compiler);
                    // then output to debugger token array values before cleanup
                    if (Program.moreVerboseDebug)
                    {
                        logging.logDebug("Dumping token arrays ...", logging.loggingSenderID.compiler);
                        for (int i = 0; i < tokenValueCount; i++)
                        { logging.logDebug("Location " + i + " in token array is [" + tokenLineOfCodeValueArray[i] + "," + tokenTokensArray[i] + "," + tokenValuesArray[i] + "]", logging.loggingSenderID.compiler); }
                    }
                    // clean up token arrays (needed anymore? usually tokens are not just not added instead of being marked as invalid for later deletion)
                    logging.logDebug("Detecting invalid tokens ... ", logging.loggingSenderID.compiler);
                    int invalidTokenCount = 0;
                    for (int i = 0; i < tokenValueCount; i++)
                    {
                        if (tokenTokensArray[i] == "0")
                        { invalidTokenCount++; if (Program.moreVerboseDebug) { logging.logDebug("Array location " + i + " detected with invalid token", logging.loggingSenderID.compiler); } }
                    }

                    logging.logDebug(invalidTokenCount.ToString() + " invalid tokens detected", logging.loggingSenderID.compiler);

                    // to remove invalid entries from array properly, we'll have to remove ONE AT A TIME!
                    logging.logDebug("Removing invalid tokens ...", logging.loggingSenderID.compiler);
                    int removeOffset = 0;
                    int newTokenValueCount = tokenValueCount - invalidTokenCount;
                    while (invalidTokenCount != 0)
                    {
                        bool enableRemove = true;
                        for (int i = 0; i < tokenValueCount - removeOffset; i++)
                        {
                            if (tokenTokensArray[i] == "0" && enableRemove == true)
                            {
                                // this enableRemove variable exists to be able to remove one at a time
                                enableRemove = false;
                                removeOffset++;
                                tokenTokensArray = functionLibrary.removeElementFromArray(tokenTokensArray, i);
                                tokenValuesArray = functionLibrary.removeElementFromArray(tokenValuesArray, i);
                                tokenLineOfCodeValueArray = functionLibrary.removeElementFromArray(tokenLineOfCodeValueArray, i);
                                invalidTokenCount--;
                            }
                        }
                    }
                    logging.logDebug("Removed all invalid tokens from token arrays", logging.loggingSenderID.compiler);
                    logging.logDebug(newTokenValueCount.ToString() + " tokens in token arrays after removal of invalid tokens", logging.loggingSenderID.compiler);
                    logging.logDebug("Dumping updated master code base token arrays ...", logging.loggingSenderID.compiler);
                    for (int i = 0; i < newTokenValueCount; i++)
                    { logging.logDebug("Location " + i + " in token array is [" + tokenLineOfCodeValueArray[i] + "," + tokenTokensArray[i] + "," + tokenValuesArray[i] + "]", logging.loggingSenderID.compiler); }
                    #endregion
                    #region Compile - stage 05 [Syntax check]
                    syntax.scanMainHLL();
                    #endregion
                    #region Compile - stage 06 [HLL to assembly parsing - stage 1]
                    logging.logDebug("Beginning HLL to assembly parsing ...", logging.loggingSenderID.assemblyParser, true);
                    logging.logDebug("Starting HLL to assembly parsing stage 1 ...", logging.loggingSenderID.assemblyParser);
                    assemblyParser.parseStage1();
                    logging.logDebug("Dumping current parsed assembly list ...", logging.loggingSenderID.compiler);
                    for (int i = 0; i < parsedAssemblyList.Count; i++)
                    { logging.logDebug("[" + (i + 1).ToString() + "]: " + parsedAssemblyList[i], logging.loggingSenderID.compiler); }
                    #endregion
                    #region Compile - stage 07 [Optimizations]
                    // we're going to 'perform optimizations' even when optimizations are disabled - i.e. merging test statements
                    if (enableOptimizations)
                    {
                        logging.logDebug("Optimizing program ...", logging.loggingSenderID.optimizer, true);
                        if (enableAutomaticFunctionGrouping) { optimizer.optimizeAutomaticFunctionGrouping(); }
                        if (!enableIndependentConditionalBranchingOptimization) { optimizer.optimizeIndependentConditionalBranching(); }
                        if (enableInstructionMergingOptimizations) { optimizer.optimizeInstructionMerging(); }
                        if (enableMemoryManagementOptimizations) { optimizer.optimizeMemoryManagement(); }
                        if (enableRedundantCodeRemovalOptimization) { optimizer.optimizeRedundantCodeRemoval1(); optimizer.optimizeRedundantCodeRemoval2(); optimizer.optimizeRedundantCodeRemoval3(); }
                        if (enableVariableClockSpeedOptimization) { optimizer.optimizeVariableClockSpeed(); }

                        logging.logDebug("Dumping current parsed assembly list after optimizations ...", logging.loggingSenderID.optimizer);
                        for (int i = 0; i < parsedAssemblyList.Count; i++)
                        { logging.logDebug("[" + (i + 1).ToString() + "]: " + parsedAssemblyList[i], logging.loggingSenderID.optimizer); }
                    }

                    #endregion
                    #region Compile - stage 08 [Generating Memory Allocation Tables pt. 1]
                    // stage 8 - generating memory allocation tables - won't generate jumps!
                    logging.logDebug("Generating memory allocation tables part 1...", logging.loggingSenderID.compiler, true);
                    generateMAT1();
                    // once function finishes, dump MAT tables
                    logging.logDebug("Memory allocation table generation complete", logging.loggingSenderID.compiler);
                    logging.logDebug("Dumping memory allocation table ...", logging.loggingSenderID.compiler);
                    for (int i = 0; i < MATAddresses.Length; i++)
                    { logging.logDebug("[" + i + "]: [" + MATAddressPointers[i] + "," + MATAddresses[i].ToString() + "," + MemoryAllocationType[i].ToString() + "]", logging.loggingSenderID.compiler); }

                    #endregion
                    // you will want to modify all parsed code BEFORE this stage, as this and some stages after add branches in and can't easily be changed!
                    #region Compile - stage 09 [assembly parsing - stage 2]
                    logging.logDebug("Beginning assembly parsing stage 2 ...", logging.loggingSenderID.assemblyParser, true);
                    assemblyParser.parseStage2();
                    // once function completes, dump updated / generated assembly
                    logging.logDebug("Stage 2 of assembly parsing complete", logging.loggingSenderID.assemblyParser, true);
                    logging.logDebug("Dumping stage 2 parsed assembly list  ...", logging.loggingSenderID.compiler);
                    for (int i = 0; i < parsedAssemblyList.Count; i++)
                    { logging.logDebug("[" + (i + 1).ToString() + "]: " + parsedAssemblyList[i], logging.loggingSenderID.compiler); }

                    #endregion
                    #region Compile - stage 10 [Updating assembly code with Memory Allocation Table data / generating MAT part 2 & final last-minute assembly code generation]
                    // stage 9 - updating assembly code with memory allocation table data - will also generate jumps here
                    logging.logDebug("Generating memory allocation tables part 2 ...", logging.loggingSenderID.compiler, true);
                    generateMAT2();
                    logging.logDebug("Dumping updated memory allocation table ...", logging.loggingSenderID.compiler);
                    for (int i = 0; i < MATAddresses.Length; i++)
                    { logging.logDebug("[" + i + "]: [" + MATAddressPointers[i] + "," + MATAddresses[i].ToString() + "," + MemoryAllocationType[i].ToString() + "]", logging.loggingSenderID.compiler); }
                    logging.logDebug("Updating assembly code with memory allocation table data ...", logging.loggingSenderID.compiler, true);
                    updateAssemblyFromMAT();

                    // fix for gigano indicator light bug (and ones like it)
                    // have to have it here b/c we cannot implement this until our memory locations are implemented into the assembly
                    #region generate %forawrd% tags for RAM to output register data flow (RC5.0 ONLY)
                    if (Program.compileTarget == "3")
                    {
                        logging.logDebug("Generating last-minute RC5.0 specific RAM --> output reg %forward% tags ...", logging.loggingSenderID.assemblyParser);
                        for (int i = 0; i < parsedAssemblyList.Count; i++)
                        {
                            // a little lazy - see if we can get away with this
                            if (parsedAssemblyList[i].Contains("@output ") && parsedAssemblyList[i].Count(x => x == '$') == 2)
                            { parsedAssemblyList[i] += " %forward%"; }
                        }
                    }
                    #endregion

                    // remove @ notation
                    logging.logDebug("Removing temporary opcode identifiers ...", logging.loggingSenderID.assemblyParser); assemblyParser.removeOpcodeNotation();
                    // once assembly code parsing is done, dump updated list
                    logging.logDebug("Updated assembly code with MAT data", logging.loggingSenderID.compiler);
                    logging.logDebug("Dumping updated parsed assembly list ...", logging.loggingSenderID.compiler);
                    for (int i = 0; i < parsedAssemblyList.Count; i++) { logging.logDebug("[" + (i + 1).ToString() + "]: " + parsedAssemblyList[i], logging.loggingSenderID.compiler); }
                    #endregion
                    #region Compile - stage 11 [source to machine code only] [assembly to machine code translation]
                    if (!Program.compileOnlyAssembly)
                    { logging.logDebug("Beginning machine code generation ...", logging.loggingSenderID.compiler, true); machineCodeParser.generateMachineCode(); }
                    #endregion
                    // WILL NEED TO VERIFY INTEGRITY OF MACHINE CODE AFTER IF UPLOADING! (i.e. if length is longer than expected, or if lines of code exceed available program mem space!)
                    #region Compile - stage 12 [Compile complete! Post operations]
                    logging.logDebug("Compilation complete! This program took " + logging.getStopwatchTimeInMS() / 1000.00 + " seconds to compile!", logging.loggingSenderID.compiler, true);
                    if (Program.compileOnlyAssembly)
                    {
                        // trim all assembly code
                        for (int i = 0; i < parsedAssemblyList.Count; i++) { parsedAssemblyList[i] = parsedAssemblyList[i].Trim(); }
                        logging.logDebug(parsedAssemblyList.Count.ToString() + " lines of assembly code generated for compiler target!", logging.loggingSenderID.compiler, true);
                        // grab size of generated assembly code
                        int generatedCodeSize = 0;
                        for (int i = 0; i < parsedAssemblyList.Count; i++) { generatedCodeSize += parsedAssemblyList[i].Length; }
                        generatedCodeSize /= 8;
                        logging.logDebug("Size of compiled assembly program is " + generatedCodeSize.ToString() + " bytes", logging.loggingSenderID.compiler);
                        // calculating MD5 of program
                        StringBuilder sb = new StringBuilder();
                        foreach (string value in parsedAssemblyList) { sb.Append(value); }
                        logging.logDebug("MD5 hash of compiled assembly program is " + functionLibrary.calculateMD5Hash(sb.ToString()), logging.loggingSenderID.compiler);
                        // write compiled assembly code to file for further manipulation

                        // generate assembly helpers if selected
                        if (Program.insertAssemblyHelpers)
                        {
                            logging.logDebug("Generating assembly helpers ...", logging.loggingSenderID.compiler);
                            // generating line of code markers
                            // pad digits and add uniform line of code markers on the right-hand side
                            int digits = Convert.ToInt16(Math.Floor(Math.Log10(parsedAssemblyList.Count) + 1)); int lengthLongest = -1;
                            for (int i = 0; i < parsedAssemblyList.Count; i++) if (parsedAssemblyList[i].Length > lengthLongest) lengthLongest = parsedAssemblyList[i].Length;
                            for (int i = 0; i < parsedAssemblyList.Count; i++)
                            {
                                int spacesToAdd = lengthLongest - parsedAssemblyList[i].Length;
                                for (int j = 0; j < spacesToAdd; j++) parsedAssemblyList[i] += " ";
                                parsedAssemblyList[i] += " "; parsedAssemblyList[i] += " ";
                                parsedAssemblyList[i] += "// [" + (i + 1).ToString("D" + digits) + "]";
                            }
                            // add program name to the top
                            parsedAssemblyList.Insert(0, "// " + Path.GetFileNameWithoutExtension(Program.sourceFileLocation) + " compiled to ARCISS-3L"); parsedAssemblyList.Insert(1, "");
                            // add memory mappings
                            parsedAssemblyList.Add(""); parsedAssemblyList.Add("// ----Memory mappings auto-generated by compiler----");
                            // variables - global
                            parsedAssemblyList.Add(""); parsedAssemblyList.Add("// --Global variable mappings--");
                            for (int i = 0; i < MATAddresses.Length; i++)
                            { if (MemoryAllocationType[i] == 1) parsedAssemblyList.Add("// var [" + MATAddressPointers[i] + "] mapped to physical address [" + MATAddresses[i].ToString() + "]"); }
                            // variables - local
                            parsedAssemblyList.Add(""); parsedAssemblyList.Add("// --Local variable mappings--");
                            for (int i = 0; i < MATAddresses.Length; i++)
                            { if (MemoryAllocationType[i] == 3) parsedAssemblyList.Add("// var [" + MATAddressPointers[i] + "] mapped to physical address [" + MATAddresses[i].ToString() + "]"); }
                            // jump locations
                            parsedAssemblyList.Add(""); parsedAssemblyList.Add("// --Jump location mappings--");
                            for (int i = 0; i < MATAddresses.Length; i++)
                            { if (MemoryAllocationType[i] == 2) parsedAssemblyList.Add("// loc [" + MATAddressPointers[i] + "] mapped to line of code [" + MATAddresses[i].ToString() + "]"); }
                        }

                        // .pa extension - program assembly
                        logging.logDebug("Exported compiled assembly program to '" + Path.GetDirectoryName(Program.sourceFileLocation) + "/" + compiledProgramName + ".pa'", logging.loggingSenderID.compiler, true);
                        File.WriteAllLines(Path.GetDirectoryName(Program.sourceFileLocation) + "/" + compiledProgramName + ".pa", parsedAssemblyList);
                    }
                    else
                    {
                        logging.logDebug(machineCodeParser.machineCodeList.Count.ToString() + " lines of machine code generated for compiler target!", logging.loggingSenderID.compiler, true);
                        // grab size of generated machine code
                        int generatedCodeSize = 0;
                        for (int i = 0; i < machineCodeParser.machineCodeList.Count; i++) { generatedCodeSize += machineCodeParser.machineCodeList[i].Length; }
                        generatedCodeSize /= 8;
                        logging.logDebug("Size of compiled program is " + generatedCodeSize.ToString() + " bytes", logging.loggingSenderID.compiler);
                        // calculating MD5 of program
                        StringBuilder sb = new StringBuilder();
                        foreach (string value in machineCodeParser.machineCodeList) { sb.Append(value); }
                        logging.logDebug("MD5 hash of compiled program is " + functionLibrary.calculateMD5Hash(sb.ToString()), logging.loggingSenderID.compiler);
                        // write compiled machine code to file for further manipulation
                        // .pb extension - program binary
                        logging.logDebug("Exported compiled program to '" + Path.GetDirectoryName(Program.sourceFileLocation) + "/" + compiledProgramName + ".pb'", logging.loggingSenderID.compiler, true);
                        File.WriteAllLines(Path.GetDirectoryName(Program.sourceFileLocation) + "/" + compiledProgramName + ".pb", machineCodeParser.machineCodeList);
                    }
                    // if debug mode enabled, write debug log to text file
                    functionLibrary.writeDebugLogOut();
                    if (Program.debug) { logging.logDebug("Press [enter] to continue ...", logging.loggingSenderID.compiler, true); Console.ReadLine(); Environment.Exit(0); }
                    #endregion
                }
            }
            catch (UnauthorizedAccessException) { Console.WriteLine("The DRCHLLC Core does not have access to write the requested files to disk! The compiler will now exit."); Console.ReadLine(); }
            catch (Exception e)
            {
                Console.WriteLine("[*FATAL ERROR*]: " + e.ToString());
                Console.WriteLine("[*FATAL ERROR*]: If you see this please run the program again with the '-d' option to generate a debug log, and then file a bug report!");
                Console.WriteLine("[*FATAL ERROR*]: The program has crashed. Press [enter] to continue...");
                logging.logDebug("[*FATAL ERROR OCCURED*]: " + e.ToString(), logging.loggingSenderID.compiler);
                functionLibrary.writeDebugLogOut();
                Console.ReadLine();
                Environment.Exit(1);
            }
        }

        // consolidate stages with main compile method and then branch so we have less code to maintain
        public static void compileFromAssembly()
        {
            logging.startStopwatch();
            logging.logDebug("Now is " + DateTime.Now, logging.loggingSenderID.compiler);
            logging.logDebug("Compiler arguments: [" + Program.userCommand + "]", logging.loggingSenderID.compiler);
            logging.logDebug("Compile job from assembly started", logging.loggingSenderID.compiler, true);
            try
            {
                compiledProgramName = Path.GetFileNameWithoutExtension(Program.sourceFileLocation);
                StreamReader sr = new StreamReader(Program.sourceFileLocation);
                logging.logDebug("File reader opened on file " + Program.sourceFileLocation, logging.loggingSenderID.compiler);

                // begin compile
                logging.logDebug("Beginning compile ...", logging.loggingSenderID.compiler, true);
                logging.logDebug(File.ReadAllLines(Program.sourceFileLocation).Length.ToString() + " lines of code detected in initial file", logging.loggingSenderID.compiler, true);

                #region Compile - stage 1 [Moving all assembly into buffers]
                logging.logDebug("Moving all data from file into buffer ...", logging.loggingSenderID.compiler, true);
                string lineOfText;
                int counter = 1;
                while ((lineOfText = sr.ReadLine()) != null)
                {
                    parsedAssemblyList.Add(lineOfText + " ");
                    logging.logDebug(counter.ToString() + ": [" + lineOfText + "]", logging.loggingSenderID.compiler);
                    counter++;
                }
                sr.Close();
                logging.logDebug("File reader closed and resources released", logging.loggingSenderID.compiler);
                #endregion
                #region Compile - stage 2 [removing all non-code objects]
                logging.logDebug("Removing all non-code objects ...", logging.loggingSenderID.compiler, true);
                logging.logDebug("Removing all comments from buffer ... ", logging.loggingSenderID.compiler);
                // remove all commented lines from all code before syntax checking
                for (int i = 0; i < parsedAssemblyList.Count; i++)
                {
                    if (parsedAssemblyList[i].Contains("//"))
                    {
                        parsedAssemblyList[i] = parsedAssemblyList[i].Remove(parsedAssemblyList[i].IndexOf("//"));
                        if (Program.moreVerboseDebug) logging.logDebug("Comments removed from line " + i, logging.loggingSenderID.compiler);
                    }
                }
                logging.logDebug("Removing all empty lines of code from buffer ... ", logging.loggingSenderID.compiler);
                // for consistency
                parsedAssemblyList.RemoveAll(new Predicate<string>(functionLibrary.checkForZero));
                // remove excessive whitespaces
                for (int i = 0; i < parsedAssemblyList.Count; i++) { parsedAssemblyList[i].Trim(); }
                logging.logDebug("Buffer now contains " + parsedAssemblyList.Count.ToString() + " lines of code", logging.loggingSenderID.compiler);
                #endregion
                #region Compile - stage 3 [assembly to machine code translation]
                logging.logDebug("Beginning machine code generation ...", logging.loggingSenderID.compiler, true);
                machineCodeParser.generateMachineCode();
                #endregion
                // WILL NEED TO VERIFY INTEGRITY OF MACHINE CODE AFTER IF UPLOADING! (i.e. if length is longer than expected, or if lines of code exceed available program mem space!)
                #region Compile - stage 4 [Compile complete! Post operations]
                logging.logDebug("Compilation complete! This program took " + logging.getStopwatchTimeInMS() / 1000.00 + " seconds to compile!", logging.loggingSenderID.compiler, true);
                logging.logDebug(machineCodeParser.machineCodeList.Count.ToString() + " lines of machine code generated for compiler target!", logging.loggingSenderID.compiler, true);
                // grab size of generated machine code
                int generatedCodeSize = 0;
                for (int i = 0; i < machineCodeParser.machineCodeList.Count; i++) { generatedCodeSize += machineCodeParser.machineCodeList[i].Length; }
                generatedCodeSize /= 8;
                logging.logDebug("Size of compiled program is " + generatedCodeSize.ToString() + " bytes", logging.loggingSenderID.compiler);
                // calculating MD5 of program
                StringBuilder sb = new StringBuilder();
                foreach (string value in machineCodeParser.machineCodeList) { sb.Append(value); }
                logging.logDebug("MD5 hash of compiled program is " + functionLibrary.calculateMD5Hash(sb.ToString()), logging.loggingSenderID.compiler);
                // write compiled machine code to file for further manipulation
                // .pb extension - program binary
                logging.logDebug("Exported compiled program to '" + Path.GetDirectoryName(Program.sourceFileLocation) + "/" + compiledProgramName + ".pb'", logging.loggingSenderID.compiler, true);
                File.WriteAllLines(Path.GetDirectoryName(Program.sourceFileLocation) + "/" + compiledProgramName + ".pb", machineCodeParser.machineCodeList);
                // if debug mode enabled, write debug log to text file
                functionLibrary.writeDebugLogOut();
                if (Program.debug) { logging.logDebug("Press [enter] to continue ...", logging.loggingSenderID.compiler, true); Console.ReadLine(); Environment.Exit(0); }
                #endregion
            }
            catch (UnauthorizedAccessException) { Console.WriteLine("The DRCHLLC Core does not have access to write the requested files to disk! The compiler will now exit."); Console.ReadLine(); }
            catch (Exception e)
            {
                Console.WriteLine("[*FATAL ERROR*]: " + e.ToString());
                Console.WriteLine("[*FATAL ERROR*]: If you see this please run the program again with the '-d' option, and then file a bug report!");
                Console.WriteLine("[*FATAL ERROR*]: The program has crashed. Press [enter] to continue...");
                logging.logDebug("[*FATAL ERROR OCCURED*]: " + e.ToString(), logging.loggingSenderID.compiler);
                functionLibrary.writeDebugLogOut();
                Console.ReadLine();
                Environment.Exit(1);
            }
        }

        public static void LexData(string lineOfCode, string lineOfCodeValue, int sender)
        {
            // sender 0 = compiler, sender 1 = importer
            // split up all 'words' based on defined delimeters and send data to tokenizer to be processed
            if (Program.moreVerboseDebug) logging.logDebug("Starting lexdata function", logging.loggingSenderID.tokenizer);

            // regex way to accomplish lexing - apparently a performance hit, so stay with String methods
            //List<string> splitData = Regex.Split(lineOfCode, @"([.;,(){}])|\s").ToList();
            //splitData.RemoveAll(item => item == "");
            //foreach (string str in splitData) { logging.logDebug("[" + lineOfCodeValue + "]: [" + str + "]", logging.loggingSenderID.tokenizer); }

            StringReader sr = new StringReader(lineOfCode);
            int indexLocation = 0;
            int hashKeyCount = 0;
            char currentChar;
            string bufferString = "";
            string bufferString2;
            string tokenedValue;
            while (indexLocation != lineOfCode.Length)
            {
                currentChar = (char)sr.Read();
                bufferString += char.ToString(currentChar);
                indexLocation++;

                // for debug
                if (Program.moreVerboseDebug) { logging.logDebug("bufferstring: " + bufferString, logging.loggingSenderID.tokenizer); }
                switch (currentChar)
                {
                    // detecting whitespace
                    case ' ':
                        // remove whitespace
                        bufferString = functionLibrary.removeWhitespace(bufferString);
                        logging.logDebug("reached special character [whitespace] " + bufferString + " " + bufferString.Length.ToString(), logging.loggingSenderID.tokenizer);
                        tokenedValue = tokens.convertStr(tokens.Tokenize(bufferString, sender));
                        // add to token array [token, value]
                        if (tokenedValue != "0")
                        {
                            logging.logDebug("Buffer tokened as " + tokenedValue, logging.loggingSenderID.tokenizer);
                            functionLibrary.addToTokenArrays(tokenedValue, bufferString, lineOfCodeValue, sender);
                            if (Program.moreVerboseDebug) { logging.logDebug("Token and buffer added to token arrays", logging.loggingSenderID.tokenizer); }
                        }
                        else { if (Program.moreVerboseDebug) logging.logDebug("Invalid token detected - will omit from token buffers", logging.loggingSenderID.tokenizer); }
                        bufferString = null; // reset buffer to go again to tokenize
                        break;
                    // detecting jump regions
                    case '#':
                        hashKeyCount++;
                        if (hashKeyCount == 2)
                        {
                            // remove whitespace
                            bufferString = functionLibrary.removeWhitespace(bufferString);
                            logging.logDebug("reached special case [##] " + bufferString + " " + bufferString.Length.ToString(), logging.loggingSenderID.tokenizer);
                            tokenedValue = tokens.convertInt("identifier_jumploc").ToString(); // special token case
                            functionLibrary.addToTokenArrays(tokenedValue, bufferString, lineOfCodeValue, sender);
                            if (Program.moreVerboseDebug) { logging.logDebug("Token and buffer added to token arrays", logging.loggingSenderID.tokenizer); }
                            bufferString = null;
                        }
                        break;
                    // detecting data groupings 
                    case '(':
                        // remove whitespace
                        // this is a unique case
                        // if bufferstring is more than one character, use special double token case
                        // if bufferstring is one character, send a regular token
                        // bufferstring - value before parentheses if longer than one character
                        // bufferstring2 - parentheses
                        logging.logDebug("reached special charcater [(] " + bufferString + " " + bufferString.Length.ToString(), logging.loggingSenderID.tokenizer);
                        logging.logDebug("reached unique conditional double/single token case", logging.loggingSenderID.tokenizer);
                        bufferString = functionLibrary.removeWhitespace(bufferString);
                        if (bufferString.Length == 1)
                        {
                            // tokenize regularly
                            tokenedValue = tokens.convertStr(tokens.Tokenize(bufferString, sender));
                            logging.logDebug("Buffer tokened as " + tokenedValue, logging.loggingSenderID.tokenizer);
                            functionLibrary.addToTokenArrays(tokenedValue, bufferString, lineOfCodeValue, sender);
                            if (Program.moreVerboseDebug) { logging.logDebug("Token and buffer added to token arrays", logging.loggingSenderID.tokenizer); }
                            bufferString = null;
                            break;
                        }
                        else
                        {
                            // tokenize both the parentheses and the object before it
                            bufferString2 = bufferString;
                            bufferString = bufferString.Replace('(', ' ');
                            bufferString = functionLibrary.removeWhitespace(bufferString);
                            bufferString2 = bufferString2.Remove(0, bufferString2.Length - 1);
                            tokenedValue = tokens.convertStr(tokens.Tokenize(bufferString, sender));
                            logging.logDebug("Buffer1 tokened as " + tokenedValue, logging.loggingSenderID.tokenizer);
                            functionLibrary.addToTokenArrays(tokenedValue, bufferString, lineOfCodeValue, sender);
                            if (Program.moreVerboseDebug) { logging.logDebug("Token and buffer added to token arrays", logging.loggingSenderID.tokenizer); }
                            tokenedValue = tokens.convertStr(tokens.Tokenize(bufferString2, sender));
                            logging.logDebug("Buffer2 tokened as " + tokenedValue, logging.loggingSenderID.tokenizer);
                            functionLibrary.addToTokenArrays(tokenedValue, bufferString2, lineOfCodeValue, sender);
                            if (Program.moreVerboseDebug) { logging.logDebug("Token and buffer added to token arrays", logging.loggingSenderID.tokenizer); }
                            bufferString = null;
                            break;
                        }
                    case ')':
                        // remove whitespace
                        // tokenize BOTH the value BEFORE the parentheses AND the parentheses!
                        // bufferString - value before parentheses
                        // bufferString2 - parentheses
                        bufferString2 = bufferString;
                        bufferString = bufferString.Replace(')', ' ');
                        bufferString = functionLibrary.removeWhitespace(bufferString);
                        bufferString2 = bufferString2.Remove(0, bufferString2.Length - 1);
                        logging.logDebug("reached special character [)] " + bufferString + " " + bufferString.Length.ToString(), logging.loggingSenderID.tokenizer);
                        logging.logDebug("reached special double token case", logging.loggingSenderID.tokenizer);
                        tokenedValue = tokens.convertStr(tokens.Tokenize(bufferString, sender));
                        logging.logDebug("Buffer1 tokened as " + tokenedValue, logging.loggingSenderID.tokenizer);
                        functionLibrary.addToTokenArrays(tokenedValue, bufferString, lineOfCodeValue, sender);
                        if (Program.moreVerboseDebug) { logging.logDebug("Token and buffer added to token arrays", logging.loggingSenderID.tokenizer); }
                        tokenedValue = tokens.convertStr(tokens.Tokenize(bufferString2, sender));
                        logging.logDebug("Buffer2 tokened as " + tokenedValue, logging.loggingSenderID.tokenizer);
                        functionLibrary.addToTokenArrays(tokenedValue, bufferString2, lineOfCodeValue, sender);
                        if (Program.moreVerboseDebug) { logging.logDebug("Token and buffer added to token arrays", logging.loggingSenderID.tokenizer); }
                        bufferString = null;
                        break;
                    case '{':
                        // remove whitespace
                        bufferString = functionLibrary.removeWhitespace(bufferString);
                        logging.logDebug("reached special character [{] " + bufferString + " " + bufferString.Length.ToString(), logging.loggingSenderID.tokenizer);
                        tokenedValue = tokens.convertStr(tokens.Tokenize(bufferString, sender));
                        logging.logDebug("Buffer tokened as " + tokenedValue, logging.loggingSenderID.tokenizer);
                        functionLibrary.addToTokenArrays(tokenedValue, bufferString, lineOfCodeValue, sender);
                        if (Program.moreVerboseDebug) { logging.logDebug("Token and buffer added to token arrays", logging.loggingSenderID.tokenizer); }
                        bufferString = null;
                        break;
                    case '}':
                        // remove whitespace
                        bufferString = functionLibrary.removeWhitespace(bufferString);
                        logging.logDebug("reached special character [}] " + bufferString + " " + bufferString.Length.ToString(), logging.loggingSenderID.tokenizer);
                        tokenedValue = tokens.convertStr(tokens.Tokenize(bufferString, sender));
                        logging.logDebug("Buffer tokened as " + tokenedValue, logging.loggingSenderID.tokenizer);
                        functionLibrary.addToTokenArrays(tokenedValue, bufferString, lineOfCodeValue, sender);
                        if (Program.moreVerboseDebug) { logging.logDebug("Token and buffer added to token arrays", logging.loggingSenderID.tokenizer); }
                        bufferString = null;
                        break;
                    // detecting semicolon
                    case ';':
                        // remove whitespace and semicolon from bufferstring
                        bufferString2 = bufferString;
                        bufferString = bufferString.Replace(';', ' ');
                        bufferString = functionLibrary.removeWhitespace(bufferString);
                        bufferString2 = bufferString2.Remove(0, bufferString2.Length - 1);
                        logging.logDebug("reached special character [semicolon/EOL] " + bufferString + " " + bufferString.Length.ToString(), logging.loggingSenderID.tokenizer);
                        logging.logDebug("reached special double token case", logging.loggingSenderID.tokenizer);
                        tokenedValue = tokens.convertStr(tokens.Tokenize(bufferString, sender));
                        logging.logDebug("Buffer tokened as " + tokenedValue, logging.loggingSenderID.tokenizer);
                        functionLibrary.addToTokenArrays(tokenedValue, bufferString, lineOfCodeValue, sender);
                        tokenedValue = tokens.convertStr(tokens.Tokenize(bufferString2, sender));
                        logging.logDebug("Buffer2 tokened as " + tokenedValue, logging.loggingSenderID.tokenizer);
                        functionLibrary.addToTokenArrays(tokenedValue, bufferString2, lineOfCodeValue, sender);
                        if (Program.moreVerboseDebug) { logging.logDebug("Token and buffer added to token arrays", logging.loggingSenderID.tokenizer); }
                        bufferString = null; // reset buffer string to go again to tokenize
                        break;
                    // put all special conditions here
                    default:
                        if (bufferString != null && bufferString.Contains(","))
                        {
                            // in this case, we will just token the value w/o the comma, and continue lexing
                            logging.logDebug("reached special case [,] " + bufferString + " " + bufferString.Length.ToString(), logging.loggingSenderID.tokenizer);
                            logging.logDebug("comma will be ignored in tokenization", logging.loggingSenderID.tokenizer);
                            bufferString = bufferString.Replace(',', ' ');
                            bufferString = functionLibrary.removeWhitespace(bufferString);
                            tokenedValue = tokens.convertStr(tokens.Tokenize(bufferString, sender));
                            logging.logDebug("Buffer tokened as " + tokenedValue, logging.loggingSenderID.tokenizer);
                            functionLibrary.addToTokenArrays(tokenedValue, bufferString, lineOfCodeValue, sender);
                            if (Program.moreVerboseDebug) { logging.logDebug("Token and buffer added to token arrays", logging.loggingSenderID.tokenizer); }
                            bufferString = null;
                        }
                        if (bufferString != null && bufferString.Contains("else"))
                        {
                            logging.logDebug("reached special case [else] " + bufferString + " " + bufferString.Length.ToString(), logging.loggingSenderID.tokenizer);
                            bufferString = functionLibrary.removeWhitespace(bufferString);
                            tokenedValue = tokens.convertStr(tokens.Tokenize(bufferString, sender));
                            logging.logDebug("Buffer tokened as " + tokenedValue, logging.loggingSenderID.tokenizer);
                            functionLibrary.addToTokenArrays(tokenedValue, bufferString, lineOfCodeValue, sender);
                            if (Program.moreVerboseDebug) { logging.logDebug("Token and buffer added to token arrays", logging.loggingSenderID.tokenizer); }
                            bufferString = null;
                        }
                        if (bufferString != null && bufferString.Contains("++"))
                        {
                            // bufferstring - variable
                            // bufferstring2 - ++ operator
                            bufferString2 = bufferString;
                            bufferString = bufferString.Replace('+', ' ');
                            bufferString = functionLibrary.removeWhitespace(bufferString);
                            bufferString2 = bufferString2.Remove(0, bufferString2.Length - 2);
                            logging.logDebug("reached special case [++] " + bufferString + " " + bufferString.Length.ToString(), logging.loggingSenderID.tokenizer);
                            logging.logDebug("reached special double token case", logging.loggingSenderID.tokenizer);
                            tokenedValue = tokens.convertStr(tokens.Tokenize(bufferString, sender));
                            logging.logDebug("Buffer tokened as " + tokenedValue, logging.loggingSenderID.tokenizer);
                            functionLibrary.addToTokenArrays(tokenedValue, bufferString, lineOfCodeValue, sender);
                            tokenedValue = tokens.convertStr(tokens.Tokenize(bufferString2, sender));
                            logging.logDebug("Buffer2 tokened as " + tokenedValue, logging.loggingSenderID.tokenizer);
                            functionLibrary.addToTokenArrays(tokenedValue, bufferString2, lineOfCodeValue, sender);
                            if (Program.moreVerboseDebug) { logging.logDebug("Token and buffer added to token arrays", logging.loggingSenderID.tokenizer); }
                            bufferString = null;
                        }
                        if (bufferString != null && bufferString.Contains("--"))
                        {
                            // bufferstring - variable
                            // bufferstring2 - -- operator
                            bufferString2 = bufferString;
                            bufferString = bufferString.Replace('-', ' ');
                            bufferString = functionLibrary.removeWhitespace(bufferString);
                            bufferString2 = bufferString2.Remove(0, bufferString2.Length - 2);
                            logging.logDebug("reached special case [--] " + bufferString + " " + bufferString.Length.ToString(), logging.loggingSenderID.tokenizer);
                            logging.logDebug("reached special double token case", logging.loggingSenderID.tokenizer);
                            tokenedValue = tokens.convertStr(tokens.Tokenize(bufferString, sender));
                            logging.logDebug("Buffer tokened as " + tokenedValue, logging.loggingSenderID.tokenizer);
                            functionLibrary.addToTokenArrays(tokenedValue, bufferString, lineOfCodeValue, sender);
                            tokenedValue = tokens.convertStr(tokens.Tokenize(bufferString2, sender));
                            logging.logDebug("Buffer2 tokened as " + tokenedValue, logging.loggingSenderID.tokenizer);
                            functionLibrary.addToTokenArrays(tokenedValue, bufferString2, lineOfCodeValue, sender);
                            if (Program.moreVerboseDebug) { logging.logDebug("Token and buffer added to token arrays", logging.loggingSenderID.tokenizer); }
                            bufferString = null;
                        }
                        break;
                }
            }
        }

        private static void generateMAT1()
        {
            // will be ID'd as assemblyParser
            // we will go through tokens until we find a variable token, then add MAT entry
            int MATIndex = -1;
            int AddressCounterGlobal = 0; // so that we have most efficient use of address space as possible
            int AddressCounterLocal = 0;
            int currentTokenIndex = -1;
            string currentTokenID;
            string currentTokenValue;
            bool mallocCalled = false;
            bool temp1Allocated = false;
            bool temp2Allocated = false;

            // before main variable MAT generation, run through and see if we have ~temp1~ or ~temp2~ mem locations generated! If there are, add to MAT
            for (int i = 0; i < parsedAssemblyList.Count; i++)
            {
                if (parsedAssemblyList[i].Contains("~temp1~") && temp1Allocated == false)
                {
                    temp1Allocated = true;
                    MATIndex++;
                    if (!enableMemoryManagementOptimizations) AddressCounterGlobal++;
                    else AddressCounterLocal++;
                    Array.Resize(ref MATAddresses, MATAddresses.Length + 1);
                    Array.Resize(ref MATAddressPointers, MATAddressPointers.Length + 1);
                    Array.Resize(ref MemoryAllocationType, MemoryAllocationType.Length + 1);
                    if (!enableMemoryManagementOptimizations) MATAddresses[MATIndex] = AddressCounterGlobal;
                    else MATAddresses[MATIndex] = AddressCounterLocal;
                    MATAddressPointers[MATIndex] = "~temp1~";
                    if (enableMemoryManagementOptimizations) { MemoryAllocationType[MATIndex] = 3; }
                    else { MemoryAllocationType[MATIndex] = 1; } // 1 is var, 2 is jump, 3 is cached var
                }
                else if (parsedAssemblyList[i].Contains("~temp2~") && temp2Allocated == false)
                {
                    temp2Allocated = true;
                    MATIndex++;
                    if (!enableMemoryManagementOptimizations) AddressCounterGlobal++;
                    else AddressCounterLocal++;
                    Array.Resize(ref MATAddresses, MATAddresses.Length + 1);
                    Array.Resize(ref MATAddressPointers, MATAddressPointers.Length + 1);
                    Array.Resize(ref MemoryAllocationType, MemoryAllocationType.Length + 1);
                    if (!enableMemoryManagementOptimizations) MATAddresses[MATIndex] = AddressCounterGlobal;
                    else MATAddresses[MATIndex] = AddressCounterLocal;
                    MATAddressPointers[MATIndex] = "~temp2~";
                    if (enableMemoryManagementOptimizations) { MemoryAllocationType[MATIndex] = 3; }
                    else { MemoryAllocationType[MATIndex] = 1; } // 1 is var, 2 is jump, 3 is cached var
                }
            }

        TopOfMATGenerator:
            currentTokenIndex++;
            currentTokenID = tokenTokensArray[currentTokenIndex];
            currentTokenValue = tokenValuesArray[currentTokenIndex];
            // eof check
            if (currentTokenID == tokens.convertInt("identifier_eof").ToString()) goto EndOfMATGeneration;

            if (currentTokenID == tokens.convertInt("variable").ToString())
            {
                int MATType = 1; // 1 is default
                // check if this variable already exists in MAT
                for (int i = 0; i < MATAddressPointers.Length; i++) { if (MATAddressPointers[i] == currentTokenValue) { goto TopOfMATGenerator; } }
                // check if we've ran a malloc command for our found variable
                for (int i = 0; i < parsedAssemblyList.Count; i++)
                {
                    if (parsedAssemblyList[i].Contains("@malloc") && parsedAssemblyList[i].Contains(currentTokenValue)) { mallocCalled = true; }
                    if (parsedAssemblyList[i].Contains("@lmalloc") && parsedAssemblyList[i].Contains(currentTokenValue)) { mallocCalled = true; MATType = 3; }
                }
                if (mallocCalled == true) { mallocCalled = false; }
                else { logging.logDebug("Found new variable entry for MAT, but it was not initialized beforehand! Ignoring ...", logging.loggingSenderID.assemblyParser); goto TopOfMATGenerator; }
                logging.logDebug("Found new variable entry for MAT", logging.loggingSenderID.assemblyParser);

                MATIndex++;
                if (MATType != 3) AddressCounterGlobal++;
                else AddressCounterLocal++;
                Array.Resize(ref MATAddresses, MATAddresses.Length + 1);
                Array.Resize(ref MATAddressPointers, MATAddressPointers.Length + 1);
                Array.Resize(ref MemoryAllocationType, MemoryAllocationType.Length + 1);
                if (MATType != 3) MATAddresses[MATIndex] = AddressCounterGlobal;
                else MATAddresses[MATIndex] = AddressCounterLocal;
                MATAddressPointers[MATIndex] = currentTokenValue;
                MemoryAllocationType[MATIndex] = MATType; // 1 is global var, 2 is jump, 3 is local var 

                goto TopOfMATGenerator;
            }
            else goto TopOfMATGenerator;
            EndOfMATGeneration:

            logging.logDebug("MAT Generator function complete", logging.loggingSenderID.assemblyParser);

            // perform var = 0 malloc optimization if enabled
            if (enableRedundantCodeRemovalOptimization == true && enableOptimizations == true)
            {
                logging.logDebug("Performing redundant code removal optimizations for empty memory allocations ...", logging.loggingSenderID.assemblyParser);
            TopOfV0MO:
                for (int i = 0; i < parsedAssemblyList.Count; i++)
                {
                    if (parsedAssemblyList[i].Contains("@malloc")
                        || parsedAssemblyList[i].Contains("@lmalloc"))
                    {
                        // we'll have to grab blocks
                        string[] blocks = parsedAssemblyList[i].Split(" ");
                        // only concerned w/ block3 (block1 is malloc/lmalloc opcode)
                        if (Convert.ToInt16(blocks[2]) == 0) { parsedAssemblyList.RemoveAt(i); goto TopOfV0MO; }
                    }
                }
            }
        }

        private static void generateMAT2()
        {
            // will be ID'd as assemblyParser
            // here we will generate the jump locations in MAT
            // will also generate function jump location in MAT (temporarily removed functionality)
            int MATIndex = MATAddressPointers.Length - 1;

            for (int i = 0; i < parsedAssemblyList.Count; i++)
            {
                // temporarily removing falloc functionality here until it becomes more of a priority within compiler development

                string[] blocks = parsedAssemblyList[i].Split(" ");
                if (blocks[0] == "@jalloc")
                {
                    // check to see if this jump entry already exists in MAT; if it does, abort!
                    bool jumpEntryExists = false;
                    for (int j = 0; j < MATAddressPointers.Length; j++)
                    {
                        if (MATAddressPointers[j].Contains(blocks[1]) && MATAddressPointers[j] != null)
                        {
                            logging.logDebug("ERROR: MAT Generator found more than one of the same MAT address pointers! The program will most likely not compile successfully because of this!", logging.loggingSenderID.assemblyParser, true);
                            logging.logDebug("MAT pointer: " + MATAddressPointers[j], logging.loggingSenderID.assemblyParser);
                            if (Program.debug) logging.logDebug("INTERNAL: FIX HANDLING OF THIS ERROR!", logging.loggingSenderID.assemblyParser);
                            jumpEntryExists = true; break;
                        }
                    }

                    if (!jumpEntryExists)
                    {
                        logging.logDebug("Found new jump entry for MAT 1", logging.loggingSenderID.assemblyParser);
                        MATIndex++;
                        Array.Resize(ref MATAddresses, MATAddresses.Length + 1);
                        Array.Resize(ref MATAddressPointers, MATAddressPointers.Length + 1);
                        Array.Resize(ref MemoryAllocationType, MemoryAllocationType.Length + 1);
                        MATAddresses[MATIndex] = i + 1; // to be the next line of code
                        MATAddressPointers[MATIndex] = blocks[1];
                        MemoryAllocationType[MATIndex] = 2; // 1 is var, 2 is jump, 3 is cached var
                    }
                }
            }
            logging.logDebug("MAT generation function finished", logging.loggingSenderID.assemblyParser);
        }

        private static void updateAssemblyFromMAT()
        {
            // will be ID'd as assemblyParser
            // check each line of code and see if we have a match from our MAT Address Pointers

            for (int i = 0; i < parsedAssemblyList.Count; i++)
            {
                // block assembly, go through each block and see if it matches any MAT address pointer - if so, update
                // instead of replacing text, replace values of blocks, then re-concatenate
                // b/c we can have MATAddressPointers with the same 'root' (i.e. value, value2, value3), and using Replace replaces the root when it hits the first value instead of what we want
                string[] blocks = parsedAssemblyList[i].Split(" ");
                for (int j = 1; j < blocks.Length; j++) // skip the first block b/c always an opcode
                {
                    if (MATAddressPointers.Contains(blocks[j]))
                    {
                        int indexMAT = Array.IndexOf(MATAddressPointers, blocks[j]);
                        if (Program.moreVerboseDebug)
                        { logging.logDebug("matap: " + MATAddressPointers[indexMAT], logging.loggingSenderID.assemblyParser); logging.logDebug("mata: " + MATAddresses[indexMAT].ToString(), logging.loggingSenderID.assemblyParser); }

                        if (MemoryAllocationType[indexMAT] == 1 || MemoryAllocationType[indexMAT] == 3) blocks[j] = (MemoryAllocationType[indexMAT] == 3 ? "$$" : "$") + MATAddresses[indexMAT].ToString() + (MemoryAllocationType[indexMAT] == 3 ? "$$" : "$");
                        else blocks[j] = MATAddresses[indexMAT].ToString(); /* lazy instead of checking for types 2 & 4 (jumps) b/c those are the only other options after 1 & 3 */
                    }
                }
                parsedAssemblyList[i] = string.Join(" ", blocks);
                if (Program.moreVerboseDebug)
                { logging.logDebug("Updated line " + i + " of code with MAT data", logging.loggingSenderID.assemblyParser); logging.logDebug("new line of code: " + parsedAssemblyList[i], logging.loggingSenderID.assemblyParser); }
            }

            // removing all merged jalloc statements
            // come back to this and see if this is an optimal way to go about this
            for (int i = 0; i < parsedAssemblyList.Count; i++)
            {
                if (parsedAssemblyList[i].Contains("@jalloc"))
                {
                    List<string> blocks = parsedAssemblyList[i].Split(" ").ToList();
                    blocks.RemoveAt(0); blocks.RemoveAt(0);
                    parsedAssemblyList[i] = string.Join(" ", blocks);
                }

                //if (parsedAssemblyList[i].Contains("@jalloc"))
                //{
                //    logging.logDebug("Found @jalloc at index " + parsedAssemblyList[i].IndexOf("@jalloc"), logging.loggingSenderID.assemblyParser); // this debug statement shows that it's always at index 0 - so we will just assume @jalloc always appears at index 0 (CAREFUL!)
                //    int IDIndexCounter = 1;
                //TopOfFunction:
                //    try
                //    { Convert.ToInt16(parsedAssemblyList[i].Substring(8, IDIndexCounter)); IDIndexCounter++; goto TopOfFunction; }
                //    catch (FormatException) { parsedAssemblyList[i] = parsedAssemblyList[i].Remove(parsedAssemblyList[i].IndexOf("@jalloc"), IDIndexCounter + 7); }
                //}
            }
            // removing all merged falloc statements
            for (int i = 0; i < parsedAssemblyList.Count; i++)
            {
                if (parsedAssemblyList[i].Contains("falloc"))
                {
                    int IDIndexCounter = 1;
                TopOfFunction:
                    try
                    { Convert.ToInt16(parsedAssemblyList[i].Substring(7, IDIndexCounter)); IDIndexCounter++; goto TopOfFunction; }
                    catch (FormatException) { parsedAssemblyList[i] = parsedAssemblyList[i].Remove(parsedAssemblyList[i].IndexOf("falloc"), IDIndexCounter + 6); }
                }
            }
        }

    }
}
