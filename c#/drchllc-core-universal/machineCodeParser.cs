﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace drchllc_core_universal
{
    class machineCodeParser
    {
        /*
         * This class will contain all functions and information needed for parsing code from
         * ARCIS assembly to native machine code on specific targets.
         * 
         * Created 9/5/2016 by AGuyWhoIsBored
         * 
         */

        // machine code list
        public static List<string> machineCodeList = new List<string>();
        static int digits = -1;

        public static void generateMachineCode()
        {
            // WILL CHECK FOR FUNCTIONS / INSTRUCTIONS THAT HAVE NOT BEEN CREATED YET!
            // ramw / ramr - RAM write / RAM read
            // cachew / cacher - cache write / cache read

            logging.logDebug("Target computer generating for: " + Program.compileTarget, logging.loggingSenderID.machineCodeParser);
            logging.logDebug("Core generating for: " + Program.compileCoreTarget, logging.loggingSenderID.machineCodeParser);
            digits = Convert.ToInt16(Math.Floor(Math.Log10(compiler.parsedAssemblyList.Count) + 1));

            // this is where we will generate our machine code!
            // length of word on RC3.0 - 41 bits
            // length of word on RC4.0 - 84 bits
            // length of word on RC5.0 - 48 bits

            #region All Language Opcodes
            //ARCISS v1.1 COMPILER ALL PARSED GENERATED FUNCTIONS:
            //opcodes are preceded by @ during assembly code generation temporarily to denote them being an opcode vs a regular variable to the compiler
            //however by the time we reach machine code generation, all opcodes are denoted by their values as seen below

            //TEMP: (created in stage 1 of parsing, removed in stage 2 of parsing):
            //memw             
            //memr             
            //jalloc           

            //GLOBAL:
            //ramw             
            //ramr             
            //cachew           [RC4.0+ only]
            //cacher           [RC4.0+ only]
            //malloc           
            //lmalloc          [for local var - RC4.0+ only]
            //test             
            //exit             [invalidated if used as var]
            //flush            [legacy opcode]
            //output           [invalidate if used as var]
            //input            [invalidate if used as var]
            //jump             [invalidate if used as var]
            //bsr
            //bsl
            //ad1              [move to remove - replace w/ add counterpart] 
            //sb1              [move to remove - replace w/ sub counterpart]
            //not
            //add
            //sub

            //RC5.0 HARDWARE SPECIFIC:
            //cmp

            //new in v1.1:
            //sleep            [invalidate if used as var]

            //RC4.0 extension
            //gpuencode 
            //gpuclr
            //gpudraw
            //gpuerase
            //gpugu
            //updspd

            //RC5.0 extension:
            //gpuencode
            //gpuclr
            //gpudraw
            //gpuerase
            //gpugu
            //con 
            //coff
            //xor
            //or
            //and
            //xnor
            //nor

            #endregion

            // for v1.7a
            // TODO: 
            // ****UPDATE RC3.0 AND RC4.0 MACHINE CODE TO CATCH UP WITH V1.1 SPEC****
            // ****IMPLEMENT CACHING INTO RC4.0****
            // ****CHECK MACHINE CODE FOR RC5.0 DUE TO HARDWARE CHANGES (%forward% for RAM data forwarding to master output bus, immediate data passthrough enable lines)****
            // ****FIX RC3.0 DISCREPANCY ON ORIENTATIONS & POSITIONS WITHIN MACHINE CODE GENERATOR AND UPLOADER!!
            // ****REVAMP GENERATION TECHNIQUE BY SPLITTING EVERYTHING UP INTO BLOCKS, AND THEN PARSING!!**** (review all lines with SPECIAL CASE - CHECK)
            #region RC3.0 machine code generation
            if (Program.compileTarget == "1")
            {
                #region RC3.0 IS Bit Layouts
                // instruction set bit layout for RC3.0 core 1 (facing north) (left to right)
                // [0 0 0 0 0 ] [0 0 0 0 0 ] [0 0 0 0 0 ] [0   0  0  0  0 0 0 0] [0 0 0 0] [0 0 0 0] [0  0 0 0 0] [0 0] [0 0 0] 
                // [1 2 4 8 16] [1 2 4 8 16] [1 2 4 8 16] [128 64 32 16 8 4 2 1] [1 2 4 8] [1 2 4 8] [16 8 4 2 1] [1 2] [1 2 4]
                // [GOTO FALSE] [GOTO TRUE ] [CONDITION ] [IMMEDIATE           ] [RAM W  ] [RAM R  ] [OPCODE    ] [DR ] [EXT. ]

                // instruction set bit layout for RC3.0 core 2 (facing north) (left to right)
                // [0 0 0 0 0 ] [0 0 0 0 0 ] [0 0 0 0 0 ] [0   0  0  0  0 0 0 0] [0 0 0 0] [0 0 0 0] [0 0 0 0 0 ] [0 0] [0 0 0]
                // [1 2 4 8 16] [1 2 4 8 16] [1 2 4 8 16] [128 64 32 16 8 4 2 1] [1 2 4 8] [1 2 4 8] [1 2 4 8 16] [1 2] [1 2 4]
                // [GOTO FALSE] [GOTO TRUE ] [CONDITION ] [IMMEDIATE           ] [RAM W  ] [RAM R  ] [OPCODE    ] [DR ] [EXT. ]

                // instruction set bit layout for RC3.0 core 3 (facing north) (left to right)
                // [0  0 0 0 0] [0  0 0 0 0] [0 0 0 0 0 ] [0   0  0  0  0 0 0 0] [0 0 0 0] [0 0 0 0] [0 0 0 0 0 ] [0 0] [0 0 0]
                // [16 8 4 2 1] [16 8 4 2 1] [1 2 4 8 16] [128 64 32 16 8 4 2 1] [1 2 4 8] [1 2 4 8] [1 2 4 8 16] [1 2] [1 2 4]
                // [GOTO FALSE] [GOTO TRUE ] [CONDITION ] [IMMEDIATE           ] [RAM W  ] [RAM R  ] [OPCODE    ] [DR ] [EXT. ]

                // instruction set bit layout for RC3.0 core 4 (facing north) (left to right)
                // [0  0 0 0 0] [0  0 0 0 0] [0 0 0 0 0 ] [0   0  0  0  0 0 0 0] [0 0 0 0] [0 0 0 0] [0 0 0 0 0 ] [0 0] [0 0 0]
                // [16 8 4 2 1] [16 8 4 2 1] [1 2 4 8 16] [128 64 32 16 8 4 2 1] [1 2 4 8] [1 2 4 8] [1 2 4 8 16] [1 2] [1 2 4]
                // [GOTO FALSE] [GOTO TRUE ] [CONDITION ] [IMMEDIATE           ] [RAM W  ] [RAM R  ] [OPCODE    ] [DR ] [EXT. ]
                #endregion

                // var for incrementing line of code location if none provided (default)
                int fJumpDefault = 2; // on the first line of code, we jump to line 2
                bool generate = true; // if this is true, we will generated a line of code; if it's not (i.e. malloc for values returning 0), will not generate!

                for (int i = 0; i < compiler.parsedAssemblyList.Count; i++)
                {
                    string[] parsedAssemblyBlocks = compiler.parsedAssemblyList[i].Split(" "); // do it this way to make machine code generation more robust

                    // more verbose debug
                    if (Program.moreVerboseDebug) logging.logDebug("[" + compiler.parsedAssemblyList[i] + "]", logging.loggingSenderID.machineCodeParser);

                    // init
                    #region Init vars
                    string currentInstructionWord = null;

                    // will break down machine code into different segments
                    string mImmediate = "00000000";
                    string mGFalse = "00000";
                    string mGTrue = "00000";
                    string mCond = "00000";
                    string mRamW = "0000";
                    string mRamR = "0000";
                    string mOpcode = "00000";
                    string mDualReadToggle = "00";
                    string mExtension = "000";
                    #endregion

                    // now find what function or thing we need to do
                    #region malloc opcode
                    if (parsedAssemblyBlocks[0] == "malloc")
                    {
                        // grab values
                        int memAddr = acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("malloc") + 7);
                        int value = acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].LastIndexOf("$") + 1);
                        // update machine code
                        if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                        {
                            mRamW = functionLibrary.convertNumberToBinary(memAddr, true, 4);
                            mImmediate = functionLibrary.convertNumberToBinary(value, false, 8);
                        }
                        else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                        {
                            mRamW = functionLibrary.convertNumberToBinary(memAddr, false, 4);
                            mImmediate = functionLibrary.convertNumberToBinary(value, true, 8);
                        }
                    }
                    #endregion

                    #region exit opcode
                    if (parsedAssemblyBlocks[0] == "exit")
                    {
                        // generate machine code; this opcode is easy!
                        if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                        { mOpcode = "01011"; }
                        else if (Program.compileCoreTarget == "2")
                        { mOpcode = "11010"; }
                    }
                    #endregion

                    #region flush opcode
                    if (parsedAssemblyBlocks[0] == "flush")
                    {
                        // grab flush reg
                        int regToFlush = acquireMemoryAddress(compiler.parsedAssemblyList[i], 5) + 1; /* +1 is convert regtoflush from value in code to value required in ext. */
                        if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4") { mOpcode = "00111"; }
                        else if (Program.compileCoreTarget == "2") { mOpcode = "11100"; }
                        if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                        { mExtension = functionLibrary.convertNumberToBinary(regToFlush, true, 3); }
                        else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                        { mExtension = functionLibrary.convertNumberToBinary(regToFlush, false, 3); }
                    }
                    #endregion

                    // *** DEBUG OPCODE IN HARDWARE OPERATION!!! ***
                    // *** IMMEDIATE VALUE NOT ADDED IF PRESENT - FIX!! ****
                    #region output opcode
                    if (parsedAssemblyBlocks[0] == "output")
                    {
                        if (Program.compileCoreTarget == "1") { mOpcode = "01001"; }
                        else if (Program.compileCoreTarget == "2" || Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4") { mOpcode = "10010"; }

                        // acquire both blocks
                        string[] blocks = compiler.parsedAssemblyList[i].Split(" "); // ignore first block b/c opcode

                        // target specific code
                        if (blocks[2] == "%dec") { mExtension = "100"; }
                        else if (blocks[2] == "%raw") { mExtension = "010"; }
                        else if (blocks[2] == "%dp") { mExtension = "110"; }
                        else if (blocks[2] == "%out" && blocks[1] == "1") { mExtension = "001"; }
                        else if (blocks[2] == "%out" && blocks[1] == "2") { mExtension = "101"; }
                        else if (blocks[2] == "%out" && blocks[1] == "3") { mExtension = "011"; }
                        else if (blocks[2] == "%out" && blocks[1] == "4") { mExtension = "111"; }

                        if (blocks[1].Contains("$") && blocks[1].Count(x => x == '$') == 2)
                        { mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(blocks[1], 0), true, 4); }

                    }
                    #endregion

                    #region input opcode
                    if (parsedAssemblyBlocks[0] == "input")
                    {
                        mOpcode = "01010";
                        // by default data will be pushed onto 
                        // gather data
                        int inputSocket = acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("input") + 6);
                        int memAddr = 0;
                        if (compiler.parsedAssemblyList[i].Contains("$")) // SPECIAL CASE - CHECK
                        { memAddr = acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$")); }
                        // modify input socket based on hardware on RC 3.0
                        if (inputSocket == 6) { inputSocket = 1; }
                        else if (inputSocket == 5) { inputSocket = 3; }
                        else if (inputSocket == 4) { inputSocket = 2; }

                        if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                        { mExtension = functionLibrary.convertNumberToBinary(inputSocket, true, 3); }
                        else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                        { mExtension = functionLibrary.convertNumberToBinary(inputSocket, false, 3); }
                        if (memAddr != 0)
                        {
                            if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                            { mRamW = functionLibrary.convertNumberToBinary(memAddr, true, 4); }
                            else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                            { mRamW = functionLibrary.convertNumberToBinary(memAddr, false, 4); }
                        }
                    }

                    #endregion

                    #region sleep opcode
                    // literally don't do anything at all - this is here just for reference (could remove it)
                    if (parsedAssemblyBlocks[0] == "sleep") { /*do nothing*/ }
                    #endregion

                    #region bsr opcode
                    if (parsedAssemblyBlocks[0] == "bsr")
                    {
                        // generate opcode
                        if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4") { mOpcode = "00110"; }
                        else if (Program.compileCoreTarget == "2") { mOpcode = "01100"; }

                        // grab address to modify
                        if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                        { mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$")), true, 4); }
                        else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                        { mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$")), false, 4); }
                    }
                    #endregion

                    #region bsl opcode
                    if (parsedAssemblyBlocks[0] == "bsl")
                    {
                        // generate opcode
                        if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4") { mOpcode = "00101"; }
                        else if (Program.compileCoreTarget == "2") { mOpcode = "10100"; }

                        // grab address to modify
                        if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                        { mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$")), true, 4); }
                        else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                        { mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$")), false, 4); }
                    }
                    #endregion

                    #region ad1 opcode
                    if (parsedAssemblyBlocks[0] == "ad1")
                    {
                        // ad1 instruction
                        // opcode
                        if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4") { mOpcode = "00001"; }
                        else if (Program.compileCoreTarget == "2") { mOpcode = "10000"; }
                        // mem addr
                        // we have this statement here because ad1 temp[x] temp[x] instructions can be generated
                        if (compiler.parsedAssemblyList[i].Contains("$")) // SPECIAL CASE - CHECK
                        {
                            if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                            { mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$")), true, 4); }
                            else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                            { mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$")), false, 4); }
                        }
                        // immediate value
                        if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2") { mImmediate = "00000001"; }
                        else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4") { mImmediate = "10000000"; }
                    }
                    #endregion

                    #region sb1 opcode
                    if (parsedAssemblyBlocks[0] == "sb1")
                    {
                        // opcode
                        if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4") { mOpcode = "00010"; }
                        else if (Program.compileCoreTarget == "2") { mOpcode = "01000"; }
                        // mem addr
                        // we have this statement here because sb1 temp[x] temp[x] instructions can be generated
                        if (compiler.parsedAssemblyList[i].Contains("$")) // SPECIAL CASE - CHECK
                        {
                            if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                            { mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$")), true, 4); }
                            else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                            { mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$")), false, 4); }
                        }
                        // immediate value
                        if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2") { mImmediate = "00000001"; }
                        else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4") { mImmediate = "10000000"; }
                    }
                    #endregion

                    #region not opcode
                    if (parsedAssemblyBlocks[0] == "not")
                    {
                        // opcode
                        mOpcode = "00100";
                        // mem addr
                        if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                        { mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$")), true, 4); }
                        else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                        { mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$")), false, 4); }
                    }
                    #endregion

                    #region add opcode
                    if (parsedAssemblyBlocks[0] == "add")
                    {
                        // opcode
                        if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4") { mOpcode = "00001"; }
                        if (Program.compileCoreTarget == "2") { mOpcode = "10000"; }

                        // actual add instruction
                        if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2") { mDualReadToggle = "01"; }
                        else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4") { mDualReadToggle = "10"; }

                        // because of the RC3.0 architecture, we'll need to detect if either block is an
                        // immediate int. If it is, then we have to set that as immediate value, and have
                        // the other value become mramr
                        // we won't need a check for the case if both of the blocks are #'s, because the syntax 
                        // checker already checks that and takes care of that for us (although the error produced 
                        // is a bit misleading)

                        if (!parsedAssemblyBlocks[1].Contains("$"))
                        {
                            // block1 becomes immediate value
                            if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                            { mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(parsedAssemblyBlocks[1]), false, 8); }
                            else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                            { mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(parsedAssemblyBlocks[1]), true, 8); }
                            // then set other block to mramw
                            if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                            { mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[2], parsedAssemblyBlocks[2].IndexOf("$")), true, 4); }
                            else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                            { mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[2], parsedAssemblyBlocks[2].IndexOf("$")), false, 4); }
                        }
                        else if (!parsedAssemblyBlocks[2].Contains("$"))
                        {
                            // block2 becomes immediate value
                            if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                            { mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(parsedAssemblyBlocks[2]), false, 8); }
                            else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                            { mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(parsedAssemblyBlocks[2]), true, 8); }
                            // then set other block to mramw
                            if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                            { mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[1], parsedAssemblyBlocks[1].IndexOf("$")), true, 4); }
                            else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                            { mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[1], parsedAssemblyBlocks[1].IndexOf("$")), false, 4); }
                        }
                        else
                        {
                            // convert both blocks to mramr and mramw
                            if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                            { mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[1], parsedAssemblyBlocks[1].IndexOf("$")), true, 4); }
                            else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                            { mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[1], parsedAssemblyBlocks[1].IndexOf("$")), false, 4); }
                            if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                            { mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[2], parsedAssemblyBlocks[2].IndexOf("$")), true, 4); }
                            else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                            { mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[2], parsedAssemblyBlocks[2].IndexOf("$")), false, 4); }
                        }
                    }
                    #endregion

                    #region subtract opcode
                    // pretty much pasted from add opcode, too lazy to do otherwise
                    if (parsedAssemblyBlocks[0] == "sub")
                    {
                        // opcode
                        if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4") { mOpcode = "00010"; }
                        if (Program.compileCoreTarget == "2") { mOpcode = "01000"; }

                        // actual sub instruction
                        if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2") { mDualReadToggle = "01"; }
                        else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4") { mDualReadToggle = "10"; }

                        // because of the RC3.0 architecture, we'll need to detect if either block is an
                        // immediate int. If it is, then we have to set that as immediate value, and have
                        // the other value become mramr
                        // we won't need a check for the case if both of the blocks are #'s, because the syntax 
                        // checker already checks that and takes care of that for us (although the error produced 
                        // is a bit misleading)

                        if (!parsedAssemblyBlocks[1].Contains("$"))
                        {
                            // block1 becomes immediate value
                            if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                            { mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(parsedAssemblyBlocks[1]), false, 8); }
                            else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                            { mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(parsedAssemblyBlocks[1]), true, 8); }
                            // then set other block to mramw
                            if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                            { mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[2], parsedAssemblyBlocks[2].IndexOf("$")), true, 4); }
                            else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                            { mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[2], parsedAssemblyBlocks[2].IndexOf("$")), false, 4); }
                        }
                        else if (!parsedAssemblyBlocks[2].Contains("$"))
                        {
                            // block2 becomes immediate value
                            if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                            { mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(parsedAssemblyBlocks[2]), false, 8); }
                            else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                            { mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(parsedAssemblyBlocks[2]), true, 8); }
                            // then set other block to mramw
                            if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                            { mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[1], parsedAssemblyBlocks[1].IndexOf("$")), true, 4); }
                            else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                            { mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[1], parsedAssemblyBlocks[1].IndexOf("$")), false, 4); }
                        }
                        else
                        {
                            // convert both blocks to mramr and mramw
                            if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                            { mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[1], parsedAssemblyBlocks[1].IndexOf("$")), true, 4); }
                            else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                            { mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[1], parsedAssemblyBlocks[1].IndexOf("$")), false, 4); }
                            if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                            { mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[2], parsedAssemblyBlocks[2].IndexOf("$")), true, 4); }
                            else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                            { mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[2], parsedAssemblyBlocks[2].IndexOf("$")), false, 4); }
                        }
                    }
                    #endregion

                    // ****IMPLEMENT NEW CONDITIONS INTO RC3.0 LEGACY MACHINE CODE - WILL HAVE TO CREATE MULTIPLE INSTRUCTIONS IN THE ASSEMBLYPARSER****
                    #region test instruction
                    if (parsedAssemblyBlocks[0] == "test")
                    {
                        // grab what we're testing
                        // list of things we can test:
                        // SO - shift overflow
                        // SU - shift underflow
                        // <, >, ==
                        // user input 1, 2, 3, 4
                        if (parsedAssemblyBlocks[1] == "%so%")
                        {
                            // test ID
                            if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2") { mCond = "10000"; }
                            else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4") { mCond = "00001"; }
                        }
                        else if (parsedAssemblyBlocks[1] == "%su%")
                        {
                            // test ID
                            if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2") { mCond = "01000"; }
                            else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4") { mCond = "00010"; }
                        }
                        else if (parsedAssemblyBlocks[1] == "%input%")
                        {
                            if (parsedAssemblyBlocks[2] == "0")
                            {
                                if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2") { mCond = "01100"; }
                                else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4") { mCond = "00110"; }
                            }
                            else if (parsedAssemblyBlocks[2] == "1")
                            {
                                if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2") { mCond = "11100"; }
                                else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4") { mCond = "00111"; }
                            }
                            else if (parsedAssemblyBlocks[2] == "2")
                            {
                                if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2") { mCond = "00010"; }
                                else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4") { mCond = "01000"; }
                            }
                            else if (parsedAssemblyBlocks[2] == "3")
                            {
                                if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2") { mCond = "10010"; }
                                else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4") { mCond = "01001"; }
                            }
                        }
                        else
                        {
                            // setup appropriate condition, opcode, and dualreadtoggle IDs
                            switch (parsedAssemblyBlocks[1])
                            {
                                case "==":
                                    if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2") { mCond = "11000"; }
                                    else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4") { mCond = "00011"; }
                                    if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2") { mOpcode = "00011"; }
                                    else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4") { mOpcode = "11000"; }
                                    if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2") { mDualReadToggle = "01"; }
                                    else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4") { mDualReadToggle = "10"; }
                                    break;
                                case ">":
                                    mCond = "00100";
                                    if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2") { mOpcode = "00011"; }
                                    else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4") { mOpcode = "11000"; }
                                    if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2") { mDualReadToggle = "01"; }
                                    else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4") { mDualReadToggle = "10"; }
                                    break;
                                case "<":
                                    if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2") { mCond = "10100"; }
                                    else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4") { mCond = "00101"; }
                                    if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2") { mOpcode = "00011"; }
                                    else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4") { mOpcode = "11000"; }
                                    if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2") { mDualReadToggle = "01"; }
                                    else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4") { mDualReadToggle = "10"; }
                                    break;
                                default:
                                    logging.logWarning("Machine code parser hit unknown case for test - YOU SHOULD NOT BE SEEING THIS ERROR! If you see this, PLEASE FILE A BUG REPORT!!", logging.loggingSenderID.machineCodeParser);
                                    break;
                            }

                            // load two numbers in
                            if (!parsedAssemblyBlocks[2].Contains("$"))
                            {
                                // block1 becomes immediate value
                                if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                                { mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(parsedAssemblyBlocks[2]), false, 8); }
                                else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                                { mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(parsedAssemblyBlocks[2]), true, 8); }
                                // then set other block to mramw
                                if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                                { mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[3], parsedAssemblyBlocks[3].IndexOf("$")), true, 4); }
                                else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                                { mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[3], parsedAssemblyBlocks[3].IndexOf("$")), false, 4); }
                            }
                            else if (!parsedAssemblyBlocks[3].Contains("$"))
                            {
                                // block2 becomes immediate value
                                if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                                { mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(parsedAssemblyBlocks[3]), false, 8); }
                                else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                                { mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(parsedAssemblyBlocks[3]), true, 8); }
                                // then set other block to mramw
                                if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                                { mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[2], parsedAssemblyBlocks[2].IndexOf("$")), true, 4); }
                                else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                                { mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[2], parsedAssemblyBlocks[2].IndexOf("$")), false, 4); }
                            }
                            else
                            {
                                // convert both blocks to mramr and mramw
                                if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                                { mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[2], parsedAssemblyBlocks[2].IndexOf("$")), true, 4); }
                                else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                                { mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[2], parsedAssemblyBlocks[2].IndexOf("$")), false, 4); }
                                if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                                { mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[3], parsedAssemblyBlocks[3].IndexOf("$")), true, 4); }
                                else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                                { mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[3], parsedAssemblyBlocks[3].IndexOf("$")), false, 4); }
                            }
                        }

                    }
                    #endregion

                    #region ramw (ONLY) component
                    if (parsedAssemblyBlocks[0] == "ramw" && !parsedAssemblyBlocks.Contains("ramr")) // SPECIAL CASE - CHECK
                    {
                        // will assume that [ramw $address$ temp] is only case of this, so we will keep it that way
                        if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                        { mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$")), true, 4); }
                        else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                        { mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$")), false, 4); }
                    }
                    #endregion

                    #region ramr + ramw component (for copying values)
                    // can be optimized
                    if (parsedAssemblyBlocks.Contains("ramr") && parsedAssemblyBlocks.Contains("ramw")) // SPECIAL CASE - CHECK (make sure there is only one way to write)
                    {
                        // grab two memory addresses needed
                        if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                        { mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$")), true, 4); }
                        else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                        { mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$")), false, 4); }
                        int nextStartLoc = compiler.parsedAssemblyList[i].IndexOf("$");
                        nextStartLoc = compiler.parsedAssemblyList[i].IndexOf("$", nextStartLoc + 1);
                        if (Program.compileCoreTarget == "1" || Program.compileCoreTarget == "2")
                        { mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$", nextStartLoc + 1)), true, 4); }
                        else if (Program.compileCoreTarget == "3" || Program.compileCoreTarget == "4")
                        { mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$", nextStartLoc + 1)), false, 4); }
                    }
                    #endregion

                    #region fjump component
                    if (parsedAssemblyBlocks.Contains("fjump"))
                    { /*all four cores share identical mgfalse bit layout!*/ mGFalse = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("fjump") + 5), true, 5); }
                    else if (mGFalse == "00000" && generate) { mGFalse = functionLibrary.convertNumberToBinary(fJumpDefault, true, 5); }
                    #endregion

                    #region tjump component
                    if (parsedAssemblyBlocks.Contains("tjump"))
                    { /*all four cores share identical mgtrue bit layout!*/ mGTrue = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("tjump") + 5), true, 5); }
                    #endregion

                    if (generate)
                    {
                        currentInstructionWord = mGFalse + " " + mGTrue + " " + mCond + " " + mImmediate + " " + mRamW + " " + mRamR + " " + mOpcode + " " + mDualReadToggle + " " + mExtension;
                        addToMachineCodeList(currentInstructionWord);
                    }
                    fJumpDefault++;
                }
                logging.logDebug("Machine code generation complete!", logging.loggingSenderID.machineCodeParser);
                if (Program.moreVerboseDebug) logging.logDebug("Dumping generated machine code ...", logging.loggingSenderID.machineCodeParser);
                if (Program.moreVerboseDebug) for (int i = 0; i < machineCodeList.Count; i++) logging.logDebug("[" + (i + 1).ToString("D" + digits) + "]: " + machineCodeList[i] + " | len " + machineCodeList[i].Length.ToString(), logging.loggingSenderID.machineCodeParser);
            }
            #endregion
            #region RC4.0 machine code generation
            else if (Program.compileTarget == "2")
            {
                // instruction set bit layout for RC4.0 cores 1 - 4 (facing south)
                // [0  0  0  0  0  0  0 0 0 0 0 0 0 0 0 | 0 0 0 0 0 0 0 0 0  0  0  0  0  0  0] [0 0 0 0] [0 0 0 0  0  0] [0 0 0 0  0  0] [0 0 0 0] [0   0  0  0  0 0 0 0] [0 ] [0  0 0 0 0] [0  0 0 0 0] [0 0 0 0] [0] [0 0 0 0] [0 0 0 0] [0 0]
                // [15 14 13 12 11 10 9 8 7 6 5 4 3 2 1] [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15] [E 4 2 1] [1 2 4 8 16 32] [1 2 4 8 16 32] [1 2 4 8] [128 64 32 16 8 4 2 1] [E ] [16 8 4 2 1] [16 8 4 2 1] [1 2 4 8] [E] [1 2 4 8] [E 1 2 4] [2 1] (E = toggle bit, this bit will be used to toggle things)
                // [GPU GRANULAR X                     ] [GPU GRANULAR Y                     ] [CLOCKID] [GOTO TRUE    ] [GOTO FALSE   ] [TEST ID] [IMMEDIATE           ] [DR] [RAM READ  ] [RAM WRITE ] [OPCODE ] [M] [EXT.   ] [CACHE C] [ADR]
                // CLOCKID is ID of new clock speed if you want to change it; TEST ID is CONDITION; DR is dual-read toggle; M is mode toggle; CACHE C is cache control; ADR is cache address selector (0 based, so 0 is 1, 3 is 4)

                // var for incrementing line of code location if none provided (default)
                int fJumpDefault = 1; // on the first line of code, we jump to line 2
                bool generate = true; // if this is true, we will generated a line of code; if it's not (i.e. malloc for values returning 0), will not generate!

                for (int i = 0; i < compiler.parsedAssemblyList.Count; i++)
                {
                    string[] parsedAssemblyBlocks = compiler.parsedAssemblyList[i].Split(" "); // do it this way to make machine code generation more robust

                    // more verbose debug
                    if (Program.moreVerboseDebug) logging.logDebug("[" + compiler.parsedAssemblyList[i] + "]", logging.loggingSenderID.machineCodeParser);

                    fJumpDefault++;

                    // init
                    #region Init vars
                    string currentInstructionWord = null;

                    // will break down machine code into different segments
                    string mGPUGX = "000000000000000";
                    string mGPUGY = "000000000000000";
                    string mClockID = "0000";
                    string mMode = "0";
                    string mCacheC = "0000";
                    string mCacheAddr = "00";
                    string mGFalse = "000000";
                    string mGTrue = "000000";
                    string mCond = "0000";
                    string mImmediate = "00000000";
                    string mRamW = "00000";
                    string mRamR = "00000";
                    string mOpcode = "0000";
                    string mDualReadToggle = "0";
                    string mExtension = "0000";
                    #endregion

                    // now find what function or thing we need to do
                    #region malloc opcode
                    if (parsedAssemblyBlocks[0] == "malloc")
                    {
                        // grab values
                        int memAddr = acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("malloc") + 7);
                        int value = acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].LastIndexOf("$") + 1);
                        // update machine code
                        if (generate == true)
                        {
                            mRamW = functionLibrary.convertNumberToBinary(memAddr, false, 5);
                            mImmediate = functionLibrary.convertNumberToBinary(value, false, 8);
                            // only jump that we should see with a malloc
                            if (parsedAssemblyBlocks.Contains("fjump")) // SPECIAL CASE - CHECK
                            {
                                // grab fjump location and convert it to binary
                                mGFalse = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("fjump") + 5), true, 6);
                            }
                        }
                    }
                    #endregion

                    #region lmalloc opcode
                    if (parsedAssemblyBlocks[0] == "lmalloc")
                    {
                        // grab values
                        int memAddr = acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("lmalloc") + 9);
                        int value = acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].LastIndexOf("$") + 1);
                        // update machine code
                        if (generate == true)
                        {
                            mRamW = functionLibrary.convertNumberToBinary(memAddr, false, 5);
                            mImmediate = functionLibrary.convertNumberToBinary(value, false, 8);
                            // only jump that we should see with a lmalloc
                            if (parsedAssemblyBlocks.Contains("fjump")) // SPECIAL CASE - CHECK
                            {
                                // grab fjump location and convert it to binary
                                mGFalse = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("fjump") + 5), true, 6);
                            }
                        }
                    }
                    #endregion

                    #region exit opcode
                    if (parsedAssemblyBlocks[0] == "exit") { mOpcode = "1001"; }
                    #endregion

                    // officially not supported in v1.1 of ARCISS but in here for reference
                    #region flush opcode
                    if (parsedAssemblyBlocks[0] == "flush")
                    {
                        int regToFlush = acquireMemoryAddress(compiler.parsedAssemblyList[i], 6) + 2; // +2 is to make it conform to ID required in ext. data
                        mOpcode = "0011";
                        mExtension = functionLibrary.convertNumberToBinary(regToFlush, true, 4);
                    }
                    #endregion

                    #region output opcode
                    // syntax one
                    if (parsedAssemblyBlocks[0] == "output")
                    {
                        mOpcode = "0101";

                        // target specific code
                        if (parsedAssemblyBlocks[2] == "%dp_") { mExtension = "1100"; }
                        else if (parsedAssemblyBlocks[2] == "%dec") { mExtension = "1000"; }
                        else if (parsedAssemblyBlocks[2] == "%raw") { mExtension = "0100"; }
                        else if (parsedAssemblyBlocks[2] == "%out" && parsedAssemblyBlocks[1] == "0") { mExtension = "0010"; }
                        else if (parsedAssemblyBlocks[2] == "%out" && parsedAssemblyBlocks[1] == "1") { mExtension = "1010"; }
                        else if (parsedAssemblyBlocks[2] == "%out" && parsedAssemblyBlocks[1] == "2") { mExtension = "0110"; }
                        else if (parsedAssemblyBlocks[2] == "%out" && parsedAssemblyBlocks[1] == "3") { mExtension = "1110"; }

                        // global var - worry about local var in next revision of the compiler
                        if (!parsedAssemblyBlocks[1].Contains("$")) { mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(parsedAssemblyBlocks[1]), false, 8); }
                        else { mRamR = functionLibrary.convertNumberToBinary(functionLibrary.acquireBlockID(parsedAssemblyBlocks[1], 1), false, 5); }
                    }
                    #endregion

                    #region input opcode
                    if (parsedAssemblyBlocks[0] == "input")
                    {
                        mOpcode = "1101";

                        // target specific code
                        if (parsedAssemblyBlocks[1] == "%ui_") { mExtension = "1010"; }
                        else if (parsedAssemblyBlocks[1] == "%rng") { mExtension = "0110"; }
                        else if (parsedAssemblyBlocks[1] == "%dp_") { mExtension = "1110"; }

                        // global var - worry about local var in next revision of the compiler
                        if (!parsedAssemblyBlocks[2].Contains("$")) { mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(parsedAssemblyBlocks[2]), false, 8); }
                        else { mRamW = functionLibrary.convertNumberToBinary(functionLibrary.acquireBlockID(parsedAssemblyBlocks[2], 1), false, 5); }
                    }

                    #endregion

                    #region sleep opcode
                    // literally do nothing
                    if (parsedAssemblyBlocks[0] == "sleep") { /*do nothing*/ }
                    #endregion

                    #region bsr opcode
                    if (parsedAssemblyBlocks[0] == "bsr")
                    {
                        // generate opcode
                        mOpcode = "1010";
                        mDualReadToggle = "1";

                        // grab address to modify
                        // since this is bsr, and we have an architectual limit where we only can access bsr on cache b, change this to read cache b! (mramw)
                        if (!parsedAssemblyBlocks.Contains("~temp~")) mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$")), false, 5); // SPECIAL CASE - CHECK
                    }
                    #endregion

                    #region bsl opcode
                    if (parsedAssemblyBlocks[0] == "bsl")
                    {
                        // generate opcode
                        mOpcode = "0010";

                        // grab address to modify
                        if (!parsedAssemblyBlocks.Contains("~temp~")) mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], (compiler.parsedAssemblyList[i].IndexOf("$"))), false, 5); // SPECIAL CASE - CHECK
                    }
                    #endregion

                    #region ad1 opcode
                    if (parsedAssemblyBlocks[0] == "ad1")
                    {
                        // ad1 instruction
                        // opcode
                        mOpcode = "1000";

                        // mem addr
                        // we have this statement here because ad1 temp[x] temp[x] instructions can be generated
                        if (parsedAssemblyBlocks.Contains("$")) // SPECIAL CASE - CHECK
                        { mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$")), false, 5); }
                        // immediate value
                        mImmediate = "00000001";
                    }
                    #endregion

                    #region sb1 opcode
                    if (parsedAssemblyBlocks[0] == "sb1")
                    {
                        // opcode
                        mOpcode = "0100";
                        // mem addr
                        // we have this statement here because sb1 temp[x] temp[x] instructions can be generated
                        if (parsedAssemblyBlocks.Contains("$")) // SPECIAL CASE - CHECK
                        { mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$")), false, 5); }
                        // immediate value
                        mImmediate = "00000001";
                    }
                    #endregion

                    #region not opcode
                    if (parsedAssemblyBlocks[0] == "not")
                    {
                        // opcode
                        mOpcode = "1100";
                        // mem addr
                        mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$")), false, 5);
                    }
                    #endregion

                    #region add opcode
                    if (parsedAssemblyBlocks[0] == "add")
                    {
                        // opcode
                        mOpcode = "1000";

                        // actual add instruction
                        mDualReadToggle = "1";

                        // because of the RC4.0 architecture, we'll need to detect if either block is an
                        // immediate int. If it is, then we have to set that as immediate value, and have
                        // the other value become mramr
                        // we won't need a check for the case if both of the blocks are #'s, because the syntax 
                        // checker already checks that and takes care of that for us (although the error produced 
                        // is a bit misleading)
                        // also b/c of the RC4.0 architecture and the way it handles immediate values
                        // (piped into cache b instead of cache a like the RC3.0), we will have to change this slightly
                        if (!parsedAssemblyBlocks[1].Contains("$") && parsedAssemblyBlocks[1] != "~temp~")
                        {
                            // block1 becomes immediate value
                            mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(parsedAssemblyBlocks[1]), false, 8);

                            // then set other block to mramr
                            mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[2], parsedAssemblyBlocks[2].IndexOf("$")), false, 5);
                        }
                        else if (!parsedAssemblyBlocks[2].Contains("$") && parsedAssemblyBlocks[2] != "~temp~")
                        {
                            // block2 becomes immediate value
                            mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(parsedAssemblyBlocks[2]), false, 8);
                            // then set other block to mramr
                            mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[1], parsedAssemblyBlocks[1].IndexOf("$")), false, 5);
                        }
                        else
                        {
                            // convert both blocks to mramr and mramw
                            if (!parsedAssemblyBlocks[1].Contains("~temp~")) mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[1], parsedAssemblyBlocks[1].IndexOf("$")), false, 5);
                            if (!parsedAssemblyBlocks[2].Contains("~temp~")) mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[2], parsedAssemblyBlocks[2].IndexOf("$")), false, 5);
                        }
                    }
                    #endregion

                    #region subtract opcode
                    // pretty much copied from add opcode, too lazy to do otherwise
                    if (parsedAssemblyBlocks[0] == "sub")
                    {
                        // opcode
                        mOpcode = "0100";

                        // actual add instruction
                        mDualReadToggle = "1";

                        // because of the RC4.0 architecture, we'll need to detect if either block is an
                        // immediate int. If it is, then we have to set that as immediate value, and have
                        // the other value become mramr
                        // we won't need a check for the case if both of the blocks are #'s, because the syntax 
                        // checker already checks that and takes care of that for us (although the error produced 
                        // is a bit misleading)
                        // also b/c of the RC4.0 architecture and the way it handles immediate values
                        // (piped into cache b instead of cache a like the RC3.0), we will have to change this slightly
                        if (!parsedAssemblyBlocks[1].Contains("$") && parsedAssemblyBlocks[1] != "~temp~")
                        {
                            // block1 becomes immediate value
                            mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(parsedAssemblyBlocks[1]), false, 8);

                            // then set other block to mramr
                            mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[2], parsedAssemblyBlocks[2].IndexOf("$")), false, 5);
                        }
                        else if (!parsedAssemblyBlocks[2].Contains("$") && parsedAssemblyBlocks[2] != "~temp~")
                        {
                            // block2 becomes immediate value
                            mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(parsedAssemblyBlocks[2]), false, 8);
                            // then set other block to mramr
                            mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[1], parsedAssemblyBlocks[1].IndexOf("$")), false, 5);
                        }
                        else
                        {
                            // convert both blocks to mramr and mramw
                            if (!parsedAssemblyBlocks[1].Contains("~temp~")) mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[1], parsedAssemblyBlocks[1].IndexOf("$")), false, 5);
                            if (!parsedAssemblyBlocks[2].Contains("~temp~")) mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[2], parsedAssemblyBlocks[2].IndexOf("$")), false, 5);
                        }
                    }
                    #endregion

                    #region RC4.0 specific functions

                    #region gpu encode opcode
                    if (parsedAssemblyBlocks[0] == "gpuencode")
                    {
                        // opcode
                        mOpcode = "0110";
                        mDualReadToggle = "1";

                        // load two numbers in
                        // will have to do it this way b/c we can have either $x$ #, # $x$, or $x$ $x$
                        // block1 will be ramr, block2 will be ramw
                        if (parsedAssemblyBlocks[1].Contains("$"))
                        { mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[1], parsedAssemblyBlocks[1].IndexOf("$")), false, 5); } /* grab address */
                        else { mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(parsedAssemblyBlocks[1]), false, 8); }
                        if (parsedAssemblyBlocks[2].Contains("$"))
                        { mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[2], parsedAssemblyBlocks[2].IndexOf("$")), false, 5); }
                        else { mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(parsedAssemblyBlocks[2]), false, 8); }
                    }
                    #endregion

                    #region gpu clear opcode
                    if (parsedAssemblyBlocks[0] == "gpuclr")
                    { /* easy */  mOpcode = "1000"; mMode = "1"; }
                    #endregion

                    #region gpu draw opcode
                    if (parsedAssemblyBlocks[0] == "gpudraw")
                    {
                        mOpcode = "0100";
                        mMode = "1";
                        if (compiler.parsedAssemblyList[i].Contains("$")) // SPECIAL CASE - CHECK
                        {
                            // we know it's a var syntax
                            mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$")), false, 5);
                        }
                        else
                        {
                            // we know it's 2 immediate numbers syntax
                            // load two numbers in
                            string num1 = null;
                            string num2 = null;
                            num1 = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], 7), true, 4);
                            int nextStartLoc = compiler.parsedAssemblyList[i].IndexOf(" ");
                            nextStartLoc = compiler.parsedAssemblyList[i].IndexOf(" ", nextStartLoc + 1);
                            num2 = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], nextStartLoc), true, 4);
                            mImmediate = num2 + num1;
                        }
                    }
                    #endregion

                    #region gpu erase opcode
                    if (parsedAssemblyBlocks[0] == "gpuerase")
                    {
                        mOpcode = "1100";
                        mMode = "1";
                        if (compiler.parsedAssemblyList[i].Contains("$")) // SPECIAL CASE - CHECK
                        {
                            // we know it's a var syntax
                            mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$")), false, 5);
                        }
                        else
                        {
                            // we know it's 2 immediate numbers syntax
                            // load two numbers in
                            string num1 = null;
                            string num2 = null;
                            num1 = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], 8), true, 4);
                            int nextStartLoc = compiler.parsedAssemblyList[i].IndexOf(" ");
                            nextStartLoc = compiler.parsedAssemblyList[i].IndexOf(" ", nextStartLoc + 1);
                            num2 = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], nextStartLoc), true, 4);
                            mImmediate = num2 + num1;
                        }
                    }
                    #endregion

                    #region gpu granular update opcode
                    if (parsedAssemblyBlocks[0] == "gpugu")
                    {
                        mOpcode = "0010";
                        mMode = "1";

                        // hex stuff
                        mGPUGX = functionLibrary.convertNumberToBinary(int.Parse(parsedAssemblyBlocks[1].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber), false, 15);
                        mGPUGY = functionLibrary.convertNumberToBinary(int.Parse(parsedAssemblyBlocks[2].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber), false, 15);
                        // bool stuff
                        mExtension = "01" + parsedAssemblyBlocks[3] + parsedAssemblyBlocks[4];
                    }
                    #endregion

                    #region update clock speed opcode
                    if (parsedAssemblyBlocks[0] == "updspd")
                    {
                        // don't even need an opcode!
                        // grab clock id
                        string clockID = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], 7), false, 3);
                        mClockID = "1" + clockID;
                    }
                    #endregion

                    #endregion

                    // WILL ALSO NEED TO ADD IN A=0 / B=0 TESTS!
                    #region test instruction
                    if (parsedAssemblyBlocks[0] == "test")
                    {
                        // grab what we're testing
                        // list of things we can test:
                        // SO - shift overflow
                        // SU - shift underflow
                        // <, >, ==
                        // user input 1, 2, 3, 4
                        if (parsedAssemblyBlocks[1] == "%so%")
                        {
                            // test ID
                            mCond = "1000";
                        }
                        else if (parsedAssemblyBlocks[1] == "%su%")
                        {
                            // test ID
                            mCond = "0100";
                        }
                        // hardware conditions specific to RC4.0
                        else if (parsedAssemblyBlocks[1] == "%rng_on%") { mCond = "0011"; }

                        else if (parsedAssemblyBlocks[1] == "%input%")
                        {
                            // collect which one it is
                            if (parsedAssemblyBlocks[2] == "0") { mCond = "0001"; }
                            else if (parsedAssemblyBlocks[2] == "1") { mCond = "1001"; }
                            else if (parsedAssemblyBlocks[2] == "2") { mCond = "0101"; }
                            else if (parsedAssemblyBlocks[2] == "3") { mCond = "1101"; }
                        }

                        // >= and <= are not here b/c they are split up in the assembly parsing stage to match the target's available hardware
                        // try to combine == and != somehow (only difference is swapping tjump and fjump locations)
                        else
                        {
                            switch (parsedAssemblyBlocks[1])
                            {
                                case "==":
                                    // test ID
                                    mCond = "1100";
                                    mOpcode = "0001";
                                    mDualReadToggle = "1";
                                    break;
                                case "!=":
                                    // test ID
                                    mCond = "1100";
                                    mOpcode = "0001";
                                    mDualReadToggle = "1";

                                    // swap fjump and tjump
                                    compiler.parsedAssemblyList[i] = compiler.parsedAssemblyList[i].Replace("tjump", "tjumpr");
                                    compiler.parsedAssemblyList[i] = compiler.parsedAssemblyList[i].Replace("fjump", "tjump");
                                    compiler.parsedAssemblyList[i] = compiler.parsedAssemblyList[i].Replace("tjumpr", "fjump");
                                    break;
                                case ">":
                                    // test ID
                                    mCond = "0010";
                                    mOpcode = "0001";
                                    mDualReadToggle = "1";
                                    // will perform same checks as ==, check that method for more info
                                    break;
                                case "<":
                                    // test ID
                                    mCond = "1010";
                                    mOpcode = "0001";
                                    mDualReadToggle = "1";
                                    // will perform same checks as ==, check that method for more info
                                    break;
                                default:
                                    logging.logWarning("Machine code parser hit unknown case for test - YOU SHOULD NOT BE SEEING THIS ERROR! If you see this, PLEASE FILE A BUG REPORT!!", logging.loggingSenderID.machineCodeParser);
                                    break;
                            }

                            // load two numbers in

                            // because of the RC4.0 architecture, we'll need to detect if either block is an
                            // immediate int. If it is, then we have to set that as immediate value, and have the other value become mramr
                            // we won't need a check for the case if both of the blocks are #'s, because the syntax 
                            // checker already checks that and takes care of that for us (although the error produced is a bit misleading)
                            // also b/c of the RC4.0 architecture and the way it handles immediate values
                            // (piped into cache a instead of cache b like the RC3.0), we will have to change this slightly
                            if (!parsedAssemblyBlocks[2].Contains("$"))
                            {
                                // block1 becomes immediate value
                                mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(parsedAssemblyBlocks[2]), false, 8);

                                // then set other block to mramr
                                mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[3], parsedAssemblyBlocks[3].IndexOf("$")), false, 5);
                            }
                            else if (!parsedAssemblyBlocks[3].Contains("$"))
                            {
                                // block2 becomes immediate value
                                mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(parsedAssemblyBlocks[3]), false, 8);
                                // then set other block to mramr
                                mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[2], parsedAssemblyBlocks[2].IndexOf("$")), false, 5);
                            }
                            else
                            {
                                // convert both blocks to mramr and mramw
                                mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[2], parsedAssemblyBlocks[2].IndexOf("$")), false, 5);
                                mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[3], parsedAssemblyBlocks[3].IndexOf("$")), false, 5);
                            }
                        }
                    }
                    #endregion

                    #region ramw (ONLY) component
                    if (parsedAssemblyBlocks[0] == "ramw" && !parsedAssemblyBlocks.Contains("ramr")) // SPECIAL CASE - CHECK
                    {
                        // will assume that [ramw $address$ temp] is only case of this, so we will keep it that way
                        mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$")), false, 5);
                    }
                    #endregion

                    #region ramr + ramw component (for copying values)
                    // can be optimized
                    if (parsedAssemblyBlocks.Contains("ramr") && parsedAssemblyBlocks.Contains("ramw")) // SPECIAL CASE - CHECK
                    {
                        // grab two memory addresses needed
                        mRamW = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$")), false, 5);
                        int nextStartLoc = compiler.parsedAssemblyList[i].IndexOf("$");
                        nextStartLoc = compiler.parsedAssemblyList[i].IndexOf("$", nextStartLoc + 1);
                        mRamR = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$", nextStartLoc + 1)), false, 5);

                        // will also need to have an opcode to have value pass through ALU to get back to RAM; add will be sufficient - SPECIAL CASE - CHECK
                        // this may be a problem with specific instances (i.e. issue #16)
                        mOpcode = "1000";
                    }
                    #endregion

                    #region fjump component
                    if (parsedAssemblyBlocks.Contains("fjump")) // SPECIAL CASE - CHECK
                    { mGFalse = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("fjump") + 5), true, 6); }
                    else if (mGFalse == "000000" && generate)
                    { mGFalse = functionLibrary.convertNumberToBinary(fJumpDefault, true, 6); }
                    #endregion

                    #region tjump component
                    if (parsedAssemblyBlocks.Contains("tjump")) // SPECIAL CASE - CHECK
                    { mGTrue = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("tjump") + 5), true, 6); }
                    #endregion

                    if (generate)
                    {
                        currentInstructionWord = mGPUGX + " " + mGPUGY + " " + mClockID + " " + mGTrue + " " + mGFalse + " " + mCond + " " + mImmediate + " " + mDualReadToggle + " " + mRamR + " " + mRamW + " " + mOpcode + " " + mMode + " " + mExtension + " " + mCacheC + " " + mCacheAddr;
                        addToMachineCodeList(currentInstructionWord);
                    }
                }
                logging.logDebug("Machine code generation complete!", logging.loggingSenderID.machineCodeParser);
                if (Program.moreVerboseDebug) logging.logDebug("Dumping generated machine code ...", logging.loggingSenderID.machineCodeParser);
                if (Program.moreVerboseDebug) for (int i = 0; i < machineCodeList.Count; i++) logging.logDebug("[" + (i + 1).ToString("D" + digits) + "]: " + machineCodeList[i] + " | len " + machineCodeList[i].Length.ToString(), logging.loggingSenderID.machineCodeParser);
            }
            #endregion
            #region RC5.0 machine code generation
            else if (Program.compileTarget == "3")
            {
                // instruction set bit layout for RC5.0 cores 1 - 4 (facing east)
                // [0 0 0 0 0  0  0 ] [0 0 0 0 0  0  0 ] [0 0 0 0] [0 0 0 0] [0 0 0 0 0 ] [0] [0 0 0 0 0 ] [0] [0 0 0 0 0 ] [0] [0 0 0 0 0  0  0  0  ]
                // [1 2 4 8 16 32 64] [1 2 4 8 16 32 64] [1 2 4 8] [1 2 4 8] [1 2 4 8 16] [1] [1 2 4 8 16] [1] [1 2 4 8 16] [1] [1 2 4 8 16 32 64 128]
                // [GOTO FALSE      ] [GOTO TRUE       ] [EXT.   ] [OPCODE ] [SEL. BITS ] [F] [RAM READ  ] [R] [RAM WRITE ] [W] [IMMEDIATE           ]
                // F is FORWRDING TOGGLE, R is READ RAM/CACHE TOGGLE, W is WRITE RAM/CACHE TOGGLE

                // var for incrementing line of code location if none provided (default)
                int fJumpDefault = 0; // on the first line of code, we jump to line 1
                bool generate = true; // if this is true, we will generated a line of code; if it's not (i.e. malloc for values returning 0), will not generate!

                for (int i = 0; i < compiler.parsedAssemblyList.Count; i++)
                {
                    string[] parsedAssemblyBlocks = compiler.parsedAssemblyList[i].Split(" "); // do it this way to make machine code generation more robust

                    // more verbose debug
                    if (Program.moreVerboseDebug) logging.logDebug("[" + compiler.parsedAssemblyList[i] + "]", logging.loggingSenderID.machineCodeParser);

                    fJumpDefault++;

                    // init
                    #region Init vars
                    string currentInstructionWord = null;

                    // break down into separate segments
                    string mGFalse = "0000000";
                    string mGTrue = "0000000";
                    string mExtension = "0000";
                    string mOpcode = "0000";
                    string mSelectBit1 = "0"; string mSelectBit2 = "0"; string mSelectBit3 = "0"; string mSelectBit4 = "0"; string mSelectBit5 = "0";
                    string mToggleForward = "0";
                    string mRead = "00000";
                    string mReadToggle = "1"; // 0 is cache, 1 is RAM
                    string mWrite = "00000";
                    string mWriteToggle = "1"; // 0 is cache, 1 is RAM
                    string mImmediate = "00000000";
                    #endregion

                    // now find what function or thing we need to do
                    #region malloc opcode
                    if (parsedAssemblyBlocks[0] == "malloc")
                    {
                        // grab values
                        int memAddr = acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("malloc") + 7);
                        int value = acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].LastIndexOf("$") + 1);
                        // update machine code
                        if (generate == true)
                        {
                            mWrite = functionLibrary.convertNumberToBinary(memAddr, true, 5);
                            mImmediate = functionLibrary.convertNumberToBinary(value, true, 8);
                            // only jump that we should see with a malloc
                            if (parsedAssemblyBlocks.Contains("fjump")) // SPECIAL CASE - CHECK
                            {
                                // grab fjump location and convert it to binary
                                mGFalse = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("fjump") + 5), true, 7);
                            }
                        }
                    }
                    #endregion

                    #region lmalloc opcode
                    if (parsedAssemblyBlocks[0] == "lmalloc")
                    {
                        // grab values
                        int memAddr = acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("lmalloc") + 9);
                        int value = acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].LastIndexOf("$") + 1);
                        // update machine code
                        if (generate == true)
                        {
                            // for writing to cache, need leftmost bit set on mWrite + inverted mWrite address
                            mWrite = "1" + functionLibrary.convertNumberToBinary(memAddr, false, 4);
                            mImmediate = functionLibrary.convertNumberToBinary(value, true, 8);
                            mWriteToggle = "0"; // to denote caching
                            // only jump that we should see with a lmalloc
                            if (parsedAssemblyBlocks.Contains("fjump")) // SPECIAL CASE - CHECK
                            {
                                // grab fjump location and convert it to binary
                                mGFalse = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("fjump") + 5), true, 7);
                            }
                        }
                    }
                    #endregion

                    #region exit opcode
                    if (parsedAssemblyBlocks[0] == "exit")
                    {
                        // COFF w/ global off
                        mSelectBit1 = "1";
                        mOpcode = "1100";
                        mExtension = "1001";
                    }
                    #endregion

                    #region output opcode
                    if (parsedAssemblyBlocks[0] == "output")
                    {
                        // no opcode - memory based!

                        // target specific code
                        mSelectBit4 = "1";
                        if (parsedAssemblyBlocks[2] == "%dp_") { mWrite = "11000"; }
                        else if (parsedAssemblyBlocks[2] == "%dec") { mWrite = "00100"; }
                        else if (parsedAssemblyBlocks[2] == "%raw") { mWrite = "10100"; }
                        else if (parsedAssemblyBlocks[2] == "%out") { mWrite = "01100"; }
                        else if (parsedAssemblyBlocks[2] == "%gt_") { mWrite = "01000"; }
                        else if (parsedAssemblyBlocks[2] == "%spd") { mWrite = "10000"; }
                        else if (parsedAssemblyBlocks[2] == "%stk") { mWrite = "10010"; }
                        else if (parsedAssemblyBlocks[2] == "%gex") { mWrite = "00010"; }

                        // global var
                        if (parsedAssemblyBlocks[1].Contains("$") && parsedAssemblyBlocks[1].Count(x => x == '$') == 2)
                        { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[1], 0), true, 5); }
                        // local var
                        // if we have cache, need to have add or sub opcode to route cache data to 
                        else if (parsedAssemblyBlocks[1].Contains("$") && parsedAssemblyBlocks[1].Count(x => x == '$') == 4)
                        { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[1], 1), true, 4) + "1"; mReadToggle = "0"; mOpcode = "1000"; }
                        else if (!parsedAssemblyBlocks[1].Contains("$"))
                        {
                            // passthrough for immediate data
                            mRead = "00001"; mReadToggle = "0";
                            mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(parsedAssemblyBlocks[1]), true, 8);
                        }
                    }
                    #endregion

                    #region input opcode
                    if (parsedAssemblyBlocks[0] == "input") // test input syntax will be located under test opcode
                    {
                        // no opcode - memory based!

                        // target specific code
                        mSelectBit3 = "1";
                        mSelectBit5 = "1";
                        if (parsedAssemblyBlocks[1] == "%dp_") { mRead = "11000"; }
                        else if (parsedAssemblyBlocks[1] == "%ui_") { mRead = "00100"; }
                        else if (parsedAssemblyBlocks[1] == "%rng") { mRead = "10100"; }
                        else if (parsedAssemblyBlocks[1] == "%stk") { mRead = "01100"; }
                        else if (parsedAssemblyBlocks[1] == "%gt_") { mRead = "01000"; }
                        else if (parsedAssemblyBlocks[1] == "%spd") { mRead = "10000"; }

                        // global var
                        if (parsedAssemblyBlocks[2].Contains("$") && parsedAssemblyBlocks[2].Count(x => x == '$') == 2)
                        { mWrite = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[2], 0), true, 5); }
                        // local var
                        else if (parsedAssemblyBlocks[2].Contains("$") && parsedAssemblyBlocks[2].Count(x => x == '$') == 4)
                        { mWrite = "1" + functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[2], 1), false, 4); mWriteToggle = "0"; }
                    }
                    #endregion

                    #region sleep opcode
                    // literally nothing
                    if (parsedAssemblyBlocks[0] == "sleep") { /*do nothing*/ }
                    #endregion

                    #region bsr opcode
                    if (parsedAssemblyBlocks[0] == "bsr")
                    {
                        mOpcode = "1010";

                        // forward check
                        if (!parsedAssemblyBlocks.Contains("%forward%")) // SPECIAL CASE - CHECK
                        {
                            if (compiler.parsedAssemblyList[i].Count(x => x == '$') == 2)
                            { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$")), true, 5); }
                            else if (compiler.parsedAssemblyList[i].Count(x => x == '$') == 4)
                            { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$") + 1), true, 4) + "1"; mReadToggle = "0"; }
                        }
                    }
                    #endregion

                    #region bsl opcode
                    if (parsedAssemblyBlocks[0] == "bsl")
                    {
                        mOpcode = "0010";

                        // forward check
                        if (!parsedAssemblyBlocks.Contains("%forward%")) // SPECIAL CASE - CHECK
                        {
                            if (compiler.parsedAssemblyList[i].Count(x => x == '$') == 2)
                            { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$")), true, 5); }
                            else if (compiler.parsedAssemblyList[i].Count(x => x == '$') == 4)
                            { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$") + 1), true, 4) + "1"; mReadToggle = "0"; }
                        }
                    }
                    #endregion

                    #region ad1 opcode
                    if (parsedAssemblyBlocks[0] == "ad1")
                    {
                        mImmediate = "10000000";
                        mOpcode = "1000";

                        // forward check - revisit
                        if (!parsedAssemblyBlocks.Contains("%forward%")) // SPECIAL CASE - CHECK
                        {
                            // pipe variable into input A
                            if (compiler.parsedAssemblyList[i].Count(x => x == '$') == 2)
                            { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$")), true, 5); }
                            else if (compiler.parsedAssemblyList[i].Count(x => x == '$') == 4)
                            { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$") + 1), true, 4) + "1"; mReadToggle = "0"; }
                            // pipe immediate value into input B
                            mWrite = "10000";
                            mWriteToggle = "0";
                        }
                    }
                    #endregion

                    #region sb1 opcode
                    if (parsedAssemblyBlocks[0] == "sb1")
                    {
                        mImmediate = "10000000";
                        mOpcode = "0100";

                        // forward check - revisit
                        if (!parsedAssemblyBlocks.Contains("%forward%")) // SPECIAL CASE - CHECK
                        {
                            // pipe variable into input A
                            if (compiler.parsedAssemblyList[i].Count(x => x == '$') == 2)
                            { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$")), true, 5); }
                            else if (compiler.parsedAssemblyList[i].Count(x => x == '$') == 4)
                            { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$") + 1), true, 4) + "0"; mReadToggle = "0"; }
                            // pipe immediate value into input B
                            mWrite = "10000";
                            mWriteToggle = "0";
                        }
                    }
                    #endregion

                    #region not opcode
                    if (parsedAssemblyBlocks[0] == "not")
                    {
                        mOpcode = "0110";

                        // forward check
                        if (!parsedAssemblyBlocks.Contains("%forward%")) // SPECIAL CASE - CHECK
                        {
                            if (compiler.parsedAssemblyList[i].Count(x => x == '$') == 2)
                            { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$")), false, 5); }
                            else if (compiler.parsedAssemblyList[i].Count(x => x == '$') == 4)
                            { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$") + 1), true, 4) + "1"; mReadToggle = "0"; }
                        }
                    }
                    #endregion

                    // consolidate add and sub
                    #region add opcode
                    if (parsedAssemblyBlocks[0] == "add")
                    {
                        // 3 possibilities: [add var var], [add var num], and [add num var]
                        mOpcode = "1000";

                        // UNIT TESTED - all code paths here work as intended!
                        // forward check
                        if (!parsedAssemblyBlocks.Contains("%forward%")) // SPECIAL CASE - CHECK
                        {
                            if (!parsedAssemblyBlocks[1].Contains("$") && parsedAssemblyBlocks[1] != "~temp~")
                            {
                                // pipe immediate into input A, var into input B
                                mSelectBit2 = "1"; // need dual-read to perform above task
                                                   // block1 becomes immediate value
                                mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(parsedAssemblyBlocks[1]), true, 8); mRead = "00001"; mReadToggle = "0";

                                // then set other block to mWrite b/c dual-read is toggled
                                if (parsedAssemblyBlocks[2].Count(x => x == '$') == 2)
                                { mWrite = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[2], 0), true, 5); }
                                else if (parsedAssemblyBlocks[2].Count(x => x == '$') == 4)
                                { mWrite = "0" + functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[2], 1), false, 4); mWriteToggle = "0"; }
                            }
                            else if (!parsedAssemblyBlocks[2].Contains("$"))
                            {
                                // pipe immediate into input B, var into input A
                                // default inputs, so don't need to do anything fancy
                                // block2 becomes immediate value
                                mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(parsedAssemblyBlocks[2]), true, 8); mWrite = "10000"; mWriteToggle = "0";
                                // then set other block to mRead
                                if (parsedAssemblyBlocks[1].Count(x => x == '$') == 2)
                                { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[1], 0), true, 5); }
                                else if (parsedAssemblyBlocks[1].Count(x => x == '$') == 4)
                                { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[1], 1), true, 4) + "0"; mReadToggle = "0"; }
                            }
                            else
                            {
                                // convert both blocks to mRead and mWrite
                                mSelectBit2 = "1";

                                // also catches ~temp~ in first block since if it is present, these will not execute
                                if (parsedAssemblyBlocks[1].Count(x => x == '$') == 2)
                                { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[1], 0), true, 5); }
                                else if (parsedAssemblyBlocks[1].Count(x => x == '$') == 4)
                                { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[1], 1), true, 4) + "1"; mReadToggle = "0"; }

                                if (parsedAssemblyBlocks[2].Count(x => x == '$') == 2)
                                { mWrite = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[2], 0), true, 5); }
                                else if (parsedAssemblyBlocks[2].Count(x => x == '$') == 4)
                                { mWrite = "1" + functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[2], 1), false, 4); mWriteToggle = "0"; }
                            }
                        }
                    }
                    #endregion

                    #region subtract opcode
                    if (parsedAssemblyBlocks[0] == "sub")
                    {
                        // 3 possibilities: [sub var var], [sub var num], and [sub num var]
                        mOpcode = "0100";

                        // UNIT TESTED - all code paths here work as intended! (copied from add)
                        // forward check
                        if (!parsedAssemblyBlocks.Contains("%forward%")) // SPECIAL CASE - CHECK
                        {
                            if (!parsedAssemblyBlocks[1].Contains("$") && parsedAssemblyBlocks[1] != "~temp~")
                            {
                                // pipe immediate into input A, var into input B
                                mSelectBit2 = "1"; // need dual-read to perform above task
                                                   // block1 becomes immediate value
                                mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(parsedAssemblyBlocks[1]), true, 8); mRead = "00001"; mReadToggle = "0";

                                // then set other block to mWrite b/c dual-read is toggled
                                if (parsedAssemblyBlocks[2].Count(x => x == '$') == 2)
                                { mWrite = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[2], 0), true, 5); }
                                else if (parsedAssemblyBlocks[2].Count(x => x == '$') == 4)
                                { mWrite = "0" + functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[2], 1), false, 4); mWriteToggle = "0"; }
                            }
                            else if (!parsedAssemblyBlocks[2].Contains("$"))
                            {
                                // pipe immediate into input B, var into input A
                                // default inputs, so don't need to do anything fancy
                                // block2 becomes immediate value
                                mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(parsedAssemblyBlocks[2]), true, 8); mWrite = "10000"; mWriteToggle = "0";
                                // then set other block to mRead
                                if (parsedAssemblyBlocks[1].Count(x => x == '$') == 2)
                                { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[1], 0), true, 5); }
                                else if (parsedAssemblyBlocks[1].Count(x => x == '$') == 4)
                                { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[1], 1), true, 4) + "0"; mReadToggle = "0"; }
                            }
                            else
                            {
                                // convert both blocks to mRead and mWrite
                                mSelectBit2 = "1";

                                // also catches ~temp~ in first block since if it is present, these will not execute
                                if (parsedAssemblyBlocks[1].Count(x => x == '$') == 2)
                                { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[1], 0), true, 5); }
                                else if (parsedAssemblyBlocks[1].Count(x => x == '$') == 4)
                                { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[1], 1), true, 4) + "1"; mReadToggle = "0"; }

                                if (parsedAssemblyBlocks[2].Count(x => x == '$') == 2)
                                { mWrite = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[2], 0), true, 5); }
                                else if (parsedAssemblyBlocks[2].Count(x => x == '$') == 4)
                                { mWrite = "1" + functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[2], 1), false, 4); mWriteToggle = "0"; }
                            }
                        }
                    }
                    #endregion

                    #region RC5.0 specific functions

                    // the same code as add and sub - consolidate?
                    #region gpu encode opcode
                    if (parsedAssemblyBlocks[0] == "gpuencode")
                    {
                        mOpcode = "0011";

                        // UNIT TESTED - all code paths here work as intended! (copied from add)
                        if (!parsedAssemblyBlocks[1].Contains("$") && parsedAssemblyBlocks[1] != "~temp~")
                        {
                            // pipe immediate into input A, var into input B
                            mSelectBit2 = "1"; // need dual-read to perform above task
                                               // block1 becomes immediate value
                            mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(parsedAssemblyBlocks[1]), true, 8); mRead = "00001"; mReadToggle = "0";

                            // then set other block to mWrite b/c dual-read is toggled
                            if (parsedAssemblyBlocks[2].Count(x => x == '$') == 2)
                            { mWrite = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[2], 0), true, 5); }
                            else if (parsedAssemblyBlocks[2].Count(x => x == '$') == 4)
                            { mWrite = "0" + functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[2], 1), false, 4); mWriteToggle = "0"; }
                        }
                        else if (!parsedAssemblyBlocks[2].Contains("$"))
                        {
                            // pipe immediate into input B, var into input A
                            // default inputs, so don't need to do anything fancy
                            // block2 becomes immediate value
                            mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(parsedAssemblyBlocks[2]), true, 8); mWrite = "10000"; mWriteToggle = "0";
                            // then set other block to mRead
                            if (parsedAssemblyBlocks[1].Count(x => x == '$') == 2)
                            { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[1], 0), true, 5); }
                            else if (parsedAssemblyBlocks[1].Count(x => x == '$') == 4)
                            { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[1], 1), true, 4) + "0"; mReadToggle = "0"; }
                        }
                        else
                        {
                            // convert both blocks to mRead and mWrite
                            mSelectBit2 = "1";

                            // also catches ~temp~ in first block since if it is present, these will not execute
                            if (parsedAssemblyBlocks[1].Count(x => x == '$') == 2)
                            { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[1], 0), true, 5); }
                            else if (parsedAssemblyBlocks[1].Count(x => x == '$') == 4)
                            { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[1], 1), true, 4) + "1"; mReadToggle = "0"; }

                            if (parsedAssemblyBlocks[2].Count(x => x == '$') == 2)
                            { mWrite = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[2], 0), true, 5); }
                            else if (parsedAssemblyBlocks[2].Count(x => x == '$') == 4)
                            { mWrite = "1" + functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[2], 1), false, 4); mWriteToggle = "0"; }
                        }
                    }
                    #endregion

                    #region gpu clear opcode
                    if (parsedAssemblyBlocks[0] == "gpuclr")
                    { mOpcode = "0010"; mSelectBit1 = "1"; }
                    #endregion

                    #region gpu draw opcode
                    if (parsedAssemblyBlocks[0] == "gpudraw")
                    {
                        mOpcode = "1011";

                        // if two numbers, encode them on-the-fly and pipe into immediate
                        // if variable, write variable to gpu point register (inaccessible through ARCISS-HLL)
                        if (!parsedAssemblyBlocks[1].Contains("$")) // SPECIAL CASE - CHECK
                        {
                            // switched b/c target specifically requires it
                            mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(parsedAssemblyBlocks[2]), true, 4) + functionLibrary.convertNumberToBinary(Convert.ToInt16(parsedAssemblyBlocks[1]), true, 4);
                            // set read to allow read of immediate data 
                            mReadToggle = "0";
                        }
                        else // revisit this part?
                        {
                            if (compiler.parsedAssemblyList[i].Count(x => x == '$') == 2)
                            { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$")), false, 5); }
                            else if (compiler.parsedAssemblyList[i].Count(x => x == '$') == 4)
                            { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$") + 1), true, 4) + "1"; mReadToggle = "0"; }
                        }

                        // set write to gpu point reg
                        mWrite = "11100";
                        mSelectBit4 = "1";
                    }
                    #endregion

                    #region gpu erase opcode
                    if (parsedAssemblyBlocks[0] == "gpuerase")
                    {
                        // exactly the same as gpudraw except with different opcode - consolidate?
                        mOpcode = "0111";

                        // if two numbers, encode them on-the-fly and pipe into immediate
                        // if variable, write variable to gpu point register (inaccessible through ARCISS-HLL)
                        if (!parsedAssemblyBlocks[1].Contains("$")) // SPECIAL CASE - CHECK
                        {
                            // switched b/c target specifically requires it
                            mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(parsedAssemblyBlocks[2]), true, 4) + functionLibrary.convertNumberToBinary(Convert.ToInt16(parsedAssemblyBlocks[1]), true, 4);
                            // set read to allow read of immediate data 
                            mReadToggle = "0";
                        }
                        else // revisit this part?
                        {
                            if (compiler.parsedAssemblyList[i].Count(x => x == '$') == 2)
                            { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$")), false, 5); }
                            else if (compiler.parsedAssemblyList[i].Count(x => x == '$') == 4)
                            { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$") + 1), true, 4) + "1"; mReadToggle = "0"; }
                        }

                        mWrite = "11100";
                        mSelectBit4 = "1";
                    }
                    #endregion

                    #region gpu granular update opcode
                    // most of this functionality is handled via writing to %gex register AKA output opcode
                    // this is only for actually updating the display with the %gex register data
                    if (parsedAssemblyBlocks[0] == "gpugu") { mOpcode = "1111"; }
                    #endregion

                    #region core on opcode
                    if (parsedAssemblyBlocks[0] == "con")
                    {
                        mOpcode = "0100";
                        mSelectBit1 = "1";
                        mExtension = functionLibrary.convertNumberToBinary(Convert.ToInt16(functionLibrary.removeWhitespace(compiler.parsedAssemblyList[i].Substring(4))) + 1, true, 4);
                    }
                    #endregion

                    #region core off opcode
                    if (parsedAssemblyBlocks[0] == "coff")
                    {
                        mOpcode = "1100";
                        mSelectBit1 = "1";
                        mExtension = functionLibrary.convertNumberToBinary(Convert.ToInt16(functionLibrary.removeWhitespace(compiler.parsedAssemblyList[i].Substring(5))) + 5, true, 4);
                    }
                    #endregion

                    // same codebase as add, sub, gpuencode - consolidate?
                    #region xor opcode
                    if (parsedAssemblyBlocks[0] == "xor")
                    {
                        mOpcode = "1110";

                        // UNIT TESTED - all code paths here work as intended! (copied from add)
                        if (!parsedAssemblyBlocks[1].Contains("$") && parsedAssemblyBlocks[1] != "~temp~")
                        {
                            // pipe immediate into input A, var into input B
                            mSelectBit2 = "1"; // need dual-read to perform above task
                                               // block1 becomes immediate value
                            mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(parsedAssemblyBlocks[1]), true, 8); mRead = "00001"; mReadToggle = "0";

                            // then set other block to mWrite b/c dual-read is toggled
                            if (parsedAssemblyBlocks[2].Count(x => x == '$') == 2)
                            { mWrite = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[2], 0), true, 5); }
                            else if (parsedAssemblyBlocks[2].Count(x => x == '$') == 4)
                            { mWrite = "0" + functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[2], 1), false, 4); mWriteToggle = "0"; }
                        }
                        else if (!parsedAssemblyBlocks[2].Contains("$"))
                        {
                            // pipe immediate into input B, var into input A
                            // default inputs, so don't need to do anything fancy
                            // block2 becomes immediate value
                            mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(parsedAssemblyBlocks[2]), true, 8); mWrite = "10000"; mWriteToggle = "0";
                            // then set other block to mRead
                            if (parsedAssemblyBlocks[1].Count(x => x == '$') == 2)
                            { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[1], 0), true, 5); }
                            else if (parsedAssemblyBlocks[1].Count(x => x == '$') == 4)
                            { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[1], 1), true, 4) + "0"; mReadToggle = "0"; }
                        }
                        else
                        {
                            // convert both blocks to mRead and mWrite
                            mSelectBit2 = "1";

                            // also catches ~temp~ in first block since if it is present, these will not execute
                            if (parsedAssemblyBlocks[1].Count(x => x == '$') == 2)
                            { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[1], 0), true, 5); }
                            else if (parsedAssemblyBlocks[1].Count(x => x == '$') == 4)
                            { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[1], 1), true, 4) + "1"; mReadToggle = "0"; }

                            if (parsedAssemblyBlocks[2].Count(x => x == '$') == 2)
                            { mWrite = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[2], 0), true, 5); }
                            else if (parsedAssemblyBlocks[2].Count(x => x == '$') == 4)
                            { mWrite = "1" + functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[2], 1), false, 4); mWriteToggle = "0"; }
                        }
                    }
                    #endregion

                    // shares codebase with xor - consolidate?
                    #region xnor opcode
                    else if (parsedAssemblyBlocks[0] == "xnor")
                    {
                        // xor [var] [var], xor [var] [num], xor [num] [var]
                        mOpcode = "0001";

                        // UNIT TESTED - all code paths here work as intended! (copied from add)
                        if (!parsedAssemblyBlocks[1].Contains("$") && parsedAssemblyBlocks[1] != "~temp~")
                        {
                            // pipe immediate into input A, var into input B
                            mSelectBit2 = "1"; // need dual-read to perform above task
                                               // block1 becomes immediate value
                            mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(parsedAssemblyBlocks[1]), true, 8); mRead = "00001"; mReadToggle = "0";

                            // then set other block to mWrite b/c dual-read is toggled
                            if (parsedAssemblyBlocks[2].Count(x => x == '$') == 2)
                            { mWrite = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[2], 0), true, 5); }
                            else if (parsedAssemblyBlocks[2].Count(x => x == '$') == 4)
                            { mWrite = "0" + functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[2], 1), false, 4); mWriteToggle = "0"; }
                        }
                        else if (!parsedAssemblyBlocks[2].Contains("$"))
                        {
                            // pipe immediate into input B, var into input A
                            // default inputs, so don't need to do anything fancy
                            // block2 becomes immediate value
                            mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(parsedAssemblyBlocks[2]), true, 8); mWrite = "10000"; mWriteToggle = "0";
                            // then set other block to mRead
                            if (parsedAssemblyBlocks[1].Count(x => x == '$') == 2)
                            { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[1], 0), true, 5); }
                            else if (parsedAssemblyBlocks[1].Count(x => x == '$') == 4)
                            { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[1], 1), true, 4) + "0"; mReadToggle = "0"; }
                        }
                        else
                        {
                            // convert both blocks to mRead and mWrite
                            mSelectBit2 = "1";

                            // also catches ~temp~ in first block since if it is present, these will not execute
                            if (parsedAssemblyBlocks[1].Count(x => x == '$') == 2)
                            { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[1], 0), true, 5); }
                            else if (parsedAssemblyBlocks[1].Count(x => x == '$') == 4)
                            { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[1], 1), true, 4) + "1"; mReadToggle = "0"; }

                            if (parsedAssemblyBlocks[2].Count(x => x == '$') == 2)
                            { mWrite = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[2], 0), true, 5); }
                            else if (parsedAssemblyBlocks[2].Count(x => x == '$') == 4)
                            { mWrite = "1" + functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[2], 1), false, 4); mWriteToggle = "0"; }
                        }
                    }
                    #endregion

                    // shares codebase with xor - consolidate?
                    #region nor opcode
                    else if (parsedAssemblyBlocks[0] == "nor")
                    {
                        mOpcode = "1101";

                        // UNIT TESTED - all code paths here work as intended! (copied from add)
                        if (!parsedAssemblyBlocks[1].Contains("$") && parsedAssemblyBlocks[1] != "~temp~")
                        {
                            // pipe immediate into input A, var into input B
                            mSelectBit2 = "1"; // need dual-read to perform above task
                                               // block1 becomes immediate value
                            mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(parsedAssemblyBlocks[1]), true, 8); mRead = "00001"; mReadToggle = "0";

                            // then set other block to mWrite b/c dual-read is toggled
                            if (parsedAssemblyBlocks[2].Count(x => x == '$') == 2)
                            { mWrite = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[2], 0), true, 5); }
                            else if (parsedAssemblyBlocks[2].Count(x => x == '$') == 4)
                            { mWrite = "0" + functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[2], 1), false, 4); mWriteToggle = "0"; }
                        }
                        else if (!parsedAssemblyBlocks[2].Contains("$"))
                        {
                            // pipe immediate into input B, var into input A
                            // default inputs, so don't need to do anything fancy
                            // block2 becomes immediate value
                            mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(parsedAssemblyBlocks[2]), true, 8); mWrite = "10000"; mWriteToggle = "0";
                            // then set other block to mRead
                            if (parsedAssemblyBlocks[1].Count(x => x == '$') == 2)
                            { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[1], 0), true, 5); }
                            else if (parsedAssemblyBlocks[1].Count(x => x == '$') == 4)
                            { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[1], 1), true, 4) + "0"; mReadToggle = "0"; }
                        }
                        else
                        {
                            // convert both blocks to mRead and mWrite
                            mSelectBit2 = "1";

                            // also catches ~temp~ in first block since if it is present, these will not execute
                            if (parsedAssemblyBlocks[1].Count(x => x == '$') == 2)
                            { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[1], 0), true, 5); }
                            else if (parsedAssemblyBlocks[1].Count(x => x == '$') == 4)
                            { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[1], 1), true, 4) + "1"; mReadToggle = "0"; }

                            if (parsedAssemblyBlocks[2].Count(x => x == '$') == 2)
                            { mWrite = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[2], 0), true, 5); }
                            else if (parsedAssemblyBlocks[2].Count(x => x == '$') == 4)
                            { mWrite = "1" + functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[2], 1), false, 4); mWriteToggle = "0"; }
                        }
                    }
                    #endregion

                    // shares codebase w xor - consolidate?
                    #region or opcode
                    else if (parsedAssemblyBlocks[0] == "or")
                    {
                        mOpcode = "0101";

                        // UNIT TESTED - all code paths here work as intended! (copied from add)
                        if (!parsedAssemblyBlocks[1].Contains("$") && parsedAssemblyBlocks[1] != "~temp~")
                        {
                            // pipe immediate into input A, var into input B
                            mSelectBit2 = "1"; // need dual-read to perform above task
                                               // block1 becomes immediate value
                            mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(parsedAssemblyBlocks[1]), true, 8); mRead = "00001"; mReadToggle = "0";

                            // then set other block to mWrite b/c dual-read is toggled
                            if (parsedAssemblyBlocks[2].Count(x => x == '$') == 2)
                            { mWrite = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[2], 0), true, 5); }
                            else if (parsedAssemblyBlocks[2].Count(x => x == '$') == 4)
                            { mWrite = "0" + functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[2], 1), false, 4); mWriteToggle = "0"; }
                        }
                        else if (!parsedAssemblyBlocks[2].Contains("$"))
                        {
                            // pipe immediate into input B, var into input A
                            // default inputs, so don't need to do anything fancy
                            // block2 becomes immediate value
                            mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(parsedAssemblyBlocks[2]), true, 8); mWrite = "10000"; mWriteToggle = "0";
                            // then set other block to mRead
                            if (parsedAssemblyBlocks[1].Count(x => x == '$') == 2)
                            { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[1], 0), true, 5); }
                            else if (parsedAssemblyBlocks[1].Count(x => x == '$') == 4)
                            { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[1], 1), true, 4) + "0"; mReadToggle = "0"; }
                        }
                        else
                        {
                            // convert both blocks to mRead and mWrite
                            mSelectBit2 = "1";

                            // also catches ~temp~ in first block since if it is present, these will not execute
                            if (parsedAssemblyBlocks[1].Count(x => x == '$') == 2)
                            { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[1], 0), true, 5); }
                            else if (parsedAssemblyBlocks[1].Count(x => x == '$') == 4)
                            { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[1], 1), true, 4) + "1"; mReadToggle = "0"; }

                            if (parsedAssemblyBlocks[2].Count(x => x == '$') == 2)
                            { mWrite = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[2], 0), true, 5); }
                            else if (parsedAssemblyBlocks[2].Count(x => x == '$') == 4)
                            { mWrite = "1" + functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[2], 1), false, 4); mWriteToggle = "0"; }
                        }
                    }
                    #endregion

                    // shares codebase w xor - consolidate?
                    #region and opcode
                    else if (parsedAssemblyBlocks[0] == "and")
                    {
                        mOpcode = "1001";

                        // UNIT TESTED - all code paths here work as intended! (copied from add)
                        if (!parsedAssemblyBlocks[1].Contains("$") && parsedAssemblyBlocks[1] != "~temp~")
                        {
                            // pipe immediate into input A, var into input B
                            mSelectBit2 = "1"; // need dual-read to perform above task
                                               // block1 becomes immediate value
                            mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(parsedAssemblyBlocks[1]), true, 8); mRead = "00001"; mReadToggle = "0";

                            // then set other block to mWrite b/c dual-read is toggled
                            if (parsedAssemblyBlocks[2].Count(x => x == '$') == 2)
                            { mWrite = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[2], 0), true, 5); }
                            else if (parsedAssemblyBlocks[2].Count(x => x == '$') == 4)
                            { mWrite = "0" + functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[2], 1), false, 4); mWriteToggle = "0"; }
                        }
                        else if (!parsedAssemblyBlocks[2].Contains("$"))
                        {
                            // pipe immediate into input B, var into input A
                            // default inputs, so don't need to do anything fancy
                            // block2 becomes immediate value
                            mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(parsedAssemblyBlocks[2]), true, 8); mWrite = "10000"; mWriteToggle = "0";
                            // then set other block to mRead
                            if (parsedAssemblyBlocks[1].Count(x => x == '$') == 2)
                            { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[1], 0), true, 5); }
                            else if (parsedAssemblyBlocks[1].Count(x => x == '$') == 4)
                            { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[1], 1), true, 4) + "0"; mReadToggle = "0"; }
                        }
                        else
                        {
                            // convert both blocks to mRead and mWrite
                            mSelectBit2 = "1";

                            // also catches ~temp~ in first block since if it is present, these will not execute
                            if (parsedAssemblyBlocks[1].Count(x => x == '$') == 2)
                            { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[1], 0), true, 5); }
                            else if (parsedAssemblyBlocks[1].Count(x => x == '$') == 4)
                            { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[1], 1), true, 4) + "1"; mReadToggle = "0"; }

                            if (parsedAssemblyBlocks[2].Count(x => x == '$') == 2)
                            { mWrite = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[2], 0), true, 5); }
                            else if (parsedAssemblyBlocks[2].Count(x => x == '$') == 4)
                            { mWrite = "1" + functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[2], 1), false, 4); mWriteToggle = "0"; }
                        }
                    }
                    #endregion

                    #endregion

                    #region opcodes for function calls
                    #region fcall1 opcode
                    if (parsedAssemblyBlocks[0] == "fcall1")
                    {
                        // add opcode with read from %gt_ and immediate 1
                        mOpcode = "1000";
                        mSelectBit3 = "1";
                        mRead = "01000"; //mReadToggle = "1";
                        mWrite = "10000"; mWriteToggle = "0";
                        mImmediate = "10000000";
                    }
                    #endregion
                    #region fcall2 opcode
                    if (parsedAssemblyBlocks[0] == "fcall2")
                    {
                        // push ALU output to stack
                        mSelectBit4 = "1";
                        mWrite = "10010"; mWriteToggle = "1";
                        // also contains fjump component
                    }
                    #endregion
                    #region rcall1 opcode
                    if (parsedAssemblyBlocks[0] == "rcall1")
                    {
                        // read return variable and write it to ALU
                        // grab address to read
                        string addr = functionLibrary.removeWhitespace(compiler.parsedAssemblyList[i].Substring(19));
                        if (addr.Count(x => x == '$') == 2)
                        { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(addr, 0), true, 5); }
                        else if (addr.Count(x => x == '$') == 4)
                        { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(addr, 1), true, 4) + "1"; mReadToggle = "0"; }
                        // write to ALU output register via add opcode
                        mOpcode = "1000";
                    }
                    #endregion
                    #region rcall2 opcode
                    if (parsedAssemblyBlocks[0] == "rcall2")
                    {
                        // pop stack value back to goto of running core
                        // forwarding it all the way around the master bus, may take some time to be received by ALU
                        mSelectBit3 = "1"; mSelectBit5 = "1";
                        mOpcode = "1000";
                        mRead = "01100"; mReadToggle = "1";
                    }
                    #endregion
                    #endregion

                    #region cmp instruction
                    // first check for forwarding
                    if (parsedAssemblyBlocks[0] == "cmp")
                    {
                        // potentially create optimization for cases where cmp instruction isn't needed because data being tested is already loaded into ALU registers
                        mOpcode = "1100";

                        if (!parsedAssemblyBlocks[1].Contains("$") && !parsedAssemblyBlocks.Contains("%forward%")) // SPECIAL CASE - CHECK
                        {
                            // block1 becomes immediate value
                            mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(parsedAssemblyBlocks[1]), true, 8);
                            // setup read address to pipe immediate value into input A
                            mRead = "00001"; mReadToggle = "0";

                            // then set other block to mRead
                            if (parsedAssemblyBlocks[2].Count(x => x == '$') == 2)
                            { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[2], 0), true, 5); }
                            else if (parsedAssemblyBlocks[2].Count(x => x == '$') == 4)
                            { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[2], 1), true, 4) + "1"; mReadToggle = "0"; }
                        }
                        else if (!parsedAssemblyBlocks[2].Contains("$") && !parsedAssemblyBlocks.Contains("%forward%")) // SPECIAL CASE - CHECK
                        {
                            // block2 becomes immediate value
                            mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(parsedAssemblyBlocks[2]), true, 8);
                            // setup write address to pipe immediate value into input B
                            mWrite = "10000"; mWriteToggle = "0";

                            // then set other block to mramr
                            if (parsedAssemblyBlocks[1].Count(x => x == '$') == 2)
                            { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[1], 0), true, 5); }
                            else if (parsedAssemblyBlocks[1].Count(x => x == '$') == 4)
                            { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[1], 1), true, 4) + "1"; mReadToggle = "0"; }
                        }
                        else
                        {
                            // handle forwarding here as well
                            // convert both blocks to mramr and mramw
                            mSelectBit2 = "1";

                            if (parsedAssemblyBlocks[1] != "~temp~")
                            {
                                if (parsedAssemblyBlocks[1].Count(x => x == '$') == 2)
                                { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[1], 0), true, 5); }
                                else if (parsedAssemblyBlocks[1].Count(x => x == '$') == 4)
                                { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[1], 1), true, 4) + "1"; mReadToggle = "0"; }
                            }

                            if (parsedAssemblyBlocks[2] == "~temp~") { mSelectBit5 = "1"; }
                            else
                            {
                                if (parsedAssemblyBlocks[2].Count(x => x == '$') == 2)
                                { mWrite = functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[2], 0), true, 5); }
                                else if (parsedAssemblyBlocks[2].Count(x => x == '$') == 4)
                                { mWrite = "1" + functionLibrary.convertNumberToBinary(acquireMemoryAddress(parsedAssemblyBlocks[2], 1), false, 4); mWriteToggle = "0"; }
                            }
                        }
                    }
                    #endregion

                    // ****WILL ALSO NEED TO ADD IN A=0 / B=0 TESTS!****
                    // ****CANNOT HAVE COMBINED TEST INSTRUCTIONS WITH RC5.0!
                    #region test instruction
                    if (parsedAssemblyBlocks[0] == "test")
                    {
                        // cannot combine test instructions with RC5.0!
                        mSelectBit1 = "1";
                        mOpcode = "1000";
                        // set condition to test
                        // grab input number - only from 0 - 3 - add 1 b/c 0-based index - add 8 to that b/c start at 8 on extension bit
                        if (parsedAssemblyBlocks[1] == "%input%") { mExtension = functionLibrary.convertNumberToBinary(Convert.ToInt16(parsedAssemblyBlocks[2]) + 9, true, 4); }
                        else if (parsedAssemblyBlocks[1] == ">") { mExtension = "0110"; }
                        else if (parsedAssemblyBlocks[1] == "<") { mExtension = "1100"; }
                        else if (parsedAssemblyBlocks[1] == "==") { mExtension = "0010"; }
                        // special case - flip tjump and fjump values as this cond check does not exist in hardware (== does)
                        else if (parsedAssemblyBlocks[1] == "!=")
                        {
                            mExtension = "0010";
                            // swap fjump and tjump
                            compiler.parsedAssemblyList[i] = compiler.parsedAssemblyList[i].Replace("tjump", "tjumpr");
                            compiler.parsedAssemblyList[i] = compiler.parsedAssemblyList[i].Replace("fjump", "tjump");
                            compiler.parsedAssemblyList[i] = compiler.parsedAssemblyList[i].Replace("tjumpr", "fjump");
                        }
                        else if (parsedAssemblyBlocks[1] == ">=") { mExtension = "1110"; }
                        else if (parsedAssemblyBlocks[1] == "<=") { mExtension = "0001"; }
                        // cout positive
                        else if (parsedAssemblyBlocks[1] == "%so%") { mExtension = "1000"; }
                        // cout negative - swap tjump and fjump
                        else if (parsedAssemblyBlocks[1] == "%su%")
                        {
                            mExtension = "1000";
                            compiler.parsedAssemblyList[i] = compiler.parsedAssemblyList[i].Replace("tjump", "tjumpr");
                            compiler.parsedAssemblyList[i] = compiler.parsedAssemblyList[i].Replace("fjump", "tjump");
                            compiler.parsedAssemblyList[i] = compiler.parsedAssemblyList[i].Replace("tjumpr", "fjump");
                        }

                        // hardware conditions specific to RC5.0
                        else if (parsedAssemblyBlocks[1] == "%data_received%") { mExtension = "1011"; }
                        else if (parsedAssemblyBlocks[1] == "%stack_overflow%") { mExtension = "0111"; }
                        else if (parsedAssemblyBlocks[1] == "%datastream_active%") { mExtension = "1111"; }
                    }
                    #endregion

                    #region ramw (ONLY) component
                    if (parsedAssemblyBlocks[0] == "ramw" && !parsedAssemblyBlocks.Contains("ramr") && !parsedAssemblyBlocks.Contains("cacher")) // SPECIAL CASE - CHECK
                    {
                        // two cases - [ramw $adr$ ~temp~] and [ramw $adr$ (num)]
                        mWrite = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$")), true, 5);
                        if (!compiler.parsedAssemblyList[i].Contains("~")) // SPECIAL CASE - CHECK
                        {
                            // possibility of fjump at the end
                            if (parsedAssemblyBlocks.Contains("fjump")) { mImmediate = functionLibrary.convertNumberToBinary(functionLibrary.acquireBlockID(functionLibrary.removeWhitespace(compiler.parsedAssemblyList[i].Substring(compiler.parsedAssemblyList[i].LastIndexOf("$") + 1)), 0), true, 8); }
                            else { mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(functionLibrary.removeWhitespace(compiler.parsedAssemblyList[i].Substring(compiler.parsedAssemblyList[i].LastIndexOf("$") + 1))), true, 8); }
                        }
                    }
                    #endregion
                    #region cachew (ONLY) component
                    if (parsedAssemblyBlocks[0] == "cachew" && !parsedAssemblyBlocks.Contains("cacher") && !parsedAssemblyBlocks.Contains("ramr")) // SPECIAL CASE - CHECK
                    {
                        // two cases - [cachew $adr$ ~temp~] and [cachew $adr$ (num)]
                        mWrite = "1" + functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("$") + 1), false, 4);
                        if (!compiler.parsedAssemblyList[i].Contains("~")) // SPECIAL CASE - CHECK
                        { mImmediate = functionLibrary.convertNumberToBinary(Convert.ToInt16(functionLibrary.removeWhitespace(compiler.parsedAssemblyList[i].Substring(compiler.parsedAssemblyList[i].LastIndexOf("$") + 1))), true, 8); }
                        // cache specific flag
                        mWriteToggle = "0";
                        // cachew specific flag - need to enable writing output to cache
                        mToggleForward = "1"; mSelectBit5 = "1";
                    }
                    #endregion

                    // these three types of memory calls are all interconnected
                    #region ramr + ramw component (for copying global values)
                    if (parsedAssemblyBlocks.Contains("ramr") && parsedAssemblyBlocks.Contains("ramw") // SPECIAL CASE - CHECK
                        && !parsedAssemblyBlocks.Contains("cacher") && !parsedAssemblyBlocks.Contains("cachew")) // not sure if these checks are necessary
                    {
                        mWrite = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("ramw") + 5), true, 5);
                        mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("ramr") + 5), true, 5);
                    }
                    #endregion
                    #region cacher + cachew component (for copying local values)
                    else if (parsedAssemblyBlocks.Contains("cachew") && parsedAssemblyBlocks.Contains("cacher") // SPECIAL CASE - CHECK
                        && !parsedAssemblyBlocks.Contains("ramr") && !parsedAssemblyBlocks.Contains("ramw")) // not sure if these checks are necessary
                    {
                        mWrite = "1" + functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("cachew") + 8), false, 4);
                        mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("cacher") + 8), true, 4) + "1";
                        mWriteToggle = "0"; mReadToggle = "0";
                        // cachew specific flag - need to enable writing output to cache
                        mToggleForward = "1"; mSelectBit5 = "1";
                    }
                    #endregion
                    #region ramr/cacher + ramw/cachew component (for copying values across local and global boundaries)
                    else if ((parsedAssemblyBlocks.Contains("ramr") || parsedAssemblyBlocks.Contains("cacher")) // SPECIAL CASE - CHECK
                        && (parsedAssemblyBlocks.Contains("ramw") || parsedAssemblyBlocks.Contains("cachew")))
                    {
                        if (parsedAssemblyBlocks.Contains("ramr")) // SPECIAL CASE - CHECK
                        { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("ramr") + 5), true, 5); }
                        else if (parsedAssemblyBlocks.Contains("cacher")) // SPECIAL CASE - CHECK
                        { mRead = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("cacher") + 8), true, 4) + "1"; mReadToggle = "0"; }
                        if (parsedAssemblyBlocks.Contains("ramw")) // SPECIAL CASE - CHECK
                        { mWrite = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("ramw") + 5), true, 5); }
                        else if (parsedAssemblyBlocks.Contains("cachew")) // SPECIAL CASE - CHECK
                        {
                            mWrite = "1" + functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("cachew") + 5), false, 4); mWriteToggle = "0";
                            // cachew specific flag - need to enable writing output to cache
                            mToggleForward = "1"; mSelectBit5 = "1";
                        }
                    }
                    #endregion

                    #region fjump component
                    if (parsedAssemblyBlocks.Contains("fjump")) // SPECIAL CASE - CHECK
                    { mGFalse = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("fjump") + 5) - 1 /*don't know if it will work or not*/, true, 7); }
                    else if (mGFalse == "0000000" && generate)
                    { mGFalse = functionLibrary.convertNumberToBinary(fJumpDefault, true, 7); }
                    #endregion
                    #region tjump component
                    if (parsedAssemblyBlocks.Contains("tjump")) // SPECIAL CASE - CHECK
                    { mGTrue = functionLibrary.convertNumberToBinary(acquireMemoryAddress(compiler.parsedAssemblyList[i], compiler.parsedAssemblyList[i].IndexOf("tjump") + 5) - 1 /*don't know if it will work or not*/ , true, 7); }
                    #endregion

                    #region forward marker check (easy)
                    // check for %forward% marker - easy
                    if (parsedAssemblyBlocks.Contains("%forward%")) mToggleForward = "1"; // SPECIAL CASE - CHECK
                    #endregion

                    if (generate)
                    {
                        currentInstructionWord = mGFalse + " " + mGTrue + " " + mExtension + " " + mOpcode + " " + mSelectBit1 + mSelectBit2 + mSelectBit3 + mSelectBit4 + mSelectBit5 + " " + mToggleForward + " " + mRead + " " + mReadToggle + " " + mWrite + " " + mWriteToggle + " " + mImmediate;
                        addToMachineCodeList(currentInstructionWord);
                    }
                }
                logging.logDebug("Machine code generation complete!", logging.loggingSenderID.machineCodeParser);
                if (Program.moreVerboseDebug) logging.logDebug("Dumping generated machine code ...", logging.loggingSenderID.machineCodeParser);
                if (Program.moreVerboseDebug) for (int i = 0; i < machineCodeList.Count; i++) logging.logDebug("[" + (i + 1).ToString("D" + digits) + "]: " + machineCodeList[i] + " | len " + machineCodeList[i].Length.ToString(), logging.loggingSenderID.machineCodeParser);
            }
            #endregion
        }

        // related functions
        private static int acquireMemoryAddress(string codeToAcquireFrom, int lengthOfStringBefore)
        {
            // copied from functionLibrary.acquireBlockID but modified slightly
            string parseValue = codeToAcquireFrom.Substring(lengthOfStringBefore + 1).Split(" ")[0];
            return functionLibrary.speedConvertStringInt(parseValue.Replace("$", ""));
        }
        private static void addToMachineCodeList(string currentInstructionWord)
        {
            if (Program.compileTarget != "3") logging.logDebug("[" + (machineCodeList.Count + 1).ToString("D" + digits) + "]: " + currentInstructionWord + ", len " + currentInstructionWord.Length.ToString(), logging.loggingSenderID.machineCodeParser);
            // b/c RC5.0's instructions are based on a 0-based index
            else logging.logDebug("[" + machineCodeList.Count.ToString("D" + digits) + "]: " + currentInstructionWord + ", len " + currentInstructionWord.Length.ToString(), logging.loggingSenderID.machineCodeParser);
            machineCodeList.Add(functionLibrary.removeWhitespace(currentInstructionWord));
        }
    }
}
