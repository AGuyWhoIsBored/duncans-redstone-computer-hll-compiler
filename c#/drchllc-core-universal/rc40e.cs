﻿using System.Linq;

namespace drchllc_core_universal
{
    class rc40e
    {
        /*
         * This class will contain all functions and information needed for correctly enabling and 
         * handling all processes concering the internal RC40E extension. 
         * 
         * Created 8/26/2017 by AGuyWhoIsBored
         * 
         */

        public static bool enableCache = false;

        // this array is based off of the contents from the core types array
        public static string[] RC40E_types = new string[7]
        { /*1-7*/ "GPU_encode", "GPU_clear", "GPU_drawPoint", "GPU_erasePoint", "GPU_granularUpdate", "updateSpeed", "hcond_rng_on" };

        public static void enableExtension()
        {
            importer.ext_RC40E = true;
            // inject into loadedResourcesList
            importer.loadedResourcesList.Add(importer.resourceIDCounter, "ARCISS.RC40E");
            logging.logDebug("Interal Redstone Computer v4.0 Extension assigned global resource identifier " + (importer.resourceIDCounter), logging.loggingSenderID.internalRC40E);
            importer.resourceIDCounter++;
            string RC40ETableKey = importer.loadedResourcesList.First(x => x.Value == "ARCISS.RC40E").Key.ToString();
            // inject tokens and bind syntax to tokens
            tokens.addImportedToken("RC40E.GPU_encode"); tokens.bindSyntax(RC40ETableKey + ".1");
            tokens.addImportedToken("RC40E.GPU_clear"); tokens.bindSyntax(RC40ETableKey + ".2");
            tokens.addImportedToken("RC40E.GPU_drawPoint"); tokens.bindSyntax(RC40ETableKey + ".3-" + RC40ETableKey + ".4-");
            tokens.addImportedToken("RC40E.GPU_erasePoint"); tokens.bindSyntax(RC40ETableKey + ".5-" + RC40ETableKey + ".6-");
            tokens.addImportedToken("RC40E.GPU_granularUpdate"); tokens.bindSyntax(RC40ETableKey + ".7");
            tokens.addImportedToken("RC40E.updateSpeed"); tokens.bindSyntax(RC40ETableKey + ".8");
            tokens.addImportedToken("RC40E.RNG_ON"); tokens.bindSyntax(RC40ETableKey + ".9");
            // inject function syntax into syntax function table
            // ***FOR NOW*** NO NEED TO INCLUDE EOL TOKEN!
            syntax.syntaxFunctionTables.Add(RC40ETableKey + ".1", tokens.convertInt("RC40E.GPU_encode").ToString()
                + "-" + tokens.convertInt("grouper_openparentheses").ToString()
                + "-" + tokens.convertInt("variable").ToString() + "/" + tokens.convertInt("data_int").ToString() + "/"
                + "-" + tokens.convertInt("variable").ToString() + "/" + tokens.convertInt("data_int").ToString() + "/"
                + "-" + tokens.convertInt("variable").ToString()
                + "-" + tokens.convertInt("grouper_closeparentheses").ToString()
                + "-");
            syntax.syntaxFunctionTables.Add(RC40ETableKey + ".2", tokens.convertInt("RC40E.GPU_clear").ToString()
                + "-" + tokens.convertInt("grouper_openparentheses").ToString()
                + "-" + tokens.convertInt("grouper_closeparentheses").ToString()
                + "-");
            syntax.syntaxFunctionTables.Add(RC40ETableKey + ".3", tokens.convertInt("RC40E.GPU_drawPoint").ToString()
                + "-" + tokens.convertInt("grouper_openparentheses").ToString()
                + "-" + tokens.convertInt("variable").ToString()
                + "-" + tokens.convertInt("grouper_closeparentheses").ToString()
                + "-");
            syntax.syntaxFunctionTables.Add(RC40ETableKey + ".4", tokens.convertInt("RC40E.GPU_drawPoint").ToString()
                + "-" + tokens.convertInt("grouper_openparentheses").ToString()
                + "-" + tokens.convertInt("data_int").ToString()
                + "-" + tokens.convertInt("data_int").ToString()
                + "-" + tokens.convertInt("grouper_closeparentheses").ToString()
                + "-");
            syntax.syntaxFunctionTables.Add(RC40ETableKey + ".5", tokens.convertInt("RC40E.GPU_erasePoint").ToString()
                + "-" + tokens.convertInt("grouper_openparentheses").ToString()
                + "-" + tokens.convertInt("data_int").ToString()
                + "-" + tokens.convertInt("data_int").ToString()
                + "-" + tokens.convertInt("grouper_closeparentheses").ToString()
                + "-");
            syntax.syntaxFunctionTables.Add(RC40ETableKey + ".6", tokens.convertInt("RC40E.GPU_erasePoint").ToString()
                + "-" + tokens.convertInt("grouper_openparentheses").ToString()
                + "-" + tokens.convertInt("variable").ToString()
                + "-" + tokens.convertInt("grouper_closeparentheses").ToString()
                + "-");
            syntax.syntaxFunctionTables.Add(RC40ETableKey + ".7", tokens.convertInt("RC40E.GPU_granularUpdate").ToString()
                + "-" + tokens.convertInt("grouper_openparentheses").ToString()
                + "-" + tokens.convertInt("data_hex").ToString()
                + "-" + tokens.convertInt("data_hex").ToString()
                // special case that we need to focus on here for the data_ints: 0<=x<=1
                + "-" + tokens.convertInt("data_int").ToString()
                + "-" + tokens.convertInt("data_int").ToString()
                + "-" + tokens.convertInt("grouper_closeparentheses").ToString()
                + "-");
            syntax.syntaxFunctionTables.Add(RC40ETableKey + ".8", tokens.convertInt("RC40E.updateSpeed").ToString()
                + "-" + tokens.convertInt("grouper_openparentheses").ToString()
                // special case for data_int: 0<=x<=7
                + "-" + tokens.convertInt("data_int").ToString()
                + "-" + tokens.convertInt("grouper_closeparentheses").ToString()
                + "-");
            // unique case here since this is an hcond - NO syntax essentially
            syntax.syntaxFunctionTables.Add(RC40ETableKey + ".9", tokens.convertInt("RC40E.RNG_ON").ToString() + "-");
            if (compiler.enableMemoryManagementOptimizations)
            { enableCache = true; logging.logDebug("RC40E caching optimizations enabled!", logging.loggingSenderID.internalRC40E); }

            logging.logDebug("Internal Redstone Computer v4.0 Extension has loaded successfully", logging.loggingSenderID.internalRC50E);
        }
    }
}
