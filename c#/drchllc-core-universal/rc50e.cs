﻿using System.Linq;

namespace drchllc_core_universal
{
    class rc50e
    {
        /*
         * This class will contain all functions and information needed for correctly enabling and 
         * handling all processes concering the internal RC50E extension. 
         * 
         * Created 9/27/2018 by AGuyWhoIsBored
         * 
         */

        // this array is based off of the contents from the core types array
        public static string[] RC50E_types = new string[15]
        { /*01-05*/ "GPU_encode", "GPU_clear", "GPU_drawPoint", "GPU_erasePoint", "GPU_granularUpdate",
          /*06-08*/ "core_enable", "core_disable", 
          /*09-12*/ "xor", "or", "and", "xnor", "nor",
          /*13-15*/ "hcond_data_received", "hcond_stack_overflow", "hcond_datastream_active" };

        public static void enableExtension()
        {
            importer.ext_RC50E = true;
            // inject into loadedResourcesList
            importer.loadedResourcesList.Add(importer.resourceIDCounter, "ARCISS.RC50E");
            logging.logDebug("Interal Redstone Computer v5.0 Extension assigned global resource identifier " + importer.resourceIDCounter, logging.loggingSenderID.internalRC50E);
            importer.resourceIDCounter++;
            string RC50ETableKey = importer.loadedResourcesList.First(x => x.Value == "ARCISS.RC50E").Key.ToString();
            // inject tokens and bind syntax to tokens
            tokens.addImportedToken("RC50E.GPU_encode"); tokens.bindSyntax(RC50ETableKey + ".1");
            tokens.addImportedToken("RC50E.GPU_clear"); tokens.bindSyntax(RC50ETableKey + ".2");
            tokens.addImportedToken("RC50E.GPU_drawPoint"); tokens.bindSyntax(RC50ETableKey + ".3-" + RC50ETableKey + ".4-");
            tokens.addImportedToken("RC50E.GPU_erasePoint"); tokens.bindSyntax(RC50ETableKey + ".5-" + RC50ETableKey + ".6-");
            tokens.addImportedToken("RC50E.GPU_granularUpdate"); tokens.bindSyntax(RC50ETableKey + ".7-" + RC50ETableKey + ".8-");

            tokens.addImportedToken("RC50E.core_enable"); tokens.bindSyntax(RC50ETableKey + ".9");
            tokens.addImportedToken("RC50E.core_disable"); tokens.bindSyntax(RC50ETableKey + ".10");

            tokens.addImportedToken("RC50E.xor"); tokens.bindSyntax(RC50ETableKey + ".11");
            tokens.addImportedToken("RC50E.or"); tokens.bindSyntax(RC50ETableKey + ".12");
            tokens.addImportedToken("RC50E.and"); tokens.bindSyntax(RC50ETableKey + ".13");
            tokens.addImportedToken("RC50E.xnor"); tokens.bindSyntax(RC50ETableKey + ".14");
            tokens.addImportedToken("RC50E.nor"); tokens.bindSyntax(RC50ETableKey + ".15");

            tokens.addImportedToken("RC50E.DATA_RECEIVED"); tokens.bindSyntax(RC50ETableKey + ".16");
            tokens.addImportedToken("RC50E.STACK_OVERFLOW"); tokens.bindSyntax(RC50ETableKey + ".17");
            tokens.addImportedToken("RC50E.DATASTREAM_ACTIVE"); tokens.bindSyntax(RC50ETableKey + ".18");
            // inject function syntax into syntax function table
            // ***FOR NOW*** NO NEED TO INCLUDE EOL TOKEN!

            // GPU functions
            syntax.syntaxFunctionTables.Add(RC50ETableKey + ".1", tokens.convertInt("RC50E.GPU_encode").ToString()
                + "-" + tokens.convertInt("grouper_openparentheses").ToString()
                + "-" + tokens.convertInt("variable").ToString() + "/" + tokens.convertInt("data_int").ToString() + "/"
                + "-" + tokens.convertInt("variable").ToString() + "/" + tokens.convertInt("data_int").ToString() + "/"
                + "-" + tokens.convertInt("variable").ToString()
                + "-" + tokens.convertInt("grouper_closeparentheses").ToString()
                + "-");
            syntax.syntaxFunctionTables.Add(RC50ETableKey + ".2", tokens.convertInt("RC50E.GPU_clear").ToString()
                + "-" + tokens.convertInt("grouper_openparentheses").ToString()
                + "-" + tokens.convertInt("grouper_closeparentheses").ToString()
                + "-");
            syntax.syntaxFunctionTables.Add(RC50ETableKey + ".3", tokens.convertInt("RC50E.GPU_drawPoint").ToString()
                + "-" + tokens.convertInt("grouper_openparentheses").ToString()
                + "-" + tokens.convertInt("variable").ToString()
                + "-" + tokens.convertInt("grouper_closeparentheses").ToString()
                + "-");
            syntax.syntaxFunctionTables.Add(RC50ETableKey + ".4", tokens.convertInt("RC50E.GPU_drawPoint").ToString()
                + "-" + tokens.convertInt("grouper_openparentheses").ToString()
                + "-" + tokens.convertInt("data_int").ToString()
                + "-" + tokens.convertInt("data_int").ToString()
                + "-" + tokens.convertInt("grouper_closeparentheses").ToString()
                + "-");
            syntax.syntaxFunctionTables.Add(RC50ETableKey + ".5", tokens.convertInt("RC50E.GPU_erasePoint").ToString()
                + "-" + tokens.convertInt("grouper_openparentheses").ToString()
                + "-" + tokens.convertInt("data_int").ToString()
                + "-" + tokens.convertInt("data_int").ToString()
                + "-" + tokens.convertInt("grouper_closeparentheses").ToString()
                + "-");
            syntax.syntaxFunctionTables.Add(RC50ETableKey + ".6", tokens.convertInt("RC50E.GPU_erasePoint").ToString()
                + "-" + tokens.convertInt("grouper_openparentheses").ToString()
                + "-" + tokens.convertInt("variable").ToString()
                + "-" + tokens.convertInt("grouper_closeparentheses").ToString()
                + "-");
            syntax.syntaxFunctionTables.Add(RC50ETableKey + ".7", tokens.convertInt("RC50E.GPU_granularUpdate").ToString()
                + "-" + tokens.convertInt("grouper_openparentheses").ToString()
                + "-" + tokens.convertInt("data_hex").ToString()
                + "-" + tokens.convertInt("data_hex").ToString()
                // special case that we need to focus on here for the data_ints: 0<=x<=1
                + "-" + tokens.convertInt("data_int").ToString()
                + "-" + tokens.convertInt("data_int").ToString()
                + "-" + tokens.convertInt("grouper_closeparentheses").ToString()
                + "-");
            syntax.syntaxFunctionTables.Add(RC50ETableKey + ".8", tokens.convertInt("RC50E.GPU_granularUpdate").ToString()
                + "-" + tokens.convertInt("grouper_openparentheses").ToString()
                + "-" + tokens.convertInt("variable").ToString()
                + "-" + tokens.convertInt("variable").ToString()
                + "-" + tokens.convertInt("variable").ToString()
                + "-" + tokens.convertInt("variable").ToString()
                + "-" + tokens.convertInt("grouper_closeparentheses").ToString()
                + "-");

            // RC50E special functions
            syntax.syntaxFunctionTables.Add(RC50ETableKey + ".9", tokens.convertInt("RC50E.core_enable").ToString()
                + "-" + tokens.convertInt("grouper_openparentheses").ToString()
                // special case for data_int: 0<=x<=3
                + "-" + tokens.convertInt("data_int").ToString()
                + "-" + tokens.convertInt("grouper_closeparentheses").ToString()
                + "-");
            syntax.syntaxFunctionTables.Add(RC50ETableKey + ".10", tokens.convertInt("RC50E.core_disable").ToString()
                + "-" + tokens.convertInt("grouper_openparentheses").ToString()
                // special case for data_int: 0<=x<=3
                + "-" + tokens.convertInt("data_int").ToString()
                + "-" + tokens.convertInt("grouper_closeparentheses").ToString()
                + "-");

            // RC50E bitwise functions
            syntax.syntaxFunctionTables.Add(RC50ETableKey + ".11", tokens.convertInt("RC50E.xor").ToString()
                + "-" + tokens.convertInt("grouper_openparentheses").ToString()
                + "-" + tokens.convertInt("variable").ToString() + "/" + tokens.convertInt("data_int").ToString() + "/"
                + "-" + tokens.convertInt("variable").ToString() + "/" + tokens.convertInt("data_int").ToString() + "/"
                + "-" + tokens.convertInt("grouper_closeparentheses").ToString()
                + "-");
            syntax.syntaxFunctionTables.Add(RC50ETableKey + ".12", tokens.convertInt("RC50E.or").ToString()
                + "-" + tokens.convertInt("grouper_openparentheses").ToString()
                + "-" + tokens.convertInt("variable").ToString() + "/" + tokens.convertInt("data_int").ToString() + "/"
                + "-" + tokens.convertInt("variable").ToString() + "/" + tokens.convertInt("data_int").ToString() + "/"
                + "-" + tokens.convertInt("grouper_closeparentheses").ToString()
                + "-");
            syntax.syntaxFunctionTables.Add(RC50ETableKey + ".13", tokens.convertInt("RC50E.and").ToString()
                + "-" + tokens.convertInt("grouper_openparentheses").ToString()
                + "-" + tokens.convertInt("variable").ToString() + "/" + tokens.convertInt("data_int").ToString() + "/"
                + "-" + tokens.convertInt("variable").ToString() + "/" + tokens.convertInt("data_int").ToString() + "/"
                + "-" + tokens.convertInt("grouper_closeparentheses").ToString()
                + "-");
            syntax.syntaxFunctionTables.Add(RC50ETableKey + ".14", tokens.convertInt("RC50E.xnor").ToString()
                + "-" + tokens.convertInt("grouper_openparentheses").ToString()
                + "-" + tokens.convertInt("variable").ToString() + "/" + tokens.convertInt("data_int").ToString() + "/"
                + "-" + tokens.convertInt("variable").ToString() + "/" + tokens.convertInt("data_int").ToString() + "/"
                + "-" + tokens.convertInt("grouper_closeparentheses").ToString()
                + "-");
            syntax.syntaxFunctionTables.Add(RC50ETableKey + ".15", tokens.convertInt("RC50E.nor").ToString()
                + "-" + tokens.convertInt("grouper_openparentheses").ToString()
                + "-" + tokens.convertInt("variable").ToString() + "/" + tokens.convertInt("data_int").ToString() + "/"
                + "-" + tokens.convertInt("variable").ToString() + "/" + tokens.convertInt("data_int").ToString() + "/"
                + "-" + tokens.convertInt("grouper_closeparentheses").ToString()
                + "-");

            // hardware conditions - essentially NO SYNTAX!
            syntax.syntaxFunctionTables.Add(RC50ETableKey + ".16", tokens.convertInt("RC50E.DATA_RECEIVED").ToString() + "-");
            syntax.syntaxFunctionTables.Add(RC50ETableKey + ".17", tokens.convertInt("RC50E.STACK_OVERFLOW").ToString() + "-");
            syntax.syntaxFunctionTables.Add(RC50ETableKey + ".18", tokens.convertInt("RC50E.DATASTREAM_ACTIVE").ToString() + "-");

            logging.logDebug("Internal Redstone Computer v5.0 Extension has loaded successfully", logging.loggingSenderID.internalRC50E);
        }
    }
}
