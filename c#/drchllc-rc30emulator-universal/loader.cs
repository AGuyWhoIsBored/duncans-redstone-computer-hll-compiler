﻿using System;
using System.Collections.Generic;
using System.IO;

namespace drchllc_rc30emulator_universal
{
    class loader
    {
        /*
         * This class contains all functions needed to load and fetch
         * program data from the Redstone Computer v3.0 "program memory" 
         * emulated hardware.
         * 
         * Created 8/23/2019 by AGuyWhoIsBored
         * 
         */

        public static string[] core1ProgramMemory = null;
        public static string[] core2ProgramMemory = null;
        public static string[] core3ProgramMemory = null;
        public static string[] core4ProgramMemory = null;

        /// <summary>
        /// Enables emulator to read in a precompiled program binary file from disk and load it in to be executed.
        /// </summary>
        /// <param name="filelocation">The location of the program binary file to load.</param>
        /// <param name="core">The core to load and execute the provided program on.</param>
        /// <param name="extendExecutionToggle">Whether to enable extended execution for the provided program and core.</param>
        public static void loadProgramFromFile(string filelocation, string core, bool extendExecutionToggle)
        {
            Console.WriteLine("Attempting to load program binary with the following data:");
            Console.WriteLine("file [" + filelocation + "], core [" + core + "], extendExecution [" + string.Format(extendExecutionToggle ? "SET" : "UNSET") + "]");

            // make sure data is correct
            // use exception model to throw errors as well
            // add check for length of program words
            if (filelocation == null)                                   throw new ProgramLoadException("File location for program binary file is required");
            if (!File.Exists(filelocation))                             throw new ProgramLoadException("File location for program binary file does not exist");
            if (Path.GetExtension(filelocation).ToLower() != ".pb")     throw new ProgramLoadException("Provided file is not a program binary file");        
            if (core == null)                                           throw new ProgramLoadException("Core flag is required");
            if (Convert.ToInt16(core) < 1 || Convert.ToInt16(core) > 4) throw new ProgramLoadException("Core flag provided is invalid");

            switch (core)
            {
                case "1":
                    core1ProgramMemory = File.ReadAllLines(filelocation);
                    for (int i = 0; i < core1ProgramMemory.Length; i++) { Console.WriteLine("c1 [" + i.ToString("D2") + "]: [" + core1ProgramMemory[i] + "] len " + core1ProgramMemory[i].Length); }
                    break;
                case "2":
                    core2ProgramMemory = File.ReadAllLines(filelocation);
                    for (int i = 0; i < core2ProgramMemory.Length; i++) { Console.WriteLine("c2 [" + i.ToString("D2") + "]: [" + core2ProgramMemory[i] + "] len " + core2ProgramMemory[i].Length); }
                    break;
                case "3":
                    core3ProgramMemory = File.ReadAllLines(filelocation);
                    for (int i = 0; i < core3ProgramMemory.Length; i++) { Console.WriteLine("c3 [" + i.ToString("D2") + "]: [" + core3ProgramMemory[i] + "] len " + core3ProgramMemory[i].Length); }
                    break;
                case "4":
                    core4ProgramMemory = File.ReadAllLines(filelocation);
                    for (int i = 0; i < core4ProgramMemory.Length; i++) { Console.WriteLine("c4 [" + i.ToString("D2") + "]: [" + core4ProgramMemory[i] + "] len " + core4ProgramMemory[i].Length); }
                    break;
            }

        }
    }
}
