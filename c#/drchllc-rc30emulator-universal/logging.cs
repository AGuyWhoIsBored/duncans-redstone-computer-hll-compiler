﻿using System;
using System.Collections.Generic;
using System.Text;

namespace drchllc_rc30emulator_universal
{
    /*
     * This class will contain all functions and information needed to enable
     * versatile logging methods and capabilities with the emulator and debug 
     * information provided by the uploader
     * 
     * Created 8/20/2019 by AGuyWhoIsBored
     * 
     */
    class logging
    {
    }

    // dedicated exceptions for emulator
    class ConfigurationEditException : Exception { public ConfigurationEditException(string message) : base(message) { } }
    class ProgramLoadException : Exception { public ProgramLoadException(string message) : base(message) { } }
    class EmulatorExecutionException : Exception { public EmulatorExecutionException(string message) : base (message) { } }

}
