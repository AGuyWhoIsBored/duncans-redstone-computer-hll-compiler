﻿using System;
using System.Text;
using System.Threading;

namespace drchllc_rc30emulator_universal
{
    class rc30emulator
    {
        /*
         * This class will be the primary class that will perform the
         * emulation functionality of the Redstone Computer v3.0.
         * 
         * Created 8/19/2019 by AGuyWhoIsBored
         * 
         */

        #region RC3.0 IS Bit Layouts
        // instruction set bit layouts acquired from compiler
        // instruction set bit layout for RC3.0 core 1 (facing north)
        // [0 0 0 0 0 ] [0 0 0 0 0 ] [0 0 0 0 0 ] [0   0  0  0  0 0 0 0] [0 0 0 0] [0 0 0 0] [0  0 0 0 0] [0 0] [0 0 0] 
        // [1 2 4 8 16] [1 2 4 8 16] [1 2 4 8 16] [128 64 32 16 8 4 2 1] [1 2 4 8] [1 2 4 8] [16 8 4 2 1] [1 2] [1 2 4]
        // [GOTO FALSE] [GOTO TRUE ] [CONDITION ] [IMMEDIATE           ] [RAM W  ] [RAM R  ] [OPCODE    ] [DR ] [EXT. ]

        // instruction set bit layout for RC3.0 core 2 (facing north)
        // [0 0 0 0 0 ] [0 0 0 0 0 ] [0 0 0 0 0 ] [0   0  0  0  0 0 0 0] [0 0 0 0] [0 0 0 0] [0 0 0 0 0 ] [0 0] [0 0 0]
        // [1 2 4 8 16] [1 2 4 8 16] [1 2 4 8 16] [128 64 32 16 8 4 2 1] [1 2 4 8] [1 2 4 8] [1 2 4 8 16] [1 2] [1 2 4]
        // [GOTO FALSE] [GOTO TRUE ] [CONDITION ] [IMMEDIATE           ] [RAM W  ] [RAM R  ] [OPCODE    ] [DR ] [EXT. ]

        // instruction set bit layout for RC3.0 core 3 (facing north)
        // [0  0 0 0 0] [0  0 0 0 0] [0 0 0 0 0 ] [0   0  0  0  0 0 0 0] [0 0 0 0] [0 0 0 0] [0 0 0 0 0 ] [0 0] [0 0 0]
        // [16 8 4 2 1] [16 8 4 2 1] [1 2 4 8 16] [128 64 32 16 8 4 2 1] [1 2 4 8] [1 2 4 8] [1 2 4 8 16] [1 2] [1 2 4]
        // [GOTO FALSE] [GOTO TRUE ] [CONDITION ] [IMMEDIATE           ] [RAM W  ] [RAM R  ] [OPCODE    ] [DR ] [EXT. ]

        // instruction set bit layout for RC3.0 core 4 (facing north)
        // [0  0 0 0 0] [0  0 0 0 0] [0 0 0 0 0 ] [0   0  0  0  0 0 0 0] [0 0 0 0] [0 0 0 0] [0 0 0 0 0 ] [0 0] [0 0 0]
        // [16 8 4 2 1] [16 8 4 2 1] [1 2 4 8 16] [128 64 32 16 8 4 2 1] [1 2 4 8] [1 2 4 8] [1 2 4 8 16] [1 2] [1 2 4]
        // [GOTO FALSE] [GOTO TRUE ] [CONDITION ] [IMMEDIATE           ] [RAM W  ] [RAM R  ] [OPCODE    ] [DR ] [EXT. ]
        #endregion

        #region emulator configuration variables
        // user settings
        static bool clearRAMUIOnShutdown = false; // do we even need this?
        static bool reportProgramExceptionsIO = false; // if set, will throw up warning when code is being executed - if not set, will simply update that bus value
        static bool emulateHardwareDelay = true; // not sure if we will fully implement, but the idea is that we may emulate delay in a manner similar to what the hardware has
        static bool randomNumberGenerator = false;
        // emulator speed settings
        // these are put here because these can be configured by the user
        static int computerClockSpeed = 0;
        static int computerClockMode = 0; // 0 - use timings from physical hardware, 1 - use "real" timings
        #endregion

        #region emulator internal variables
        // processors information
        static bool core1On = true;
        static bool core2On = false;
        static bool core3On = false;
        static bool core4On = false;
        static int computerState = 0; // 0 - off, 1 - on, 2 - sleep (off with no reset on regs and RAM)

        // registers
        static short _core1ALUDataOutput = 0; static short _core1ALUFlagOutput = 0;
        static short _core2ALUDataOutput = 0; static short _core2ALUFlagOutput = 0;
        static short _core3ALUDataOutput = 0; static short _core3ALUFlagOutput = 0;
        static short _core4ALUDataOutput = 0; static short _core4ALUFlagOutput = 0;

        // register [locations]
        static short _rawBinDisplay = 0;
        static short _decimalDisplay = 0;
        static short _outputPort = 0;

        static short _inputPort = 0;
        static short _userInput = 0;
        static short _randomNumberGenerator = 0;

        // RAM
        static short[] randomAccessMemory = new short[15];

        // master output bus
        static short _masterOutputBus = 0;

        // program output panel
        static bool shiftOverflow = false;
        static bool shiftUnderflow = false;
        static bool compIndicator1 = false;
        static bool compIndicator2 = false;
        static bool compIndicator3 = false;
        static bool compIndicator4 = false;

        // line of code counters
        static short core1LOC = 0;
        static short core2LOC = 0;
        static short core3LOC = 0;
        static short core4LOC = 0;
        #endregion

        #region emulator configuration methods
        /// <summary>
        /// Prints all emulator configuration data in an human-readable format.
        /// </summary>
        public static void printConfigurationData()
        {
            Console.WriteLine("");
            Console.WriteLine(" ----DRCHLL-C Redstone Computer v3.0 Emulator Configuration Data---- ");
            Console.WriteLine("   config node             value");
            Console.WriteLine("");
            Console.WriteLine(" clearRAMUIOnShutdown       " + clearRAMUIOnShutdown.ToString());
            Console.WriteLine(" reportProgramExceptionsIO  " + reportProgramExceptionsIO.ToString());
            Console.WriteLine(" emulateHardwareDelay       " + emulateHardwareDelay.ToString());
            Console.WriteLine(" randomNumberGenerator      " + randomNumberGenerator.ToString());
            Console.WriteLine(" computerClockSpeed         " + computerClockSpeed.ToString());
            Console.WriteLine(" computerClockMode          " + computerClockMode.ToString());
            Console.WriteLine(" ------------------------------------------------------------------- ");
            Console.WriteLine("");
        }

        /// <summary>
        /// Allows any other part of the emulator program to modify emulator configuration data.
        /// Exceptions will be thrown if the attempted configuration edit cannot be performed.
        /// </summary>
        /// <param name="node">The configuration node to edit.</param>
        /// <param name="value">The value to set the requested configuration node to.</param>
        /// <param name="report">Whether information regarding the attempted configuration edit should be pushed to the console.</param>
        public static void editConfigurationData(string node, string value)
        {
            // use method to make sure edits only happen in this class
            // we can do lots of stuff easier with this way as well
            // throw exceptions for when values are invalid

            Console.WriteLine("Attempting configuration edit: [" + node + " -> " + value + "]");
            switch (node)
            {
                case "clearRAMUIOnShutdown":
                    try { clearRAMUIOnShutdown = Convert.ToBoolean(value); Console.WriteLine("Configuration edit successful"); }
                    catch (Exception) { throw new ConfigurationEditException("Configuration value for node [" + node + "] is invalid"); }
                    break;
                case "reportProgramExceptionsIO":
                    try { reportProgramExceptionsIO = Convert.ToBoolean(value); Console.WriteLine("Configuration edit successful"); }
                    catch (Exception) { throw new ConfigurationEditException("Configuration value for node [" + node + "] is invalid"); }
                    break;
                case "emulateHardwareDelay":
                    try { emulateHardwareDelay = Convert.ToBoolean(value); Console.WriteLine("Configuration edit successful"); }
                    catch (Exception) { throw new ConfigurationEditException("Configuration value for node [" + node + "] is invalid"); }
                    break;
                case "randomNumberGenerator":
                    try { randomNumberGenerator = Convert.ToBoolean(value); Console.WriteLine("Configuration edit successful"); }
                    catch (Exception) { throw new ConfigurationEditException("Configuration value for node [" + node + "] is invalid"); }
                    break;
                case "computerClockSpeed":
                    try { computerClockSpeed = Convert.ToInt16(value); Console.WriteLine("Configuration edit successful"); }
                    catch (Exception) { throw new ConfigurationEditException("Configuration value for node [" + node + "] is invalid"); }
                    break;
                case "computerClockMode":
                    try { computerClockMode = Convert.ToInt16(value); Console.WriteLine("Configuration edit successful"); }
                    catch (Exception) { throw new ConfigurationEditException("Configuration value for node [" + node + "] is invalid"); }
                    break;
                default:
                    throw new ConfigurationEditException("Configuration node is invalid");
            }
        }
        #endregion

        #region emulator execution methods
        public static void displayEmulatorStatus(bool continuousUpdate)
        {
            // computer state information
            Console.Write("\n --------GLOBAL STATE-------- ");                                                    Console.Write(" -----OUTPUT LOCATIONS----- ");                                      Console.Write(" ---INPUT LOCATIONS--- \n");
            Console.Write(" | computerState:  [" + computerState + "]      | ");                                  Console.Write(" | _rawBinDisplay:  [" + _rawBinDisplay.ToString("D3") + "] | ");    Console.Write(" | _userInput: [" +_userInput.ToString("D3") + "] | \n");
            Console.Write(" | shiftOverflow:  [" + string.Format(shiftOverflow ? " SET " : "UNSET") + "]  | ");   Console.Write(" | _decimalDisplay: [" + _decimalDisplay.ToString("D3") + "] | ");   Console.Write(" | _inputPort: [" + _inputPort.ToString("D3") + "] | \n");
            Console.Write(" | shiftUnderflow: [" + string.Format(shiftUnderflow ? " SET " : "UNSET") + "]  | ");  Console.Write(" | _outputPort:     [" + _outputPort.ToString("D3") + "] | ");       Console.Write(" | _ranNumGen: [" + _randomNumberGenerator.ToString("D3") + "] | \n");
            Console.Write(" | indicator1:     [" + string.Format(compIndicator1 ? " SET " : "UNSET") + "]  | ");  Console.Write(" -------------------------- ");                                      Console.Write(" --------------------- \n");
            Console.Write(" | indicator2:     [" + string.Format(compIndicator2 ? " SET " : "UNSET") + "]  | \n");  
            Console.Write(" | indicator3:     [" + string.Format(compIndicator3 ? " SET " : "UNSET") + "]  | \n");
            Console.Write(" | indicator4:     [" + string.Format(compIndicator4 ? " SET " : "UNSET") + "]  | \n");
            Console.Write(" ---------------------------- \n\n");

            // core information
            Console.Write(" -------CORE 1------- ");                                            Console.Write(" -------CORE 2------- ");                                            Console.Write(" -------CORE 3------- ");                                            Console.Write(" -------CORE 4------- \n");
            Console.Write(" | state:    [" + string.Format(core1On ? " ON" : "OFF") + "]  | "); Console.Write(" | state:    [" + string.Format(core2On ? " ON" : "OFF") + "]  | "); Console.Write(" | state:    [" + string.Format(core3On ? " ON" : "OFF") + "]  | "); Console.Write(" | state:    [" + string.Format(core4On ? " ON" : "OFF") + "]  | \n");
            Console.Write(" | loc:      [" + core1LOC.ToString("D2") + "]   | ");               Console.Write(" | loc:      [" + core2LOC.ToString("D2") + "]   | ");               Console.Write(" | loc:      [" + core3LOC.ToString("D2") + "]   | ");               Console.Write(" | loc:      [" + core4LOC.ToString("D2") + "]   | \n");
            Console.Write(" | _dataOut: [" + _core1ALUDataOutput.ToString("D3") + "]  | ");     Console.Write(" | _dataOut: [" + _core2ALUDataOutput.ToString("D3") + "]  | ");     Console.Write(" | _dataOut: [" + _core3ALUDataOutput.ToString("D3") + "]  | ");     Console.Write(" | _dataOut: [" + _core4ALUDataOutput.ToString("D3") + "]  | \n");
            Console.Write(" | _flagOut: [" + _core1ALUFlagOutput.ToString("D3") + "]  | ");     Console.Write(" | _flagOut: [" + _core2ALUFlagOutput.ToString("D3") + "]  | ");     Console.Write(" | _flagOut: [" + _core3ALUFlagOutput.ToString("D3") + "]  | ");     Console.Write(" | _flagOut: [" + _core4ALUFlagOutput.ToString("D3") + "]  | \n");
            Console.Write(" -------------------- ");                                            Console.Write(" -------------------- ");                                            Console.Write(" -------------------- ");                                            Console.Write(" -------------------- \n\n");
        }

        //public static void testEmulatorDisplay()
        //{
        //    Random random = new Random();
        //    while (true)
        //    {
        //        Console.Write("\r");
        //        displayEmulatorStatus(false);
        //        _rawBinDisplay = random.Next(0, 256); _decimalDisplay = random.Next(0, 256); _outputPort = random.Next(0, 256);
        //        Thread.Sleep(1000);
        //    }
        //}

        public static void emulatorExcecute(int cycles, int core)
        {
            // how emulator will emulate code
            //  - grab and decode next program instruction from stored LOC 
            //  - update various emulated components based on decoded instruction
            for (int i = 0; i < cycles; i++)
            {
                // for initialization of emulator if it hasn't started yet
                // grab and decode first line of code
                //if (core == 1 && core1LOC == 0) core1LOC++;
                //else if (core == 2 && core2LOC == 0) core2LOC++;
                //else if (core == 3 && core3LOC == 0) core3LOC++;
                //else if (core == 4 && core4LOC == 0) core4LOC++;

                // decode
                // diference between GOTO btwn core1&2 and core3&4
                // break up into different segments based on how compiler writes out instructions
                short mImmediate;
                short mGFalse;
                short mGTrue;
                short mCond;
                short mRamW;
                short mRamR;
                short mOpcode;
                short mDualReadToggle;
                short mExtension;

                bool mCondSet = false;
                if (core == 1)
                {
                    // perform integer conversions initially for performance benefit
                    mGFalse =           Convert.ToInt16(functionLibrary.flipString(loader.core1ProgramMemory[core1LOC].Substring(0, 5)), 2);
                    mGTrue =            Convert.ToInt16(functionLibrary.flipString(loader.core1ProgramMemory[core1LOC].Substring(5, 5)), 2);
                    mCond =             Convert.ToInt16(functionLibrary.flipString(loader.core1ProgramMemory[core1LOC].Substring(10, 5)), 2);
                    mImmediate =        Convert.ToInt16(loader.core1ProgramMemory[core1LOC].Substring(15, 8), 2);
                    mRamW =             Convert.ToInt16(functionLibrary.flipString(loader.core1ProgramMemory[core1LOC].Substring(23, 4)), 2);
                    mRamR =             Convert.ToInt16(functionLibrary.flipString(loader.core1ProgramMemory[core1LOC].Substring(27, 4)), 2);
                    mOpcode =           Convert.ToInt16(loader.core1ProgramMemory[core1LOC].Substring(31, 5), 2);
                    mDualReadToggle =   Convert.ToInt16(functionLibrary.flipString(loader.core1ProgramMemory[core1LOC].Substring(36, 2)), 2);
                    mExtension =        Convert.ToInt16(functionLibrary.flipString(loader.core1ProgramMemory[core1LOC].Substring(38)), 2);
                    Console.WriteLine(mGFalse + " " + mGTrue + " " + mCond + " " + mImmediate + " " + mRamW + " " + mRamR + " " + mOpcode + " " + mDualReadToggle + " " + mExtension);

                    // determine which operation the instruction is for
                    if (mOpcode == 0 && mRamW != 0)
                    {
                        // ramw - do not distinguish between malloc and ramw
                        Console.WriteLine("ramw");
                        // update RAM
                        if (mImmediate == 0 && _masterOutputBus != 0)
                        {
                            Console.WriteLine("value from master output bus [" + _masterOutputBus + "] --> RAM address [" + mRamW + "]");
                            randomAccessMemory[mRamW - 1] = _masterOutputBus;
                        }
                        else
                        {
                            Console.WriteLine("value [" + mImmediate + "] --> RAM address [" + mRamW + "]");
                            randomAccessMemory[mRamW - 1] = mImmediate; // whenever accessing RAM, address - 1
                        }
                        Console.WriteLine("dump RAM contents");
                        for (int j = 0; j < randomAccessMemory.Length; j++) { Console.WriteLine("[" + j + "]: [" + randomAccessMemory[j] + "]"); }
                    }
                    else if (mOpcode == 0 && mRamW != 0 && mRamR != 0)
                    {
                        // ramr + ramw copy
                        Console.WriteLine("ramr + ramw copy");
                        // read and write
                        Console.WriteLine("value at RAM address [" + mRamR + "] --> RAM address [" + mRamW + "]");
                        Console.WriteLine("value [" + randomAccessMemory[mRamR - 1] + "] --> RAM address [" + mRamW + "]");
                        randomAccessMemory[mRamW - 1] = randomAccessMemory[mRamR - 1];
                        Console.WriteLine("dump RAM contents");
                        for (int j = 0; j < randomAccessMemory.Length; j++) { Console.WriteLine("[" + j + "]: [" + randomAccessMemory[j] + "]"); }
                    }
                    else if (mOpcode == 11)
                    {
                        // exit
                        Console.WriteLine("exit");
                    }
                    else if (mOpcode == 7)
                    {
                        // flush
                        Console.WriteLine("flush");
                    }
                    else if (mOpcode == 9)
                    {
                        // output
                        Console.WriteLine("output");
                    }
                    else if (mOpcode == 10)
                    {
                        // input
                        Console.WriteLine("input");
                    }
                    else if (mImmediate == 0 && mGTrue == 0 && mCond == 0 && mRamW == 0 && mRamR == 0 && mOpcode == 0 && mDualReadToggle == 0 && mExtension == 0)
                    {
                        // sleep - all 0 except for mGFalse
                        // may not differentiate btwn. pure fjump and sleep (they are identical to the hardware)
                        Console.WriteLine("sleep / fjump only");
                    }
                    else if (mOpcode == 6)
                    {
                        // bsr
                        Console.WriteLine("bsr");
                    }
                    else if (mOpcode == 5)
                    {
                        // bsl
                        Console.WriteLine("bsl");
                    }
                    else if (mOpcode == 1)
                    {
                        // add
                        Console.WriteLine("add");
                        // several cases for adding:
                        //      - RAM A into cache A, RAM B into cache B
                        //      - immediate into cache A, RAM B into cache B (w/ dualread toggle)
                        //      - immediate into cache A, RAM B into cache B (w/ RAM read to cache B)
                        // compiler always toggled dual-read mode, but user can manually program in using RAM read to cache B and it should work
                        // immediates are pushed to input A of ALU in RC3.0 hardware
                        short ALUInputA = 0;
                        short ALUInputB = 0;
                        if (mImmediate != 0) ALUInputA = mImmediate;
                        if (mDualReadToggle == 2 && ALUInputA == 0) { ALUInputA = randomAccessMemory[mRamR - 1]; ALUInputB = randomAccessMemory[mRamW - 1]; }
                        if (mDualReadToggle == 1) { ALUInputB = randomAccessMemory[mRamR - 1]; }
                        Console.WriteLine("add [" + ALUInputA + "] [" + ALUInputB + "]");
                        // just to follow hardware path of RC3.0
                        _core1ALUDataOutput = (short)(ALUInputA + ALUInputB); _masterOutputBus = _core1ALUDataOutput;
                        // make sure to update flag register too
                        updateALUFlagReg(1, ALUInputA, ALUInputB);
                    }
                    else if (mOpcode == 2)
                    {
                        // subtract
                        Console.WriteLine("sub");
                        // several cases for subtracting:
                        //      - RAM A into cache A, RAM B into cache B
                        //      - immediate into cache A, RAM B into cache B (w/ dualread toggle)
                        //      - immediate into cache A, RAM B into cache B (w/ RAM read to cache B)
                        // compiler always toggled dual-read mode, but user can manually program in using RAM read to cache B and it should work
                        // immediates are pushed to input A of ALU in RC3.0 hardware
                        short ALUInputA = 0;
                        short ALUInputB = 0;
                        if (mImmediate != 0) ALUInputA = mImmediate;
                        if (mDualReadToggle == 2 && ALUInputA == 0) { ALUInputA = randomAccessMemory[mRamR - 1]; ALUInputB = randomAccessMemory[mRamW - 1]; }
                        if (mDualReadToggle == 1) { ALUInputB = randomAccessMemory[mRamR - 1]; }
                        Console.WriteLine("add [" + ALUInputA + "] [" + ALUInputB + "]");
                        // just to follow hardware path of RC3.0
                        // subtract CACHEB - CACHEA
                        // make sure to update flag register too at some point
                        _core1ALUDataOutput = (short)(ALUInputB - ALUInputA); _masterOutputBus = _core1ALUDataOutput;
                        updateALUFlagReg(1, ALUInputA, ALUInputB);
                    }
                    else if (mOpcode == 4)
                    {
                        // not
                        Console.WriteLine("not");
                    }
                    else if (mCond != 0)
                    {
                        // test
                        Console.WriteLine("test");
                        // check condition 
                        // evaluate if condition is true or not (mostly checking flag reg)
                        switch (mCond)
                        {
                            case 1:
                                Console.WriteLine("check shift overflow");
                                if ((8 & _core1ALUFlagOutput) == 8)
                                { Console.WriteLine("shift overflow"); mCondSet = true; } 
                                break;
                            case 2:
                                Console.WriteLine("check shift underflow");
                                if ((16 & _core1ALUFlagOutput) == 16)
                                { Console.WriteLine("shift underflow"); mCondSet = true; }
                                break;
                            case 3:
                                Console.WriteLine("check A=B");
                                if ((4 & _core1ALUFlagOutput) == 4)
                                { Console.WriteLine("A=B"); mCondSet = true; }
                                break;
                            case 4:
                                Console.WriteLine("check A>B");
                                if ((2 & _core1ALUFlagOutput) == 2)
                                { Console.WriteLine("A>B"); mCondSet = true; }
                                break;
                            case 5:
                                Console.WriteLine("check A<B");
                                if ((1 & _core1ALUFlagOutput) == 1)
                                { Console.WriteLine("A<B"); mCondSet = true; }
                                break;
                            case 6:
                                Console.WriteLine("check computer input flag 1");
                                if (compIndicator1)
                                { Console.WriteLine("computer input flag 1"); mCondSet = true; }
                                break;
                            case 7:
                                Console.WriteLine("check computer input flag 2");
                                if (compIndicator2)
                                { Console.WriteLine("computer input flag 2"); mCondSet = true; }
                                break;
                            case 8:
                                Console.WriteLine("check computer input flag 3");
                                if (compIndicator3)
                                { Console.WriteLine("computer input flag 3"); mCondSet = true; }
                                break;
                            case 9:
                                Console.WriteLine("check computer input flag 4");
                                if (compIndicator4)
                                { Console.WriteLine("computer input flag 4"); mCondSet = true; }
                                break;
                            default:
                                throw new EmulatorExecutionException("Conditional flag provided in test instruction has is invalid");
                        }
                    }

                    if (mCondSet) core1LOC = (short)(mGTrue - 1);
                    else core1LOC = (short)(mGFalse - 1);
                }
                else if (core == 2)
                {
                }
                else if (core == 3)
                {
                }
                else if (core == 4)
                {
                }
            }
        }

        static void updateALUFlagReg(short core, short inputA, short inputB)
        {
            // emulator ALU flag register data structure
            // [16 8 4 2 1]
            // 16 - shift underflow
            // 8 - shift overflow
            // 4 - A=B
            // 2 - A>B
            // 1 - A<B

            switch (core)
            {
                case 1:
                    _core1ALUFlagOutput = 0; /* reset */ 
                    if (_core1ALUDataOutput < 0) _core1ALUFlagOutput += 16;
                    if (_core1ALUDataOutput > 255) _core1ALUFlagOutput += 8;
                    if (inputA == inputB) _core1ALUFlagOutput += 4;
                    if (inputA > inputB) _core1ALUFlagOutput += 2;
                    if (inputA < inputB) _core1ALUFlagOutput += 1;
                    Console.WriteLine("ALU flag register for core 1 updated: [" + _core1ALUFlagOutput + "] [" + functionLibrary.convertNumberToBinary(_core1ALUFlagOutput, false, 5) + "]");
                    break;
                case 2:
                    _core2ALUFlagOutput = 0; /* reset */
                    if (_core2ALUDataOutput < 0) _core2ALUFlagOutput += 16;
                    if (_core2ALUDataOutput > 255) _core2ALUFlagOutput += 8;
                    if (inputA == inputB) _core2ALUFlagOutput += 4;
                    if (inputA > inputB) _core2ALUFlagOutput += 2;
                    if (inputA < inputB) _core2ALUFlagOutput += 1;
                    Console.WriteLine("ALU flag register for core 2 updated: [" + _core2ALUFlagOutput + "] [" + functionLibrary.convertNumberToBinary(_core2ALUFlagOutput, false, 5) + "]");
                    break;
                case 3:
                    _core3ALUFlagOutput = 0; /* reset */
                    if (_core3ALUDataOutput < 0) _core3ALUFlagOutput += 16;
                    if (_core3ALUDataOutput > 255) _core3ALUFlagOutput += 8;
                    if (inputA == inputB) _core3ALUFlagOutput += 4;
                    if (inputA > inputB) _core3ALUFlagOutput += 2;
                    if (inputA < inputB) _core3ALUFlagOutput += 1;
                    Console.WriteLine("ALU flag register for core 3 updated: [" + _core3ALUFlagOutput + "] [" + functionLibrary.convertNumberToBinary(_core3ALUFlagOutput, false, 5) + "]");
                    break;
                case 4:
                    _core4ALUFlagOutput = 0; /* reset */
                    if (_core4ALUDataOutput < 0) _core4ALUFlagOutput += 16;
                    if (_core4ALUDataOutput > 255) _core4ALUFlagOutput += 8;
                    if (inputA == inputB) _core4ALUFlagOutput += 4;
                    if (inputA > inputB) _core4ALUFlagOutput += 2;
                    if (inputA < inputB) _core4ALUFlagOutput += 1;
                    Console.WriteLine("ALU flag register for core 4 updated: [" + _core4ALUFlagOutput + "] [" + functionLibrary.convertNumberToBinary(_core4ALUFlagOutput, false, 5) + "]");
                    break;
                default:
                    throw new EmulatorExecutionException("Core provided in method 'updateALUFlagReg' is invalid");
            }
        }

        public static void resetEmulator()
        {

        }


        #endregion
    }
}
