﻿using System;
using System.IO;

namespace drchllc_rc30emulator_universal
{
    class Program
    {
        /* Duncan's Redstone Computer HLL Compiler [DRCHLL-C] Redstone Computer v3.0 Emulator | drchllc-rc30emulator-universal
         * -------------------------

          Created 8/19/2019.
          Copyright © AGuyWhoIsBored. All rights reserved.

          DRCHLLC Uploader
          Project started on 8/19/2019

        * --------------------------

        this program will act as a software emulator for the hardware-based Redstone Computer v3.0.

        TO-DO LIST:
            -fully emulate Redstone Computer v3.0 (except for MC and hardware-only quirks)
            -ability to load in configuration file to easily tweak emulator variables
            -implement possible scripting engine to execute various emulator commands one after another?
            -implement tab-completion for commands?


        */

        // versioning info
        public static string emulatorProgramVersion = "1.0a";
        public static string emulatorLanguageVersion = "1.1";
        public static string emulatorHardwareVersion = "1.3.1";

        static string userCommand = null;


        static void Main(string[] args)
        {
            // introduction and versioning information
            Console.Title = "DRCHLLC Redstone Computer v3.0 Emulator";
            Console.WriteLine("--------");
            Console.WriteLine("DRCHLL-C Redstone Computer v3.0 Emulator v" + emulatorProgramVersion);
            Console.WriteLine("Supports ARCISS spec v" + emulatorLanguageVersion);
            Console.WriteLine("Supports Redstone Computer v3.0 Hardware v" + emulatorHardwareVersion);
            Console.WriteLine("Written and developed by AGuyWhoIsBored");
            Console.WriteLine("--------");
            Console.WriteLine("");
            
            // enter main console command loop
            TopOfEmulatorCLI:
            Console.WriteLine("Waiting for command ...");
            Console.WriteLine("Use 'help' for help");
            userCommand = Console.ReadLine().Trim();
            string commandRoot = null;

            // command parsing
            try { commandRoot = userCommand.Substring(0, userCommand.IndexOf(" ")); } // if this succeeds, the command is a multi-word command
            catch (Exception) { /*do nothing*/ } // if it doesn't, leave commandRoot null

            #region help command
            if (userCommand == "help")
            {
                Console.WriteLine("DRCHLL-C Redstone Computer v3.0 Emulator Commands: (* indicate required flags)");
                Console.WriteLine("[config]: root command for configuring the emulator");
                Console.WriteLine("   [show]: will print all of the current configuration data that is loaded and set");
                Console.WriteLine("   [modify] [node] [value]: modify the specified configuration node to the given value");
                Console.WriteLine("   [load] [-f]: load configuration data from an emulator configuration (.econfig) file");
                Console.WriteLine("     * -f: the path of the emulator configuration file to load");
                Console.WriteLine("[program]: root command for loading and processing external program data");
                Console.WriteLine("   [load] [-fce]: load an ARCISS v1.1 program binary file (.pb) into the emulator");
                Console.WriteLine("     * -f: path of the program binary file to load");
                Console.WriteLine("     * -c: the core to load the compiled program into");
                Console.WriteLine("       -e: enable the execution of programs between 21 and 31 lines of code");
                Console.WriteLine("           (v1.3.1 hardware has support for 20 lines of code installed by default)");
                Console.WriteLine("   [info] [-ca]: display information about the currently loaded program(s)");
                Console.WriteLine("     * -c: the core to read program data from");
                Console.WriteLine("       -a: display program data from all four cores");
                Console.WriteLine("[emulator]: root command for emulator execution status");
                Console.WriteLine("   [status]: display all execution status information");
                Console.WriteLine("   [start]: begins emulator execution with the loaded program(s) and settings");
                Console.WriteLine("   [increment] [x]: manually increments the emulator state by [x] amount of clock cycle(s)");
                Console.WriteLine("[exit]: exits the emulator");
            }
            #endregion
            #region config command
            else if (commandRoot == "config")
            {
                // grab next word in command
                userCommand = userCommand.Replace(commandRoot + " ", ""); 
                try { commandRoot = userCommand.Substring(0, userCommand.IndexOf(" ")); }
                catch (Exception) { commandRoot = userCommand; }

                if (commandRoot == "show") rc30emulator.printConfigurationData();
                else if (commandRoot == "modify")
                {
                    // grab node and value - commandroot node, userCommand value
                    userCommand = userCommand.Replace(commandRoot + " ", "");
                    try { commandRoot = userCommand.Substring(0, userCommand.IndexOf(" ")); userCommand = userCommand.Replace(commandRoot + " ", ""); }
                    catch (Exception) { Console.WriteLine("Configuration modify command incomplete - please specify a node and value"); }

                    try { rc30emulator.editConfigurationData(commandRoot, userCommand); }
                    catch (ConfigurationEditException e) { Console.WriteLine("Configuration edit failed: " + e.Message); }

                }
                else if (commandRoot == "load") { /*implement later*/ }
                else Console.WriteLine("Configuration command incomplete - please specify a suboperation");
            }
            else if (userCommand == "config") { Console.WriteLine("Configuration command incomplete - please specify a suboperation"); }
            #endregion
            #region program command
            else if (commandRoot == "program")
            {
                // grab next word in command
                userCommand = userCommand.Replace(commandRoot + " ", "");
                try { commandRoot = userCommand.Substring(0, userCommand.IndexOf(" ")); }
                catch (Exception) { commandRoot = userCommand; }

                if (commandRoot == "load")
                {
                    // grab flags
                    int index = 0;
                    int flagCollectID = 0; // 1 - .pa location | 2 - core to load| 3 - enable extended execution
                    int quoteCount = 0;
                    char currentChar;
                    string bufferString = null;

                    string programFileLocation = null;
                    string coreLoadExecution = null;
                    bool extendedExecution = false;

                    // check for flags and acquire data
                    userCommand = userCommand.Replace("load", "");
                    userCommand += " ";
                    StringReader sr = new StringReader(userCommand);
                    while (index != userCommand.Length)
                    {
                        currentChar = (char)sr.Read();
                        index++;
                        bufferString += currentChar;
                        switch (currentChar)
                        {
                            case ' ':
                                if (bufferString.Contains("-f")) { flagCollectID = 1; }
                                else if (bufferString.Contains("-c")) { flagCollectID = 2; }
                                else if (bufferString.Contains("-e")) { extendedExecution = true; }
                                else if (flagCollectID == 2) { coreLoadExecution = bufferString; coreLoadExecution = functionLibrary.RemoveWhitespace(coreLoadExecution); flagCollectID = 0; }
                                if (quoteCount == 0) { bufferString = null; }
                                break;
                            case '"':
                                quoteCount++;
                                if (flagCollectID == 1 && quoteCount == 2) { programFileLocation = bufferString; programFileLocation = programFileLocation.Remove(programFileLocation.Length - 1); quoteCount = 0; flagCollectID = 0; }
                                bufferString = null;
                                break;
                        }
                    }
                    sr.Dispose();
                    try
                    {
                        loader.loadProgramFromFile(programFileLocation, coreLoadExecution, extendedExecution); Console.WriteLine("Program [" + programFileLocation + "] loaded successfully into core [" + coreLoadExecution + "]");
                    }
                    catch (ProgramLoadException e) { Console.WriteLine("Program load failed: " + e.Message); }
                }
                else if (commandRoot == "info")
                {
                    // grab flags

                }
                else Console.WriteLine("Program command incomplete - please specify a suboperation");
            }
            #endregion
            #region emulator command
            else if (commandRoot == "emulator")
            {
                // grab next word in command
                userCommand = userCommand.Replace(commandRoot + " ", "");
                try { commandRoot = userCommand.Substring(0, userCommand.IndexOf(" ")); }
                catch (Exception) { commandRoot = userCommand; }

                if (commandRoot == "status") rc30emulator.displayEmulatorStatus(false);
                else if (commandRoot == "start") { /**/ }
                else if (commandRoot == "increment") { rc30emulator.emulatorExcecute(1, 1); }
                else Console.WriteLine("Emulator command incomplete - please specify a suboperation");
            }
            else if (userCommand == "emulator") { Console.WriteLine("Emulator command incomplete - please specify a suboperation"); }
            #endregion
            #region exit command
            else if (userCommand == "exit") Environment.Exit(0);            
            #endregion

            else { Console.WriteLine("Command not recognized"); }

            goto TopOfEmulatorCLI;

        }
    }
}
