﻿using System;
using System.Windows.Forms;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using System.IO.Compression;

namespace DRCHLLC_Windows_GUI
{
    public partial class MainGUI : Form
    {
         /*Duncan's Redstone Computer HLL Compiler GUI Wrapper
         * -------------------------

          Created 1/1/2017. 
          Copyright © AGuyWhoIsBored. All rights reserved.

          This program is a simple GUI wrapper for the actual compiler and uploader command line binaries

          Project started on 6/21/15
          GUI Wrapper project started on 1/1/2017
          Build v1.0 pushed 12/17/2018

        * --------------------------*/

        public string version = "1.0 (Windows edition)";

        // binary location variables
        string compilerBinaryLocation = "";
        string uploaderBinaryLocation = "";

        // compile variables - will be passed to compiler console app
        string resourceFolderLocation = "";

        // upload variables - will be passed to uploader console app
        string overrideStartCoordinates = "";
        string countdownInt = "-1";

        // collective variables - needed for both binaries
        string compileTarget = "-1";
        string compileCoreTarget = "-1";
        string sourceFileLocation = "";
        string userCommand = null;

        // for downloading
        static WebClient client = new WebClient();

        public MainGUI() { InitializeComponent(); }

        private void MainGUI_Load(object sender, EventArgs e)
        {
            Text = "DRCHLLC GUI v" + version;

            // load all of the tooltip info
            ToolTip tt1 = new ToolTip(); ToolTip tt2 = new ToolTip(); ToolTip tt3 = new ToolTip(); ToolTip tt4 = new ToolTip(); ToolTip tt5 = new ToolTip(); ToolTip tt6 = new ToolTip(); ToolTip tt7 = new ToolTip();
            tt1.SetToolTip(loadSourceFileButton, "Select the source file you want to compile / upload.");
            tt2.SetToolTip(uploadButton, "Launch the uploader with the selected settings.");
            tt3.SetToolTip(compileButton, "Launch the compiler with the selected settings.");
            tt4.SetToolTip(label1, "Enter specific arguments here to pass to either the compiler or uploader.");
            tt5.SetToolTip(label3, "Select the location of the compiler resource folder if necessary here.");
            tt6.SetToolTip(compilerTargetListBox, "Select the computer target to which you want to upload or compile to.");
            tt6.SetToolTip(groupBox1, "Select the computer target to which you want to upload or compile to.");
            tt7.SetToolTip(autoMakeButton, "Compile and upload the source program into a datapack easily.");

            // set overwrite before upload and upload via datapack for default options with uploader
            uploadOptionsListBox.SetItemChecked(0, true); uploadOptionsListBox.SetItemChecked(3, true);
        }

        private void compilerTargetListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            // make sure to not change the index
            switch(compilerTargetListBox.SelectedIndex)
            {
                case 00: compileTarget = "1"; compileCoreTarget = "1"; break;
                case 01: compileTarget = "1"; compileCoreTarget = "2"; break;
                case 02: compileTarget = "1"; compileCoreTarget = "3"; break;
                case 03: compileTarget = "1"; compileCoreTarget = "4"; break;
                case 04: compileTarget = "2"; compileCoreTarget = "1"; break;
                case 05: compileTarget = "2"; compileCoreTarget = "2"; break;
                case 06: compileTarget = "2"; compileCoreTarget = "3"; break;
                case 07: compileTarget = "2"; compileCoreTarget = "4"; break;
                case 08: compileTarget = "3"; compileCoreTarget = "1"; break;
                case 09: compileTarget = "3"; compileCoreTarget = "2"; break;
                case 10: compileTarget = "3"; compileCoreTarget = "3"; break;
                case 11: compileTarget = "3"; compileCoreTarget = "4"; break;
            }

            compilerOutputTextBox.Text += "[" + DateTime.Now + "]: Compile target changed to [" + compileTarget + "," + compileCoreTarget + "]" + Environment.NewLine;
        }

        private void selectResourceFolderButton_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fd = new FolderBrowserDialog();
            fd.ShowDialog();
            if(fd.SelectedPath != "")
            {
                resourceFolderLocation = fd.SelectedPath;
                compilerOutputTextBox.Text += "[" + DateTime.Now + "]: Resource folder changed to [" + resourceFolderLocation + "]" + Environment.NewLine;
                resourceFolderTextBox.Text = resourceFolderLocation;
            }
            fd.Dispose();
        }

        private void loadSourceFileButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog fd = new OpenFileDialog
            { Filter = "ARCISS Source Files (*.txt)|*.txt|ARCISS Binary Files (*.pb)|*.pb", Title = "Select file for Compiler/Uploader" };
            fd.ShowDialog();
            if (fd.FileName != "")
            {
                sourceFileLocation = fd.FileName;
                compilerOutputTextBox.Text += "[" + DateTime.Now + "]: Source file changed to [" + sourceFileLocation + "]" + Environment.NewLine;
            }
            fd.Dispose();
        }

        private void uploadButton_Click(object sender, EventArgs e)
        {
            if (sourceFileLocation != "")
            {
                if (compileTarget != "-1")
                { 
                    compilerOutputTextBox.Text += "[" + DateTime.Now + "]: Job UPLOAD started" + Environment.NewLine;
                    if (uploaderBinaryLocation == "") MessageBox.Show("Uploader binary location required for upload!");
                    else
                    {
                        Process upro = new Process();
                        upro.StartInfo.FileName = uploaderBinaryLocation; upro.StartInfo.Arguments = buildUploadCommand();
                        upro.Start(); upro.WaitForExit();
                        compilerOutputTextBox.Text += "[" + DateTime.Now + "]: Job UPLOAD finished!" + Environment.NewLine;
                    }
                }
                else MessageBox.Show("Compile target is required for upload!");
            }
            else MessageBox.Show("Source file is required for upload!");
        }

        private void compileButton_Click(object sender, EventArgs e)
        {
            if (sourceFileLocation != "")
            {
                if (compileTarget != "-1")
                {
                    compilerOutputTextBox.Text += "[" + DateTime.Now + "]: Job COMPILE started" + Environment.NewLine;
                    if (compilerBinaryLocation == "") MessageBox.Show("Compiler binary location required for compilation!");
                    else
                    {
                        Process cpro = new Process();
                        cpro.StartInfo.FileName = compilerBinaryLocation; cpro.StartInfo.Arguments = buildCompileCommand();
                        cpro.Start(); cpro.WaitForExit();
                        compilerOutputTextBox.Text += "[" + DateTime.Now + "]: Job COMPILE finished!" + Environment.NewLine;
                    }
                }
                else { MessageBox.Show("Compile target is required for compilation!"); }
            }
            else { MessageBox.Show("Source file is required for compilation!"); }
        }

        private void CLArgumentsTextBox_TextChanged(object sender, EventArgs e) { userCommand = CLArgumentsTextBox.Text; }

        private void MainGUI_FormClosing(object sender, FormClosingEventArgs e)
        {
            // close all running console apps
            foreach (Process p in Process.GetProcesses())
            { if (p.MainWindowTitle == "DRCHLLC Uploader" || p.MainWindowTitle == "DRCHLLC Core Compiler") p.Kill(); }
        }

        private void loadCompilerBinaryLocationButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog fd = new OpenFileDialog
            { Filter = "DRCHLL-C Compiler Binary (*.exe)|*.exe", Title = "Select Compiler Binary" };
            fd.ShowDialog();
            if (fd.FileName != "")
            {
                compilerBinaryLocation = fd.FileName;
                compilerOutputTextBox.Text += "[" + DateTime.Now + "]: Compiler binary location changed to [" + compilerBinaryLocation + "]" + Environment.NewLine;
            }
            fd.Dispose();
            compilerBinaryLocationTextBox.Text = compilerBinaryLocation;
        }

        private void uploaderBinaryLocationButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog fd = new OpenFileDialog
            { Filter = "DRCHLL-C Uploader Binary (*.exe)|*.exe", Title = "Select Uploader Binary" };
            fd.ShowDialog();
            if (fd.FileName != "")
            {
                uploaderBinaryLocation = fd.FileName;
                compilerOutputTextBox.Text += "[" + DateTime.Now + "]: Uploader binary location changed to [" + uploaderBinaryLocation + "]" + Environment.NewLine;
            }
            fd.Dispose();
            uploaderBinaryLocationTextbox.Text = uploaderBinaryLocation;
        }

        private void autoMakeButton_Click(object sender, EventArgs e)
        {
            // will compile ARCISS source file and then immediately upload it into a datapack for the user to paste into their MC save
            // compiling
            if (sourceFileLocation != "")
            {
                if (compileTarget != "-1")
                {
                    compilerOutputTextBox.Text += "[" + DateTime.Now + "]: Job COMPILE started" + Environment.NewLine;
                    if (compilerBinaryLocation == "") MessageBox.Show("Compiler binary location required for compilation!");
                    else
                    {
                        Process cpro = new Process();
                        cpro.StartInfo.FileName = compilerBinaryLocation; cpro.StartInfo.Arguments = buildCompileCommand();
                        cpro.Start(); cpro.WaitForExit();
                        compilerOutputTextBox.Text += "[" + DateTime.Now + "]: Job COMPILE finished!" + Environment.NewLine;
                    }
                }
                else { MessageBox.Show("Compile target is required for compilation!"); }
            }
            else { MessageBox.Show("Source file is required for compilation!"); }
            // change source file location to .pb file that was just generated
            string sourceFileLocTemp = sourceFileLocation;
            sourceFileLocation = Path.GetDirectoryName(sourceFileLocation) + "/" + Path.GetFileNameWithoutExtension(sourceFileLocation) + ".pb";
            // uploading
            if (sourceFileLocation != "")
            {
                if (compileTarget != "-1")
                {
                    compilerOutputTextBox.Text += "[" + DateTime.Now + "]: Job UPLOAD started" + Environment.NewLine;
                    if (uploaderBinaryLocation == "") MessageBox.Show("Uploader binary location required for upload!");
                    else
                    {
                        Process upro = new Process();
                        upro.StartInfo.FileName = uploaderBinaryLocation; upro.StartInfo.Arguments = buildUploadCommand();
                        upro.Start(); upro.WaitForExit();
                        compilerOutputTextBox.Text += "[" + DateTime.Now + "]: Job UPLOAD finished!" + Environment.NewLine;
                    }
                }
                else MessageBox.Show("Compile target is required for upload!");
            }
            else MessageBox.Show("Source file is required for upload!");

            // when done, delete drchllc-bin folder and reset sourceFileLocation
            sourceFileLocation = sourceFileLocTemp;
        }

        private string buildCompileCommand()
        {
            string autoUserCommand = null;
            string optimizationCollectiveBinaryID = null;
            autoUserCommand += "-f \"\"\"" + sourceFileLocation + "\"\"\"";
            if (resourceFolderLocation != "") { autoUserCommand += " -r \"\"\"" + resourceFolderLocation + "\"\"\""; }
            // serves for both target and core
            autoUserCommand += " -t " + compileTarget + " -c " + compileCoreTarget;
            // build optimizationCollectiveID
            for (int i = 0; i < optimizationConfigCheckedListBox.Items.Count; i++)
            {
                if (optimizationConfigCheckedListBox.GetItemChecked(i)) { optimizationCollectiveBinaryID += "1"; }
                else { optimizationCollectiveBinaryID += "0"; }
            }
            // use the same variable for efficiency
            optimizationCollectiveBinaryID = Convert.ToString(Convert.ToInt32(optimizationCollectiveBinaryID, 2));
            if (optimizationCollectiveBinaryID != "0") { autoUserCommand += " -o " + optimizationCollectiveBinaryID; }

            // add extra CL arguments
            if (userCommand != null) { autoUserCommand += " " + userCommand; }
            if (autoUserCommand.Contains("-d")) { MessageBox.Show("Input Arguments: " + autoUserCommand + "]"); }
            return autoUserCommand;
        }

        private string buildUploadCommand()
        {
            string autoUserCommand = null;
            string uploadOptionCollectiveBinaryID = null;
            autoUserCommand += "-f \"\"\"" + sourceFileLocation + "\"\"\"";
            // serves for both target and core
            autoUserCommand += " -t " + compileTarget + " -c " + compileCoreTarget; 
            // build uploadOptionCollectiveBinaryID
            for (int i = 0; i < uploadOptionsListBox.Items.Count; i++)
            {
                if (uploadOptionsListBox.GetItemChecked(i)) { uploadOptionCollectiveBinaryID += "1"; }
                else { uploadOptionCollectiveBinaryID += "0"; }
            }
            // use the same variable for efficiency
            uploadOptionCollectiveBinaryID = Convert.ToString(Convert.ToInt32(uploadOptionCollectiveBinaryID, 2));
            if (uploadOptionCollectiveBinaryID != "0") { autoUserCommand += " -o " + uploadOptionCollectiveBinaryID; }

            // add extra CL arguments
            if (userCommand != null) { autoUserCommand += " " + userCommand; }
            if (autoUserCommand.Contains("-d")) { MessageBox.Show("Input Arguments: [" + autoUserCommand + "]"); }
            return autoUserCommand;
        }

        private void downloadLatestBinaries()
        {
            // remove drchllc-bin folder if present to overwrite
            if (Directory.Exists(Environment.CurrentDirectory + "/drchllc-bin")) { Directory.Delete(Environment.CurrentDirectory + "/drchllc-bin", true); }

            compilerOutputTextBox.Text += "[" + DateTime.Now + "]: Downloading latest version of required binaries ..." + Environment.NewLine;
            // check for Internet access first
            if (!checkForInternet("https://www.google.com")) { compilerOutputTextBox.Text += "[" + DateTime.Now + "]: Could not establish Internet connection" + Environment.NewLine; }
            else
            {
                string compilerDL = null; string uploaderDL = null;
                Task.Run(async () => await downloadFile("https://www.dropbox.com/s/214qjgdisfi0qma/drchllc-latest-builds.txt?dl=1", Environment.CurrentDirectory + "/drchllc-latest-builds.txt")).Wait();
                string[] latestBuilds = File.ReadAllLines(Environment.CurrentDirectory + "/drchllc-latest-builds.txt"); File.Delete(Environment.CurrentDirectory + "/drchllc-latest-builds.txt");
                compilerDL = latestBuilds[0].Remove(0, 12); uploaderDL = latestBuilds[1].Remove(0, 12);
                // download binaries
                // futureproofing - will eventually remove .zip files and just have the .exes available for download
                Directory.CreateDirectory(Environment.CurrentDirectory + "/drchllc-bin/c");
                Directory.CreateDirectory(Environment.CurrentDirectory + "/drchllc-bin/u");
                if (compilerDL.Contains(".zip"))
                {
                    compilerOutputTextBox.Text += "[" + DateTime.Now + "]: Extracting compiler binary ..." + Environment.NewLine;
                    // download zip and extract relevant executable
                    Task.Run(async () => await downloadFile(compilerDL, Environment.CurrentDirectory + "/drchllc-compiler.zip")).Wait();
                    ZipFile.ExtractToDirectory(Environment.CurrentDirectory + "/drchllc-compiler.zip", Environment.CurrentDirectory + "/drchllc-bin/c");
                    File.Delete(Environment.CurrentDirectory + "/drchllc-compiler.zip");
                    // search for compiler executable in working directory
                    string[] compilerBinary = Directory.GetFiles(Environment.CurrentDirectory + "/drchllc-bin/c", "*.exe", SearchOption.AllDirectories);
                    compilerBinaryLocation = compilerBinary[0]; compilerBinaryLocationTextBox.Text = compilerBinaryLocation;
                }
                else if (compilerDL.Contains(".exe"))
                {
                    Task.Run(async () => await downloadFile(compilerDL, Environment.CurrentDirectory + "/drchllc-bin/c/drchllc-compiler.exe")).Wait();
                    compilerBinaryLocation = Environment.CurrentDirectory + "/drchllc-bin/c/drchllc-compiler.exe"; compilerBinaryLocationTextBox.Text = compilerBinaryLocation;
                }
                if (uploaderDL.Contains(".zip"))
                {
                    compilerOutputTextBox.Text += "[" + DateTime.Now + "]: Extracting uploader binary ..." + Environment.NewLine;
                    Task.Run(async () => await downloadFile(uploaderDL, Environment.CurrentDirectory + "/drchllc-uploader.zip")).Wait();
                    ZipFile.ExtractToDirectory(Environment.CurrentDirectory + "/drchllc-uploader.zip", Environment.CurrentDirectory + "/drchllc-bin/u");
                    File.Delete(Environment.CurrentDirectory + "/drchllc-uploader.zip");
                    // search for compiler executable in working directory
                    string[] uploaderBinary = Directory.GetFiles(Environment.CurrentDirectory + "/drchllc-bin/u", "*.exe", SearchOption.AllDirectories);
                    uploaderBinaryLocation = uploaderBinary[0]; uploaderBinaryLocationTextbox.Text = uploaderBinaryLocation;
                }
                else if (uploaderDL.Contains(".exe"))
                {
                    Task.Run(async () => await downloadFile(uploaderDL, Environment.CurrentDirectory + "/drchllc-uploader.exe")).Wait();
                    uploaderBinaryLocation = Environment.CurrentDirectory + "/drchllc-bin/u/drchllc-uploader.exe"; uploaderBinaryLocationTextbox.Text = uploaderBinaryLocation;
                }
            }

        }

        // Networking related functions
        private async Task downloadFile(string file, string location) { await client.DownloadFileTaskAsync(new Uri(file), location); }
        private bool checkForInternet(string website) { try { using (var stream = client.OpenRead(website)) { return true; } } catch { return false; } }

        private void downloadLatestBinariesButton_Click(object sender, EventArgs e) { downloadLatestBinaries(); }
    }
}
