This is the location of the 'Duncan's Redstone Computer to HLL Compiler' (or DRCHLL-C) Project repository! The source files and accompanying documentation of this project will be located here.

The purpose of this project is to be able to write programs in a proprietary language called 'AGuyWhoIsBored's Redstone Computer Instruction Set Specification', or ARCISS, and then be able to run those programs on the redstone computer(s) that I have built in Minecraft. 
The main two binaries of this project (the DRCHLL Compiler and Uploader) act as a "bridge" between programming in a high level language such as ARCISS, and running these programs on the redstone computers that I have developed, allowing for realistic programming of these computers! 

Here is a quick list of videos showcasing the different components of this massive project!

* [Redstone Computer v3.0 (Ultimate) Showcase](https://youtu.be/V3ycmN0rSU8)
* [Redstone Computer v4.0 Showcase](https://youtu.be/SPaI5BJxs5M)
* [Redstone Computer v5.0 Showcase](https://youtu.be/SbO0tqH8f5I)
* [Duncan's Redstone Computer HLL Compiler (DRCHLL-C) Showcase](https://youtu.be/vKNb2Fblrxw)
* [ARCISS v1.0 Breakdown](https://youtu.be/3WtkvKgn3Ig)

Here is a current list of features of the latest version(s) of the DRCHLL-C project!

* Compiles and supports ARCISS v1.0 and ARCISS v1.1, a custom programming language designed for these redstone computers 
* Compiler can compile ARCISS-HLL (High Level Language) to ARCISS-3L (Low Level Language / Assembly)
* Compiler can compile ARCISS-3L to ARCISS Machine Code
    * Machine code can be run on redstone computer targets
* Compiler contains built-in syntax checker at compilation
* Compiler includes some optimizations to help optimize code that you have written (will improve in the future)
* A separate uploader program exists to be able to upload compiled programs (in ARCISS Machine Code) to redstone computer targets
* The Windows Edition has a GUI that makes it more user-friendly (WIP)

Planned features:
(More features may be coming per user requests!)

* Function support!
* More advanced optimizations
* More advanced syntax checker
* Support for custom language extensions
* Multiplatform support

**NOTE** This is still a package of programs that are very much in development! Which means there will be bugs and such! If you find a bug, please file a bug report in the Issues section on how to reproduce it! Thank you very much!

Downloads for the DRCHLLC Project are provided in the [Downloads](https://bitbucket.org/AGuyWhoIsBored/duncans-redstone-computer-hll-compiler/downloads/) section of the Repository!   
Documentation for the binaries and language are provided in the [Wiki](https://bitbucket.org/AGuyWhoIsBored/duncans-redstone-computer-hll-compiler/wiki/Home) section of the Repository!  
If you find any bugs / issues with any binary or component of the DRCHLLC Project, please be sure to leave a bug report in the [Issues](https://bitbucket.org/AGuyWhoIsBored/duncans-redstone-computer-hll-compiler/issues?status=new&status=open) section of the Repository!  

Thank you all very much for the support you all have given me! It really means a lot and helps me to keep pushing forward! :) 